/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 10 february 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_MainWindow class offering the |
  | main window of Tactile Anywhere                                         |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Valentin Coulon -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_MAINWINDOW_H
#define TA_MAINWINDOW_H

/*****************************************************************************/
/******                             DEFINES                             ******/
/*****************************************************************************/

//Images
#define TA_MAINWINDOW_LOGO ":/images/logo.png"
#define TA_MAINWINDOW_LOGO_ON ":/images/logo-active.png"
#define TA_MAINWINDOW_LOAD ":/images/load.png"
#define TA_MAINWINDOW_SAVE ":/images/save.png"
#define TA_MAINWINDOW_SAVEAS ":/images/saveas.png"
#define TA_MAINWINDOW_HELP ":/images/help.png"
#define TA_MAINWINDOW_HOME ":/images/home.png"
#define TA_MAINWINDOW_SETTINGS ":/images/settings.png"
#define TA_MAINWINDOW_TINY_LOGO ":/images/tiny-logo.png"

//Toolbar
#define TA_MAINWINDOW_TOOLBAR_UP ":/images/up.png"
#define TA_MAINWINDOW_TOOLBAR_DOWN ":/images/down.png"

//Help
#define TA_MAINWINDOW_HELP_HTML "qrc:///html/mainwindow.html"

//CSS
#define TA_MAINWINDOW_CSS "QToolBar {spacing:20%;} QStatusBar::item{border: 0px solid black;}"

#define TA_MAINWINDOW_LICENCE "Touchy Lab - 2014 - V1.0\nLicence LGPL V3.0"

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QMainWindow>
#include <QLabel>
#include <QSystemTrayIcon>


//Fwd
class QMenuBar;
class QMenu;
class QHBoxLayout;
class QStatusBar;
class QAction;
class QSettings;
class TA_ToolBar;
class TA_StatusBar;

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
    This file contains declarations of the TA_MainWindow class offering the
    main window of Tactile Anywhere
*/
class TA_MainWindow : public QMainWindow {
    Q_OBJECT
    public:
        /*
            Initializes an instance of this class as a singleton
        */
        static void initialize();

        /*
            Gets the unique instance of this class. initialize() must be called before
                @return : the unique instance of this class or NULL if this class is not initialized
        */
        static TA_MainWindow* getInstance();

        /*
            Cleans properly this class at the destruction
        */
        ~TA_MainWindow();

        /*
            Build the toolBar's main window
        */
        void initToolBar();

        /*
            Build the first screen to specify the launch mode
        */
        void buildMainWidget();

        /*
            Attach the html help page to this window
                @param htmlPage : the html page to attach
        */
        void setHelp(QString htmlPage);

        /*
            Attach the html help page to this window
                @param limited : disable toolBar's buttons
                @param limitHome : disable the home button
        */
        void setLimitedToolBar(bool limited, bool limitHome);

        /*
            Get the screen resolution
                @return : the screen size in pixel
        */
        QRect getScreenSize();

        /*
            Get the TA_StatusBar object
                @return : the statusBar
        */
        TA_StatusBar* getStatusBar();

        /*
            Get the system tray icon
                @return : the system tray
        */
        //QSystemTrayIcon* getTrayIcon();

    private slots:

        /*
            Open the server part from this window
        */
        void openServer();

        /*
            Open the client part from this window
        */
        void openClient();

        /*
            Save the current configuration handling an empty filename
        */
        void save();

        /*
            Save the current configuration with a specific filename
        */
        void saveAs();

        /*
            Load the current configuration
        */
        void load();

        /*
            Display alternatively the help section
        */
        void help();

        /*
            Display parameters screen
        */
        void param();

        /*
            Display home screen
        */
        void home();

        /*
            Handle interactions with icon system tray
                @param reason : the type of action
        */
        //void trayIconClick(QSystemTrayIcon::ActivationReason reason);

        /*
            Hide the window on minimized window
                @param event: the event ask
        */
        //void changeEvent(QEvent *event);

    private:
        /*
            This constructor creates and initializes this class
                @param parent : widget parent
        */
        TA_MainWindow(QWidget *parent=0);

        /*
            Initialize the main window (icon, size, title)
        */
        void initWindow();

        /*
            Build the tool bar for this window
        */
        void buildToolBar();

        /*
            Build the status bar for this window
        */
        void buildStatusBar();

        /*
            Build the system tray
        */
        //void buildSystemTray();

        /*
            Create all actions for buttons in the tool bar
        */
        void createActions();

        /*
            Save the current configuration
        */
        void _save();

        /*
            Handle closing application.
                @param event: the close event
        */
        void closeEvent(QCloseEvent *event);

        /*
            Update the current widget and compute drawing zone
            for button thant hide and display the tool bar
        */
        void update();

        /*
            Draw the button for hide and display the tool bar
            @param : the draw event
        */
        void paintEvent(QPaintEvent *);

        /*
            Handle the click on the button for hide and display the tool bar
            @param : the mouse event
        */
        void mousePressEvent(QMouseEvent *event);


        static TA_MainWindow* singleton;
        QHBoxLayout* mainLayout;
        QWidget *centerWidget = NULL;
        QWidget *rightWidget = NULL;
        QRect screenSize;

        //MenuBar
        QMenuBar *menuBar;

        //StatusBar
        TA_StatusBar *statusBar;

        //ToolBar
        TA_ToolBar *toolBar;
        QRect rectArrow;
        QPixmap arrowPixmap;

        //Actions
        QAction *loadAct;
        QAction *saveAct;
        QAction *saveAsAct;
        QAction *paramAct;
        QAction *helpAct;
        QAction *homeAct;

        //System tray
        /*QSystemTrayIcon *trayIcon;
        QMenu *trayIconMenu;
        QAction *minimizeAction;
        QAction *maximizeAction;
        QAction *restoreAction;
        QAction *quitAction;
        QSettings *settings;*/


        //Configuration filename
        QString fileName;

        // Help options
        bool showHelp;

};

#endif // TA_MAINWINDOW_H
