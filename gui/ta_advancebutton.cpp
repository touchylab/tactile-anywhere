/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 27 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_AdvanceButton to allow user to     |
  | display advances features                                               |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Valentin COULON -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_advancebutton.h"

/*
    Instanciate a new instance of TA_AdvanceButton
*/
TA_AdvanceButton::TA_AdvanceButton() : QPushButton() {
    setText(TA_ADVANCEBUTTON_INACTIVE_TEXT);
    setIcon(QIcon(TA_ADVANCEBUTTON_INACTIVE_IMAGE));
    setToolTip(TA_ADVANCEBUTTON_TOOLTIP);
    setCheckable(true);
    connect(this, SIGNAL(toggled(bool)), this, SLOT(setActiveStyle(bool)));
}

/*
    Activate or not the TA_AdvanceButton
        @param isActive : set as true to activate the button, false else
*/
void TA_AdvanceButton::setActiveStyle(bool isActive) {
    if (isActive) {
        setText(TA_ADVANCEBUTTON_ACTIVE_TEXT);
        setIcon(QIcon(TA_ADVANCEBUTTON_ACTIVE_IMAGE));
    } else {
        setText(TA_ADVANCEBUTTON_INACTIVE_TEXT);
        setIcon(QIcon(TA_ADVANCEBUTTON_INACTIVE_IMAGE));
    }
}
