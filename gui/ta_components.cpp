/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 14 January 2013                                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_Components class containing        |
  | utility for popups and others regularly used gui components             |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Sébastien DESTABEAU -- Initial version                        |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Local includes
#include "gui/ta_components.h"

/*
    Display a popup message in GUI.
		@param title : the title of the popup.
		@param message : the message to display.
*/
void TA_Components::displayPopup(QString& title, QString& message){
    // Create a popup with a message and a title
    QMessageBox* msgBox = new QMessageBox();
    msgBox->setWindowTitle(title);
    msgBox->setText(message);
    // Set it always on top
    msgBox->setWindowFlags(Qt::WindowStaysOnTopHint);
    // Set an icon in the message
    msgBox->setIconPixmap(QPixmap("cupButton.gif"));
    // Set the background picture
    QPalette palette;
    palette.setBrush(QPalette::Background,QBrush(QImage("sunflower.jpg")));
    msgBox->setPalette(palette);        
    // Display the popup
    msgBox->show();
}

/*
    Display a popup message in gui.
		@param message : the message to display.
*/
void TA_Components::displayPopupMessage(QString& message){
    QMessageBox::information(0, "Information", message);
}

/*
    Display a warning message in gui.
		@param message : the message to display.
*/
void TA_Components::displayWarningMessage(QString& message){
    QMessageBox::warning(0, "Warning", message);
}

/*
    Display a popup message in gui.
		@param message : the message to display.
*/
void TA_Components::displayFatalMessage(QString& message){
    QMessageBox::critical(0, "Fatal", message);
}

/*
    Display a popup message in gui with the purpose of a choice between Ok and Cancel.
		@param title : the title of the popup.
		@param message : the message to display.
		@return : an integer to identify the QMessageBox enum choice.
*/
int TA_Components::displayOkCancelMessage(QString& title, QString& message){
    QMessageBox msgBox;
    msgBox.setWindowTitle(title);
    msgBox.setText(message);
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Ok);
    return msgBox.exec();
}
