/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 20 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_FlowLayout class containing   |
  | specific layout adapted for tactile anywhere.                           |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu Loslier -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_FLOWLAYOUT_H
#define TA_FLOWLAYOUT_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QtWidgets>
#include <QLayout>
#include <QRect>

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
	This class offer a specific layout adapted to Tactile Anywhere.
	The code is coming from the website http://qt-project.org/doc/qt-5.0/qtwidgets/layouts-flowlayout.html
*/
class TA_FlowLayout : public QLayout{
    public:
		/*
			Instanciates this class
				@param margin : the initial margin
				@param hSpacing : the initial horizontal spacing
				@param vSpacing : the inital vertical spacing
		*/
        TA_FlowLayout(int margin = -1, int hSpacing = -1, int vSpacing = -1);
		
		/*
			The class destructor
		*/
        ~TA_FlowLayout();

		/*
			Add an item to le layout
				@param item : the item to add
		*/
        void addItem(QLayoutItem *item);
		
		/*
			Return the horizontal spacing
				@return : the horizontal spacing
		*/
        int horizontalSpacing() const;
		
		/*
			Return the vertical spacing
				@return : the vertical spacing
		*/
        int verticalSpacing() const;
		
		/*
			Check if the Height for Width is activate
				@return : true if activated, false else
		*/
        bool hasHeightForWidth() const;
		
		/*
			Get the height for the given width
				@param width : the width to ask
				@return : the height for the given width
		*/
        int heightForWidth(int width) const;
		
		/*
			Count the number of items
				@return the number of items
		*/
        int count() const;
		
		/*
			Get the item at the given position
				@param index : the position of the item
				@return: the asked item
		*/
        QLayoutItem *itemAt(int index) const;
		
		/*
			Take the item at the given position
				@param index : the position of the item
				@return: the asked item
		*/
		QLayoutItem *takeAt(int index);
		
		/*
			Return the minimum size of the layout
				@return: the minimum size of the layout
		*/
        QSize minimumSize() const;
		
		/*
			Return the size hint of the layout
				@return: the size hint of the layout
		*/
        QSize sizeHint() const;
    private:
		/*
			Apply the given geometry on the layout
				@param rect: the geometry to apply
				@return: the height of the widget
		*/
        int doLayout(const QRect &rect) const;
		
		/*
			Get the best spacing to apply on the layout
				@param pm : the metric of the layout
				@return: the best spacing
		*/
        int smartSpacing(QStyle::PixelMetric pm) const;

        QList<QLayoutItem *> itemList;
        int m_hSpace;
        int m_vSpace;
};

#endif // TA_FLOWLAYOUT_H
