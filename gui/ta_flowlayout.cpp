/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 20 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_FlowLayout class containing        |
  | specific layout adapted for tactile anywhere.                           |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu Loslier -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_flowlayout.h"

// The code is coming from the website http://qt-project.org/doc/qt-5.0/qtwidgets/layouts-flowlayout.html

/*****************************************************************************/
/******                     CONSTRUCTORS/DESTRUCTOR                     ******/
/*****************************************************************************/

/*
	Instanciates this class
		@param margin : the initial margin
		@param hSpacing : the initial horizontal spacing
		@param vSpacing : the inital vertical spacing
*/
TA_FlowLayout::TA_FlowLayout(int margin, int hSpacing, int vSpacing): QLayout(), m_hSpace(hSpacing), m_vSpace(vSpacing){
    setContentsMargins(margin, margin, margin, margin);
}

/*
	The class destructor
*/
TA_FlowLayout::~TA_FlowLayout(){
    QLayoutItem *item;
    while ((item = takeAt(0)))
        delete item;
}

/*****************************************************************************/
/******                         GETTERS/SETTERS                         ******/
/*****************************************************************************/

/*
	Add an item to le layout
		@param item : the item to add
*/
void TA_FlowLayout::addItem(QLayoutItem *item){
    itemList.append(item);
}

/*
	Return the horizontal spacing
		@return : the horizontal spacing
*/
int TA_FlowLayout::horizontalSpacing() const{
    if (m_hSpace >= 0)
        return m_hSpace;
    else
        return smartSpacing(QStyle::PM_LayoutHorizontalSpacing);
}

/*
	Return the vertical spacing
		@return : the vertical spacing
*/
int TA_FlowLayout::verticalSpacing() const{
    if (m_vSpace >= 0)
        return m_vSpace;
    else
        return smartSpacing(QStyle::PM_LayoutVerticalSpacing);
}

/*
	Check if the Height for Width is activate
		@return : true if activated, false else
*/
bool TA_FlowLayout::hasHeightForWidth() const{
    return true;
}

/*
	Get the height for the given width
		@param width : the width to ask
		@return : the height for the given width
*/
int TA_FlowLayout::heightForWidth(int width) const{
    int height = doLayout(QRect(0, 0, width, 0));
    return height;
}

/*
	Count the number of items
		@return the number of items
*/
int TA_FlowLayout::count() const{
    return itemList.size();
}

/*
	Get the item at the given position
		@param index : the position of the item
		@return: the asked item
*/
QLayoutItem *TA_FlowLayout::itemAt(int index) const{
    return itemList.value(index);
}

/*
	Take the item at the given position
		@param index : the position of the item
		@return: the asked item
*/
QLayoutItem *TA_FlowLayout::takeAt(int index){
    if (index >= 0 && index < itemList.size())
        return itemList.takeAt(index);
    else
        return 0;
}

/*
	Return the size hint of the layout
		@return: the size hint of the layout
*/
QSize TA_FlowLayout::sizeHint() const{
    return minimumSize();
}

/*
	Return the minimum size of the layout
		@return: the minimum size of the layout
*/
QSize TA_FlowLayout::minimumSize() const{
    QSize size;
    QLayoutItem *item;
    foreach (item, itemList)
        size = size.expandedTo(item->minimumSize());

    size += QSize(2*margin(), 2*margin());
    return size;
}

/*****************************************************************************/
/******                           OPERATIONS                            ******/
/*****************************************************************************/

/*
	Apply the given geometry on the layout
		@param rect: the geometry to apply
		@return: the height of the widget
*/
int TA_FlowLayout::doLayout(const QRect &rect) const{
    int left, top, right, bottom;
    getContentsMargins(&left, &top, &right, &bottom);
    QRect effectiveRect = rect.adjusted(+left, +top, -right, -bottom);
    int x = effectiveRect.x();
    int y = effectiveRect.y();
    int lineHeight = 0;

    QLayoutItem *item;
    foreach (item, itemList) {
        QWidget *wid = item->widget();
        int spaceX = horizontalSpacing();
        if (spaceX == -1)
            spaceX = wid->style()->layoutSpacing(QSizePolicy::Label, QSizePolicy::Label, Qt::Horizontal);
        int spaceY = verticalSpacing();
        if (spaceY == -1)
            spaceY = wid->style()->layoutSpacing(QSizePolicy::Label, QSizePolicy::Label, Qt::Vertical);

        int nextX = x + item->sizeHint().width() + spaceX;
        if (nextX - spaceX > effectiveRect.right() && lineHeight > 0) {
            x = effectiveRect.x();
            y = y + lineHeight + spaceY;
            nextX = x + item->sizeHint().width() + spaceX;
            lineHeight = 0;
        }

        item->setGeometry(QRect(QPoint(x, y), item->sizeHint()));

        x = nextX;
        lineHeight = qMax(lineHeight, item->sizeHint().height());
    }
    return y + lineHeight - rect.y() + bottom;
}

/*
	Get the best spacing to apply on the layout
		@param pm : the metric of the layout
		@return: the best spacing
*/
int TA_FlowLayout::smartSpacing(QStyle::PixelMetric pm) const{
    QObject *parent = this->parent();
    if (!parent) {
        return -1;
    } else if (parent->isWidgetType()) {
        QWidget *pw = static_cast<QWidget *>(parent);
        return pw->style()->pixelMetric(pm, 0, pw);
    } else {
        return static_cast<QLayout *>(parent)->spacing();
    }
}
