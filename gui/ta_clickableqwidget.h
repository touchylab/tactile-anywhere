/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 27 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_ClickableQWidget to allow user|
  | to manage clicks on QWidget                                             |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Valentin COULON -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/
/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/
#ifndef TA_CLICKABLEQWIDGET_H
#define TA_CLICKABLEQWIDGET_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/
#include <QWidget>

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
	Class to manage clicks on QWidgets
*/
class TA_ClickableQWidget : public QWidget {
    Q_OBJECT
	public:
		/*
			Instanciates this class
				@param parent : the parent widget
		*/
		explicit TA_ClickableQWidget(QWidget *parent = 0);

	signals:
		/*
			This signal is send when the widget is clicked
		*/
		void clicked();
	private :
		/*
			This method is called when the widget is clicked
		*/
		void mouseReleaseEvent(QMouseEvent*);
};

#endif // TA_CLICKABLEQWIDGET_H
