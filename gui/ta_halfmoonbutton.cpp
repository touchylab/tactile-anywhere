/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 24 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_HalfMoonButton class          |
  | drawing an half moon button                                             |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Sébastien Destabeau -- Initial version                        |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_halfmoonbutton.h"

/*
    Create an instance of TA_HalfMoonButton
*/
TA_HalfMoonButton::TA_HalfMoonButton(QWidget *parent):QWidget(parent){
    resize(400,400);
    button = new QPushButton("Click here to go back");
    QHBoxLayout *hLayout = new QHBoxLayout;
    hLayout->addWidget(button);
    setLayout(hLayout);
//    QPixmap mask(":/images/logo.png");
//    button->setMask(mask.mask());
//    button->setStyleSheet("QPushButton {background-color:transparent;}");
    button->setStyleSheet("border-image: url(:/images/logo.png); border-top: 0px transparent; border-bottom: 0px transparent; border-right: 0px transparent; border-left: 0px transparent;");
}


/*
    Destroy properly this class
*/
TA_HalfMoonButton::~TA_HalfMoonButton(){
}

/*****************************************************************************/
/******                             EVENT                               ******/
/*****************************************************************************/

/*****************************************************************************/
/******                             GETTER                              ******/
/*****************************************************************************/

/*****************************************************************************/
/******                             SETTER                              ******/
/*****************************************************************************/

/*****************************************************************************/
/******                               SLOTS                             ******/
/*****************************************************************************/

/*****************************************************************************/
/******                             FUNCTIONS                           ******/
/*****************************************************************************/
