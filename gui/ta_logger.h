/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 10 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_Logger about logged messages  |
  | to display in gui                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Sébastien DESTABEAU -- Initial version                        |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_LOGGER_H
#define TA_LOGGER_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Fwd
class QTextBrowser;

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/
#define TA_LOGGER_TITLE "Logger"

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

class TA_Logger {

public:

    /*
        Destroy properly this instance
    */
    ~TA_Logger();

    /*
        Get an instance of a logger.
            @return instance : the unique instance of the logger.
    */
    static TA_Logger* getInstance();

    /*
        Log a message and append it in GUI Logger.
            @param message : the message to log.
    */
    void log(const QString &message);

    /*
        Log a critical message and append it in GUI Logger.
            @param message : the message to log.
    */
    void logCritical(const QString &message);

    /*
        Clear the logger content.
    */
    void clear();

    /*
        Get the logger widget. The widget must be deleted after use
            @return : the logger widget object.
    */
    QTextBrowser* getLoggerWidget();

    /*
        Stop the log
    */
    void stopLog();

private:

    /* singleton for the log object */
    static TA_Logger* instance;

    /* Text Browser for logger */
    QTextBrowser* loggerContentEditor = NULL;

};

#endif // TA_LOGGER_H
