/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 14 January 2013                                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_Components class containing   |
  | utility for popups and others regularly used gui components             |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Sébastien DESTABEAU -- Initial version                        |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_COMPONENTS_H
#define TA_COMPONENTS_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QMessageBox>
#include <QPushButton>

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
    Class to get some graphical components commonly used in client as much as server GUI.
*/
class TA_Components {

public:

    /*
        Display a popup message in GUI.
			@param title : the title of the popup.
			@param message : the message to display.
    */
    static void displayPopup(QString& title, QString& message);

    /*
        Display a popup message in gui.
			@param message : the message to display.
    */
    static void displayPopupMessage(QString& message);

    /*
        Display a warning message in gui.
			@param message : the message to display.
    */
    static void displayWarningMessage(QString& message);

    /*
        Display a popup message in gui.
			@param message : the message to display.
    */
    static void displayFatalMessage(QString& message);

    /*
        Display a popup message in gui with the purpose of a choice between Ok and Cancel.
			@param title : the title of the popup.
			@param message : the message to display.
			@return : an integer to identify the QMessageBox enum choice.
    */
    static int displayOkCancelMessage(QString& title, QString& message);
};

#endif // TA_COMPONENTS_H
