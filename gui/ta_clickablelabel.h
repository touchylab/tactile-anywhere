/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 20 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_ClickableLabel to allow user  |
  | to manage clicks on QLabel structures                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Sébastien DESTABEAU -- Initial version                        |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_CLICKABLELABEL_H
#define TA_CLICKABLELABEL_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QLabel>

//Local includes
#include "entity/ta_server.h"

// Fwd
class TA_InfoClient;

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/
#define TA_CLICKABLELABEL_LOADING_IMAGE ":/images/loading.gif"
#define TA_CLICKABLELABEL_NOT_AVAILABLE_IMAGE ":/images/noAvailable.png"

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
   Class to manage clickable QLabels
*/
class TA_ClickableLabel : public QLabel {
    Q_OBJECT

    public:
        /*
			Builder about a customized clickable QLabel
				@param infoClient : the client owning the webcam
				@param webcam : the webcam to display
		*/
        TA_ClickableLabel(TA_InfoClient* infoClient, WebcamStruct* webcam);

    signals:
        /*
			Signal to manage clicked action on clickable label.
				@param infoClient : the client informations about the choosen webcam
				@param webcamId : the choosen webcam identifier
        */
        void clicked(TA_InfoClient* infoClient, int webcamId);

    public slots:
		/*
			Called when the mousse press the label
        */
        void mouseReleaseEvent();

        /*
            Called when the mousse press the label
                @param event : the mouse event
        */
        void mouseReleaseEvent(QMouseEvent * event);

    private:
        TA_InfoClient* infoClient; // informations about the client
        WebcamStruct *webcam;
};

#endif // TA_CLICKABLELABEL_H
