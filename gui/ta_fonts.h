/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 10 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_Fonts class containing        |
  | utility for text fonts                                                  |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Sébastien DESTABEAU -- Initial version                        |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_FONTS_H
#define TA_FONTS_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QFont>

/*****************************************************************************/
/******                         DEFINES                                ******/
/*****************************************************************************/
#define TA_FONTS_FAMILY "Calibri"

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

class TA_Fonts {

public:
		/*
			Get a font for default text.
				@return : the default font
		*/
        static QFont getDefaultFont();

        /*
            Get a font for title text.
				@return : the title font
        */
        static QFont getTitleFont();

        /*
            Get a font for subtitle text.
				@return : the subtitle font
        */
        static QFont getSubTitleFont();

        /*
            Get a font for button text.
				@return : the button font
        */
        static QFont getButtonFont();

        /*
            Get a font for description text.
				@return : the description font
        */
        static QFont getDescriptionFont();
};

#endif // TA_FONTS_H
