/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 27 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_AdvanceButton to allow user   |
  | to display advances features                                            |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Valentin COULON -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_ADVANCEBUTTON_H
#define TA_ADVANCEBUTTON_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/
#include <QPushButton>

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/
#define TA_ADVANCEBUTTON_TOOLTIP "Mode avancé"
#define TA_ADVANCEBUTTON_ACTIVE_IMAGE ":/images/star.png"
#define TA_ADVANCEBUTTON_ACTIVE_TEXT "Mode avancé activé"
#define TA_ADVANCEBUTTON_INACTIVE_IMAGE ":/images/grey-star.png"
#define TA_ADVANCEBUTTON_INACTIVE_TEXT "Mode avancé désactivé"

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
   This class offer a button to display advances features
*/
class TA_AdvanceButton : public QPushButton {
    Q_OBJECT
    public:
        /*
            Instanciate a new instance of TA_AdvanceButton
        */
        TA_AdvanceButton();
    public slots:

        /*
            Activate or not the TA_AdvanceButton
                @param isActive : set as true to activate the button, false else
        */
        void setActiveStyle(bool isActive);
};

#endif // TA_ADVANCEBUTTON_H
