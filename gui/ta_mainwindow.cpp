/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 10 february 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_MainWindow class offering the |
  | main window of Tactile Anywhere                                         |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Valentin Coulon -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

//Qt
#include <QLabel>
#include <QMenuBar>
#include <QMenu>
#include <QToolBar>
#include <QDesktopWidget>
#include <QAction>
#include <QHBoxLayout>
#include <QPushButton>
#include <QFileDialog>
#include <QCloseEvent>
#include <QMessageBox>
#include <QSystemTrayIcon>
#include <QIcon>
#include <QApplication>
#include <QSettings>

//Local includes
#include "server/ta_servermainwidget.h"
#include "client/ta_clientmainwidget.h"
#include "communication/server/ta_infoclient.h"
#include "ta_help.h"
#include "ta_toolbar.h"
#include "ta_statusbar.h"
#include "ta_halfmoonbutton.h"
#include "entity/ta_client.h"
#include "entity/ta_server.h"
#include "entity/ta_xmlparser.h"
#include "ta_fonts.h"
#include "ta_mainwindow.h"

TA_MainWindow * TA_MainWindow::singleton = NULL;

/*
    Initializes an instance of this class as a singleton
*/
void TA_MainWindow::initialize(){
    //singleton = new TA_MainWindow(new QWidget());
    singleton = new TA_MainWindow();
}

/*
    Gets the unique instance of this class. initialize() must be called before
        @return : the unique instance of this class or NULL if this class is not initialized
*/
TA_MainWindow* TA_MainWindow::getInstance(){
    return singleton;
}

/*
    This constructor creates and initializes this class
        @param parent : widget parent
*/
TA_MainWindow::TA_MainWindow(QWidget *parent) : QMainWindow(parent){
    showHelp = false;
    createActions();
    initWindow();
    buildStatusBar();
    //settings = new QSettings("TA", "Tactile-Anywhere");
    //buildSystemTray();
    buildMainWidget();
    qApp->setStyleSheet(TA_MAINWINDOW_CSS);
}

/*
    Cleans properly this class at the destruction
*/
TA_MainWindow::~TA_MainWindow() {
    TA_SystemEventManager::destroy();
}

/*
    Build the toolBar's main window
*/
void TA_MainWindow::initToolBar(){
    buildToolBar();
}

/*
    Attach the html help page to this window
        @param htmlPage : the html page to attach
*/
void TA_MainWindow::setHelp(QString htmlPage){
    if(rightWidget){
        delete rightWidget;
        rightWidget = NULL;
    }
    rightWidget = new TA_Help(htmlPage);
    if(!showHelp){
        rightWidget->hide();
    }
    this->mainLayout->insertWidget(1, rightWidget,1);
}

/*
    Attach the html help page to this window
        @param limited : disable toolBar's buttons
        @param limitHome : disable the home button
*/
void TA_MainWindow::setLimitedToolBar(bool limited, bool limitHome){
    homeAct->setDisabled(limitHome);
    saveAct->setDisabled(limited);
    saveAsAct->setDisabled(limited);
    loadAct->setDisabled(limited);
    paramAct->setDisabled(limited);
}

/*****************************************************************************/
/******                      PRIVATE METHODS                            ******/
/*****************************************************************************/

/*
    Initialize the main window (icon, size, title)
*/
void TA_MainWindow::initWindow() {
    setWindowIcon(QIcon(TA_MAINWINDOW_LOGO));
    setWindowTitle("Tactile Anywhere");

    QWidget* mainWidget = new QWidget;
    mainLayout = new QHBoxLayout;
    mainWidget->setLayout(mainLayout);
    setCentralWidget(mainWidget);
    QDesktopWidget widget;
    screenSize = widget.availableGeometry(widget.primaryScreen());
    move(screenSize.width()*0.2,screenSize.height()*0.2);
}

/*
    Build the tool bar for this window
*/
void TA_MainWindow::buildToolBar(){
    toolBar = new TA_ToolBar(this);
    addToolBar(toolBar);

    QWidget* spacer1 = new QWidget();
    QWidget* spacer2 = new QWidget();
    spacer1->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    spacer2->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    toolBar->addWidget(spacer1);
    toolBar->addAction(homeAct);
    toolBar->addAction(saveAct);
    toolBar->addAction(saveAsAct);
    toolBar->addAction(loadAct);
    toolBar->addAction(paramAct);
    toolBar->addAction(helpAct);
    toolBar->addWidget(spacer2);
}

/*
    Build the status bar for this window
*/
void TA_MainWindow::buildStatusBar(){
    statusBar = new TA_StatusBar(this);
    setStatusBar(statusBar);
}

/*
    Build the system tray
*/
/*void TA_MainWindow::buildSystemTray(){
    QApplication::setQuitOnLastWindowClosed(false);
    // System tray actions
    minimizeAction = new QAction("Minimiser", this);
    connect(minimizeAction, SIGNAL(triggered()), this, SLOT(showMinimized()));

    maximizeAction = new QAction("Maximiser", this);
    connect(maximizeAction, SIGNAL(triggered()), this, SLOT(showMaximized()));

    restoreAction = new QAction("Restaurer", this);
    connect(restoreAction, SIGNAL(triggered()), this, SLOT(showNormal()));

    quitAction = new QAction("Quitter", this);
    connect(quitAction, SIGNAL(triggered()), this, SLOT(close()));

    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(minimizeAction);
    trayIconMenu->addAction(maximizeAction);
    trayIconMenu->addAction(restoreAction);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(quitAction);

    trayIcon = new QSystemTrayIcon(QIcon(TA_MAINWINDOW_LOGO),this);
    trayIcon->setContextMenu(trayIconMenu);
    trayIcon->show();

    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(trayIconClick(QSystemTrayIcon::ActivationReason)));
}*/


/*
    Create all actions for buttons in the tool bar
*/
void TA_MainWindow::createActions() {
    //File
    loadAct = new QAction(QIcon(TA_MAINWINDOW_LOAD), "Charger (Ctrl+O)", this);
    loadAct->setShortcuts(QKeySequence::Open);
    loadAct->setWhatsThis("Charger une configuration existante");
    connect(loadAct, SIGNAL(triggered()), this, SLOT(load()));

    saveAct = new QAction(QIcon(TA_MAINWINDOW_SAVE), "Enregistrer (Ctrl+S)", this);
    saveAct->setShortcuts(QKeySequence::Save);
    saveAct->setWhatsThis("Enregistrer la configuration actuelle");
    connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));

    saveAsAct = new QAction(QIcon(TA_MAINWINDOW_SAVEAS), "Enregistrer sous (Ctrl+Shift+S)", this);
    saveAsAct->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_S));
    saveAsAct->setWhatsThis("Enregistrer la configuration actuelle sous");
    connect(saveAsAct, SIGNAL(triggered()), this, SLOT(saveAs()));

    //Options
    homeAct = new QAction(QIcon(TA_MAINWINDOW_HOME), "Accueil (Ctrl+A)", this);
    homeAct->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_A));
    connect(homeAct, SIGNAL(triggered()), this, SLOT(home()));

    paramAct = new QAction(QIcon(TA_MAINWINDOW_SETTINGS), "Paramètres (Ctrl+P)", this);
    paramAct->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_P));
    connect(paramAct, SIGNAL(triggered()), this, SLOT(param()));

    #ifdef Q_OS_MAC
        helpAct = new QAction(QIcon(TA_MAINWINDOW_HELP), "Aide (Ctrl+?)", this);
    #else
        helpAct = new QAction(QIcon(TA_MAINWINDOW_HELP), "Aide (F1)", this);
    #endif
    helpAct->setToolTip(QString("L'aide contextuelle s'affiche dans une fenêtre droite au moment où vous cliquez sur ce bouton dans la barre d'outils. Pour cacher l'aide, cliquez à nouveau sur ce même bouton."));

    helpAct->setShortcut(QKeySequence::HelpContents);
    helpAct->setWhatsThis("Aide");
    connect(helpAct, SIGNAL(triggered()), this, SLOT(help()));
}

/*
    Build the first screen to specify the launch mode
*/
void TA_MainWindow::buildMainWidget() {
    QWidget* mainW = new QWidget(this);
    QVBoxLayout* mainLayout = new QVBoxLayout();

    QHBoxLayout *headerLayout = new QHBoxLayout();
    QLabel *titleLabel = new QLabel("Tactile Anywhere");
    QFont font = TA_Fonts::getTitleFont();
    font.setPixelSize(24);
    titleLabel->setFont(font);
    QPushButton *helpButton = new QPushButton(QIcon(TA_MAINWINDOW_HELP),"");
    helpButton->setToolTip(QString("L'aide contextuelle s'affiche dans une fenêtre à droite au moment où vous cliquez sur ce bouton dans la barre d'outils. Pour cacher l'aide, cliquez à nouveau sur ce même bouton."));
    headerLayout->addWidget(titleLabel,1, Qt::AlignCenter| Qt::AlignTop);
    headerLayout->addWidget(helpButton,0, Qt::AlignRight| Qt::AlignTop);
    connect(helpButton,SIGNAL(clicked()), this, SLOT(help()));
    mainLayout->addLayout(headerLayout);
    mainLayout->addStretch();

    QLabel *description = new QLabel();
    description->setPixmap(QPixmap(TA_MAINWINDOW_TINY_LOGO));
    mainLayout->addWidget(description, 0, Qt::AlignCenter| Qt::AlignHCenter);
    mainLayout->addStretch();

    QVBoxLayout* buttonLayout = new QVBoxLayout;
    QPushButton* serverButton = new QPushButton("Lancer Tactile Anywhere");
    serverButton->setFont(TA_Fonts::getTitleFont());
    serverButton->setFixedHeight(screenSize.height()/12);
    connect(serverButton, SIGNAL(clicked()), this, SLOT(openServer()));
    buttonLayout->addWidget(serverButton);

    QPushButton* clientButton = new QPushButton("Connecter ses webcams à distance");
    clientButton->setFont(TA_Fonts::getSubTitleFont());
    connect(clientButton, SIGNAL(clicked()), this, SLOT(openClient()));
    buttonLayout->addWidget(clientButton);

    QHBoxLayout* centerLayout = new QHBoxLayout;
    centerLayout->addStretch(1);
    centerLayout->addLayout(buttonLayout,1);
    centerLayout->addStretch(1);
    mainLayout->addLayout(centerLayout);
    mainLayout->addStretch();

    QHBoxLayout* bottomLayout = new QHBoxLayout;
   // label copyleft
   QLabel* logoLabel = new QLabel();
   logoLabel->setPixmap(QPixmap(":/images/Logo-TouchyLab.png"));
   bottomLayout->addWidget(logoLabel, 0, Qt::AlignBottom | Qt::AlignLeft);
   QLabel* copyLeft = new QLabel(TA_MAINWINDOW_LICENCE);
   font = TA_Fonts::getDefaultFont();
   font.setPixelSize(11);
   copyLeft->setFont(font);
   bottomLayout->addWidget(copyLeft, 0, Qt::AlignBottom | Qt::AlignRight);
   mainLayout->addLayout(bottomLayout);

   mainW->setLayout(mainLayout);
   if(centerWidget){
       delete centerWidget;
       centerWidget = NULL;
   }
   centerWidget = mainW;
   this->mainLayout->insertWidget(0, mainW);
   statusBar->hide();
   setHelp(TA_MAINWINDOW_HELP_HTML);
}

/*
    Save the current configuration
*/
void TA_MainWindow::_save(){
    ConfSaveReturn ret = TA_XmlParser::saveConfiguration(fileName);
    if(!fileName.isEmpty()){
        switch(ret){
        case CONFSAVE_SUCCESS:
            statusBar->showMessage("Configuration sauvegardée avec succès.");
            TA_Server::getInstance()->isSaved(true);
            break;
        case CONFSAVE_CANTCREATEDIR:
            statusBar->showMessage("Erreur : Impossible de créer le dossier spécifié.","Error");
            break;
        case CONFSAVE_CANTCREATEFILE:
            statusBar->showMessage("Erreur : Impossible de créer le fichier spécifié.","Error");
            break;
        }
    }
}


/*****************************************************************************/
/******                        PRIVATE SLOTS                            ******/
/*****************************************************************************/

/*
    Open the server part from this window
*/
void TA_MainWindow::openServer() {
    TA_ServerMainWidget::initialize();
    if (centerWidget) {
        delete centerWidget;
        centerWidget = NULL;
    }
    statusBar->show();
    centerWidget = TA_ServerMainWidget::getInstance();
    this->mainLayout->insertWidget(0, centerWidget);
}

/*
    Open the client part from this window
*/
void TA_MainWindow::openClient() {
    TA_ClientMainWidget::initialize();
    if(centerWidget){
        delete centerWidget;
        centerWidget = NULL;
    }
    statusBar->show();
    centerWidget = TA_ClientMainWidget::getInstance();
    this->mainLayout->insertWidget(0, centerWidget,1);
}

/*
    Save the current configuration
*/
void TA_MainWindow::save() {
    if(fileName.isNull()){
        #if defined(Q_OS_WIN) || defined(Q_OS_MAC)
            fileName = QFileDialog::getSaveFileName(this, "Sauvegarder la configuration","","Files(*)");
        #else
            fileName = QFileDialog::getSaveFileName(this, "Sauvegarder la configuration","","Files(*)",0,QFileDialog::DontUseNativeDialog);
        #endif
    }
    _save();
}

/*
    Save the current configuration with a specific filename
*/
void TA_MainWindow::saveAs() {
    #if defined(Q_OS_WIN) || defined(Q_OS_MAC)
        fileName = QFileDialog::getSaveFileName(this, "Sauvegarder la configuration sous","","Files(*)");
    #else
        fileName = QFileDialog::getSaveFileName(this, "Sauvegarder la configuration sous","","Files(*)",0,QFileDialog::DontUseNativeDialog);
    #endif
    _save();
}

/*
    Load the current configuration
*/
void TA_MainWindow::load() {
    #if defined(Q_OS_WIN) || defined(Q_OS_MAC)
        fileName = QFileDialog::getOpenFileName(this, "Charger une configuration","","Files(*)");
    #else
        fileName = QFileDialog::getOpenFileName(this, "Charger une configuration","","Files(*)",0,QFileDialog::DontUseNativeDialog);
    #endif

    if(!fileName.isEmpty()){
        ConfLoadReturn ret = TA_XmlParser::loadConfiguration(fileName);
        switch(ret){
        case CONFLOAD_SUCCESS:
            if(TA_ServerMainWidget::getInstance()){
                TA_ServerMainWidget::getInstance()->openHomeScreen(true);
            }
            statusBar->showMessage("Configuration chargée avec succès");
            TA_Server::getInstance()->isSaved(true);
            break;
        case CONFLOAD_CANTCREATEDIR:
            statusBar->showMessage("Impossible de créer le dossier spécifié.","Error");
            break;
        case CONFLOAD_CANTOPENFILE:
            statusBar->showMessage("Impossible d'ouvrir le fichier spécifié.","Error");
            break;
        case CONFLOAD_BADFORMAT:
            statusBar->showMessage("Erreur de format sur le fichier.","Error");
            break;
        }
    }
}

/*
    Display alternatively the help section
*/
void TA_MainWindow::help(){
    if(!rightWidget)
        return;

    if(showHelp){
        rightWidget->hide();
        showHelp = false;
    } else {
        rightWidget->show();
        showHelp = true;
    }
}

/*
    Display parameters screen
*/
void TA_MainWindow::param(){
    TA_ServerMainWidget::getInstance()->openOptions();
}

/*
    Display home screen
*/
void TA_MainWindow::home(){
    TA_ServerMainWidget::getInstance()->openHomeScreen();
}

/*
    Handle interactions with icon system tray
        @param reason : the type of action
*/
/*void TA_MainWindow::trayIconClick(QSystemTrayIcon::ActivationReason reason){
    if(reason==QSystemTrayIcon::Trigger){
        if((windowState() & Qt::WindowMinimized)){
            QMainWindow::showNormal();
            setWindowState(Qt::WindowActive);
            if(settings->contains("pos")){
                move(settings->value("pos").toPoint());
            }
            if(settings->contains("size")){
                resize(settings->value("size").toSize());
            }
            if(settings->value("maximized").toBool()){
                setWindowState(Qt::WindowMaximized);
            }
        }
        else {
            settings->setValue("pos", pos());
            if(!isMaximized()){
                settings->setValue("size", size());
            }
            settings->setValue("maximized", isMaximized());
            showMinimized();
        }
    }
}*/

/*****************************************************************************/
/******                        GETTERS                                  ******/
/*****************************************************************************/

/*
    Get the screen resolution
        @return : the screen size in pixel
*/
QRect TA_MainWindow::getScreenSize(){
    return screenSize;
}

/*
    Get the TA_StatusBar object
        @return : the statusBar
*/
TA_StatusBar* TA_MainWindow::getStatusBar(){
    return statusBar;
}

/*
    Get the system tray icon
        @return : the system tray
*/
/*QSystemTrayIcon* TA_MainWindow::getTrayIcon(){
    return trayIcon;
}*/



/*****************************************************************************/
/******                        OVERLOAD                                 ******/
/*****************************************************************************/

/*
    Hide the window on minimized window
        @param event: the event ask
*/
/*void TA_MainWindow::changeEvent(QEvent *event){
    if(event->type()==QEvent::WindowStateChange){
        if(windowState() & Qt::WindowMinimized){
            hide();
        }
    }
    QMainWindow::changeEvent(event);
}*/

/*
    Handle closing application.
        @param event: the close event
*/
void TA_MainWindow::closeEvent(QCloseEvent *event){
    if(TA_Server::getInstance()!=NULL){
        if(!TA_Server::getInstance()->isSaved()){
            int response = QMessageBox::question(this, "Enregistrer la configuration", "Voulez-vous enregistrez la configuration actuelle avant de quitter Tactile Anywhere ?", QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel);
            if(response == QMessageBox::Cancel){
                event->ignore();
                return;
            }

            if(response == QMessageBox::Yes){
                save();
            }
        } else {
            if(QMessageBox::No == QMessageBox::question(this, "Confirmation d'action", "Voulez-vous fermer Tactile Anywhere ?", QMessageBox::Yes|QMessageBox::No)){
                event->ignore();
                return;
            }
        }
        delete TA_Server::getInstance();
    } else if(TA_Client::getInstance()!=NULL){
        if(QMessageBox::No == QMessageBox::question(this, "Confirmation d'action", "Voulez-vous fermer Tactile Anywhere ?", QMessageBox::Yes|QMessageBox::No))
            event->ignore();
        else
            delete TA_Client::getInstance();
    }
    QApplication::quit();
}

/*
    Update the current widget and compute drawing zone
    for button thant hide and display the tool bar
*/
void TA_MainWindow::update(){
    rectArrow = QRect((width()/2)-(arrowPixmap.width()/2),toolBar->pos().y()+toolBar->height(),arrowPixmap.width(),arrowPixmap.height());
    QWidget::update();
}

/*
    Draw the button for hide and display the tool bar
    @param : the draw event
*/
void TA_MainWindow::paintEvent(QPaintEvent *event){
    Q_UNUSED(event)
    QPainter painter(this);
    if(TA_Server::getInstance()){
        if(!toolBar->isVisible())
            arrowPixmap = QPixmap(TA_MAINWINDOW_TOOLBAR_DOWN);
        else
            arrowPixmap = QPixmap(TA_MAINWINDOW_TOOLBAR_UP);
        painter.drawPixmap(rectArrow,arrowPixmap);
        update();
    }
    painter.end();
}

/*
    Handle the click on the button for hide and display the tool bar
    @param : the mouse event
*/
void TA_MainWindow::mousePressEvent(QMouseEvent *event){
    if(TA_Server::getInstance()){
        if(rectArrow.contains(event->pos()))
            toolBar->slide();
        update();
    }
}

