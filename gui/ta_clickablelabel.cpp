/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 20 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_ClickableLabel to allow user       |
  | to manage clicks on QLabel structures                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Sébastien DESTABEAU -- Initial version                        |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/
/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QLabel>
#include <QPixmap>
#include <QMovie>

//Local includes
#include "communication/server/ta_infoclient.h"
#include "gui/ta_clickablelabel.h"

/*
    Builder about a customized clickable QLabel
		@param infoClient : the client owning the webcam
		@param webcam : the webcam to display
*/
TA_ClickableLabel::TA_ClickableLabel(TA_InfoClient* infoClient, WebcamStruct* webcam) : QLabel(){
    this->infoClient = infoClient;
    this->webcam = webcam;
    setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed));
    if(!webcam || webcam->image.isNull() || webcam->image.byteCount() == 0 || webcam->image.size() == QSize(0,0)){
        setPixmap(QPixmap(TA_CLICKABLELABEL_NOT_AVAILABLE_IMAGE));
    }else{
        QMovie* movie = new QMovie(TA_CLICKABLELABEL_LOADING_IMAGE);
        movie->setScaledSize(QSize(50,50));
        setMovie(movie);
        movie->start();
    }
}

/*****************************************************************************/
/******                           SLOTS                                 ******/
/*****************************************************************************/

/*
    Called when the mousse press the label
*/
void TA_ClickableLabel::mouseReleaseEvent(){
    emit clicked(infoClient, webcam? webcam->id: -1);
}

/*
	Called when the mousse press the label
        @param event : the mouse event
*/
void TA_ClickableLabel::mouseReleaseEvent(QMouseEvent *){
    mouseReleaseEvent();
}
