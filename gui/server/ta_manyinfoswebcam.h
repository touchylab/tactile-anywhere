/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 20 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_ManyInfosWebcam to display widget  |
  | to display content informations about a choosen webcam                  |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Sébastien DESTABEAU -- Initial version                        |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_MANYINFOSWEBCAM_H
#define TA_MANYINFOSWEBCAM_H

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/
#define TA_MANYINFOSWEBCAM_TITLE "%1 " // arg1 : webcam name

#define TA_MANYINFOSWEBCAM_CLIENT_NAME_LABEL "\nPossesseur de la caméra : %1" // arg1 : client name

#define TA_MANYINFOSWEBCAM_IMAGE_RESOLUTION_LABEL "Résolution de la webcam : %1x%2 px"
// arg1 : webcam's width, arg2 : webcam's height

#define TA_MANYINFOSWEBCAM_CONFIG_LABEL "Zones définies : %1   Marqueurs repérés : %2\n"
// arg1 : number of areas, arg2 : number of markers

#define TA_MANYINFOSWEBCAM_NOT_AVAILABLE_WEBCAM_ICON ":/images/noAvailable.png"

#define TA_MANYINFOSWEBCAM_CONNECTION_OK_ICON ":/images/cercle-vert.png"
#define TA_MANYINFOSWEBCAM_CONNECTION_OK_TOOL_TIP "Fonctionnelle"

#define TA_MANYINFOSWEBCAM_NO_CONNECTION_ICON ":/images/cercle-gris.png"
#define TA_MANYINFOSWEBCAM_NO_CONNECTION_TOOL_TIP "Non Fonctionnelle"

#define TA_MANYINFOSWEBCAM_SHOW_BUTTON_ICON ":/images/show.png"
#define TA_MANYINFOSWEBCAM_SHOW_BUTTON_TEXT "Affichée"
#define TA_MANYINFOSWEBCAM_SHOW_BUTTON_TOOL_TIP "Masquer la webcam"

#define TA_MANYINFOSWEBCAM_NOT_SHOW_BUTTON_ICON ":/images/notshow.png"
#define TA_MANYINFOSWEBCAM_NOT_SHOW_BUTTON_TEXT "Masquée"
#define TA_MANYINFOSWEBCAM_NOT_SHOW_BUTTON_TOOL_TIP "Afficher la webcam"

#define TA_MANYINFOSWEBCAM_RETURN_BUTTON_TEXT "Retour"

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QWidget>

//Fwd
class QLabel;
class QPushButton;
class TA_InfoClient;
class TA_SectorViewer;

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
    Class to display many informations about a selected webcam
*/
class TA_ManyInfosWebcam : public QWidget{
    Q_OBJECT

    public:
        /*
            Display informations about a choosen webcam.
            @param infoClient : the client informations about the webcam
            @param webcamId : the choosen webcam identifier
        */
        TA_ManyInfosWebcam(TA_InfoClient* infoClient, int webcamId);

signals:

private slots:

        /*
            Cancel action and go back to the previous screen (home screen)
        */
        void cancel();

        /*
            Updates the webcams state
        */
        void changedWebcams();

        /*
            Updates the webcam's markers
                @param client : the client of the webcam
                @param webcamId : the webcam identifier
        */
        void changedMarkers(TA_InfoClient* client, int webcamId);

        /*
            Updates the webcam's frame
                @param client : the client of the webcam
                @param webcamId : the webcam identifier
        */
        void changedFrame(TA_InfoClient* client, int webcamId);

        /*
            Updates the view
        */
        void changedClient();

        /*
            Change the displayed mode of the webcam
        */
        void changeDisplayWebcam();

private:
        /*
            Change the displayed button for the mode of the webcam
            @param visible : true => visible, false => not visible
        */
        void changeButtonDisplayWebcam(bool visible);

        TA_SectorViewer* webcamViewer; // The webcam Viewer
        QLabel* titleLabel; // The name label
        QLabel* stateLabel; // The state label
        QLabel* imageResolutionLabel; // The resolution label
        QLabel* configLabel; // The configuration label
        QPushButton *showButton; // The show button to hide or not the webcam
        int webcamId; // The webcam Id
        TA_InfoClient* client; // The client of the webcam

};

#endif // TA_MANYINFOSWEBCAM_H
