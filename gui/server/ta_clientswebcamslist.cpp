
/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 19 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_ClientsWebcamsList to display      |
  | widget to display all clients webcams                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Sébastien DESTABEAU -- Initial version                        |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QLayout>
#include <QLabel>
#include <QScrollArea>

//Local includes
#include "gui/server/ta_servermainwidget.h"
#include "gui/ta_clickablelabel.h"
#include "gui/ta_fonts.h"
#include "gui/ta_mainwindow.h"
#include "gui/ta_flowlayout.h"
#include "communication/server/ta_infoclient.h"
#include "ta_clientswebcamslist.h"

/*
  Default builder
*/
TA_ClientsWebcamsList::TA_ClientsWebcamsList() : QWidget(){
    frameSide = TA_MainWindow::getInstance()->getScreenSize().width()/5;

    flowLayout = new TA_FlowLayout();

    QWidget *mainWidget= new QWidget();
    mainWidget->setLayout(flowLayout);

    QScrollArea *scrollArea = new QScrollArea();
    scrollArea->setWidgetResizable(true);
    scrollArea->setWidget(mainWidget);

    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addWidget(scrollArea);
    setLayout(mainLayout);

    //Get the clients webcams list.
    ConfigTree* clientsTree = TA_Server::getInstance()->getConfig();

    // Display showed webcams
    QList<WebcamStruct*> notShowed;
    foreach(TA_InfoClient* infoClient, clientsTree->keys()){
        foreach(int webcamId, ((*clientsTree)[infoClient])->keys()){
            WebcamStruct* webcam = TA_Server::getInstance()->getWebcamInfo(infoClient,webcamId);
            if(webcam->isDisplayed){
                addCell(infoClient,webcamId);
            } else {
                notShowed << webcam;
            }
        }
    }

    //Display not showed webcams
    foreach(WebcamStruct* webcam, notShowed){
        addCell(webcam->client,webcam->id);
    }

    // Connections
    connect(TA_Server::getInstance(), SIGNAL(changedFrame(TA_InfoClient*,int)), this, SLOT(changedFrame(TA_InfoClient*,int)));
	connect(TA_Server::getInstance(), SIGNAL(changedWebcams(TA_InfoClient*,QVector<int>)), this, SLOT(changedWebcams(TA_InfoClient*,QVector<int>)));
    connect(TA_Server::getInstance(), SIGNAL(changedClient()),this,SLOT(changedClient()));

    // Frames
    TA_Server::getInstance()->askFrame(false);

    setLayout(mainLayout);
    this->setMinimumSize(frameSide*1.5,frameSide*1.5);
}

/*****************************************************************************/
/******                          OPERATIONS                             ******/
/*****************************************************************************/

/*
    Add a cell to the widget
        @param client : the client to owning the webcam
        @param webcamId : the webcam's id
*/
void TA_ClientsWebcamsList::addCell(TA_InfoClient* client, int webcamId){
    WebcamStruct* webcam = TA_Server::getInstance()->getWebcamInfo(client,webcamId);
    QVBoxLayout *cellLayout = new QVBoxLayout();

    QHBoxLayout * headerLayout = new QHBoxLayout;
    TA_ClickableLabel *camTitle = new TA_ClickableLabel(client, webcam);
    camTitle->setText(webcam->name);
    camTitle->setFont(TA_Fonts::getSubTitleFont());
    headerLayout->addWidget(camTitle,0,Qt::AlignHCenter);

    if(!webcam->isDisplayed){
        TA_ClickableLabel *notShowIcon = new TA_ClickableLabel(client, webcam);
        notShowIcon->setPixmap(QPixmap(TA_CLIENTSWEBCAMSLIST_NOT_SHOW_ICON).scaledToWidth(20,Qt::SmoothTransformation));
        notShowIcon->setToolTip(TA_CLIENTSWEBCAMSLIST_NOT_SHOW_TEXT);
        headerLayout->addWidget(notShowIcon,0,Qt::AlignLeft);
    }

    QHBoxLayout* centerHeaderLayout = new QHBoxLayout;
    centerHeaderLayout->addStretch(1);
    centerHeaderLayout->addLayout(headerLayout);
    centerHeaderLayout->addStretch(1);

    cellLayout->addLayout(centerHeaderLayout);

    // label picture
    TA_ClickableLabel* webcamPictureLabel = new TA_ClickableLabel(client, webcam);

    imgMap[webcam] = webcamPictureLabel;
    cellLayout->addWidget(webcamPictureLabel,1,Qt::AlignCenter);
    webcamPictureLabel->setFixedSize(frameSide,frameSide);
    QImage frame = webcam->image;

    if(frame.size() == QSize(0,0) || !webcam->active){
        frame.load(TA_CLIENTSWEBCAMSLIST_NOT_AVAILABLE_WEBCAM);
    }

    webcamPictureLabel->setEnabled(webcam->isDisplayed);
    webcamPictureLabel->setPixmap(QPixmap::fromImage(frame).scaled(frameSide,frameSide));
    QObject::connect(webcamPictureLabel, SIGNAL(clicked(TA_InfoClient*, int)), this, SLOT(clickLabel(TA_InfoClient*, int)));
    QObject::connect(camTitle, SIGNAL(clicked(TA_InfoClient*, int)), this, SLOT(clickLabel(TA_InfoClient*, int)));

    QWidget *w = new QWidget();
    w->setLayout(cellLayout);
    flowLayout->addWidget(w);
	cellMap[webcam] = w;
    clientMap[webcam] = client;
    idMap[webcam] = webcam->id;
    update();
}

/*
    Add a cell to the widget
        @param webcam : the webcam structure
*/
void TA_ClientsWebcamsList::removeCell(WebcamStruct* webcam){
    flowLayout->removeWidget(cellMap[webcam]);
    delete cellMap[webcam];
    imgMap.remove(webcam);
    cellMap.remove(webcam);
    clientMap.remove(webcam);
    idMap.remove(webcam);
    update();
}

/*****************************************************************************/
/******                           SLOTS                                 ******/
/*****************************************************************************/
/*
  Go to ManyInformationsWebcam on click
*/
void TA_ClientsWebcamsList::clickLabel(TA_InfoClient* infoClient, int webcamId){
    TA_ServerMainWidget::getInstance()->openManyInformationsWebcam(infoClient, webcamId);
}

/*
  Updates the webcams frame
  @param client : the client's webcam
  @param webcamId : the webcam identifier to update frame for
*/
void TA_ClientsWebcamsList::changedFrame(TA_InfoClient* client, int webcamId){
    if(imgMap.contains(TA_Server::getInstance()->getWebcamInfo(client,webcamId))){
        QPixmap pixmap = QPixmap::fromImage(TA_Server::getInstance()->getWebcamInfo(client,webcamId)->image.scaled(frameSide,frameSide));
        imgMap[TA_Server::getInstance()->getWebcamInfo(client,webcamId)]->setPixmap(pixmap);
        update();
    }
}

/*
    Updates the webcams state
        @param client: the client owning the changed webcams
        @param webcams : the changed webcams' id
*/
void TA_ClientsWebcamsList::changedWebcams(TA_InfoClient* client, QVector<int> webcams){
    Q_UNUSED(client)
    Q_UNUSED(webcams)
    TA_ServerMainWidget::getInstance()->openHomeScreen();
}

/*
    Updates the webcams list
*/
void TA_ClientsWebcamsList::changedClient(){
    foreach(WebcamStruct* webcam , clientMap.keys()){
        if(!TA_Server::getInstance()->getWebcamInfo(clientMap[webcam],idMap[webcam])){
            removeCell(webcam);
        }
    }
}
