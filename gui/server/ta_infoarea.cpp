/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 26 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_InfoArea to display widget to      |
  | display content informations about a choosen area                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QLayout>
#include <QLabel>
#include <QPushButton>

// Local includes
#include "entity/ta_server.h"
#include "gui/server/ta_servermainwidget.h"
#include "gui/server/ta_areawebcamslist.h"
#include "gui/ta_fonts.h"
#include "ta_infoarea.h"

/*
    Display informations about a choosen area
        @param areaName : the area's name
*/
TA_InfoArea::TA_InfoArea(QString areaName) : QWidget(){
    AreaStruct* area = TA_Server::getInstance()->getAreaInfo(areaName);

    //Header
    QHBoxLayout* headerLayout = new QHBoxLayout;
    QLabel* titleLabel = new QLabel(areaName);
    titleLabel->setFont(TA_Fonts::getTitleFont());
    headerLayout->addWidget(titleLabel);
    headerLayout->setAlignment(Qt::AlignHCenter);

    QVBoxLayout* mainLayout = new QVBoxLayout;
    mainLayout->addLayout(headerLayout,1);

    // top widget and horizontal layout
    QVBoxLayout* topVLayout = new QVBoxLayout;

    // Color
    QHBoxLayout* colorTypeLayout = new QHBoxLayout;
    QLabel* typeLabel;
    if(area->type == NORMAL_AREA){
        typeLabel = new QLabel(TA_INFOAREA_NORMAL_TYPE_LABEL);
    } else {
        typeLabel = new QLabel(TA_INFOAREA_GROUP_TYPE_LABEL);
    }
    typeLabel->setFont(TA_Fonts::getDefaultFont());
    colorTypeLayout->addWidget(typeLabel);
    colorTypeLayout->addStretch();
    colorTypeLayout->addStretch();

    QLabel* colorTitle = new QLabel(TA_INFOAREA_COLOR_TITLE);
    colorTitle->setFont(TA_Fonts::getDefaultFont());
    QLabel* colorLabel = new QLabel();
    colorLabel->setFont(TA_Fonts::getDefaultFont());
    colorLabel->setAutoFillBackground(true);
    colorLabel->setFixedWidth(50);
    QPalette colorPalette;
    colorPalette.setColor(QPalette::Background, area->color);
    colorLabel->setPalette(colorPalette);
    colorTypeLayout->addWidget(colorTitle);
    colorTypeLayout->addWidget(colorLabel);
    topVLayout->addLayout(colorTypeLayout);

    if(area->type == GROUP_AREA){
        QString children = "Sous-Zones : ";
        if(area->down.isEmpty()){
            children += TA_INFOAREA_NO_CHILD_TEXT;
        } else {
            children += area->down[0];
            for(int i=1; i < area->down.size(); i++){
                children += QString(TA_INFOAREA_CHILD_SEPERATOR) + area->down[i];
            }
        }
        QLabel* groupLabel = new QLabel(children);
        groupLabel->setFont(TA_Fonts::getDefaultFont());
        topVLayout->addWidget(groupLabel);
    }

    topVLayout->addStretch();

    //Actions
    if(!area->file.isEmpty()){
        QLabel* fileLabel = new QLabel(QString(TA_INFOAREA_FILE_LABEL) + area->file);
        fileLabel->setFont(TA_Fonts::getDefaultFont());
        topVLayout->addWidget(fileLabel);
    }
    if(!area->url.isEmpty()){
        QLabel* urlLabel = new QLabel(QString(TA_INFOAREA_URL_LABEL) + area->url);
        urlLabel->setFont(TA_Fonts::getDefaultFont());
        topVLayout->addWidget(urlLabel);
    }
    if(!area->command.isEmpty()){
        QLabel* commandLabel = new QLabel(QString(TA_INFOAREA_COMMAND_LABEL) + area->command);
        commandLabel->setFont(TA_Fonts::getDefaultFont());
        topVLayout->addWidget(commandLabel);
    }
    if(!area->file.isEmpty() || !area->url.isEmpty() || !area->command.isEmpty()){
        topVLayout->addStretch();
    }

    QHBoxLayout* centerLayout = new QHBoxLayout;
    centerLayout->addStretch(1);
    centerLayout->addLayout(topVLayout,5);
    centerLayout->addStretch(1);
    mainLayout->addLayout(centerLayout,1);


    //Footer
    QHBoxLayout* footerLayout = new QHBoxLayout();
    QHBoxLayout* buttonLayout = new QHBoxLayout();
    QPushButton* returnButton = new QPushButton(TA_INFOAREA_RETURN_BUTTON_TEXT);
    returnButton->setFont(TA_Fonts::getButtonFont());
    buttonLayout->addStretch();
    buttonLayout->addWidget(returnButton,0,Qt::AlignTop);
    buttonLayout->addStretch();
    footerLayout->addLayout(buttonLayout);
    connect(returnButton, SIGNAL(clicked()), this, SLOT(cancel()));

    TA_AreaWebcamsList* webcamList = new TA_AreaWebcamsList(areaName);

    if(webcamList->isEmpty()){
        delete webcamList;
        QLabel* webcamLabel = new QLabel(QString(TA_INFOAREA_NO_WEBCAMS_TEXT));
        webcamLabel->setFont(TA_Fonts::getDefaultFont());
        mainLayout->addWidget(webcamLabel, 1, Qt::AlignHCenter);
    } else {
        QLabel* webcamLabel = new QLabel(QString(TA_INFOAREA_WEBCAM_LABEL));
        QFont font = TA_Fonts::getDefaultFont();
        font.setBold(true);
        webcamLabel->setFont(font);
        mainLayout->addWidget(webcamLabel,1,Qt::AlignHCenter);
        QHBoxLayout* labelLayout = new QHBoxLayout;
        labelLayout->addStretch(1);
        labelLayout->addWidget(webcamList,10);
        labelLayout->addStretch(1);
        mainLayout->addLayout(labelLayout,10);
    }

    mainLayout->addLayout(footerLayout,1);

    setLayout(mainLayout);

    emit TA_Server::getInstance()->showAllAreasNoChange();
}

/*****************************************************************************/
/******                             SLOTS                               ******/
/*****************************************************************************/

/*
    Cancel action and go back to the previous screen (home screen)
*/
void TA_InfoArea::cancel(){
    TA_ServerMainWidget::getInstance()->openHomeScreen();
}
