/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 24 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_ServerNameForm class asking for a  |
  | name and a password for the server                                      |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QLayout>
#include <QFormLayout>
#include <QLabel>
#include <QPushButton>
#include <QCheckBox>
#include <QLineEdit>

// Local Includes
#include "entity/ta_server.h"
#include "gui/server/ta_servermainwidget.h"
#include "gui/ta_mainwindow.h"
#include "gui/ta_fonts.h"
#include "ta_servernameform.h"

/*
    Instanciates this class with default values
        @param name : the initial name
        @param password : the password of the server
*/
TA_ServerNameForm::TA_ServerNameForm(QString name, QString password) : QWidget() {
    QVBoxLayout *mainLayout = new QVBoxLayout();

    //Header
    QHBoxLayout *headerLayout = new QHBoxLayout();
    QLabel *titleLabel = new QLabel(TA_SERVERNAMEFORM_TITLE);
    titleLabel->setFont(TA_Fonts::getTitleFont());
    QPushButton *helpButton = new QPushButton(QIcon(TA_SERVERNAMEFORM_HELP_BUTTON_ICON), QString());
    helpButton->setToolTip(QString(TA_SERVERNAMEFORM_HELP_BUTTON_TOOL_TIP));
    helpButton->setFont(TA_Fonts::getButtonFont());
    headerLayout->addWidget(titleLabel, 1, Qt::AlignHCenter| Qt::AlignTop);
    headerLayout->addWidget(helpButton, 0, Qt::AlignRight| Qt::AlignTop);
    connect(helpButton,SIGNAL(clicked()), TA_MainWindow::getInstance(), SLOT(help()));
    mainLayout->addLayout(headerLayout);
    mainLayout->addStretch(1);

    //Form layout
    QFormLayout *formLayout = new QFormLayout();

    // Name
    nameLineEdit = new QLineEdit(name);
    nameLineEdit->setFont(TA_Fonts::getDefaultFont());
    nameLineEdit->setMaxLength(200);
    connect(nameLineEdit,SIGNAL(editingFinished()),this,SLOT(checkName()));
    QLabel* nameLabel = new QLabel(TA_SERVERNAMEFORM_NAME_LABEL);
    nameLabel->setFont(TA_Fonts::getDefaultFont());
    formLayout->addRow(nameLabel, nameLineEdit);
    QCheckBox* activePassword = new QCheckBox;
    activePassword->setFont(TA_Fonts::getDefaultFont());
    activePassword->setText(TA_SERVERNAMEFORM_ACTIVE_PASSWORD_LABEL);
    activePassword->setMinimumWidth(200);
    connect(activePassword,SIGNAL(toggled(bool)),this,SLOT(changePasswordState(bool)));
    formLayout->addWidget(activePassword);

    // Password
    passwordLineEdit = new QLineEdit(password);
    passwordLineEdit->setFont(TA_Fonts::getDefaultFont());
    passwordLineEdit->setMaxLength(200);
    passwordLineEdit->setEchoMode(QLineEdit::Password);
    passwordLineEdit->setDisabled(true);
    QLabel* motDePasseLabel = new QLabel(TA_SERVERNAMEFORM_PASSWORD_LABEL);
    motDePasseLabel->setFont(TA_Fonts::getDefaultFont());
    formLayout->addRow(motDePasseLabel, passwordLineEdit);
    showPasswordCheckbox = new QCheckBox;
    showPasswordCheckbox->setText(TA_SERVERNAMEFORM_SHOW_PASSWORD_LABEL);
    showPasswordCheckbox->setFont(TA_Fonts::getDefaultFont());
    showPasswordCheckbox->setDisabled(true);
    connect(showPasswordCheckbox,SIGNAL(toggled(bool)),this,SLOT(showPassword(bool)));
    formLayout->addWidget(showPasswordCheckbox);
    QHBoxLayout *centralLayout = new QHBoxLayout();
    centralLayout->addStretch();
    centralLayout->addLayout(formLayout);
    centralLayout->addStretch();
    mainLayout->addLayout(centralLayout);

    //Footer
    QHBoxLayout *footerLayout = new QHBoxLayout();

    QPushButton *returnButton = new QPushButton(TA_SERVERNAMEFORM_RETURN_BUTTON_LABEL);
    returnButton->setFont(TA_Fonts::getButtonFont());
    connect(returnButton, SIGNAL(clicked()), this, SLOT(cancel()));
    footerLayout->addStretch();
    footerLayout->addWidget(returnButton, 0, Qt::AlignTop);

    QPushButton *launchButton = new QPushButton(TA_SERVERNAMEFORM_LAUNCH_BUTTON_LABEL);
    launchButton->setFont(TA_Fonts::getButtonFont());
    connect(launchButton, SIGNAL(clicked()), this, SLOT(launchServer()));
    footerLayout->addStretch();
    footerLayout->addWidget(launchButton, 0, Qt::AlignTop);
    footerLayout->addStretch();
    mainLayout->addStretch(1);
    mainLayout->addLayout(footerLayout);

    setLayout(mainLayout);
}


/*****************************************************************************/
/******                             SLOTS                               ******/
/*****************************************************************************/

/*
    Change the state of the password field
        @param state: true to activate the password, false else
*/
void TA_ServerNameForm::changePasswordState(bool state){
    passwordLineEdit->setEnabled(state);
    passwordLineEdit->setText(QString());
    showPasswordCheckbox->setEnabled(state);
    showPasswordCheckbox->setChecked(false);
}

/*
    Show or not the password
        @param show: true to show the password, false else
*/
void TA_ServerNameForm::showPassword(bool show){
    if(show){
        passwordLineEdit->setEchoMode(QLineEdit::Normal);
    } else {
        passwordLineEdit->setEchoMode(QLineEdit::Password);
    }
}

/*
    Checks the name of the server field
*/
void TA_ServerNameForm::checkName(){
    if(nameLineEdit->text().isNull() || nameLineEdit->text().size() == 0)
        nameLineEdit->setText(QHostInfo::localHostName());
}

/*
    Launch the server with the default configuration
*/
void TA_ServerNameForm::launchServer(){
    TA_SystemEventManager::init();
    TA_Server::initialize(nameLineEdit->text(), passwordLineEdit->text());
    TA_MainWindow::getInstance()->initToolBar();
    TA_ServerMainWidget::getInstance()->openHomeScreen();
}

/*
    Cancel the operation and return to the previous view
*/
void TA_ServerNameForm::cancel() {
    TA_MainWindow::getInstance()->buildMainWidget();
}
