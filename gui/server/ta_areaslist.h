/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 12 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_SystemEventManager namespace       |
  | offering to send events to the system. These events can be get by other |
  | programs                                                                |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Valentin COULON -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/
#ifndef TA_AREASLIST_H
#define TA_AREASLIST_H

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/
#define TA_AREASLIST_GROUP_AREA_TITLE "Groupes de zones"

#define TA_AREASLIST_DELETE_AREA_CONFIRMATION_TITLE  "Confirmation de suppression"
#define TA_AREASLIST_DELETE_BUTTON_ICON ":/images/exit.png"
#define TA_AREASLIST_DELETE_AREA_CONFIRMATION_MESSAGE  "Voulez vous vraiment supprimer la zone %1 ?" // arg1 : area name
#define TA_AREASLIST_DELETE_MULTIPLE_AREA_CONFIRMATION_MESSAGE  "Voulez vous vraiment supprimer ces %1 zones ?"
// arg1 : number of areas to delete

#define TA_AREASLIST_NO_WEBCAM_SELECTED_MESSAGE  "Erreur : Veuillez sélectionner au moins une zone."

#define TA_AREASLIST_INFO_BUTTON_ICON ":/images/info.png"
#define TA_AREASLIST_EDIT_BUTTON_ICON ":/images/new.png"


/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QListWidget>


/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
    Class to manage the areas under list format
*/
class TA_AreasList : public QListWidget {
    Q_OBJECT

public:

    /*
        Default builder
            @param inlineButtons : set inline button or not (info, edit, del)
    */
    TA_AreasList(bool inlineButtons = true);

private slots :

    /*
        Remove the specified area from its name.
        @param areaName : the name of the area to remove
        @param validationMessage : if it's true, display a confirmation message in a popup
    */
    void removeArea(QString areaName, bool validationMessage = true);

    /*
        Delete all selected areas
    */
    void deleteAllSelected();

    /*
        Create a group of selected areas
    */
    void createAreaGroup();

    /*
        Display the informations about an area
            @param areaName : the area's name
            @param inlineButtons : set inline button or not (info, edit, del)
    */
    void launchInfoArea(QString areaName);

    /*
        Display only areas passed in parameter. Also the areas are selected
            @param areas : areas to display
    */
    void selectAndShowAreaList(QList<QString> areas);

    /*
        Display all the areas and unselect them
    */
    void showAll();

    /*
        Display all the areas but don't change the selection
    */
    void showAllNoChange();

    /*
        Unselect all the areas
    */
    void unselectAll();

    /*
        Select an area
            @param area: area's name
    */
    void selectArea(QString area);

    /*
        Emit a AreaSelectionChanged signal on the TA_Server
    */
    void emitAreaSelectionChanged();

private :

    /*
        Add an area from its name
        @param areaName : the name of the area to add
    */
    void addArea(QString areaName, bool inlineButtons);

    /*
        Delete the name of the areas group if it is the latest in the list
    */
    void deleteGroupAreaTitleIfIsLast();

    QListWidgetItem* groupAreaTitle = NULL;
    QMap<QListWidgetItem*, QString> items_areaNamesMap;
    QMap<QString, QListWidgetItem*> areaNames_itemsMap;
};

#endif // TA_AREASLIST_H
