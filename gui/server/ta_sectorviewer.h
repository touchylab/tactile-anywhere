/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 23 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_SectorViewer class drawing    |
  | the sectors of a webcam                                                 |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_SECTORVIEWER_H
#define TA_SECTORVIEWER_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QWidget>
#include <QPen>
#include <QImage>

// Local includes
#include "entity/ta_server.h"

//Fwd
class QPen;
class QPixmap;
class TA_InfoClient;

/*****************************************************************************/
/******                         STRUCTS                                 ******/
/*****************************************************************************/

struct DrawableArea {
    QString areaName; // the name of the area
    QVector<QPointF>* points;     // points for area outline using floating position for scale
    QColor color;     // color for area
};

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
    TA_SectorViewer represent a derived QPainter to draw
    the area outlines of a webcam
*/
class TA_SectorViewer : public QWidget{
    Q_OBJECT

    public:
        /*
            Create an instance of TA_SectorViewer
                @param client : the client owning the webcam
                @param webcamId: the webcam's id
                @param area: limit the sector's to this area
        */
        TA_SectorViewer(TA_InfoClient* client, int webcamId, QString area = QString());

        /*
            Destroy properly this class
        */
        ~TA_SectorViewer();

        /*
            Change the frame concerned by the area outline
                @param frame : frame of webcam
        */
        void setCurrentFrame(QImage frame);

        /*
            Get the areas defining an area (normal or group)
                @param area: the area's name
                @return : the list of area representing the area
        */
        static QList<QString> getNormalAreas(QString area);

        /*
            Paint the image and the Sectors on the painters
                @param painter : painter to use
                @param size : the size to use to paint
        */
        void paintImageAndSectors(QPainter *painter, QSize size);

    signals:
        /*
            Signal emit when clicked
        */
        void clicked();

        /*
            Signal emit when important update
        */
        void importantUpdate();

    protected:
        /*
            Event to paint the entire surface of QPainter, draw
            points, background and outline
        */
        void paintEvent(QPaintEvent *);

        /*
            Load the points list and draw them on screen
                @param areaName : area name to use
                @param outline : the points vector to draw
                @param color : the color to use
        */
        void addOutlineArea(QString areaName, QVector<QPoint>& outline, QColor color);

        /*
            Draw a shape from a painter component
            @param painter : the painter object to draw the shape
            @param size : size of the painter
        */
        void drawShape(QPainter *painter, QSize size);

    private slots :

        /*
            Change the value of visibleAreas
            @param visibleAreas : the new visibleAreas
        */
        void setVisibleAreas(QList<QString> visibleAreas);

    private :

        /*
            Emit signal when clicked
            @param Not used
        */
        void mousePressEvent(QMouseEvent*);

        WebcamStruct* webcam; // Webcam's informations
        QString area; // area's name

        QVector<DrawableArea*> drawableList;     // list of drawable area
        QList<QString> visibleAreas;     // list of area visible
        QPixmap currentFrame;            // the current background frame

        QPen penCircle;
        QPen penLine;
        int frameSide;
};

#endif // TA_SECTORVIEWER_H
