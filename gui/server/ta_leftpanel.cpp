/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 19 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_LeftPanelArea to display a widget  |
  | at left side                                                            |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Remi KEOPHILA -- Initial version                              |
  | [-] 1.1 - Sébastien DESTABEAU -- Correctives adds                       |
  | [-] 1.2 - Valentin COULON -- Version complete for Tactile Anywhere 1.0  |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Local Includes
#include "ta_areatab.h"
#include "ta_clienttab.h"
#include "ta_leftpanel.h"

/*
    Default builder
 */
TA_LeftPanel::TA_LeftPanel() : QTabWidget() {
    this->addTab(new TA_AreaTab(), TA_LEFTPANEL_AREA_TAB_TEXT);
    this->addTab(new TA_ClientTab(), TA_LEFTPANEL_CLIENT_TAB_TEXT);
}
