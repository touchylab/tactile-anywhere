/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 10 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_SystemEventManager namespace       |
  | offering to send events to the system. These events can be get by other |
  | programs                                                                |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Valentin COULON -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QHBoxLayout>

//Local includes
#include "entity/ta_server.h"
#include "gui/server/ta_homewidget.h"
#include "gui/server/ta_detectionrunning.h"
#include "gui/server/ta_leftpanel.h"
#include "gui/server/ta_createarea.h"
#include "gui/server/ta_webcamschoice.h"
#include "gui/server/ta_drawarea.h"
#include "gui/server/ta_manyinfoswebcam.h"
#include "gui/server/ta_infoclientwidget.h"
#include "gui/server/ta_servernameform.h"
#include "gui/server/ta_options.h"
#include "gui/server/ta_infoarea.h"
#include "gui/server/ta_drawleftpanel.h"
#include "gui/ta_mainwindow.h"
#include "gui/ta_statusbar.h"
#include "ta_servermainwidget.h"

/*
    Define the instance of class as singleton.
*/
TA_ServerMainWidget* TA_ServerMainWidget::singleton = NULL;


/*****************************************************************************/
/******                        INITIALISATION                           ******/
/*****************************************************************************/

/*
    Initializes an instance of this class as a singleton
*/
void TA_ServerMainWidget::initialize() {
    TA_ServerMainWidget::singleton = new TA_ServerMainWidget();
    TA_ServerMainWidget::singleton->openNameForm();
}

/*
    Gets the unique instance of this class. initialize() must be called before
    @return : the unique instance of this class or NULL if this class is not initialized
*/
TA_ServerMainWidget *TA_ServerMainWidget::getInstance() {
    return TA_ServerMainWidget::singleton;
}

/*
    Private builder of the class
*/
TA_ServerMainWidget::TA_ServerMainWidget() : QWidget() {
    mainLayout = new QHBoxLayout;
    setLayout(mainLayout);
}


/*****************************************************************************/
/******                         PUBLIC SLOTS                            ******/
/*****************************************************************************/

/*
    open the Name Form
*/
void TA_ServerMainWidget::openNameForm() {
    TA_MainWindow::getInstance()->setHelp(TA_SERVERMAINWIDGET_NAME_FORM_HELP_FILE);
    delWidget(&leftWidget);
    delWidget(&rightWidget);
    setCenterWidget(new TA_ServerNameForm);
}

/*
    Open the Home Screen
        @param forceLeftPanel : set as true to force a new left panel, false else
*/
void TA_ServerMainWidget::openHomeScreen(bool forceLeftPanel) {
    TA_MainWindow::getInstance()->setLimitedToolBar(false,true);
    TA_Server::getInstance()->stopFrame();
    TA_MainWindow::getInstance()->setHelp(TA_SERVERMAINWIDGET_HOME_HELP_FILE);
    if(forceLeftPanel || leftWidget == NULL) {
        setLeftWidget(new TA_LeftPanel);
    }
    setCenterWidget(new TA_HomeWidget);
    delWidget(&rightWidget);
}

/*
    open the Options
*/
void TA_ServerMainWidget::openOptions() {
    TA_MainWindow::getInstance()->setLimitedToolBar(false,false);
    TA_MainWindow::getInstance()->setHelp(TA_SERVERMAINWIDGET_OPTIONS_HELP_FILE);
    delWidget(&leftWidget);
    delWidget(&rightWidget);
    setCenterWidget(new TA_Options);
}

/*
    Load the creation of area GUI
    @param isGroupArea : defining if it's an area group or not
    @param children : a list of children areas
*/
void TA_ServerMainWidget::openCreateArea(bool isGroupArea, QList<QString> children) {
    TA_Server::getInstance()->stopFrame();
    openEditArea("", isGroupArea, children);
}

/*
    Load the widget screen allowing to edit to an area
    @param areaName : the name of the choosen area
    @param isGroupArea : defining if it's an area group or not
    @param children : a list of children areas
*/
void TA_ServerMainWidget::openEditArea(QString areaName, bool isGroupArea, QList<QString> children) {
    TA_Server::getInstance()->stopFrame();
    TA_MainWindow::getInstance()->setLimitedToolBar(true,false);

    if(areaName != ""){
        isGroupArea = TA_Server::getInstance()->getAreaInfo(areaName)->type == GROUP_AREA;
    }

    if(isGroupArea){
        TA_MainWindow::getInstance()->setHelp(TA_SERVERMAINWIDGET_ADD_GROUP_AREA_HELP_FILE);
    } else {
        TA_MainWindow::getInstance()->setHelp(TA_SERVERMAINWIDGET_ADD_AREA_HELP_FILE);
    }

    setCenterWidget(new TA_CreateArea(areaName, isGroupArea, children));
    delWidget(&leftWidget);
    delWidget(&rightWidget);
}

/*
    Load the widget screen allowing to add webcams to an area
    @param area : the area's information
*/
void TA_ServerMainWidget::openAddWebcamsToArea(TempAreaStruct area) {
    TA_Server::getInstance()->stopFrame();
    TA_MainWindow::getInstance()->setLimitedToolBar(true,false);
    TA_MainWindow::getInstance()->setHelp(TA_SERVERMAINWIDGET_WEBCAM_CHOICE_HELP_FILE);
    setCenterWidget(new TA_WebcamsChoice(area));
    delWidget(&leftWidget);
    delWidget(&rightWidget);
}

/*
    Load the draw area screen for a specified area name
    @param area : the area's information
*/
void TA_ServerMainWidget::openDrawArea(TempAreaStruct area) {
    TA_Server::getInstance()->stopFrame();
    TA_MainWindow::getInstance()->setLimitedToolBar(true,false);
    TA_MainWindow::getInstance()->setHelp(TA_SERVERMAINWIDGET_DRAW_AREA_HELP_FILE);
    setLeftWidget(new TA_DrawLeftPanel());
    setCenterWidget(new TA_DrawArea(area));
    delWidget(&rightWidget);
}

/*
    Load the detection running screen
*/
void TA_ServerMainWidget::openDetectionRunningScreen() {
    TA_Server::getInstance()->stopFrame();
    TA_MainWindow::getInstance()->setLimitedToolBar(true, true);
    TA_MainWindow::getInstance()->setHelp(TA_SERVERMAINWIDGET_DETECTION_RUNNING_HELP_FILE);
    setCenterWidget(new TA_DetectionRunning());
    delWidget(&leftWidget);
    delWidget(&rightWidget);
}

/*
    Load the center widget with the informations about a choosen webcam.
    @param infoClient : the client informations
    @param webcamId : the webcam identifier
*/
void TA_ServerMainWidget::openManyInformationsWebcam(TA_InfoClient* infoClient, int webcamId) {
    TA_Server::getInstance()->stopFrame();
    TA_MainWindow::getInstance()->setLimitedToolBar(false,false);
    TA_MainWindow::getInstance()->setHelp(TA_SERVERMAINWIDGET_MANY_INFO_WEBCAM_HELP_FILE);
    setCenterWidget(new TA_ManyInfosWebcam(infoClient, webcamId));
}

/*
    Load the center widget with the informations about a choosen area
        @param areaName : the area's name
*/
void TA_ServerMainWidget::openInfoArea(QString areaName){
    TA_Server::getInstance()->stopFrame();
    TA_MainWindow::getInstance()->setLimitedToolBar(false,false);
    TA_MainWindow::getInstance()->setHelp(TA_SERVERMAINWIDGET_INFO_AREA_HELP_FILE);
    setCenterWidget(new TA_InfoArea(areaName));
}

#include <QDebug>

/*
    Load the center widget with the informations about a choosen client
        @param infoClient : the client to display
*/
void TA_ServerMainWidget::openInfoClient(TA_InfoClient* infoClient){
    TA_Server::getInstance()->stopFrame();
    TA_MainWindow::getInstance()->setLimitedToolBar(false,false);
    TA_MainWindow::getInstance()->setHelp(TA_SERVERMAINWIDGET_INFO_CLIENT_HELP_FILE);
    setCenterWidget(new TA_InfoClientWidget(infoClient));
}

/*****************************************************************************/
/******                      PRIVATE METHODS                            ******/
/*****************************************************************************/

/*
    Remove the specified widget out of the main layout
    @param widget : the widget to remove
*/
void TA_ServerMainWidget::delWidget(QWidget **widget) {
    if (*widget != NULL) {
        delete *widget;
        *widget = NULL;
    }
}

/*
    Set left widget removing old layout and create new layout
    containing widget
    @param : the widget to integrated in center of window
*/
void TA_ServerMainWidget::setLeftWidget(QWidget *widget) {
    delWidget(&leftWidget);
    leftWidget = widget;
    mainLayout->insertWidget(0, widget);
}

/*
    Set center widget removing old layout and create new layout
    containing widget
    @param : the widget to integrated in center of window
*/
void TA_ServerMainWidget::setCenterWidget(QWidget *widget){
    TA_MainWindow::getInstance()->getStatusBar()->clearMessage();
    delWidget(&centerWidget);
    centerWidget = widget;
    mainLayout->insertWidget(1, widget, 1);
}

/*
    Set right widget removing old layout and create new layout
    containing widget
    @param : the widget to integrated in right of window
*/
void TA_ServerMainWidget::setRightWidget(QWidget *widget){
    delWidget(&rightWidget);
    rightWidget = widget;
    mainLayout->insertWidget(2, widget);
}

