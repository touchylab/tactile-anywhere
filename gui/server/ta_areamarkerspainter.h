/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 24 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_AreaMarkersPainter class      |
  | drawing and editing a marker sector                                     |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_AREAMARKERSPAINTER_H
#define TA_AREAMARKERSPAINTER_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QWidget>
#include <QPen>

//Local includes
#include "entity/ta_server.h"
#include "ta_sectorviewer.h"

//Fwd
class TA_InfoClient;


/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
    TA_AreaMarkersPainter represent a derived QPainter to draw
    the area outline and listen mouse events.
*/
class TA_AreaMarkersPainter : public QWidget{
    Q_OBJECT

    public:
        /*
            Create an instance of TA_AreaMarkersPainter
                @param webcam : the webcam used to defining the sector
                @param color : the color to use for pen
                @param markerPath: the path of markers to use to initialize this instance
                @param unique : set as true if an unique marker have to be selected, false if it is a path of markers
        */
        TA_AreaMarkersPainter(WebcamStruct* webcam, QColor color, QList<int>* markerPath, bool unique);

        /*
            Create an instance of TA_AreaMarkersPainter
                 @param color : the color to use for pen
        */
        TA_AreaMarkersPainter(QColor color);
        /*
            Destroy properly this class
        */
        ~TA_AreaMarkersPainter();

        /*
            Get the marker path
                @return: the marker path
        */
        QList<int> *getMarkerPath();

        /*
            Change the frame concerned by the area outline
        */
        void updateImage();

        /*
            Load the marker path and the webcam informations and draw it selected on screen
                @param webcam : the webcam's informations
                @param path : the marker path
                @param unique : set as true if an unique marker have to be selected, false if it is a path of markers
        */
        void setWebcamAndPath(WebcamStruct* webcam, QList<int> *path, bool unique);

    signals:
        /*
            Send when a new marker is selected
        */
        void newSelectedMarker();

    private slots:

        /*
            Refresh the markers positions
                @param client : the client informations
                @param webcamId : the id of the webcam
        */
        void newMarkers(TA_InfoClient* client, int webcamId);

    protected:
        /*
            Event to paint the entire surface of QPainter, draw
            points, background and outline
        */
        void paintEvent(QPaintEvent *);

        /*
            Manage the mouse press event for create and edit points
                @param event : event emited
        */
        void mousePressEvent(QMouseEvent *event);

        /*
            Manage the mouse move event for create and edit points
                @param event : event emited
        */
        void mouseMoveEvent(QMouseEvent *event);

        /*
            Return index of the marker to select or -1 if it doesn't exist
                @param point : point to compare with marker's position
        */
        int idOfMarkerToSelect(QPointF point);

        /*
            Draw a shape from a painter component
            @param painter : the painter object to draw the shape
        */
        void drawShape(QPainter *painter);

    private:
        /*
            Initialize the class
        */
        void init(QColor color);

        // Marker is clicked if the mouse is at 10 pixels around the marker
        static const int X_MARKER_CLICKED = 10;
        static const int Y_MARKER_CLICKED = 10;

        bool unique;         // unique marker to select or not
        bool shapeComplete;  // shape close

        QList<int> *markersPath;        // markers path
        QPointF mousePoint;             // the mouse position

        WebcamStruct* webcam; // The webcam concerned by this definition
        TA_SectorViewer* sectorViewer; //the background used for paint

        QPen penSquare; //QPen used to print squares
        QPen penLine; // QPen used to print lines
};

#endif // TA_AREAMARKERSPAINTER_H
