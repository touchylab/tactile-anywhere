/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 12 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_DataBeacon class containing   |
  | the informations about the client and the server coordinates            |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu LOSLIER -- Initial version                            |
  | [-] 1.1 - Valentin COULON -- Add Group area and refactoring             |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_CREATEAREA_H
#define TA_CREATEAREA_H

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/

#define TA_CREATEAREA_CREATE_TITLE "Créer une zone"
#define TA_CREATEAREA_CREATE_GROUP_TITLE "Créer un groupe de zones"
#define TA_CREATEAREA_EDIT_TITLE "Editer une zone"
#define TA_CREATEAREA_EDIT_GROUP_TITLE "Editer un groupe de zones"

#define TA_CREATEAREA_NAME_LABEL "Nom de la zone : "
#define TA_CREATEAREA_NAME_PLACEHOLDER "50 caractères maximum"
#define TA_CREATEAREA_NAME_VALIDATION_REGEXP "[A-Za-z0-9&éèçàâêîôû _]{1,50}"
#define TA_CREATEAREA_NAME_INVALID_ERROR "Nom invalide. Veuillez saisir un nom valide (1 à 50 caractères: A-Z a-z 0-9 éèçàâêîôû _)"
#define TA_CREATEAREA_NAME_NOT_UNIQUE_ERROR "Ce nom est déjà utilisé, veuillez utiliser un nom unique"

#define TA_CREATEAREA_COLOR_LABEL "Couleur : "
#define TA_CREATEAREA_COLOR_PANEL_TITLE "Sélectionnez une couleur"

#define TA_CREATEAREA_CHILDREN_LABEL "Zones : "
#define TA_CREATEAREA_CHILDREN_SELECTED_LABEL "Sélectionnées"
#define TA_CREATEAREA_CHILDREN_UNSELECTED_LABEL "Non Sélectionnées"

#define TA_CREATEAREA_ACTION_LABEL "Actions possibles à exécuter au touché de la zone :"

#define TA_CREATEAREA_URL_LABEL "Ouvrir une page Web : "
#define TA_CREATEAREA_URL_PLACEHOLDER "Adresse de la page Web"

#define TA_CREATEAREA_FILE_LABEL "Ouvrir un fichier : "
#define TA_CREATEAREA_FILE_PLACEHOLDER "Chemin du fichier"

#define TA_CREATEAREA_COMMAND_LINE_LABEL "Lancer une commande système : "
#define TA_CREATEAREA_COMMAND_LINE_PLACEHOLDER "Commande système à lancer"


#define TA_CREATEAREA_CANCEL_BUTTON_TEXT "Annuler"
#define TA_CREATEAREA_SAVE_BUTTON_TEXT "Enregistrer et quitter"
#define TA_CREATEAREA_CONTINUE_BUTTON_TEXT "Continuer"

#define TA_CREATEAREA_ERROR_LINE_EDIT_CSS "QLineEdit{border: 1px solid rgba(255,0,0,150);}"

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QWidget>

//Fwd
class QListWidgetItem;
class QListWidget;
class QLineEdit;
class QLabel;
class TA_ServerMainWidget;


/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
    Class to manage the GUI for creating or editing area
*/
class TA_CreateArea : public QWidget {
    Q_OBJECT

    public:
        /*
            Default Constructor
                @param areaName : name of the area
                @param isGroupArea : true if the area is a group area
                @param children : list of children if it is a group area
        */
        TA_CreateArea(QString areaName, bool isGroupArea = false, QList<QString> children = QList<QString>());


    private slots:
        /*
            Set the color of the panel
        */
        void setColor();

        /*
            Save the area and quit
        */
        void saveArea();

        /*
            Select if actions are displayed
            @param show : true to display actions, false to hise
        */
        void displayActions(bool show);

        /*
            Cancel the creation
        */
        void cancel();

        /*
            Continue the creation
        */
        void continueCreation();

    private:

        /*
            check if the confiuration is valid
            @return : true, configuration valid, false, configuration invalid
        */
        bool validConfiguration();

        /*
            Create a dropList with the children
            @param children : list of children to include in the dropList
            @return : QWidget created
        */
        QWidget* createChildrenSelection(QList<QString> children);

        /*
            Create a QListWidgetItem item for an area child
            @param areaName : the name of the area child
            @return : the QListWidgetItem created
        */
        QListWidgetItem* createChildItem(QString areaName);

        /*
            Get the list of children setelected
            @return : QList<String> children
        */
        QList<QString> getChildren();

        /*
            Get the list of area who can't be selected like child (like the parents or the area himself)
            @return : QList<String> unselectable areas
        */
        QList<QString> getUnselectableAreas();

        QString oldAreaName;
        QLineEdit *nameLineEdit;

        QWidget* actionsWidget;
        QLineEdit *urlLineEdit;
        QLineEdit *fileLineEdit;
        QLineEdit *commandLineEdit;

        bool isGroupArea;
        QListWidget *childrenList;

        QLabel *colorLabel;
        QPalette colorPalette;
        QColor color;


};

#endif // TA_CREATEAREA_H
