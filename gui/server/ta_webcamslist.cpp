/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 12 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_WebcamList offering a widget to    |
  | manage client's webcams under list form.                                |                                                                        |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Valentin COULON -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QToolButton>
#include <QLabel>
#include <QMessageBox>
#include <QHBoxLayout>
#include <QSignalMapper>
#include <QListWidgetItem>

//Local includes
#include "gui/ta_fonts.h"
#include "communication/server/ta_infoclient.h"
#include "ta_webcamslist.h"
#include "gui/server/ta_servermainwidget.h"
#include "gui/ta_clickablelabel.h"
#include "gui/ta_clickableqwidget.h"

/*****************************************************************************/
/******                           CONSTRUCTOR                           ******/
/*****************************************************************************/

/*
    Default builder to create a webcams list about all clients connected to the server.
*/
TA_WebcamsList::TA_WebcamsList() : QListWidget() {
    ConfigTree* clientsTree = TA_Server::getInstance()->getConfig();
    foreach(TA_InfoClient* infoClient, clientsTree->keys()){
        if(infoClient->isAccepted()){
            addClient(infoClient, ((*clientsTree)[infoClient])->values());
        }
    }
}

/*****************************************************************************/
/******                           OPERATIONS                            ******/
/*****************************************************************************/

/*
    Add a client in the list
        @param infoClient : the client to add
        @param webcams : the webcams owning by the client
*/
void TA_WebcamsList::addClient(TA_InfoClient* infoClient, QList<WebcamStruct*> webcams) {
    clientsList[clientsList.size()] = infoClient;

    // Item
    QListWidgetItem *item = new QListWidgetItem();
    item->setSelected(false);
    item->setFlags(Qt::NoItemFlags);
    addItem(item);

    // Layout
    QHBoxLayout *layout= new QHBoxLayout();

    // Name
    TA_ClickableLabel *label = new TA_ClickableLabel(infoClient, NULL);
    label->setText(infoClient->getClientName());
    QFont font = TA_Fonts::getDefaultFont();
    font.setBold(true);
    font.setPixelSize(18);
    label->setFont(font);
    layout->addWidget(label,0,Qt::AlignLeft);
    connect(label, SIGNAL(clicked(TA_InfoClient*, int)), this, SLOT(clickClient(TA_InfoClient*)));

    if(!infoClient->isLocal()){
        // Disconnected
        QLabel *disconnectedLabel = new QLabel();
        if (infoClient->getClientState() == DISCONNECTED) {
            disconnectedLabel->setPixmap(QPixmap(TA_WEBCAMSLIST_DISCONNECT_ICON).scaled(20,20));
        } else {
            disconnectedLabel->setPixmap(QPixmap(TA_WEBCAMSLIST_UNDISCONNECTED_ICON).scaled(20,20));
        }
        layout->addWidget(disconnectedLabel,0,Qt::AlignRight);

        // Close
        QToolButton *delButton = new QToolButton();
        delButton->setIcon(QIcon(TA_WEBCAMSLIST_DELETE_BUTTON_ICON));
        delButton->setToolTip(TA_WEBCAMSLIST_DELETE_BUTTON_TOOL_TIP);
        QSignalMapper* delMapper = new QSignalMapper();
        connect(delButton, SIGNAL(clicked()), delMapper, SLOT(map()));
        delMapper->setMapping(delButton, clientsList.size()-1);
        connect(delMapper, SIGNAL(mapped(int)), this, SLOT(closeClient(int)));
        layout->addWidget(delButton,0,Qt::AlignRight);
    }

    // Webcams
    foreach(WebcamStruct* webcam, webcams){
        addWebcam(infoClient, webcam);
    }

    // Add widget to item
    TA_ClickableQWidget* clicWidget = new TA_ClickableQWidget;
    clicWidget->setLayout(layout);
    clicWidget->setAttribute(Qt::WA_MouseTracking,false);
    connect(clicWidget,SIGNAL(clicked()),label,SLOT(mouseReleaseEvent()));
    item->setSizeHint(clicWidget->sizeHint());
    setItemWidget(item, clicWidget);
}

/*
    Add a webcam in the list
        @param infoClient: the client owning the webcam to add
        @param webcamId: the concerned webcam's id
*/
void TA_WebcamsList::addWebcam(TA_InfoClient* infoClient, WebcamStruct* webcam) {
    webcamsList[webcamsList.size()] = webcam;

    // Item
    QListWidgetItem *item = new QListWidgetItem();
    item->setBackgroundColor(Qt::white);
    item->setFlags(Qt::NoItemFlags);
    addItem(item);

    // Title
    TA_ClickableLabel *label = new TA_ClickableLabel(infoClient, webcam);
    label->setFont(TA_Fonts::getDefaultFont());
    QString title = QString(TA_WEBCAMSLIST_TITLE).arg(webcam->name);
    title = QFontMetrics(label->font()).elidedText(title, Qt::ElideRight, 170);
    title = title.replace("…", " ");
    label->setText(title);
    connect(label, SIGNAL(clicked(TA_InfoClient*, int)), this, SLOT(clickLabel(TA_InfoClient*, int)));

    // Disconnected
    QLabel *disconnectedLabel = new QLabel();
    if(infoClient->getClientState() == DISCONNECTED || !webcam->active) {
        disconnectedLabel->setPixmap(QPixmap(TA_WEBCAMSLIST_DISCONNECT_ICON).scaled(20,20));
    } else {
        disconnectedLabel->setPixmap(QPixmap(TA_WEBCAMSLIST_UNDISCONNECTED_ICON).scaled(20,20));
    }
    // Show button
    QToolButton *showButton = new QToolButton();
    if(webcam->isDisplayed){
        showButton->setIcon(QIcon(TA_WEBCAMSLIST_SHOW_BUTTON_ICON));
        showButton->setToolTip(TA_WEBCAMSLIST_SHOW_BUTTON_TOOL_TIP);
    } else {
        showButton->setIcon(QIcon(TA_WEBCAMSLIST_NOT_SHOW_BUTTON_ICON));
        showButton->setToolTip(TA_WEBCAMSLIST_NOT_SHOW_BUTTON_TOOL_TIP);
    }
    QSignalMapper* showMapper = new QSignalMapper();
    connect(showButton, SIGNAL(clicked()), showMapper, SLOT(map()));
    showMapper->setMapping(showButton, webcamsList.size()-1);
    connect(showMapper, SIGNAL(mapped(int)), this, SLOT(changeDisplayWebcam(int)));

    QHBoxLayout *layout= new QHBoxLayout();
    layout->addWidget(label,0,Qt::AlignLeft);
    layout->addWidget(disconnectedLabel,0,Qt::AlignRight);
    layout->addWidget(showButton,0,Qt::AlignRight);
    TA_ClickableQWidget* clicWidget = new TA_ClickableQWidget;
    connect(clicWidget,SIGNAL(clicked()),label,SLOT(mouseReleaseEvent()));
    clicWidget->setLayout(layout);
    item->setSizeHint(clicWidget->sizeHint());
    setItemWidget(item, clicWidget);
}

/*****************************************************************************/
/******                           SLOTS                                 ******/
/*****************************************************************************/

/*
    Opens informations about a webcam
        @param infoClient: the client owning the concerned webcam
        @param webcamId: the concerned webcam's id
*/
void TA_WebcamsList::clickLabel(TA_InfoClient* infoClient, int webcamId){
    TA_ServerMainWidget::getInstance()->openManyInformationsWebcam(infoClient, webcamId);
}

/*
    Opens informations about a client
        @param infoClient: the clicked client
*/
void TA_WebcamsList::clickClient(TA_InfoClient* infoClient){
    TA_ServerMainWidget::getInstance()->openInfoClient(infoClient);
}

/*
    Close the given client
        @param idClient: the id to get the client
*/
void TA_WebcamsList::closeClient(int idClient){
    if (clientsList.contains(idClient)) {
        TA_Server::getInstance()->closeClient(clientsList[idClient]);
    }
}

/*
    Change the displayed mode of the given webcam
        @param idWebcam: the id to get the webcam
*/
void TA_WebcamsList::changeDisplayWebcam(int idWebcam){
    if (!webcamsList.contains(idWebcam)) {
        return;
    }
    TA_Server::getInstance()->changeDisplayModeWebcam(webcamsList[idWebcam]->client,
                                                      webcamsList[idWebcam]->id,!webcamsList[idWebcam]->isDisplayed);
}
