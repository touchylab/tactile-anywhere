/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 13 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_WebcamsChoice to display widget    |
  | to choose webcams about an area                                         |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Sébastien DESTABEAU -- Initial version                        |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                          STRUCT                                 ******/
/*****************************************************************************/

//Qt
#include <QLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QCheckBox>
#include <QMovie>

//Local includes
#include "gui/server/ta_servermainwidget.h"
#include "gui/ta_flowlayout.h"
#include "gui/ta_mainwindow.h"
#include "gui/ta_fonts.h"
#include "ta_webcamschoice.h"
#include "communication/server/ta_infoclient.h"

/*
    Builder with an area name
    @param areaName : the area name
*/
TA_WebcamsChoice::TA_WebcamsChoice(TempAreaStruct area) : QWidget(){
    sideFrame = TA_MainWindow::getInstance()->getScreenSize().width()/5;

    this->area = area;
    nbChecked = 0;

    flowLayout = new TA_FlowLayout();

    QWidget *mainWidget= new QWidget();
    mainWidget->setLayout(flowLayout);

    QScrollArea *scrollArea = new QScrollArea();
    scrollArea->setWidgetResizable(true);
    scrollArea->setWidget(mainWidget);

    QVBoxLayout *mainLayout = new QVBoxLayout();
    QLabel* titleLabel = new QLabel(QString(TA_WEBCAMSCHOICE_TITLE).arg(area.name));
    titleLabel->setFont(TA_Fonts::getTitleFont());
    //Header
    mainLayout->addWidget(titleLabel, 0, Qt::AlignHCenter | Qt::AlignTop);
    //Webcams
    mainLayout->addWidget(scrollArea);

    setLayout(mainLayout);

    // List the defined webcams
    if(area.oldname != ""){
        foreach(StateSectorStruct* sector, TA_Server::getInstance()->getAreaInfo(area.oldname)->sectors){
            WebcamStruct* webcam = TA_Server::getInstance()->getWebcamInfo(sector->client,sector->webcamId);
            defined[webcam] = webcam;
        }
    }

    //Creates the cells
    ConfigTree* clientsTree = TA_Server::getInstance()->getConfig();

    foreach(TA_InfoClient* infoClient, clientsTree->keys()){
        foreach(int webcamId, ((*clientsTree)[infoClient])->keys()){
            addCell(infoClient,webcamId);
        }
    }

    //Footer
    QHBoxLayout* footerLayout = new QHBoxLayout();
    QHBoxLayout* buttonLayout = new QHBoxLayout();
    QPushButton* returnButton = new QPushButton(TA_WEBCAMSCHOICE_RETURN_BUTTON_LABEL);
    returnButton->setFont(TA_Fonts::getButtonFont());
    addWebcamsButton = new QPushButton(TA_WEBCAMSCHOICE_ADD_WEBCAM_BUTTON_LABEL);
    addWebcamsButton->setFont(TA_Fonts::getButtonFont());
    addWebcamsButton->setEnabled(nbChecked);
    buttonLayout->addStretch();
    buttonLayout->addWidget(returnButton,0,Qt::AlignTop);
    buttonLayout->addStretch();
    buttonLayout->addWidget(addWebcamsButton,0,Qt::AlignTop);
    buttonLayout->addStretch();
    footerLayout->addLayout(buttonLayout);

    mainLayout->addLayout(footerLayout);

    // Connections
    connect(addWebcamsButton, SIGNAL(pressed()), this, SLOT(addChoosenWebcams()));
    connect(returnButton, SIGNAL(pressed()), this, SLOT(cancel()));
    connect(TA_Server::getInstance(), SIGNAL(changedFrame(TA_InfoClient*,int)), this, SLOT(changedFrame(TA_InfoClient*,int)));
    connect(TA_Server::getInstance(), SIGNAL(changedWebcams(TA_InfoClient*,QVector<int>)), this, SLOT(changedWebcams(TA_InfoClient*,QVector<int>)));
    connect(TA_Server::getInstance(), SIGNAL(changedClient()),this,SLOT(changedClient()));

    // Frames
    TA_Server::getInstance()->askFrame(false);

    setLayout(mainLayout);
    this->setMinimumSize(sideFrame*1.5,sideFrame*1.5);
}

/*****************************************************************************/
/******                          OPERATIONS                             ******/
/*****************************************************************************/

/*
    Add a cell to the widget
        @param client : the client to owning the webcam
        @param webcamId : the webcam's id
*/
void TA_WebcamsChoice::addCell(TA_InfoClient* client, int webcamId){
    WebcamStruct* webcam = TA_Server::getInstance()->getWebcamInfo(client,webcamId);
    webcam->isSelected = false;
    if(!webcam->isDisplayed)   {
        return;
    }

    QVBoxLayout *cellLayout = new QVBoxLayout();

    // top widget and horizontal layout
    QLabel *camTitle = new QLabel(webcam->name);
    camTitle->setFont(TA_Fonts::getSubTitleFont());
    QHBoxLayout* topHLayout = new QHBoxLayout;
    topHLayout->addWidget(camTitle,0,Qt::AlignHCenter);

    QCheckBox* webcamCheckBox = new QCheckBox();
    if(defined.contains(webcam)){
        webcamCheckBox->setChecked(true);
        nbChecked++;
    } else {
        webcamCheckBox->setChecked(false);
    }

    topHLayout->addWidget(webcamCheckBox,0,Qt::AlignRight);
    cellLayout->addLayout(topHLayout);

    connect(webcamCheckBox,SIGNAL(toggled(bool)),this,SLOT(changedCheck(bool)));
    cbwbMap[webcam] = webcamCheckBox;

    // label picture
    QLabel* webcamPictureLabel = new QLabel();
    imgMap[webcam] = webcamPictureLabel;
    webcamPictureLabel->setFixedSize(sideFrame,sideFrame);
    QImage frame = webcam->image;

    if(frame.size() == QSize(0,0) || !webcam->active){
        frame.load(TA_WEBCAMSCHOICE_NOT_AVAILABLE_WEBCAM_ICON);
    }

    webcamPictureLabel->setPixmap(QPixmap::fromImage(frame).scaled(sideFrame,sideFrame));
    cellLayout->addWidget(webcamPictureLabel,1,Qt::AlignCenter);

    TA_ClickableQWidget *clickableQWidget = new TA_ClickableQWidget();
    connect(clickableQWidget, SIGNAL(clicked()), webcamCheckBox, SLOT(click()));
    clickableQWidget->setLayout(cellLayout);
    flowLayout->addWidget(clickableQWidget);
    cellMap[webcam] = clickableQWidget;
    clientMap[webcam] = client;
    idMap[webcam] = webcam->id;
    update();
}

/*
    Remove a cell to the widget
        @param webcam : the webcam structure
*/
void TA_WebcamsChoice::removeCell(WebcamStruct* webcam){
    if(cbwbMap[webcam]->isChecked()){
        changedCheck(false);
    }
    flowLayout->removeWidget(cellMap[webcam]);
    delete cellMap[webcam];
    cbwbMap.remove(webcam);
    imgMap.remove(webcam);
    cellMap.remove(webcam);
    clientMap.remove(webcam);
    idMap.remove(webcam);
    update();
}

/*****************************************************************************/
/******                             SLOTS                               ******/
/*****************************************************************************/

/*
    Add the choosen webcams to the webcams list to manage in detection
*/
void TA_WebcamsChoice::addChoosenWebcams(){
    QMap<WebcamStruct*, QCheckBox*>::ConstIterator ii;
    for(ii = cbwbMap.constBegin(); ii != cbwbMap.constEnd(); ++ii){
        QCheckBox* cb = ii.value();
        WebcamStruct* wb = ii.key();
        wb->isSelected = cb->isChecked();
    }
    TA_ServerMainWidget::getInstance()->openDrawArea(area);
}

/*
  Cancel action and go back to the previous screen
*/
void TA_WebcamsChoice::cancel(){
    TA_ServerMainWidget::getInstance()->openEditArea(area.oldname);
}

/*
  Checks for selected webcams number.
  @param state : the checkbox state
*/
void TA_WebcamsChoice::changedCheck(bool state){
    nbChecked += state?1:-1;
    addWebcamsButton->setEnabled(nbChecked>0);
    update();
}

/*
  Updates the webcams frame
  @param client : the client's webcam
  @param webcamId : the webcam identifier to update frame for
*/
void TA_WebcamsChoice::changedFrame(TA_InfoClient* client, int webcamId){
    if(imgMap.contains(TA_Server::getInstance()->getWebcamInfo(client,webcamId))){
        QPixmap pixmap = QPixmap::fromImage(TA_Server::getInstance()->getWebcamInfo(client,webcamId)->image.scaled(sideFrame,sideFrame));
        imgMap[TA_Server::getInstance()->getWebcamInfo(client,webcamId)]->setPixmap(pixmap);
        update();
    }
}

/*
    Updates the webcams state
        @param client: the client owning the changed webcams
        @param webcams : the changed webcams' id
*/
void TA_WebcamsChoice::changedWebcams(TA_InfoClient* client, QVector<int> webcams){
    TA_DataStream packet;
    packet.setToLaunch(true);
    packet.setUniqueFrame(false);

    foreach(int id, webcams){
        WebcamStruct* webcam = TA_Server::getInstance()->getWebcamInfo(client,id);
        if(imgMap.contains(webcam)){
            if(!webcam->active)
                imgMap[webcam]->setPixmap(QPixmap::fromImage(QImage(TA_WEBCAMSCHOICE_NOT_AVAILABLE_WEBCAM_ICON)).scaled(sideFrame,sideFrame));
        } else {
            addCell(client,id);
            packet.setIdentifier(id);
            client->sendPacket(&packet);
        }
    }
}

/*
    Updates the webcams list
*/
void TA_WebcamsChoice::changedClient(){
    foreach(WebcamStruct* webcam , clientMap.keys()){
        if(!TA_Server::getInstance()->getWebcamInfo(clientMap[webcam],idMap[webcam])){
            removeCell(webcam);
        }
    }
}
