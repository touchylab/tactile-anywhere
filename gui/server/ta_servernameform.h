/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 24 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_ServerNameForm class asking   |
  | for a name and a password for the server                                |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_SERVERNAMEFORM_H
#define TA_SERVERNAMEFORM_H

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/

#define TA_SERVERNAMEFORM_TITLE "Informations"

#define TA_SERVERNAMEFORM_HELP_BUTTON_ICON ":/images/help.png"
#define TA_SERVERNAMEFORM_HELP_BUTTON_TOOL_TIP "L'aide contextuelle apparaît dans \
une fenêtre à droite au moment où vous cliquez sur ce bouton. Pour cacher l'aide, cliquez à nouveau sur ce même bouton."

#define TA_SERVERNAMEFORM_NAME_LABEL "Nom : "

#define TA_SERVERNAMEFORM_ACTIVE_PASSWORD_LABEL "Utiliser un mot de passe"
#define TA_SERVERNAMEFORM_PASSWORD_LABEL "Mot de passe : "
#define TA_SERVERNAMEFORM_SHOW_PASSWORD_LABEL "Afficher"

#define TA_SERVERNAMEFORM_RETURN_BUTTON_LABEL "Retour"

#define TA_SERVERNAMEFORM_LAUNCH_BUTTON_LABEL "Lancer Tactile Anywhere"

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QWidget>
#include <QHostInfo>

//Fwd
class QLineEdit;
class QCheckBox;

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
    Class to manage the GUI for asking server's name and password
*/
class TA_ServerNameForm : public QWidget{
    Q_OBJECT

    public:
        /*
            Instanciates this class with default values
                @param name : the initial name
                @param password : the password of the server
        */
        TA_ServerNameForm(QString name = QHostInfo::localHostName(), QString password = QString(""));

    private slots:
        /*
            Change the state of the password field
                @param state: true to activate the password, false else
        */
        void changePasswordState(bool state);

        /*
            Show or not the password
                @param show: true to show the password, false else
        */
        void showPassword(bool show);

        /*
            Checks the name of the server field
        */
        void checkName();

        /*
            Launch the server with the default configuration
        */
        void launchServer();

        /*
            Cancel the operation and return to the previous view
        */
        void cancel();

    private:
        QLineEdit *nameLineEdit;
        QLineEdit *passwordLineEdit;
        QCheckBox *showPasswordCheckbox;
};

#endif // TA_SERVERNAMEFORM_H
