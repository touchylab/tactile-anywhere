/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 14 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_SystemEventManager namespace       |
  | offering to send events to the system. These events can be get by other |
  | programs                                                                |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu LOSLIER -- Initial version                            |
  | [-] 1.1 - Valentin COULON -- Version complete for Tactile Anywhere 1.0  |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QMessageBox>
#include <QButtonGroup>

//Local includes
#include "ta_areapointspainter.h"
#include "ta_areamarkerspainter.h"
#include "ta_servermainwidget.h"
#include "communication/server/ta_infoclient.h"
#include "detection/webcam/ta_webcam.h"
#include "gui/ta_mainwindow.h"
#include "gui/ta_advancebutton.h"
#include "gui/ta_fonts.h"
#include "gui/ta_statusbar.h"
#include "ta_drawarea.h"

/*
    Create an instance of TA_DrawArea
        @param area : the area's information
*/
TA_DrawArea::TA_DrawArea(TempAreaStruct area) : QWidget() , it_map(map){
    this->area = area;
    this->initialization = true;
    QVBoxLayout *mainLayout = new QVBoxLayout();

    //Header
    QLabel* titleLabel = new QLabel(QString(TA_DRAWAREA_TITLE).arg(area.name));
    titleLabel->setFont(TA_Fonts::getTitleFont());
    mainLayout->addWidget(titleLabel, 0, Qt::AlignHCenter | Qt::AlignTop);

    //Advanced mode
    advanceButton = new TA_AdvanceButton();
    mainLayout->addWidget(advanceButton, 0, Qt::AlignHCenter | Qt::AlignTop);
    connect(advanceButton, SIGNAL(toggled(bool)), this, SLOT(setAdvancedMode(bool)));

    //choice
    QHBoxLayout *centerChoiceLayout = new QHBoxLayout();
    QHBoxLayout *choiceLayout = new QHBoxLayout();

    pointToPointButton = new QPushButton(TA_DRAWAREA_POINT_TO_POINT_BUTTON_TEXT);
    pointToPointButton->setFont(TA_Fonts::getButtonFont());
    pointToPointButton->setCheckable(true);
    pointToPointButton->setChecked(true);
    connect(pointToPointButton, SIGNAL(clicked()), this, SLOT(goToPointToPoint()));
    choiceLayout->addWidget(pointToPointButton, 0, Qt::AlignRight | Qt::AlignTop);

    multipleMarkersButton = new QPushButton(TA_DRAWAREA_MULTIPLE_MARKERS_BUTTON_TEXT);
    multipleMarkersButton->setFont(TA_Fonts::getButtonFont());
    multipleMarkersButton->setCheckable(true);
    connect(multipleMarkersButton, SIGNAL(clicked()), this, SLOT(goToMultipleMarkers()));
    choiceLayout->addWidget(multipleMarkersButton, 0, Qt::AlignLeft | Qt::AlignTop);

    QButtonGroup* groupChoice = new QButtonGroup(this);
    groupChoice->addButton(pointToPointButton);
    groupChoice->addButton(multipleMarkersButton);
    groupChoice->setExclusive(true);

    //cancel reference marker selection
    cancelSelectReferenceMarkerButton = new QPushButton(TA_DRAWAREA_CANCEL_SELECT_REFERENCE_MARKER_BUTTON_TEXT);
    cancelSelectReferenceMarkerButton->setFont(TA_Fonts::getButtonFont());
    connect(cancelSelectReferenceMarkerButton, SIGNAL(clicked()), this, SLOT(goToPointToPoint()));
    choiceLayout->addWidget(cancelSelectReferenceMarkerButton, 0, Qt::AlignCenter | Qt::AlignTop);

    centerChoiceLayout->addStretch(1);
    centerChoiceLayout->addLayout(choiceLayout);
    centerChoiceLayout->addStretch(1);
    mainLayout->addLayout(centerChoiceLayout);

    //selectionTitle
    SelectionTitle = new QLabel();
    SelectionTitle->setFont(TA_Fonts::getSubTitleFont());
    mainLayout->addWidget(SelectionTitle, 1, Qt::AlignCenter | Qt::AlignHCenter);

    //marker toolbar
    QHBoxLayout *markerToolbarLayout = new QHBoxLayout();

    //select reference marker
    selectReferenceMarkerButton = new QPushButton(TA_DRAWAREA_SELECT_REFERENCE_MARKER_BUTTON_TEXT);
    selectReferenceMarkerButton->setFont(TA_Fonts::getButtonFont());
    connect(selectReferenceMarkerButton, SIGNAL(clicked()), this, SLOT(goToSelectReferenceMarker()));
    markerToolbarLayout->addWidget(selectReferenceMarkerButton, 0, Qt::AlignCenter | Qt::AlignTop);

    mainLayout->addLayout(markerToolbarLayout);

    //Painter
    QHBoxLayout *centralLayout = new QHBoxLayout();
    painterLayout = new QVBoxLayout();
    //Get the clients webcams list.
    ConfigTree* clientsTree = TA_Server::getInstance()->getConfig();
    foreach(TA_InfoClient* infoClient, clientsTree->keys()){
        QList<WebcamStruct*> webcams = ((*clientsTree)[infoClient])->values();
        foreach (WebcamStruct* webcam, webcams) {
            if (webcam->isSelected) {
                map[webcam] = new AreaDefinition();
                map[webcam]->type = AREADRAWNTYPE_POINTS;
                map[webcam]->markers = new QList<MarkerStruct>;
                map[webcam]->points = new QList<QPointF>();
                map[webcam]->referenceMarker = NULL;
            }
        }
    }

    // Initializes with sectors
    if (area.oldname != "") {
        // For each sector
        foreach (QString sectorId, TA_Server::getInstance()->getAreaInfo(area.oldname)->sectors.keys()) {
            WebcamStruct *webcam = TA_Server::getInstance()->getWebcamInfo(area.oldname, sectorId);
            // If the webcam is concerned
            if (map.contains(webcam)) {
                SectorStruct *sector = TA_Server::getInstance()->getSectorInfo(area.oldname,sectorId);
                if (sector->type == POINTS_SECTOR || sector->type == UNIQUE_MARKER_SECTOR) {
                    map[webcam]->type = AREADRAWNTYPE_POINTS;
                    // Add the points
                    foreach (QPoint point, sector->points) {
                        *(map[webcam]->points) << QPointF(point.x(),point.y());
                    }
                    if (sector->type == UNIQUE_MARKER_SECTOR) {
                        map[webcam]->referenceMarker = new MarkerStruct;
                        map[webcam]->referenceMarker->position = sector->markers.first().position;
                        map[webcam]->referenceMarker->id = sector->markers.first().id;
                    }
                } else {
                    map[webcam]->type = AREADRAWNTYPE_MARKERS;
                    foreach (MarkerStruct marker, sector->markers) {
                        *(map[webcam]->markers) << marker;
                    }
                }
            }
        }
    }

    painterPoints = new TA_AreaPointsPainter(area.color);
    painterLayout->addWidget(painterPoints);

    painterReferenceMarker = new TA_AreaMarkersPainter(area.color);
    painterLayout->addWidget(painterReferenceMarker);

    painterMarkers = new TA_AreaMarkersPainter(area.color);
    painterLayout->addWidget(painterMarkers);

    it_map = QMapIterator<WebcamStruct*, AreaDefinition*>(map);
    it_map.toFront();
    it_map.next();

    QPushButton *previousButton = new QPushButton();
    previousButton->setFont(TA_Fonts::getButtonFont());
    QPushButton *nextButton = new QPushButton();
    nextButton->setFont(TA_Fonts::getButtonFont());

    if(map.size() > 1){
        previousButton->setIcon(QIcon(TA_DRAWAREA_PREVIOUS_WEBCAM_BUTTON_ICON));
        nextButton->setIcon(QIcon(TA_DRAWAREA_NEXT_WEBCAM_BUTTON_ICON));
    } else {
        previousButton->blockSignals(true);
        nextButton->blockSignals(true);
    }


    previousButton->setFlat(true);
    previousButton->setIconSize(QSize(50, 50));
    nextButton->setFlat(true);
    nextButton->setIconSize(QSize(50, 50));

    centralLayout->addWidget(previousButton, 1, Qt::AlignCenter);
    centralLayout->addLayout(painterLayout, 10);
    centralLayout->addWidget(nextButton, 1, Qt::AlignCenter);

    //Footer
    QHBoxLayout *footerLayout = new QHBoxLayout();
    QHBoxLayout *buttonLayout= new QHBoxLayout();
    QPushButton *returnButton = new QPushButton(TA_DRAWAREA_RETURN_BUTTON_TEXT);
    returnButton->setFont(TA_Fonts::getButtonFont());
    QPushButton *refreshButton = new QPushButton(QIcon(TA_DRAWAREA_REFRESH_BUTTON_ICON), TA_DRAWAREA_REFRESH_BUTTON_TEXT);
    refreshButton->setFont(TA_Fonts::getButtonFont());
    clearButton = new QPushButton(QIcon(TA_DRAWAREA_CLEAR_BUTTON_ICON), TA_DRAWAREA_CLEAR_BUTTON_TEXT);
    clearButton->setFont(TA_Fonts::getButtonFont());

    QPushButton *endButton = new QPushButton(TA_DRAWAREA_END_BUTTON_TEXT);
    endButton->setFont(TA_Fonts::getButtonFont());

    buttonLayout->addStretch();
    buttonLayout->addWidget(returnButton, 0, Qt::AlignTop);
    buttonLayout->addStretch();
    buttonLayout->addWidget(refreshButton, 0, Qt::AlignRight | Qt::AlignTop);
    buttonLayout->addWidget(clearButton, 0, Qt::AlignLeft | Qt::AlignTop);
    buttonLayout->addStretch();
    buttonLayout->addWidget(endButton, 0, Qt::AlignTop);
    buttonLayout->addStretch();
    footerLayout->addLayout(buttonLayout);

    mainLayout->addLayout(centralLayout,10);
    mainLayout->addLayout(footerLayout,1);

    changeCurrentWebcam(map.keys()[0]);

    setLayout(mainLayout);

    connect(previousButton, SIGNAL(clicked()), this, SLOT(previous()));
    connect(nextButton, SIGNAL(clicked()), this, SLOT(next()));
    connect(returnButton, SIGNAL(clicked()), this, SLOT(cancel()));
    connect(refreshButton, SIGNAL(clicked()), this, SLOT(refresh()));
    connect(clearButton, SIGNAL(clicked()), this, SLOT(clear()));
    connect(endButton, SIGNAL(clicked()), this, SLOT(end()));
    connect(painterPoints,SIGNAL(newPoint()),this,SLOT(activateClearButton()));
    connect(TA_Server::getInstance(), SIGNAL(changedFrame(TA_InfoClient*,int)), this, SLOT(changedFrame(TA_InfoClient*,int)));
    connect(painterReferenceMarker, SIGNAL(newSelectedMarker()), this, SLOT(referenceMarkerSelected()));
    connect(painterMarkers, SIGNAL(newSelectedMarker()), this, SLOT(activateClearButton()));
    connect(TA_Server::getInstance(),SIGNAL(changedMarkers(TA_InfoClient*,int)),this,SLOT(newMarkers(TA_InfoClient*,int)));

    setAdvancedMode(false);
    this->initialization = false;
}

/*
    Delete properly this instance
*/
TA_DrawArea::~TA_DrawArea(){
    for(QMap<WebcamStruct*,AreaDefinition*>::Iterator itWebcams = map.begin(); itWebcams != map.end(); itWebcams++){
        delete itWebcams.value();
    }
}

void TA_DrawArea::newMarkers(TA_InfoClient *client, int webcamId) {
    if (currentWebcam->client == client && currentWebcam->id == webcamId) {
        refresh();
    }
}

/*****************************************************************************/
/******                             SLOTS                               ******/
/*****************************************************************************/

/*
    Activate the clear button
*/
void TA_DrawArea::activateClearButton(){
    clearButton->setEnabled(true);
}

/*
    Save the current outline for the currentWebcam and switch to the previous webcam
    or the last if the currentWebcam was the first in webcams map.
*/
void TA_DrawArea::previous(){
    updateMap();

    if (map.keys()[0] == currentWebcam) {
        it_map.toBack();
        it_map.previous();
        changeCurrentWebcam(map.keys()[map.keys().size()-1]);
    } else {
        WebcamStruct* temp = it_map.key();
        if (temp == it_map.previous().key()) {
            it_map.previous();
        }
        changeCurrentWebcam(it_map.key());
    }
}

/*
    Save the current outline for the currentWebcam and switch to the next webcam
    or the first if the currentWebcam was the last in webcams map.
*/
void TA_DrawArea::next(){
    updateMap();

    if(map.keys()[map.keys().size()-1] == currentWebcam){
        changeCurrentWebcam(map.keys()[0]);
        it_map.toFront();
        it_map.next();
    } else {
        WebcamStruct* temp = it_map.key();
        if (temp == it_map.next().key()) {
            it_map.next();
        }
        changeCurrentWebcam(it_map.key());
    }
}

/*
    Cancel any modifications on any webcams and return to the webcam selection
*/
void TA_DrawArea::cancel(){
    TA_ServerMainWidget::getInstance()->openAddWebcamsToArea(area);
}

/*
    Clear the points on the areaPainter
*/
void TA_DrawArea::clear() {
    if (painterPoints->isVisible()) {
        painterPoints->clearOutlineArea();
        clearButton->setDisabled(true);
    } else if (painterReferenceMarker->isVisible()) {
        painterReferenceMarker->setWebcamAndPath(currentWebcam, new QList<int>, true);
        painterPoints->setMarker(-1);
        currentWebcam->markers.clear();
    } else if(painterMarkers->isVisible()) {
        painterMarkers->setWebcamAndPath(currentWebcam, new QList<int>, false);
        currentWebcam->markers.clear();
    }
    refresh();
}

/*
    Ask to refresh the view
*/
void TA_DrawArea::refresh() {
    TA_DataStream packet;
    packet.setIdentifier(currentWebcam->id);
    packet.setToLaunch(true);
    packet.setUniqueFrame(true);
    currentWebcam->client->sendPacket(&packet);
}

/*
    Save outlines for the area specified and apply changes to back end
*/
void TA_DrawArea::end() {
    updateMap();

    // Checks for unspecified sectors
    QString listUnspecifiedWebcams = QString();
    for (QMap<WebcamStruct*, AreaDefinition*>::Iterator itWebcams = map.begin(); itWebcams != map.end(); itWebcams++) {
        if (!isSpecifiedArea(itWebcams.value())) {
            listUnspecifiedWebcams += QString(TA_DRAWAREA_UNSPECIFIED_WEBCAM_TEXT).arg(itWebcams.key()->name);
        }
    }

    // If there is unspecified sectors, advertises the user
    if (listUnspecifiedWebcams != QString()) {
        QString message = QString(TA_DRAWAREA_UNSPECIFIED_WEBCAMS_ERROR_TEXT).arg(area.name).arg(listUnspecifiedWebcams);
        if (QMessageBox::No == QMessageBox::question(this, TA_DRAWAREA_UNSPECIFIED_WEBCAMS_ERROR_TITLE, message,
                                                     QMessageBox::Yes|QMessageBox::No)) {
            return;
        }
    }

    if (area.oldname != "" && TA_Server::getInstance()->getAreaInfo(area.oldname)) {
        // Edit
        if (area.oldname != area.name) {
            //Rename
            TA_Server::getInstance()->renameArea(area.oldname, area.name);
        }
        TA_Server::getInstance()->getAreaInfo(area.name)->color = area.color;

        // Clear all sectors
        foreach (QString sectorId, TA_Server::getInstance()->getAreaInfo(area.name)->sectors.keys()) {
            TA_Server::getInstance()->removeSector(area.name,sectorId);
        }
    } else {
        TA_Server::getInstance()->addArea(area.name, area.color);
    }


    for (QMap<WebcamStruct*, AreaDefinition*>::Iterator itWebcams = map.begin(); itWebcams != map.end(); itWebcams++) {
        // For each draw webcams
        if (!isSpecifiedArea(itWebcams.value())) {
            continue;
        }
        TA_Server *server = TA_Server::getInstance();
        TA_InfoClient *client = itWebcams.key()->client;
        int webcamId = itWebcams.key()->id;
        QColor color = server->getAreaInfo(area.name)->color;

        if (itWebcams.value()->type == AREADRAWNTYPE_MARKERS) {
            QVector<MarkerStruct> markers;
            foreach (MarkerStruct marker, *(itWebcams.value()->markers)) {
                markers << marker;
            }
            server->addMultiMarkerSector(client, webcamId, area.name, color, markers);
            continue;
        }

        //type AREADRAWNTYPE_POINTS
        QVector<QPoint> points;
        foreach (QPointF point, *(itWebcams.value()->points)) {
            points << QPoint(point.x(),point.y());
        }

        if (itWebcams.value()->referenceMarker == NULL) {
            server->addPointSector(client, webcamId, area.name, color, points);
        } else {
            server->addUniqueMarkerSector(client, webcamId, area.name, color,
                                                            *(itWebcams.value()->referenceMarker), points);
        }
    }

    TA_Server::getInstance()->getAreaInfo(area.name)->file = area.file;
    TA_Server::getInstance()->getAreaInfo(area.name)->url = area.url;
    TA_Server::getInstance()->getAreaInfo(area.name)->command = area.command;

    TA_ServerMainWidget::getInstance()->openHomeScreen(true);
}

/*
    Add the reference marker and go to point to point interface
*/
void TA_DrawArea::referenceMarkerSelected() {
    painterPoints->setMarker(painterReferenceMarker->getMarkerPath()->first());
    goToPointToPoint();
}

/*****************************************************************************/
/******                             PRIVATE                             ******/
/*****************************************************************************/

/*
    Change the current webcam
    @param newWebcam : webcam to change
*/
void TA_DrawArea::changeCurrentWebcam(WebcamStruct *newWebcam) {
    currentWebcam = newWebcam;

    QList<int> *referenceMarkerList = new QList<int>(); //empty or with unique element
    int referenceMarker = -1;
    if (map[currentWebcam]->referenceMarker != NULL &&
            currentWebcam->markers.contains(map[currentWebcam]->referenceMarker->id)) {
        referenceMarker = map[currentWebcam]->referenceMarker->id;
        referenceMarkerList->append(map[currentWebcam]->referenceMarker->id);
    }

    painterPoints->setWebcamAndOutlineArea(currentWebcam, map[currentWebcam]->points, referenceMarker);
    painterReferenceMarker->setWebcamAndPath(currentWebcam, referenceMarkerList, true);

    QList<int> *markerList = new QList<int>();
    foreach (MarkerStruct marker, *(map[currentWebcam]->markers)) {
        *markerList << marker.id;
    }

    painterMarkers->setWebcamAndPath(currentWebcam, markerList, false);

    if (map[currentWebcam]->type == AREADRAWNTYPE_POINTS) {
        goToPointToPoint();
    } else {
        goToMultipleMarkers();
    }
}

/*
    Update the variable map to include current state
*/
void TA_DrawArea::updateMap() {
    if (map[currentWebcam]->referenceMarker != NULL) {
        delete map[currentWebcam]->referenceMarker;
    }
    if (painterReferenceMarker->getMarkerPath()->isEmpty()) {
        map[currentWebcam]->referenceMarker = NULL;
    } else {
        map[currentWebcam]->referenceMarker = new MarkerStruct();
        int referenceMarkerId = painterReferenceMarker->getMarkerPath()->first();
        map[currentWebcam]->referenceMarker->id = referenceMarkerId;
        map[currentWebcam]->referenceMarker->position.setX(painterPoints->getMarkerPosition().x());
        map[currentWebcam]->referenceMarker->position.setY(painterPoints->getMarkerPosition().y());

    }

    QList<QPointF>* points = painterPoints->getOutlineArea();
    (*(map[currentWebcam]->points)) = (*points);
    delete points;    // Clear sectors

    delete map[currentWebcam]->markers;
    map[currentWebcam]->markers = new QList<MarkerStruct>;
    QList<int> markers = *(painterMarkers->getMarkerPath());
    for (int i = 0; i < markers.length(); i++) {
        MarkerStruct tmpMarker = MarkerStruct();
        tmpMarker.id = markers[i];
        if (!currentWebcam->markers.contains(markers[i])) {
            continue;
        }
        tmpMarker.position.setX(currentWebcam->markers[markers[i]].x());
        tmpMarker.position.setY(currentWebcam->markers[markers[i]].y());
        map[currentWebcam]->markers->append(tmpMarker);
    }

    if (pointToPointButton->isChecked()) {
        map[currentWebcam]->type = AREADRAWNTYPE_POINTS;
    } else {
        map[currentWebcam]->type = AREADRAWNTYPE_MARKERS;
    }
}

/*
    Check if the area is specified
*/
bool TA_DrawArea::isSpecifiedArea(AreaDefinition *areaDef) {
    return (areaDef->type == AREADRAWNTYPE_POINTS && !areaDef->points->isEmpty())
            || (areaDef->type == AREADRAWNTYPE_MARKERS && !areaDef->markers->isEmpty());
}

/*
    Go to point to point interface
 */
void TA_DrawArea::goToPointToPoint() {
    pointToPointButton->setChecked(true);
    setAdvancedMode(advanceButton->isChecked());
    advanceButton->setActiveStyle(advanceButton->isChecked());
    advanceButton->setDisabled(false);
    painterReferenceMarker->hide();
    cancelSelectReferenceMarkerButton->hide();
    painterMarkers->hide();

    SelectionTitle->setText(TA_DRAWAREA_POINT_TO_POINT_TITLE);
    if (!initialization) {
        painterPoints->setVisible(true);
    }
    clearButton->setDisabled(painterPoints->getOutlineArea()->isEmpty());
}

/*
    Go to select reference marker interface
 */
void TA_DrawArea::goToSelectReferenceMarker() {
    setAdvancedMode(true);
    advanceButton->setActiveStyle(true);
    advanceButton->setDisabled(true);
    painterPoints->hide();
    selectReferenceMarkerButton->hide();
    painterMarkers->hide();
    pointToPointButton->hide();
    multipleMarkersButton->hide();

    SelectionTitle->setText(TA_DRAWAREA_SELECT_REFERENCE_MARKER_TITLE);
    if (!initialization) {
        cancelSelectReferenceMarkerButton->setVisible(true);
        painterReferenceMarker->setVisible(true);
    }
    clearButton->setDisabled(currentWebcam->markers.isEmpty());
    if (currentWebcam->markers.isEmpty()) {
        TA_MainWindow::getInstance()->getStatusBar()->showMessage(TA_DRAWAREA_MULTIPLE_MARKERS_NO_MARKER_WARNING, "Warning");
    }
}

/*
    Go to multiple markers interface
 */
void TA_DrawArea::goToMultipleMarkers() {
    multipleMarkersButton->setChecked(true);
    setAdvancedMode(true);
    advanceButton->setActiveStyle(true);
    advanceButton->setDisabled(true);
    painterPoints->hide();
    selectReferenceMarkerButton->hide();
    cancelSelectReferenceMarkerButton->hide();
    painterReferenceMarker->hide();


    SelectionTitle->setText(TA_DRAWAREA_MULTIPLE_MARKERS_TITLE);
    if (!initialization) {
        painterMarkers->setVisible(true);
        pointToPointButton->setVisible(true);
        multipleMarkersButton->setVisible(true);
    }
    clearButton->setDisabled(currentWebcam->markers.isEmpty());
    if (currentWebcam->markers.count() < 3) {
        TA_MainWindow::getInstance()->getStatusBar()->showMessage(TA_DRAWAREA_MULTIPLE_MARKERS_NOT_ENOUGH_MARKERS_WARNING, "Warning");
    }
}

/*
    Active and unactive the advanced mode
    @param show : true => active, false => unactive
*/
void TA_DrawArea::setAdvancedMode(bool show) {
    if (show && !initialization) {
        pointToPointButton->setVisible(true);
        multipleMarkersButton->setVisible(true);
        selectReferenceMarkerButton->setVisible(true);
    } else {
        pointToPointButton->hide();
        multipleMarkersButton->hide();
        selectReferenceMarkerButton->hide();
    }
}

/*
    Updates the current webcam's frame
        @param client : the client of the webcam
        @param webcamId : the webcam identifier
*/
void TA_DrawArea::changedFrame(TA_InfoClient* client, int webcamId){
    if (client != currentWebcam->client || webcamId != currentWebcam->id)
        return;
    painterPoints->updateImage();
    painterReferenceMarker->updateImage();
    painterMarkers->updateImage();
}
