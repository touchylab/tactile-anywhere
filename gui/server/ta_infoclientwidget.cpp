/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 05 March 2014                                      |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_InfoClientWidget to display widget |
  | to display content informations about a choosen client                  |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QLayout>
#include <QLabel>
#include <QPushButton>

// Local includes
#include "entity/ta_server.h"
#include "gui/server/ta_servermainwidget.h"
#include "gui/server/ta_clientwebcamslist.h"
#include "gui/ta_fonts.h"
#include "ta_infoclientwidget.h"

/*
    Display informations about a choosen client
        @param client : the client to display
*/
TA_InfoClientWidget::TA_InfoClientWidget(TA_InfoClient* client) : QWidget(){
    this->client = client;

    //Header
    QHBoxLayout* headerLayout = new QHBoxLayout;
    QLabel* titleLabel = new QLabel(client->getClientName());
    titleLabel->setFont(TA_Fonts::getTitleFont());
    headerLayout->addWidget(titleLabel);
    headerLayout->setAlignment(Qt::AlignHCenter);

    QVBoxLayout* mainLayout = new QVBoxLayout;
    mainLayout->addLayout(headerLayout,1);
    mainLayout->addStretch();

    // Coordinates
    if(!client->isLocal()){
        QHBoxLayout* coordinatesLayout = new QHBoxLayout;
        QLabel* addressLabel = new QLabel(QString(TA_INFOCLIENTWIDGET_IP_TITLE) + client->getClientAddress().toString());
        addressLabel->setFont(TA_Fonts::getDefaultFont());
        coordinatesLayout->addStretch();
        coordinatesLayout->addWidget(addressLabel);
        coordinatesLayout->addStretch();

        QLabel* portTitle = new QLabel(QString(TA_INFOCLIENTWIDGET_PORT_TITLE) + QString::number(client->getClientSocket()->peerPort()));
        portTitle->setFont(TA_Fonts::getDefaultFont());
        coordinatesLayout->addWidget(portTitle);
        coordinatesLayout->addStretch();
        mainLayout->addLayout(coordinatesLayout,1);
        mainLayout->addStretch();
    }

    TA_ClientWebcamsList* webcamList = new TA_ClientWebcamsList(client);

    if(webcamList->isEmpty()){
        delete webcamList;
        QLabel* webcamLabel = new QLabel(QString(TA_INFOCLIENTWIDGET_NO_WEBCAMS_TEXT));
        webcamLabel->setFont(TA_Fonts::getDefaultFont());
        mainLayout->addWidget(webcamLabel, 1, Qt::AlignHCenter);
    } else {
        QLabel* webcamLabel = new QLabel(QString(TA_INFOCLIENTWIDGET_WEBCAM_LABEL));
        QFont font = TA_Fonts::getDefaultFont();
        font.setBold(true);
        webcamLabel->setFont(font);
        mainLayout->addWidget(webcamLabel,1,Qt::AlignHCenter);
        QHBoxLayout* labelLayout = new QHBoxLayout;
        labelLayout->addStretch(1);
        labelLayout->addWidget(webcamList,10);
        labelLayout->addStretch(1);
        mainLayout->addLayout(labelLayout,10);
    }

    //Footer
    QHBoxLayout* footerLayout = new QHBoxLayout();
    QHBoxLayout* buttonLayout = new QHBoxLayout();
    QPushButton* returnButton = new QPushButton(TA_INFOCLIENTWIDGET_RETURN_BUTTON_TEXT);
    returnButton->setFont(TA_Fonts::getButtonFont());
    buttonLayout->addStretch();
    buttonLayout->addWidget(returnButton,0,Qt::AlignTop);
    buttonLayout->addStretch();
    footerLayout->addLayout(buttonLayout);
    connect(returnButton, SIGNAL(clicked()), this, SLOT(cancel()));
    mainLayout->addLayout(footerLayout,1);

    setLayout(mainLayout);
    connect(TA_Server::getInstance(), SIGNAL(changedClient()), this, SLOT(changedClient()));
}

/*****************************************************************************/
/******                             SLOTS                               ******/
/*****************************************************************************/

/*
    Cancel action and go back to the previous screen (home screen)
*/
void TA_InfoClientWidget::cancel(){
    TA_ServerMainWidget::getInstance()->openHomeScreen();
}

/*
    Updates the view
*/
void TA_InfoClientWidget::changedClient(){
    if(!TA_Server::getInstance()->getConfig()->contains(client)){
        cancel();
    }
}
