/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 26 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_Options class containing the       |
  | informations about the configuration of the server                      |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

//Qt
#include <QLayout>
#include <QFormLayout>
#include <QCheckBox>
#include <QLabel>
#include <QLineEdit>
#include <QButtonGroup>
#include <QRadioButton>
#include <QPushButton>

// Local includes
#include "entity/ta_server.h"
#include "gui/server/ta_servermainwidget.h"
#include "gui/ta_fonts.h"
#include "ta_options.h"

/*
    Default builder
 */
TA_Options::TA_Options() : QWidget() {
    QVBoxLayout *mainLayout = new QVBoxLayout();

    //Header
    QHBoxLayout *headerLayout = new QHBoxLayout();
    QLabel *titleLabel = new QLabel(TA_OPTIONS_TITLE);
    titleLabel->setFont(TA_Fonts::getTitleFont());
    headerLayout->addWidget(titleLabel,1, Qt::AlignHCenter| Qt::AlignTop);
    mainLayout->addLayout(headerLayout);
    mainLayout->addStretch(1);

    QVBoxLayout* optionLayout = new QVBoxLayout();

    QLabel *infoLabel = new QLabel(TA_OPTIONS_INFORMATIONS_LABEL);
    QFont font = TA_Fonts::getDefaultFont();
    font.setBold(true);
    infoLabel->setFont(font);
    optionLayout->addWidget(infoLabel);

    //Form layout
    QFormLayout *formLayout = new QFormLayout();

    // Name
    nameLineEdit = new QLineEdit(TA_Server::getInstance()->getServerName());
    nameLineEdit->setFont(TA_Fonts::getDefaultFont());
    nameLineEdit->setMaxLength(200);
    connect(nameLineEdit,SIGNAL(editingFinished()),this,SLOT(checkName()));
    QLabel* nomLabel = new QLabel(TA_OPTIONS_NAME_LABEL);
    nomLabel->setFont(TA_Fonts::getDefaultFont());
    formLayout->addRow(nomLabel, nameLineEdit);
    QCheckBox* activePassword = new QCheckBox;
    activePassword->setFont(TA_Fonts::getDefaultFont());
    activePassword->setText(TA_OPTIONS_ACTIVE_PASSWORD_LABEL);
    activePassword->setMinimumWidth(200);
    activePassword->setChecked(TA_Server::getInstance()->getServerClearPassword() != QString());
    connect(activePassword,SIGNAL(toggled(bool)),this,SLOT(changePasswordState(bool)));
    formLayout->addWidget(activePassword);

    // Password
    passwordLineEdit = new QLineEdit(TA_Server::getInstance()->getServerClearPassword());
    passwordLineEdit->setFont(TA_Fonts::getDefaultFont());
    passwordLineEdit->setMaxLength(200);
    passwordLineEdit->setEchoMode(QLineEdit::Password);
    passwordLineEdit->setDisabled(TA_Server::getInstance()->getServerClearPassword() == QString());
    QLabel* motDePasseLabel = new QLabel(TA_OPTIONS_PASSWORD_LABEL);
    motDePasseLabel->setFont(TA_Fonts::getDefaultFont());
    formLayout->addRow(motDePasseLabel, passwordLineEdit);
    showPasswordCheckbox = new QCheckBox;
    showPasswordCheckbox->setFont(TA_Fonts::getDefaultFont());
    showPasswordCheckbox->setText(TA_OPTIONS_SHOW_PASSWORD_LABEL);
    showPasswordCheckbox->setDisabled(TA_Server::getInstance()->getServerClearPassword() == QString());
    connect(showPasswordCheckbox,SIGNAL(toggled(bool)),this,SLOT(showPassword(bool)));
    formLayout->addWidget(showPasswordCheckbox);

    optionLayout->addLayout(formLayout);
    optionLayout->addStretch();

    // Ratio Label
    QLabel *ratioLabel = new QLabel(TA_OPTIONS_RATIO_LABEL);
    ratioLabel->setFont(TA_Fonts::getDefaultFont());
    ratioLabel->setFont(font);
    optionLayout->addWidget(ratioLabel);

    // Ratio
    int id = 1.0/TA_Server::getInstance()->getDimensionRatio();
    ratioButtons = new QButtonGroup;
    QRadioButton* ratio10 = new QRadioButton(QString(TA_OPTIONS_RATIO_CHOICE_TEXT).arg(10));
    ratio10->setFont(TA_Fonts::getDefaultFont());
    if(id == 10 || id == 9) ratio10->setChecked(true);
    QRadioButton* ratio5 = new QRadioButton(QString(TA_OPTIONS_RATIO_CHOICE_TEXT).arg(5));
    ratio5->setFont(TA_Fonts::getDefaultFont());
    if(id == 5 || id == 4) ratio5->setChecked(true);
    QRadioButton* ratio2 = new QRadioButton(QString(TA_OPTIONS_RATIO_CHOICE_TEXT).arg(2));
    ratio2->setFont(TA_Fonts::getDefaultFont());
    if(id == 2) ratio2->setChecked(true);
    QRadioButton* ratio1 = new QRadioButton(TA_OPTIONS_NO_RATIO_CHOICE_TEXT);
    ratio1->setFont(TA_Fonts::getDefaultFont());
    if(id == 1) ratio1->setChecked(true);
    ratioButtons->addButton(ratio10, 10);
    ratioButtons->addButton(ratio5, 5);
    ratioButtons->addButton(ratio2, 2);
    ratioButtons->addButton(ratio1, 1);

    QHBoxLayout* ratioLayout = new QHBoxLayout;
    ratioLayout->addWidget(ratio10);
    ratioLayout->addWidget(ratio5);
    ratioLayout->addWidget(ratio2);
    ratioLayout->addWidget(ratio1);

    optionLayout->addLayout(ratioLayout);

    // Centralize
    QHBoxLayout *centralLayout = new QHBoxLayout();
    centralLayout->addStretch();
    centralLayout->addLayout(optionLayout);
    centralLayout->addStretch();
    mainLayout->addLayout(centralLayout);

    //Footer
    QHBoxLayout *footerLayout = new QHBoxLayout();

    QPushButton *cancelButton = new QPushButton(TA_OPTIONS_CANCEL_BUTTON_TEXT);
    cancelButton->setFont(TA_Fonts::getButtonFont());
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(cancel()));
    footerLayout->addStretch();
    footerLayout->addWidget(cancelButton, 0, Qt::AlignTop);

    QPushButton *saveButton = new QPushButton(TA_OPTIONS_SAVE_BUTTON_TEXT);
    saveButton->setFont(TA_Fonts::getButtonFont());
    connect(saveButton, SIGNAL(clicked()), this, SLOT(save()));
    footerLayout->addStretch();
    footerLayout->addWidget(saveButton, 0, Qt::AlignTop);
    footerLayout->addStretch();
    mainLayout->addStretch(1);
    mainLayout->addLayout(footerLayout);

    setLayout(mainLayout);
}


/*****************************************************************************/
/******                             SLOTS                               ******/
/*****************************************************************************/

/*
    Change the state of the password field
        @param state: true to activate the password, false else
*/
void TA_Options::changePasswordState(bool state){
    passwordLineEdit->setEnabled(state);
    passwordLineEdit->setFont(TA_Fonts::getDefaultFont());
    passwordLineEdit->setText(QString());
    showPasswordCheckbox->setEnabled(state);
    showPasswordCheckbox->setChecked(false);
}

/*
    Show or not the password
        @param show: true to show the password, false else
*/
void TA_Options::showPassword(bool show){
    if(show){
        passwordLineEdit->setEchoMode(QLineEdit::Normal);
    } else {
        passwordLineEdit->setEchoMode(QLineEdit::Password);
    }
}

/*
    Checks the name of the server field
*/
void TA_Options::checkName(){
    if(nameLineEdit->text().isNull() || nameLineEdit->text().size() == 0)
        nameLineEdit->setText(QHostInfo::localHostName());
}

/*
    Save and return to the main view
*/
void TA_Options::save() {
    TA_Server::getInstance()->setServerName(nameLineEdit->text());
    TA_Server::getInstance()->setServerPassword(passwordLineEdit->text());
    TA_Server::getInstance()->setDimensionRatio(1.0/ratioButtons->checkedId());
    TA_ServerMainWidget::getInstance()->openHomeScreen();
}

/*
    Cancel and return to the main view
*/
void TA_Options::cancel() {
    TA_ServerMainWidget::getInstance()->openHomeScreen();
}
