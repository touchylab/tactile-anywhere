/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 19 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_LeftPanelArea to display a widget  |
  | at left side                                                            |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Valentin COULON -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QVBoxLayout>
#include <QPushButton>
#include <QHBoxLayout>
#include <QToolButton>
#include <QLabel>

//Local includes
#include "gui/ta_fonts.h"
#include "gui/server/ta_servermainwidget.h"
#include "ta_areaslist.h"
#include "ta_areatab.h"

/*
    Default builder
*/
TA_AreaTab::TA_AreaTab() : QWidget() {

    /*** Areas layout ***/
    QVBoxLayout* layout = new QVBoxLayout;

    /*** BottomToolbar ***/
    areasList = new TA_AreasList();
    layout->addWidget(areasList);
    connect(areasList, SIGNAL(itemSelectionChanged()), this, SLOT(changedSelectedItems()));

    /*** BottomToolbar ***/
    QWidget* bottomToolbar = new QWidget();
    QHBoxLayout* layoutToolbar = new QHBoxLayout();

    //add Button
    QToolButton* addButton = new QToolButton();
    addButton->setText("+");
    addButton->setFont(TA_Fonts::getButtonFont());
    addButton->setFixedHeight(27);
    connect(addButton, SIGNAL(clicked()), TA_ServerMainWidget::getInstance(), SLOT(openCreateArea()));

    //fusion button
    QPushButton* fusionButton = new QPushButton(TA_AREATAB_GROUP_BUTTON_TEXT);
    fusionButton->setFont(TA_Fonts::getButtonFont());
    connect(fusionButton, SIGNAL(clicked()), areasList, SLOT(createAreaGroup()));

    //del button
    delButton = new QToolButton();
    delButton->setIcon(QIcon(TA_AREATAB_DELETE_BUTTON_ICON));
    delButton->setDisabled(true);
    delButton->setFixedSize(27,27);
    connect(delButton, SIGNAL(clicked()), areasList, SLOT(deleteAllSelected()));

    //add button to bottomToolbar
    layoutToolbar->addWidget(addButton);
    layoutToolbar->addWidget(fusionButton);
    layoutToolbar->addWidget(delButton);
    layoutToolbar->setMargin(0);
    bottomToolbar->setLayout(layoutToolbar);

    layout->addWidget(bottomToolbar);
    /*** End BottomToolbar ***/
    setLayout(layout);
}

/*
    Desable or enable the del button if the list is empty or not
*/
void TA_AreaTab::changedSelectedItems(){
    if(areasList->selectedItems().size()){
        delButton->setEnabled(true);
    } else {
        delButton->setDisabled(true);
    }
}
