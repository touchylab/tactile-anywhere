/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 13 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_AreaPointsPainter class drawing    |
  | and editing a points sector                                             |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu LOSLIER -- Initial version                            |
  | [-] 1.1 - Valentin COULON -- Version complete for Tactile Anywhere 1.0  |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/
/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QVBoxLayout>
#include <QPainter>
#include <QEvent>
#include <QMouseEvent>
#include <QPaintEvent>

//Local includes
#include "ta_areapointspainter.h"

/*
    Create an instance of TA_AreaPointsPainter
        @param frame : the frame to display
        @param color : the color to use for pen
        @param outline: the outline to use at the initialization
        @param marker: the marker position
*/
TA_AreaPointsPainter::TA_AreaPointsPainter(WebcamStruct *webcam, QColor color, QList<QPointF> *outline, int marker) : QWidget(){
    init(color);
    *pointsList = *outline;
    markerId = marker;
    this->webcam = webcam;
    updateMarkerPoint();
}

/*
    Create an instance of TA_AreaPointsPainter
        @param color : the color to use for pen
*/
TA_AreaPointsPainter::TA_AreaPointsPainter(QColor color) : QWidget() {
    init(color);
}

/*
    Destroy properly this class
*/
TA_AreaPointsPainter::~TA_AreaPointsPainter(){
    delete pointsList;
    delete sectorViewer;
}

/*****************************************************************************/
/******                             EVENT                               ******/
/*****************************************************************************/

/*
   Event to paint the entire surface of QPainter, draw
   points, background and outline
*/
void TA_AreaPointsPainter::paintEvent(QPaintEvent *){
    QPainter painter(this);
    sectorViewer->setCurrentFrame(webcam->image);
    sectorViewer->paintImageAndSectors(&painter, size());
    drawShape(&painter);
    drawMousePosition(&painter);
    painter.end();
}

/*
    Draw a shape from a painter component
    @param painter : the painter object to draw the shape
*/
void TA_AreaPointsPainter::drawShape(QPainter *painter){
    // Draw square for marker
    if (!markerPoint.isNull()) {
        painter->setPen(penCircle);
        painter->drawRect(markerPoint.x() * getDiffX() - 5, markerPoint.y() * getDiffY() - 5, 5, 5);
    }
    if (!pointsList->isEmpty()) {
        // Draw circles foreach points in list
        painter->setPen(penCircle);
        for(int i = 0; i < pointsList->size(); i++)
            painter->drawEllipse(QPointF(((*pointsList)[i].x() + markerPoint.x()) * getDiffX(),
                                         ((*pointsList)[i].y() + markerPoint.y()) * getDiffY()), 5, 5);

        QVector<QPointF> points;

        foreach (QPointF point, *pointsList) {
            points << QPointF((point.x() + markerPoint.x()) * getDiffX(), (point.y() + markerPoint.y()) * getDiffY());
        }

        // Draw shapes
        if (shapeComplete){
            points << points.first();
            penLine.setStyle(Qt::DashLine);// Shape is drawn in dash line when its complete
            painter->setPen(penLine);
        } else {
            // Draw line with mouse position
            if (!pointsList->last().isNull()) {
                painter->drawLine(QPointF((pointsList->last().x() + markerPoint.x()) * getDiffX(),
                                          (pointsList->last().y() + markerPoint.y()) * getDiffY()), mousePoint);
            }
            penLine.setStyle(Qt::SolidLine);
            painter->setPen(penLine);
        }

        painter->drawPolyline(points);// Draw shape
    }
}

/*
    Initialize the class
*/
void TA_AreaPointsPainter::init(QColor color) {
    pointsList = new QList<QPointF>;
    shapeComplete = false;
    editMode = false;
    indexEditPoint = -1;
    sectorViewer = NULL;
    setMouseTracking(true);
    setMinimumSize(1,1);    // bug if the background frame have a null size
    penCircle = QPen(color, 5, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    penLine = QPen(color, 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    connect(TA_Server::getInstance(),SIGNAL(changedMarkers(TA_InfoClient*,int)),this,SLOT(newMarkers(TA_InfoClient*,int)));
}

/*
    Update the position of the referenceMarker
*/
void TA_AreaPointsPainter::updateMarkerPoint() {
    if (markerId != -1 && webcam->markers.contains(markerId)) {
        markerPoint = webcam->markers[markerId];
    }
}

/*
    get the difference between reel size and visualSize for axe X
*/
float TA_AreaPointsPainter::getDiffX() {
    return (float)size().width() / (float)webcam->image.size().width();
}

/*
    get the difference between reel size and visualSize for axe Y
*/
float TA_AreaPointsPainter::getDiffY() {
    return (float)size().height() / (float)webcam->image.size().height();
}

/*
    Draw a mouse position point from a painter object
    @param painter : the painting object to draw the mouse position
*/
void TA_AreaPointsPainter::drawMousePosition(QPainter *painter){
    painter->setPen(penCircle);
    if(!shapeComplete && !mousePoint.isNull() && isInPainter(mousePoint.toPoint()))
       painter->drawEllipse(mousePoint,5,5);
}

/*
   Manage the mouse press event for create and edit points
*/
void TA_AreaPointsPainter::mousePressEvent(QMouseEvent *event){
    if(!shapeComplete){
        // Point pressed, last point to append to the points list
        QPointF point = event->localPos();
        // If the last point is enought close to the first point, shape is complete
        if (!pointsList->isEmpty() && pointsList->size() >= 2 &&
                abs((int)((pointsList->first().x() + markerPoint.x()) * getDiffX() - point.x())) <= X_SHAPE_COMPLETE &&
                abs((int)((pointsList->first().y() + markerPoint.y()) * getDiffY() - point.y())) <= Y_SHAPE_COMPLETE ) {
            shapeComplete = true;
        } else {
           pointsList->append(QPointF((point.x() / getDiffX())  - markerPoint.x(), (point.y() / getDiffY()) - markerPoint.y()));
           emit newPoint();
        }
    }

    // Edit points
    else if(shapeComplete && !editMode){
        if((indexEditPoint = indexOfPointToEdit(event->pos()))!=-1)
            editMode=true;
    }
    update();   // call paintEvent
}

/*
    Manage the mouse move event for create and edit points
*/
void TA_AreaPointsPainter::mouseReleaseEvent(QMouseEvent *){
    editMode = false;
    indexEditPoint = -1;
}

/*
    Manage the mouse move event for create and edit points
        @param event : event emited
*/
void TA_AreaPointsPainter::mouseMoveEvent(QMouseEvent *event){
    if(!shapeComplete){
        mousePoint = event->pos();
        update();
    } else if(editMode){
        // if the edit point is out of painter
        if (!isInPainter(event->pos())) {
            editMode = false;
        } else {
            pointsList->operator [](indexEditPoint) = QPointF((event->localPos().x() / getDiffX())  - markerPoint.x(),
                                                              (event->localPos().y() / getDiffY()) - markerPoint.y());
            update();
        }
    }
}

/*****************************************************************************/
/******                             GETTER                              ******/
/*****************************************************************************/

/*
    Return the area outline for the currentWebcam adapted to the
    webcam's size
*/
QList<QPointF>* TA_AreaPointsPainter::getOutlineArea(){
    QList<QPointF>* points = new QList<QPointF>;

    if (!shapeComplete) {
        return points;
    }

    foreach (QPointF point, *pointsList){
        *points << point;
    }

    return points;
}

/*
    get the reference marker position
        @return marker: the position of the marker position
*/
QPointF TA_AreaPointsPainter::getMarkerPosition() {
    return markerPoint;
}
/*****************************************************************************/
/******                             SETTER                              ******/
/*****************************************************************************/

/*
    Sets the reference marker position
        @param marker: the position of the marker position
*/
void TA_AreaPointsPainter::setMarker(int marker) {
    QPointF oldMarker = this->markerPoint;
    this->markerId = marker;
    this->markerPoint = QPointF();
    updateMarkerPoint();
    QList<QPointF> oldList = *pointsList;
    pointsList->clear();
    foreach (QPointF point, oldList) {
       *pointsList << QPointF(point.x() + oldMarker.x() - markerPoint.x(), point.y() + oldMarker.y() - markerPoint.y());
    }
    update();
}

/*****************************************************************************/
/******                             METHODS                             ******/
/*****************************************************************************/

/*
    Change the webcam concerned by the area outline
*/
void TA_AreaPointsPainter::updateImage() {
     update();
}

/*
    Load the points list and draw them on screen
        @param outline : the points list to draw
*/
void TA_AreaPointsPainter::setWebcamAndOutlineArea(WebcamStruct *webcam, QList<QPointF> *outline, int marker){
    this->webcam = webcam;
    if (sectorViewer != NULL) {
        delete sectorViewer;
    }
    sectorViewer = new TA_SectorViewer(webcam->client, webcam->id);
    emit TA_Server::getInstance()->unselectAllAreas();
    pointsList->clear();

    foreach (QPointF point, *outline) {
        *pointsList << point;
    }

    markerId = marker;
    markerPoint = QPointF();
    updateMarkerPoint();
    shapeComplete = (pointsList->size() == 0) ? false : true;
    mousePoint = QPointF();
    update();
}

/*
    Remove all the points of the area
*/
void TA_AreaPointsPainter::clearOutlineArea() {
    pointsList->clear();
    shapeComplete = false;
    mousePoint = QPointF();
    update();
}

/*
    Refresh the markers positions
        @param client : the client informations
        @param webcamId : the id of the webcam
*/
void TA_AreaPointsPainter::newMarkers(TA_InfoClient *client, int webcamId) {
    if (webcam->client == client && webcam->id == webcamId) {
        updateMarkerPoint();
        update();
    }
}

/*****************************************************************************/
/******                             FUNCTIONS                           ******/
/*****************************************************************************/

/*
    Check if the point is in surface of QPainter
        @param p : point to compare
*/
bool TA_AreaPointsPainter::isInPainter(QPoint p){
    return p.x() >= 0 && p.x() <= width() && p.y() >=0 && p.y() <= height();
}

/*
    Return index of the point to edit or -1 if it doesn't exist
    The aim is to find if point is a point existing (with a difference
    +5 or -5 pixel) in the pointsList composing the outline.
        @param point : point to compare with points in the pointsList
*/
int TA_AreaPointsPainter::indexOfPointToEdit(QPointF point) {
    int index = 0;
    foreach (QPointF p, *pointsList) {
        if(abs((int)((p.x() + markerPoint.x()) * getDiffX() - point.x())) <= X_EDIT_POINT &&
                abs((int)((p.y() + markerPoint.y()) * getDiffY()- point.y())) <= Y_EDIT_POINT )
            return index;
        index++;
    }
    return -1;
}
