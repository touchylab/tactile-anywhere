/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 12 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_SystemEventManager namespace       |
  | offering to send events to the system. These events can be get by other |
  | programs                                                                |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu LOSLIER -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QIcon>
#include <QPushButton>
#include <QVBoxLayout>
#include <QTextBrowser>

//Local includes
#include "gui/server/ta_servermainwidget.h"
#include "entity/ta_server.h"
#include "gui/ta_fonts.h"
#include "ta_detectionrunning.h"

/*
    Default builder
*/
TA_DetectionRunning::TA_DetectionRunning() : QWidget() {
    QVBoxLayout* layout = new QVBoxLayout;

    QWidget* loggerW = TA_Server::getLogger()->getLoggerWidget();
    layout->addWidget(loggerW);

    QPushButton *stopButton = new QPushButton(QIcon(TA_DETECTIONRUNNING_STOP_BUTTON_ICON), TA_DETECTIONRUNNING_STOP_BUTTON_TEXT);
    stopButton->setFont(TA_Fonts::getButtonFont());

    connect(stopButton, SIGNAL(clicked()), this, SLOT(stopDetection()));
    layout->addWidget(stopButton);
    setLayout(layout);


    TA_Server::getLogger()->logCritical(QString(TA_DETECTIONRUNNING_LOGGER_AVERT_1) + QString::number(TA_DETECTIONRUNNING_LAUNCH_DELAY/1000) + QString(TA_DETECTIONRUNNING_LOGGER_AVERT_2));

    timerDetection.start(TA_DETECTIONRUNNING_LAUNCH_DELAY);
    connect(&timerDetection,SIGNAL(timeout()),this,SLOT(launchDetection()));
}

/*****************************************************************************/
/******                             SLOTS                               ******/
/*****************************************************************************/

/*
    Launch the detection
*/
void TA_DetectionRunning::launchDetection(){
    timerDetection.stop();
    TA_Server::getInstance()->launchDetection();
    TA_Server::getLogger()->log(QTime::currentTime().toString(TA_DETECTIONRUNNING_LOGGER_TIME_CONVERTION) + QString(TA_DETECTIONRUNNING_LOGGER_DETECTION_START));
    TA_MainWindow::getInstance()->setWindowIcon(QIcon(TA_MAINWINDOW_LOGO_ON));
}

/*
  Stop the detection
*/
void TA_DetectionRunning::stopDetection() {
    TA_Server::getLogger()->stopLog();
    TA_Server::getInstance()->stopDetection();
    TA_ServerMainWidget::getInstance()->openHomeScreen();
    TA_MainWindow::getInstance()->setWindowIcon(QIcon(TA_MAINWINDOW_LOGO));
}
