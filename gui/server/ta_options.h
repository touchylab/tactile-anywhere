/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 26 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_Options class containing      |
  | the informations about the configuration of the server                  |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_OPTIONS_H
#define TA_OPTIONS_H

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/

#define TA_OPTIONS_TITLE "Options"

#define TA_OPTIONS_INFORMATIONS_LABEL "Informations"

#define TA_OPTIONS_NAME_LABEL "Nom : "

#define TA_OPTIONS_ACTIVE_PASSWORD_LABEL "Utiliser un mot de passe"
#define TA_OPTIONS_PASSWORD_LABEL "Mot de passe : "
#define TA_OPTIONS_SHOW_PASSWORD_LABEL "Afficher"

#define TA_OPTIONS_RATIO_LABEL "Compression de la détection"
#define TA_OPTIONS_RATIO_CHOICE_TEXT "Division par %1" // arg1 : division number
#define TA_OPTIONS_NO_RATIO_CHOICE_TEXT "Pas de compression"

#define TA_OPTIONS_CANCEL_BUTTON_TEXT "Annuler"
#define TA_OPTIONS_SAVE_BUTTON_TEXT "Sauvegarder"


/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QWidget>

//Fwd
class QLineEdit;
class QCheckBox;
class QButtonGroup;

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
    Class to manage the GUI for creating area
*/
class TA_Options : public QWidget{
    Q_OBJECT

    public:

        /*
            Default builder
         */
        TA_Options();

    private slots:

        /*
            Save and return to the main view
        */
        void save();

        /*
            Cancel and return to the main view
        */
        void cancel();

        /*
            Change the state of the password field
                @param state: true to activate the password, false else
        */
        void changePasswordState(bool state);

        /*
            Show or not the password
                @param show: true to show the password, false else
        */
        void showPassword(bool show);

        /*
            Checks the name of the server field
        */
        void checkName();

    private:
        QLineEdit *nameLineEdit;
        QLineEdit *passwordLineEdit;
        QCheckBox *showPasswordCheckbox;

        QButtonGroup* ratioButtons;
};

#endif // TA_OPTIONS_H
