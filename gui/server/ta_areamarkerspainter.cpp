/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 24 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_AreaMarkersPainter class drawing   |
  | and editing a markers sector                                            |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QPainter>
#include <QPen>
#include <QMouseEvent>
#include <QPaintEvent>

//Local includes
#include "communication/server/ta_infoclient.h"
#include "ta_areamarkerspainter.h"

/*
    Create an instance of TA_AreaMarkersPainter
        @param webcam : the webcam used to defining the sector
        @param color : the color to use for pen
        @param markerPath: the path of markers to use to initialize this instance
        @param unique : set as true if an unique marker have to be selected, false if it is a path of markers
*/
TA_AreaMarkersPainter::TA_AreaMarkersPainter(WebcamStruct* webcam, QColor color, QList<int>* markerPath, bool unique) : QWidget(){
    init(color);
    *markersPath = *markerPath;
    this->webcam = webcam;
    this->unique = unique;
    shapeComplete = markersPath->size() > 0;
}

/*
    Create an instance of TA_AreaMarkersPainter
        @param color : the color to use for pen
*/
TA_AreaMarkersPainter::TA_AreaMarkersPainter(QColor color) {
    init(color);
}

/*
    Destroy properly this class
*/
TA_AreaMarkersPainter::~TA_AreaMarkersPainter(){
    delete markersPath;
    delete sectorViewer;
}

/*****************************************************************************/
/******                             GETTER                              ******/
/*****************************************************************************/

/*
    Get the marker path
        @return: the marker path
*/
QList<int> *TA_AreaMarkersPainter::getMarkerPath(){
    QList<int>* path = new QList<int>;

    if (!shapeComplete){
        return path;
    }

    *path = *markersPath;

    return path;
}

/*****************************************************************************/
/******                             EVENT                               ******/
/*****************************************************************************/

/*
   Event to paint the entire surface of QPainter, draw
   points, background and outline
*/
void TA_AreaMarkersPainter::paintEvent(QPaintEvent *){
    QPainter painter(this);
    sectorViewer->setCurrentFrame(webcam->image);
    sectorViewer->paintImageAndSectors(&painter, size());
    drawShape(&painter);
    painter.end();
}

/*
    Draw a shape from a painter component
    @param painter : the painter object to draw the shape
*/
void TA_AreaMarkersPainter::drawShape(QPainter *painter){
    // List the non selected markers
    QList<int> nonSelected;
    foreach(int id, webcam->markers.keys()){
        if(!markersPath->contains(id))
            nonSelected << id;
    }

    float diffX = (float)size().width() / (float)webcam->image.size().width();
    float diffY = (float)size().height() / (float)webcam->image.size().height();

    // Draw the non selected markers with square
    painter->setPen(penSquare);
    foreach(int id, nonSelected){
        painter->drawRect(webcam->markers[id].x()*diffX-5,webcam->markers[id].y()*diffY-5,10,10);
    }

    // Draw the selected markers with triangle
    QVector<QPointF> points;
    foreach(int id, *markersPath){
        if(webcam->markers.contains(id)){
            QPointF triangle[4];
            triangle[0] = QPointF(webcam->markers[id].x()*diffX-7,webcam->markers[id].y()*diffY+7);
            triangle[1] = QPointF(webcam->markers[id].x()*diffX,webcam->markers[id].y()*diffY-7);
            triangle[2] = QPointF(webcam->markers[id].x()*diffX+7,webcam->markers[id].y()*diffY+7);
            triangle[3] = triangle[0];
            painter->drawPolyline(triangle,4);
            points << QPointF(webcam->markers[id].x()*diffX,webcam->markers[id].y()*diffY);
        }
    }

    if(unique)
        return;

    // Draw shapes
    if(shapeComplete){
        points << points.first();
        penLine.setStyle(Qt::DashLine);// Shape is drawn in dash line when its complete
        painter->setPen(penLine);
    }
    else {
        penLine.setStyle(Qt::SolidLine);
        painter->setPen(penLine);

        // Draw line with mouse position
        if(!markersPath->isEmpty()){
            int id = (*markersPath)[markersPath->size()-1];
            painter->drawLine(QPointF(webcam->markers[id].x()*diffX,webcam->markers[id].y()*diffY),mousePoint);
        }
    }

    painter->drawPolyline(points);// Draw shape
}

/*
    Initialize the class
*/
void TA_AreaMarkersPainter::init(QColor color) {
    markersPath = new QList<int>;
    shapeComplete = false;
    webcam = NULL;
    sectorViewer = NULL;
    setMouseTracking(true);
    setMinimumSize(1,1);    // bug if the background frame have a null size
    penSquare = QPen(color, 5, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    penLine = QPen(color, 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    connect(TA_Server::getInstance(),SIGNAL(changedMarkers(TA_InfoClient*,int)),this,SLOT(newMarkers(TA_InfoClient*,int)));
}

/*
    Manage the mouse move event for create and edit points
        @param event : event emited
*/
void TA_AreaMarkersPainter::mouseMoveEvent(QMouseEvent *event){
    if(!shapeComplete){
        mousePoint = event->pos();
        if(!markersPath->isEmpty())
            update();
    }
}

/*
   Manage the mouse press event for create and edit points
*/
void TA_AreaMarkersPainter::mousePressEvent(QMouseEvent *event){
    if (!shapeComplete || (shapeComplete && unique)){
        int id;
        if ((id = idOfMarkerToSelect(event->pos())) != -1){
            if (unique){
                markersPath->clear();
                *markersPath << id;
                shapeComplete = true;
            } else {
                if (!markersPath->isEmpty() && (*markersPath)[0] == id){
                    shapeComplete = true;
                } else {
                    *markersPath << id;
                }
            }
            update();   // call paintEvent
            emit newSelectedMarker();
        }
    }
}

/*****************************************************************************/
/******                             SETTER                              ******/
/*****************************************************************************/

/*
    Change the webcam concerned by the area outline
*/
void TA_AreaMarkersPainter::updateImage() {
    update();
}

/*
    Load the marker path and the webcam informations and draw it selected on screen
        @param webcam : the webcam's informations
        @param path : the marker path
        @param unique : set as true if an unique marker have to be selected, false if it is a path of markers
*/
void TA_AreaMarkersPainter::setWebcamAndPath(WebcamStruct* webcam, QList<int> *path, bool unique){
    *markersPath = *path;
    shapeComplete = (markersPath->size() == 0) ? false : true;
    this->webcam = webcam;
    if (sectorViewer != NULL) {
        delete sectorViewer;
    }
    sectorViewer = new TA_SectorViewer(webcam->client, webcam->id);
    emit TA_Server::getInstance()->unselectAllAreas();
    this->unique = unique;
    update();
}

/*****************************************************************************/
/******                               SLOTS                             ******/
/*****************************************************************************/

/*
    Refresh the markers positions
        @param client : the client informations
        @param webcamId : the id of the webcam
*/
void TA_AreaMarkersPainter::newMarkers(TA_InfoClient* client, int webcamId){
    if (webcam->client == client && webcam->id == webcamId) {
        update();
    }
}

/*****************************************************************************/
/******                             FUNCTIONS                           ******/
/*****************************************************************************/

/*
    Return index of the marker to select or -1 if it doesn't exist
        @param point : point to compare with marker's position
*/
int TA_AreaMarkersPainter::idOfMarkerToSelect(QPointF point){
    float diffX = (float)size().width() / (float)webcam->image.size().width();
    float diffY = (float)size().height() / (float)webcam->image.size().height();

    foreach(int id, webcam->markers.keys()) {
        if(abs((int)(webcam->markers[id].x() * diffX - point.x())) <= X_MARKER_CLICKED &&
                abs((int)(webcam->markers[id].y()*diffY - point.y())) <= Y_MARKER_CLICKED){
            if (markersPath->isEmpty() || (*markersPath)[0] == id || !markersPath->contains(id)) {
                return id;
            }
            return -1;
        }
    }
    return -1;
}
