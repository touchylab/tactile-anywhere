/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 13 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_AreaWebcamList to display     |
  | widget to show webcams of an area                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  | [-] 1.1 - Valentin COULON -- Version complete for Tactile Anywhere 1.0  |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_AREAWEBCAMSLIST_H
#define TA_AREAWEBCAMSLIST_H

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/

#define TA_AREAWEBCAMSLIST_NOT_AVAILABLE_WEBCAM ":/images/noAvailable.png"

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QWidget>

// Local includes
#include "entity/ta_server.h"

//Fwd
class TA_InfoClient;
class TA_SectorViewer;


/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
   Class to display all webcams of an area under list form.
*/
class TA_AreaWebcamsList : public QWidget{
    Q_OBJECT

    public:
        /*
            Display the sectors of an area
                @param area: the area's name
        */
        TA_AreaWebcamsList(QString area);

        /*
            Checks if this area have webcams
                @return : true if the webcam have no webcam, false else
        */
        bool isEmpty();

    public slots:
        /*
          Updates the webcams frame
          @param client : the client's webcam
          @param webcamId : the webcam identifier to update frame for
        */
        void changedFrame(TA_InfoClient* client, int webcamId);

        /*
            Updates the webcams state
        */
        void changedWebcams();

        /*
            Updates the view
        */
        void changedClient();

    private:
        int frameSide;
        QString area;
        QMap<WebcamStruct*, TA_SectorViewer*> imgMap; // the map binding a webcam structure to a sector Viewer
};

#endif // TA_AREAWEBCAMSLIST_H
