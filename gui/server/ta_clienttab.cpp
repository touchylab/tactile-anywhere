/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 28 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_ClientTab to display a widget      |
  | containing clients and webcams                                          |                                                           |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Valentin COULON -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_clienttab.h"

/*****************************************************************************/
/******                           CONSTRUCTOR                           ******/
/*****************************************************************************/

/*
    Default builder to create a right panel area.
*/
TA_ClientTab::TA_ClientTab() : QWidget() {
    layout = new QVBoxLayout;

    webcamsList = new TA_WebcamsList();
    layout->addWidget(webcamsList);

    connect(TA_Server::getInstance(),SIGNAL(changedClient()),this,SLOT(updateView()));
    connect(TA_Server::getInstance(),SIGNAL(changedWebcams(TA_InfoClient*,QVector<int>)),this,SLOT(updateView()));

    setLayout(layout);
}

/*****************************************************************************/
/******                              SLOTS                              ******/
/*****************************************************************************/

/*
    Updates the clients list
*/
void TA_ClientTab::updateView(){
    layout->removeWidget(webcamsList);
    delete webcamsList;
    webcamsList = new TA_WebcamsList();
    layout->addWidget(webcamsList);
	update();
}
