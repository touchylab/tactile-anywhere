/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 05 March 2014                                      |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_ClientWebcamList to display widget |
  | to show webcams of a client                                             |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QLayout>
#include <QLabel>
#include <QScrollArea>

//Local includes
#include "gui/ta_flowlayout.h"
#include "gui/ta_mainwindow.h"
#include "gui/ta_fonts.h"
#include "gui/server/ta_sectorviewer.h"
#include "gui/server/ta_servermainwidget.h"
#include "ta_clientwebcamslist.h"

/*
    Display the sectors of a client
        @param client: the client
*/
TA_ClientWebcamsList::TA_ClientWebcamsList(TA_InfoClient* client) : QWidget(){
    this->client = client;
    frameSide = TA_MainWindow::getInstance()->getScreenSize().width()/6;

    TA_FlowLayout* flowLayout = new TA_FlowLayout();

    QWidget *mainWidget= new QWidget();
    mainWidget->setLayout(flowLayout);

    QScrollArea *scrollArea = new QScrollArea();
    scrollArea->setWidgetResizable(true);
    scrollArea->setWidget(mainWidget);

    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addWidget(scrollArea,1);
    setLayout(mainLayout);

    //Get the clients webcams list
    foreach(WebcamStruct* webcam, *((*(TA_Server::getInstance()->getConfig()))[client])){
        QVBoxLayout *cellLayout = new QVBoxLayout();

        QLabel *camTitle = new QLabel(webcam->name);
        camTitle->setFont(TA_Fonts::getSubTitleFont());
        cellLayout->addWidget(camTitle,0,Qt::AlignHCenter);

        // label picture
        TA_SectorViewer* webcamPictureLabel = new TA_SectorViewer(client, webcam->id);

        imgMap[webcam] = webcamPictureLabel;
        cellLayout->addWidget(webcamPictureLabel,1,Qt::AlignCenter);
        webcamPictureLabel->setFixedSize(frameSide,frameSide);
        QImage frame = webcam->image;

        if(frame.size() == QSize(0,0) || !webcam->active){
            frame.load(TA_CLIENTWEBCAMSLIST_NOT_AVAILABLE_WEBCAM);
        }

        webcamPictureLabel->setCurrentFrame(frame.scaled(frameSide,frameSide));

        QWidget *w = new QWidget();
        w->setLayout(cellLayout);
        flowLayout->addWidget(w);
    }

    // Connections
    connect(TA_Server::getInstance(), SIGNAL(changedFrame(TA_InfoClient*,int)), this, SLOT(changedFrame(TA_InfoClient*,int)));
    connect(TA_Server::getInstance(), SIGNAL(changedWebcams(TA_InfoClient*,QVector<int>)), this, SLOT(changedWebcams()));

    // Frames
    TA_Server::getInstance()->askFrame(false);

    setLayout(mainLayout);
    this->setMinimumSize(frameSide * 1.8, frameSide * 1.8);
}

/*
    Checks if this client have webcams
        @return : true if the client have no webcam, false else
*/
bool TA_ClientWebcamsList::isEmpty(){
    return imgMap.isEmpty();
}

/*****************************************************************************/
/******                           SLOTS                                 ******/
/*****************************************************************************/

/*
  Updates the webcams frame
  @param client : the client's webcam
  @param webcamId : the webcam identifier to update frame for
*/
void TA_ClientWebcamsList::changedFrame(TA_InfoClient* client, int webcamId){
    if(imgMap.contains(TA_Server::getInstance()->getWebcamInfo(client,webcamId)))
        imgMap[TA_Server::getInstance()->getWebcamInfo(client,webcamId)]->setCurrentFrame(TA_Server::getInstance()->getWebcamInfo(client,webcamId)->image.scaled(frameSide,frameSide));
    update();
}

/*
    Updates the webcams state
*/
void TA_ClientWebcamsList::changedWebcams(){
    QMap<WebcamStruct*, TA_SectorViewer*>::Iterator webcamIterator;
    for(webcamIterator = imgMap.begin(); webcamIterator != imgMap.end(); ++webcamIterator){
        WebcamStruct* wb = webcamIterator.key();
        if(!wb->active){
            TA_SectorViewer* qb = webcamIterator.value();
            qb->setCurrentFrame(QImage(TA_CLIENTWEBCAMSLIST_NOT_AVAILABLE_WEBCAM).scaled(frameSide,frameSide));
        }
    }
}
