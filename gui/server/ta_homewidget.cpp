/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 12 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_SystemEventManager namespace       |
  | offering to send events to the system. These events can be get by other |
  | programs                                                                |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Valentin COULON -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/
/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QLabel>
#include <QIcon>
#include <QPushButton>
#include <QVBoxLayout>

//Local includes
#include "gui/server/ta_clientswebcamslist.h"
#include "gui/server/ta_servermainwidget.h"
#include "entity/ta_server.h"
#include "gui/ta_fonts.h"
#include "ta_homewidget.h"

/*
    Default builder
*/
TA_HomeWidget::TA_HomeWidget() : QWidget() {
    QVBoxLayout* layout = new QVBoxLayout;

    QLabel* title = new QLabel(TA_HOMEWIDGET_TITLE);
    title->setFont(TA_Fonts::getTitleFont());
    title->setAlignment(Qt::AlignHCenter);
    layout->addWidget(title);

    TA_ClientsWebcamsList* clientsWebcamsList = new TA_ClientsWebcamsList();
    layout->addWidget(clientsWebcamsList, 10);

    startButton = new QPushButton(QIcon(TA_HOMEWIDGET_START_BUTTON_ICON), TA_HOMEWIDGET_START_BUTTON_TEXT);
    startButton->setFont(TA_Fonts::getButtonFont());
    connect(startButton, SIGNAL(clicked()), this, SLOT(startDetection()));
    emit TA_Server::getInstance()->showAllAreas();
    layout->addWidget(startButton);
    setLayout(layout);

    //disable start detection button if needed
    updateStartButton();
    connect(TA_Server::getInstance(),SIGNAL(changedArea()),this,SLOT(updateStartButton()));
}


/*****************************************************************************/
/******                               SLOTS                             ******/
/*****************************************************************************/

/*
    Define what happens when the detection starts. It launches the detection
    process and its own GUI.
*/
void TA_HomeWidget::startDetection() {
    TA_ServerMainWidget::getInstance()->openDetectionRunningScreen();
}

/*
    Disable the start detection button
*/
void TA_HomeWidget::updateStartButton() {
    if(TA_Server::getInstance()->getAreas()->isEmpty()){
        startButton->setEnabled(false);
    } else {
        startButton->setEnabled(true);
    }
}
