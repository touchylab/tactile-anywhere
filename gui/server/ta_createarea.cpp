/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 12 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_SystemEventManager namespace       |
  | offering to send events to the system. These events can be get by other |
  | programs                                                                |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu LOSLIER -- Initial version                            |
  | [-] 1.1 - Valentin COULON -- Version complete for Tactile Anywhere 1.0  |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QLayout>
#include <QFormLayout>
#include <QLabel>
#include <QPushButton>
#include <QColorDialog>
#include <QLineEdit>
#include <QColor>
#include <QPalette>
#include <QRegExpValidator>
#include <QListWidget>

//Local includes
#include "entity/ta_server.h"
#include "gui/ta_mainwindow.h"
#include "gui/server/ta_servermainwidget.h"
#include "gui/ta_advancebutton.h"
#include "gui/ta_fonts.h"
#include "gui/ta_statusbar.h"
#include "ta_createarea.h"

/*
    Default Constructor
        @param areaName : name of the area
        @param isGroupArea : true if the area is a group area
        @param children : list of children if it is a group area
*/
TA_CreateArea::TA_CreateArea(QString areaName, bool isGroupArea, QList<QString> children) : QWidget() {
    oldAreaName = areaName;
    QString title;
    if (areaName == "") {
        //creation
        color = QColor(Qt::green);
        this->isGroupArea = isGroupArea;
        title = this->isGroupArea ? TA_CREATEAREA_CREATE_GROUP_TITLE : TA_CREATEAREA_CREATE_TITLE;
    } else {
        //edition
        color = TA_Server::getInstance()->getAreaInfo(areaName)->color;
        children = TA_Server::getInstance()->getAreaInfo(areaName)->down;
        this->isGroupArea = TA_Server::getInstance()->getAreaInfo(areaName)->type == GROUP_AREA;
        title = this->isGroupArea ? TA_CREATEAREA_EDIT_GROUP_TITLE : TA_CREATEAREA_EDIT_TITLE;
    }

    QVBoxLayout *mainLayout = new QVBoxLayout();

    //Header
    QVBoxLayout *headerLayout = new QVBoxLayout();
    QLabel *titleLabel = new QLabel(title);
    titleLabel->setFont(TA_Fonts::getTitleFont());
    QPushButton *advanceButton = new TA_AdvanceButton();
    headerLayout->addWidget(titleLabel, 0, Qt::AlignHCenter | Qt::AlignTop);
    headerLayout->addWidget(advanceButton,0, Qt::AlignHCenter| Qt::AlignTop);
    connect(advanceButton, SIGNAL(toggled(bool)), this, SLOT(displayActions(bool)));

    //Form layout
    QFormLayout *formLayout = new QFormLayout();

    // Area name
    nameLineEdit = new QLineEdit(areaName);
    nameLineEdit->setFont(TA_Fonts::getDefaultFont());
    nameLineEdit->setPlaceholderText(TA_CREATEAREA_NAME_PLACEHOLDER);
    QRegExpValidator *validator = new QRegExpValidator(QRegExp(TA_CREATEAREA_NAME_VALIDATION_REGEXP), this);
    nameLineEdit->setValidator(validator);

    //Color layout
    QHBoxLayout *colorLayout = new QHBoxLayout();
    colorLabel = new QLabel("");
    colorLabel->setFont(TA_Fonts::getDefaultFont());
    colorLabel->setFixedWidth(50);
    colorLabel->setAutoFillBackground(true);
    colorPalette.setColor(QPalette::Background, color);
    colorLabel->setPalette(colorPalette);
    QPushButton *colorButton = new QPushButton("...");
    colorButton->setFont(TA_Fonts::getButtonFont());
    connect(colorButton, SIGNAL(clicked()), this, SLOT(setColor()));

    // Build form layout
    QLabel* nomZoneLabel = new QLabel(TA_CREATEAREA_NAME_LABEL);
    nomZoneLabel->setFont(TA_Fonts::getDefaultFont());
    formLayout->addRow(nomZoneLabel, nameLineEdit);
    colorLayout->addWidget(colorLabel);
    colorLayout->addWidget(colorButton);
    colorLayout->addStretch();
    QLabel* couleurLabel = new QLabel(TA_CREATEAREA_COLOR_LABEL);
    couleurLabel->setFont(TA_Fonts::getDefaultFont());
    formLayout->addRow(couleurLabel, colorLayout);
    if (this->isGroupArea) {
        QLabel* zonesLabel = new QLabel(TA_CREATEAREA_CHILDREN_LABEL);
        zonesLabel->setFont(TA_Fonts::getDefaultFont());
        formLayout->addRow(zonesLabel, createChildrenSelection(children));
    }

    // Actions
    QVBoxLayout* actionsLayout = new QVBoxLayout();
    QLabel* actionLabel = new QLabel(TA_CREATEAREA_ACTION_LABEL);
    QFont font = TA_Fonts::getDefaultFont();
    font.setBold(true);
    actionLabel->setFont(font);
    actionsLayout->addWidget(actionLabel);

    urlLineEdit = new QLineEdit();
    urlLineEdit->setFont(TA_Fonts::getDefaultFont());
    urlLineEdit->setPlaceholderText(TA_CREATEAREA_URL_PLACEHOLDER);
    fileLineEdit = new QLineEdit();
    fileLineEdit->setPlaceholderText(TA_CREATEAREA_FILE_PLACEHOLDER);
    fileLineEdit->setFont(TA_Fonts::getDefaultFont());
    commandLineEdit = new QLineEdit();
    commandLineEdit->setPlaceholderText(TA_CREATEAREA_COMMAND_LINE_PLACEHOLDER);
    commandLineEdit->setFont(TA_Fonts::getDefaultFont());

    if(TA_Server::getInstance()->getAreaInfo(areaName)){
        urlLineEdit->setText(TA_Server::getInstance()->getAreaInfo(areaName)->url);
        fileLineEdit->setText(TA_Server::getInstance()->getAreaInfo(areaName)->file);
        commandLineEdit->setText(TA_Server::getInstance()->getAreaInfo(areaName)->command);
    }

    QFormLayout* formActionLayout = new QFormLayout();
    QLabel* openWebPageLabel = new QLabel(TA_CREATEAREA_URL_LABEL);
    openWebPageLabel->setFont(TA_Fonts::getDefaultFont());
    QLabel* openFileLabel = new QLabel(TA_CREATEAREA_FILE_LABEL);
    openFileLabel->setFont(TA_Fonts::getDefaultFont());
    QLabel* runSystemCommandLabel = new QLabel(TA_CREATEAREA_COMMAND_LINE_LABEL);
    runSystemCommandLabel->setFont(TA_Fonts::getDefaultFont());
    formActionLayout->addRow(openFileLabel, fileLineEdit);
    formActionLayout->addRow(openWebPageLabel, urlLineEdit);
    formActionLayout->addRow(runSystemCommandLabel, commandLineEdit);
    actionsLayout->addLayout(formActionLayout);
    actionsWidget = new QWidget;
    actionsWidget->setLayout(actionsLayout);
    actionsWidget->hide();

    QVBoxLayout *middleLayout = new QVBoxLayout();
    middleLayout->addStretch();
    middleLayout->addLayout(formLayout);
    middleLayout->addStretch();
    middleLayout->addWidget(actionsWidget);
    middleLayout->addStretch();

    QHBoxLayout *centralLayout = new QHBoxLayout();
    centralLayout->addStretch(1);
    centralLayout->addLayout(middleLayout,4);
    centralLayout->addStretch(1);

    //Footer
    QHBoxLayout *footerLayout = new QHBoxLayout();
    QHBoxLayout *buttonLayout = new QHBoxLayout();

    QPushButton *cancelButton = new QPushButton(TA_CREATEAREA_CANCEL_BUTTON_TEXT);
    cancelButton->setFont(TA_Fonts::getButtonFont());
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(cancel()));
    buttonLayout->addStretch();
    buttonLayout->addWidget(cancelButton, 0, Qt::AlignTop);

    QPushButton *saveButton = new QPushButton(TA_CREATEAREA_SAVE_BUTTON_TEXT);
    saveButton->setFont(TA_Fonts::getButtonFont());
    connect(saveButton, SIGNAL(clicked()), this, SLOT(saveArea()));
    buttonLayout->addStretch();
    buttonLayout->addWidget(saveButton, 0, Qt::AlignTop);

    if(!isGroupArea){
        QPushButton *continueButton = new QPushButton(TA_CREATEAREA_CONTINUE_BUTTON_TEXT);
        continueButton->setFont(TA_Fonts::getButtonFont());
        connect(continueButton, SIGNAL(clicked()), this, SLOT(continueCreation()));
        buttonLayout->addStretch();
        buttonLayout->addWidget(continueButton, 0, Qt::AlignTop);
    }

    buttonLayout->addStretch();
    footerLayout->addLayout(buttonLayout);

    mainLayout->addLayout(headerLayout);
    mainLayout->addLayout(centralLayout);
    mainLayout->addLayout(footerLayout);

    QTimer::singleShot(0, nameLineEdit, SLOT(setFocus()));

    setLayout(mainLayout);
}


/*****************************************************************************/
/******                             SLOTS                               ******/
/*****************************************************************************/

/*
    Set the color of the panel
*/
void TA_CreateArea::setColor(){
    color = QColorDialog::getColor(color, this, TA_CREATEAREA_COLOR_PANEL_TITLE);
    if (color.isValid()){
         colorPalette.setColor(QPalette::Background, color);
         colorLabel->setPalette(colorPalette);
         colorLabel->setFont(TA_Fonts::getDefaultFont());
    }
}

/*
    Save the area and quit
*/
void TA_CreateArea::saveArea() {
    if(!validConfiguration()){
        return;
    }
    QString areaName = nameLineEdit->text();
    if (oldAreaName != "") {
        //Edit
        if (oldAreaName != areaName) {
            TA_Server::getInstance()->renameArea(oldAreaName, areaName);
        }
        TA_Server::getInstance()->getAreaInfo(areaName)->color = color;
        if (isGroupArea) {
            foreach(QString child , TA_Server::getInstance()->getAreaInfo(areaName)->down){
                TA_Server::getInstance()->unLinkArea(areaName,child);
            }

            foreach(QString child , getChildren()){
                TA_Server::getInstance()->linkArea(areaName,child);
            }
        }
    } else {
        //Create
        if (isGroupArea) {
            QList<QString> test = getChildren();
            TA_Server::getInstance()->addGroupArea(areaName, test, color);
        } else {
            TA_Server::getInstance()->addArea(areaName, color);
        }
    }

    // Actions
    TA_Server::getInstance()->getAreaInfo(areaName)->file = fileLineEdit->text();
    TA_Server::getInstance()->getAreaInfo(areaName)->url = urlLineEdit->text();
    TA_Server::getInstance()->getAreaInfo(areaName)->command = commandLineEdit->text();

    TA_ServerMainWidget::getInstance()->openHomeScreen();
}

/*
    Select if actions are displayed
    @param show : true to display actions, false to hise
*/
void TA_CreateArea::displayActions(bool show){
    if (show) {
        actionsWidget->setVisible(true);
    } else {
        actionsWidget->hide();
    }
}

/*
    Cancel the creation
*/
void TA_CreateArea::continueCreation(){
    if (!validConfiguration()) {
        return;
    }
    TempAreaStruct areaStruct;
    areaStruct.name = nameLineEdit->text();
    areaStruct.oldname = oldAreaName;
    areaStruct.color = color;
    areaStruct.url = urlLineEdit->text();
    areaStruct.file = fileLineEdit->text();
    areaStruct.command = commandLineEdit->text();
    TA_ServerMainWidget::getInstance()->openAddWebcamsToArea(areaStruct);
}

/*
    Continue the creation
*/
void TA_CreateArea::cancel() {
    TA_ServerMainWidget::getInstance()->openHomeScreen();
}

/*****************************************************************************/
/******                           PRIVATES                              ******/
/*****************************************************************************/

/*
    check if the confiuration is valid
    @return : true, configuration valid, false, configuration invalid
*/
bool TA_CreateArea::validConfiguration(){
    if (!nameLineEdit->hasAcceptableInput()) {
        TA_MainWindow::getInstance()->getStatusBar()->showMessage(TA_CREATEAREA_NAME_INVALID_ERROR, "Error");
        nameLineEdit->setStyleSheet(TA_CREATEAREA_ERROR_LINE_EDIT_CSS);
        return false;
    }
    QString areaName = nameLineEdit->text();
    if ((oldAreaName == "" || oldAreaName != areaName) && TA_Server::getInstance()->getAreaInfo(areaName) != NULL) {
        TA_MainWindow::getInstance()->getStatusBar()->showMessage(TA_CREATEAREA_NAME_NOT_UNIQUE_ERROR, "Error");
        nameLineEdit->setStyleSheet(TA_CREATEAREA_ERROR_LINE_EDIT_CSS);
        return false;
    }
    return true;
}

/*
    Create a dropList with the children
    @param children : list of children to include in the dropList
    @return : QWidget created
*/
QWidget *TA_CreateArea::createChildrenSelection(QList<QString> children) {
    QVBoxLayout *childrenSelected = new QVBoxLayout();
    QVBoxLayout *childrenUnselected = new QVBoxLayout;

    QLabel* selectionnesLabel = new QLabel(TA_CREATEAREA_CHILDREN_SELECTED_LABEL);
    QLabel* nonSelectionneesLabel = new QLabel(TA_CREATEAREA_CHILDREN_UNSELECTED_LABEL);
    selectionnesLabel->setFont(TA_Fonts::getDefaultFont());
    nonSelectionneesLabel->setFont(TA_Fonts::getDefaultFont());

    childrenSelected->addWidget(selectionnesLabel);
    childrenUnselected->addWidget(nonSelectionneesLabel);

    childrenList = new QListWidget;
    childrenList->setDragEnabled(true);
    childrenList->setDragDropMode(QAbstractItemView::DragDrop);
    childrenList->setDefaultDropAction(Qt::MoveAction);
    childrenList->viewport()->setAcceptDrops(true);
    childrenList->setDropIndicatorShown(true);
    QListWidget *noChildrenList = new QListWidget;
    noChildrenList->setDragEnabled(true);
    noChildrenList->setDragDropMode(QAbstractItemView::DragDrop);
    noChildrenList->setDefaultDropAction(Qt::MoveAction);
    noChildrenList->viewport()->setAcceptDrops(true);
    noChildrenList->setDropIndicatorShown(true);

    QList<QString> unselectableAreas = getUnselectableAreas();
    foreach (QString areaName, TA_Server::getInstance()->getAreas()->keys()) {
        if (!unselectableAreas.contains(areaName)) {
            if (children.contains(areaName)) {
                childrenList->addItem(createChildItem(areaName));
            } else {
                noChildrenList->addItem(createChildItem(areaName));
            }
        }
    }
    childrenSelected->addWidget(childrenList);
    childrenUnselected->addWidget(noChildrenList);

    QHBoxLayout *childrenSelectionLayout = new QHBoxLayout;
    childrenSelectionLayout->addLayout(childrenSelected);
    childrenSelectionLayout->addLayout(childrenUnselected);

    QWidget* childrenSelection = new QWidget;
    childrenSelection->setLayout(childrenSelectionLayout);
    return childrenSelection;
}

/*
    Create a QListWidgetItem item for an area child
    @param areaName : the name of the area child
    @return : the QListWidgetItem created
*/
QListWidgetItem *TA_CreateArea::createChildItem(QString areaName) {
    QListWidgetItem *item = new QListWidgetItem(areaName);
    QPixmap pixmap(50, 50);
    pixmap.fill(TA_Server::getInstance()->getAreaInfo(areaName)->color);
    item->setIcon(QIcon(pixmap));
    return item;
}

/*
    Get the list of children setelected
    @return : QList<String> children
*/
QList<QString> TA_CreateArea::getChildren() {
    QList<QString> children;
    for(int i = 0; i < childrenList->count(); i++) {
        children.append(childrenList->item(i)->text());
    }
    return children;
}

/*
    Get the list of area who can't be selected like child (like the parents or the area himself)
    @return : QList<String> unselectable areas
*/
QList<QString> TA_CreateArea::getUnselectableAreas() {
    if (oldAreaName == "") {
        return QList<QString>();
    }
    QList<QString> unselectableList;
    QList<QString> parents;

    parents.append(oldAreaName);
    while(!parents.isEmpty()) {
        QString parent = parents.takeFirst();
        unselectableList.append(parent);
        foreach (QString areaName, TA_Server::getInstance()->getAreaInfo(parent)->up) {
            if (!unselectableList.contains(areaName)) {
                parents.append(areaName);
            }
        }
    }
    return unselectableList;
}
