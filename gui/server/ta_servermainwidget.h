/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 10 February 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_DataBeacon class containing   |
  | the informations about the client and the server coordinates            |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Valentin COULON -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_SERVERMAINWIDGET_H
#define TA_SERVERMAINWIDGET_H

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/

#define TA_SERVERMAINWIDGET_NAME_FORM_HELP_FILE "qrc:///html/servernameform.html"

#define TA_SERVERMAINWIDGET_HOME_HELP_FILE "qrc:///html/serverhome.html"

#define TA_SERVERMAINWIDGET_OPTIONS_HELP_FILE "qrc:///html/serveroptions.html"

#define TA_SERVERMAINWIDGET_ADD_AREA_HELP_FILE "qrc:///html/serveraddarea.html"
#define TA_SERVERMAINWIDGET_ADD_GROUP_AREA_HELP_FILE "qrc:///html/serveraddgrouparea.html"

#define TA_SERVERMAINWIDGET_WEBCAM_CHOICE_HELP_FILE "qrc:///html/serverwebcamchoice.html"

#define TA_SERVERMAINWIDGET_DRAW_AREA_HELP_FILE "qrc:///html/serverdrawarea.html"

#define TA_SERVERMAINWIDGET_DETECTION_RUNNING_HELP_FILE "qrc:///html/serverdetectionrunning.html"

#define TA_SERVERMAINWIDGET_MANY_INFO_WEBCAM_HELP_FILE "qrc:///html/servermanyinfowebcam.html"

#define TA_SERVERMAINWIDGET_INFO_AREA_HELP_FILE "qrc:///html/serverinfoarea.html"

#define TA_SERVERMAINWIDGET_INFO_CLIENT_HELP_FILE "qrc:///html/serverinfoclient.html"

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QWidget>

//Local includes
#include "gui/server/ta_drawarea.h"

//Fwd
class QHBoxLayout;
class TA_InfoClient;


// Local includes


/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
    TA_MainWindow is a class representing the server IHM in Tactile Anywhere.
*/
class TA_ServerMainWidget : public QWidget {
    Q_OBJECT

public :
    /*********************/
    /*      Getters      */
    /*********************/

    /*
        Gets the unique instance of this class. initialize() must be called before
            @return : the unique instance of this class or NULL if this class is not initialized
    */
    static TA_ServerMainWidget* getInstance();

    /*********************/
    /*      Methods      */
    /*********************/
    /*
        Initializes an instance of this class as a singleton
    */
    static void initialize();

public slots :

    /*
        open the Name Form
    */
    void openNameForm();

    /*
        Open the Home Screen
            @param forceLeftPanel : set as true to force a new left panel, false else
    */
    void openHomeScreen(bool forceLeftPanel = false) ;

    /*
        open the Options
    */
    void openOptions();

    /*
        Load the creation of area GUI
            @param isGroupArea : defining if it's an area group or not
            @param children : a list of children areas
    */
    void openCreateArea(bool isGroupArea = false, QList<QString> children = QList<QString>());

    /*
        Load the widget screen allowing to edit to an area
            @param areaName : the name of the choosen area
            @param isGroupArea : defining if it's an area group or not
            @param children : a list of children areas
    */
    void openEditArea(QString areaName = "", bool isGroupArea = false, QList<QString> children = QList<QString>());

    /*
        Load the widget screen allowing to add webcams to an area
            @param area : the area's informations
    */
    void openAddWebcamsToArea(TempAreaStruct area);

    /*
        Load the draw area screen for a specified area name
            @param area : the area's informations
    */
    void openDrawArea(TempAreaStruct area);

    /*
        Load the detection running screen
    */
    void openDetectionRunningScreen();

    /*
        Load the center widget with the informations about a choosen webcam.
            @param infoClient : the client informations
            @param webcamId : the webcam identifier
    */
    void openManyInformationsWebcam(TA_InfoClient* infoClient, int webcamId);

    /*
        Load the center widget with the informations about a choosen area
            @param areaName : the area's name
    */
    void openInfoArea(QString areaName);

    /*
        Load the center widget with the informations about a choosen client
            @param infoClient : the client to display
    */
    void openInfoClient(TA_InfoClient* infoClient);

private :

    /*
        Private builder of the class
    */
    TA_ServerMainWidget();

    /*
        Remove the specified widget out of the main layout
            @param widget : the widget to remove
    */
    void delWidget(QWidget **widget);

    /*
        Set left widget removing old layout and create new layout
        containing widget
            @param : the widget to integrated in left of window
    */
    void setLeftWidget(QWidget *widget);

    /*
        Set center widget removing old layout and create new layout
        containing widget
            @param : the widget to integrated in center of window
    */
    void setCenterWidget(QWidget *widget);

    /*
        Set right widget removing old layout and create new layout
        containing widget
            @param : the widget to integrated in right of window
    */
    void setRightWidget(QWidget *widget);

    //Layouts
    QHBoxLayout* mainLayout; // main layout

    QWidget *leftWidget = NULL; // widget for the left side
    QWidget *centerWidget = NULL; // widget for the middle side
    QWidget *rightWidget = NULL; // widget for the right side

    static TA_ServerMainWidget* singleton; // Single instance of TA_MainWindow
};

#endif // TA_MAINWINDOW_H
