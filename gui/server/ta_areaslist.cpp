/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 12 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_SystemEventManager namespace       |
  | offering to send events to the system. These events can be get by other |
  | programs                                                                |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Valentin COULON -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QToolButton>
#include <QAction>
#include <QLabel>
#include <QMap>
#include <QMessageBox>
#include <QHBoxLayout>
#include <QSignalMapper>

//Local includes
#include "entity/ta_server.h"
#include "gui/server/ta_servermainwidget.h"
#include "gui/ta_fonts.h"
#include "gui/ta_mainwindow.h"
#include "gui/ta_statusbar.h"
#include "ta_areaslist.h"

/*
    Default builder
*/
TA_AreasList::TA_AreasList(bool inlineButtons) : QListWidget() {
    QList<QString> groupAreas;
    setSelectionMode(QAbstractItemView::MultiSelection);
    foreach (QString areaName, TA_Server::getInstance()->getAreas()->keys()) {
        if (TA_Server::getInstance()->getAreaInfo(areaName)->type == NORMAL_AREA) {
            addArea(areaName, inlineButtons);
        } else {
            groupAreas.append(areaName);
        }
    }
    connect(this, SIGNAL(itemSelectionChanged()), this, SLOT(emitAreaSelectionChanged()));
    connect(TA_Server::getInstance(), SIGNAL(showAllAreas()), this, SLOT(showAll()));
    connect(TA_Server::getInstance(), SIGNAL(showAllAreasNoChange()), this, SLOT(showAllNoChange()));
    connect(TA_Server::getInstance(), SIGNAL(unselectAllAreas()), this, SLOT(unselectAll()));
    connect(TA_Server::getInstance(), SIGNAL(selectArea(QString)), this, SLOT(selectArea(QString)));
    connect(TA_Server::getInstance(), SIGNAL(selectAndShowAreas(QList<QString>)),
            this, SLOT(selectAndShowAreaList(QList<QString>)));
    if (groupAreas.isEmpty()) {
        return;
    }
    groupAreaTitle = new QListWidgetItem(TA_AREASLIST_GROUP_AREA_TITLE);
    groupAreaTitle->setFont(TA_Fonts::getDefaultFont());
    groupAreaTitle->setFlags( groupAreaTitle->flags() & !Qt::ItemIsEditable & !Qt::ItemIsSelectable );
    addItem(groupAreaTitle);
    foreach (QString areaName, groupAreas) {
        addArea(areaName, inlineButtons);
    }
}

/*
    Remove the specified area from its name.
    @param areaName : the name of the area to remove
    @param validationMessage : if it's true, display a confirmation message in a popup
*/
void TA_AreasList::removeArea(QString areaName, bool validationMessage) {
    QMessageBox::StandardButton reply;
    if (validationMessage) {
        QString message = QString(TA_AREASLIST_DELETE_AREA_CONFIRMATION_MESSAGE).arg(areaName);
        reply = QMessageBox::question(this, TA_AREASLIST_DELETE_AREA_CONFIRMATION_TITLE, message,
                                      QMessageBox::Yes|QMessageBox::Cancel);
       if (reply != QMessageBox::Yes) {
           return;
       }
    }
    TA_Server::getInstance()->removeArea(areaName);
    QListWidgetItem* item = areaNames_itemsMap[areaName];
    areaNames_itemsMap.remove(areaName);
    items_areaNamesMap.remove(item);
    delete item;
    deleteGroupAreaTitleIfIsLast();
}

/*
    Delete all selected areas
*/
void TA_AreasList::deleteAllSelected() {
    int nbAreaSelected = selectedItems().count();
    switch (nbAreaSelected) {
    case 0 :
        TA_MainWindow::getInstance()->getStatusBar()->showMessage(TA_AREASLIST_NO_WEBCAM_SELECTED_MESSAGE, "Error");
        break;
    case 1 :
        removeArea(items_areaNamesMap[selectedItems().first()]);
        break;
    default :
        QString message =   QString(TA_AREASLIST_DELETE_MULTIPLE_AREA_CONFIRMATION_MESSAGE).arg(nbAreaSelected);
        QMessageBox::StandardButton reply = QMessageBox::question(this,TA_AREASLIST_DELETE_AREA_CONFIRMATION_TITLE, message,
                                                                  QMessageBox::Yes|QMessageBox::Cancel);
        if (reply != QMessageBox::Yes) {
            return;
        }
        foreach (QListWidgetItem* item, selectedItems()) {
            removeArea(items_areaNamesMap[item], false);
        }
        deleteGroupAreaTitleIfIsLast();
    }
}


/*
    Delete the name of the areas group if it is the latest in the list
*/
void TA_AreasList::deleteGroupAreaTitleIfIsLast() {
    if (groupAreaTitle != NULL && this->item(this->count() - 1) == groupAreaTitle) {
        delete groupAreaTitle;
        groupAreaTitle = NULL;
    }
}

/*
    Display the informations about an area
        @param areaName : the area's name
*/
void TA_AreasList::launchInfoArea(QString areaName){
    TA_ServerMainWidget::getInstance()->openInfoArea(areaName);
}

/*
    Display only areas passed in parameter. Also the areas are selected
        @param areas : areas to display
*/
void TA_AreasList::selectAndShowAreaList(QList<QString> areas) {
    showAll();
    foreach (QListWidgetItem* item, items_areaNamesMap.keys()) {
        if (areas.contains(items_areaNamesMap[item])) {
            item->setSelected(true);
        } else {
            item->setHidden(true);
        }
    }
    if (groupAreaTitle != NULL) {
        groupAreaTitle->setHidden(true);
    }
}



/*
    Display all the areas and unselect them
*/
void TA_AreasList::showAll() {
    for (int i = 0; i < this->count(); i++) {
        this->item(i)->setHidden(false);
        this->item(i)->setSelected(false);
    }
}

/*
    Display all the areas but don't change the selection
*/
void TA_AreasList::showAllNoChange(){
    for (int i = 0; i < this->count(); i++) {
        this->item(i)->setHidden(false);
    }
}

/*
    Unselect all the areas
*/
void TA_AreasList::unselectAll() {
    for (int i = 0; i < this->count(); i++) {
        this->item(i)->setSelected(false);
    }
    emit TA_Server::getInstance()->areaSelectionChanged(QList<QString>());
}

/*
    Select an area
        @param area: area's name
*/
void TA_AreasList::selectArea(QString area){
    this->areaNames_itemsMap[area]->setSelected(true);
}

/*
    Emit a AreaSelectionChanged signal on the TA_Server
*/
void TA_AreasList::emitAreaSelectionChanged() {
    QList<QString> areaNames;
    foreach (QListWidgetItem* item, selectedItems()) {
        areaNames.append(items_areaNamesMap[item]);
    }
    emit TA_Server::getInstance()->areaSelectionChanged(areaNames);
}


/*
    Create a group of selected areas
*/
void TA_AreasList::createAreaGroup() {
    QList<QString> areaNames;
    foreach (QListWidgetItem* item, selectedItems()) {
        areaNames.append(items_areaNamesMap[item]);
    }
    TA_ServerMainWidget::getInstance()->openCreateArea(true, areaNames);
}

/*
    Add an area from its name
    @param areaName : the name of the area to add
*/
void TA_AreasList::addArea(QString areaName, bool inlineButtons) {
    QListWidgetItem *item = new QListWidgetItem();

    //color icon
    QPixmap pixmap(50, 50);
    pixmap.fill(TA_Server::getInstance()->getAreaInfo(areaName)->color);
    item->setIcon(QIcon(pixmap));


    addItem(item);
    items_areaNamesMap[item] = areaName;
    areaNames_itemsMap[areaName] = item;
    QHBoxLayout *layout= new QHBoxLayout();

    //label
    QLabel *label = new QLabel(areaName);
    label->setFont(TA_Fonts::getDefaultFont());
    layout->addWidget(label);

    if (inlineButtons) {
        //info button
        QToolButton *infoButton = new QToolButton();
        infoButton->setIcon(QIcon(TA_AREASLIST_INFO_BUTTON_ICON));
        QSignalMapper* infoMapper = new QSignalMapper();
        connect(infoButton, SIGNAL(clicked()), infoMapper, SLOT(map()));
        infoMapper->setMapping(infoButton, areaName);
        connect(infoMapper, SIGNAL(mapped(QString)), this, SLOT(launchInfoArea(QString)));
        layout->addWidget(infoButton);

        //edit button
        QToolButton *editButton = new QToolButton();
        editButton->setIcon(QIcon(TA_AREASLIST_EDIT_BUTTON_ICON));
        QSignalMapper* editMapper = new QSignalMapper();
        connect(editButton, SIGNAL(clicked()), editMapper, SLOT(map()));
        editMapper->setMapping(editButton, areaName);
        connect(editMapper, SIGNAL(mapped(QString)), TA_ServerMainWidget::getInstance(), SLOT(openEditArea(QString)));
        layout->addWidget(editButton);

        //del button
        QToolButton *delButton = new QToolButton();
        delButton->setIcon(QIcon(TA_AREASLIST_DELETE_BUTTON_ICON));
        QSignalMapper* delMapper = new QSignalMapper();
        connect(delButton, SIGNAL(clicked()), delMapper, SLOT(map()));
        delMapper->setMapping(delButton, areaName);
        connect(delMapper, SIGNAL(mapped(QString)), this, SLOT(removeArea(QString)));
        layout->addWidget(delButton);
    }

    QWidget *widget = new QWidget();
    widget->setLayout(layout);
    item->setSizeHint(widget->sizeHint());
    setItemWidget(item, widget);
}

