/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 12 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_WebcamList offering a widget to    |
  | manage client's webcams under list form.                                |                                                                        |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Valentin COULON -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_WEBCAMSLIST_H
#define TA_WEBCAMSLIST_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/
#define TA_WEBCAMSLIST_TITLE "    %1" // arg1 : webcam name

#define TA_WEBCAMSLIST_DISCONNECT_ICON ":/images/disconnected.png"
#define TA_WEBCAMSLIST_UNDISCONNECTED_ICON ":/images/unlock.png"

#define TA_WEBCAMSLIST_DELETE_BUTTON_ICON ":/images/exit.png"
#define TA_WEBCAMSLIST_DELETE_BUTTON_TOOL_TIP "Couper la connexion"

#define TA_WEBCAMSLIST_SHOW_BUTTON_ICON ":/images/show.png"
#define TA_WEBCAMSLIST_SHOW_BUTTON_TOOL_TIP "Masquer la webcam"

#define TA_WEBCAMSLIST_NOT_SHOW_BUTTON_ICON ":/images/notshow.png"
#define TA_WEBCAMSLIST_NOT_SHOW_BUTTON_TOOL_TIP "Afficher la webcam"

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QListWidget>

// Local includes
#include "entity/ta_server.h"

//Fwd
class QListWidgetItem;
class TA_InfoClient;

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
    Class to get webcams under list form.
*/
class TA_WebcamsList: public QListWidget {
    Q_OBJECT
    public:
        /*
            Default builder to create a webcams list about all clients connected to the server.
        */
        TA_WebcamsList();

    private :
        /*
            Add a client in the list
                @param infoClient : the client to add
                @param webcams : the webcams owning by the client
        */
        void addClient(TA_InfoClient* infoClient, QList<WebcamStruct*> webcams);

        /*
            Add a webcam in the list
                @param infoClient: the client owning the webcam to add
                @param webcamId: the concerned webcam's id
        */
        void addWebcam(TA_InfoClient* infoClient, WebcamStruct *webcam);

    private slots :
        /*
            Opens informations about a webcam
                @param infoClient: the client owning the concerned webcam
                @param webcamId: the concerned webcam's id
        */
        void clickLabel(TA_InfoClient* infoClient, int webcamId);

        /*
            Opens informations about a client
                @param infoClient: the clicked client
        */
        void clickClient(TA_InfoClient* infoClient);

        /*
            Close the given client
                @param idClient: the id to get the client
        */
        void closeClient(int idClient);

        /*
            Change the displayed mode of the given webcam
                @param idWebcam: the id to get the webcam
        */
        void changeDisplayWebcam(int idWebcam);

    private:
        QMap<int, TA_InfoClient*> clientsList; // map mapping an iteger to a client
        QMap<int, WebcamStruct*> webcamsList; // map mapping an iteger to a webcam
};

#endif // TA_WEBCAMSLIST_H
