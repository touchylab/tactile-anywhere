/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 26 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_InfoArea to display widget to |
  | display content informations about a choosen area                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_INFOAREA_H
#define TA_INFOAREA_H

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/

#define TA_INFOAREA_NORMAL_TYPE_LABEL "Type :  Zone normale   "
#define TA_INFOAREA_GROUP_TYPE_LABEL "Type :  Groupe de zone   "

#define TA_INFOAREA_COLOR_TITLE "   Couleur : "

#define TA_INFOAREA_CHILDREN_LABEL "Sous-Zones : "
#define TA_INFOAREA_NO_CHILD_TEXT "/"
#define TA_INFOAREA_CHILD_SEPERATOR ", "

#define TA_INFOAREA_FILE_LABEL "Fichier lancé lors d'un contact : "
#define TA_INFOAREA_URL_LABEL "Page Web ouverte lors d'un contact : "
#define TA_INFOAREA_COMMAND_LABEL "Commande lancée lors d'un contact : "

#define TA_INFOAREA_RETURN_BUTTON_TEXT "Retour"

#define TA_INFOAREA_NO_WEBCAMS_TEXT "Pas de webcams définies"
#define TA_INFOAREA_WEBCAM_LABEL "Webcams : "



/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QWidget>

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
    Class to display many informations about a selected webcam
*/
class TA_InfoArea : public QWidget{
    Q_OBJECT

    public:
        /*
            Display informations about a choosen area
                @param areaName : the area's name
        */
        TA_InfoArea(QString areaName);

private slots:

        /*
            Cancel action and go back to the previous screen (home screen)
        */
        void cancel();
};

#endif // TA_INFOAREA_H
