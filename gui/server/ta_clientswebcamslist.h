/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 13 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_WebcamsChoice to display widget    |
  | to choose webcams about an area                                         |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Sébastien DESTABEAU -- Initial version                        |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_CLIENTSWEBCAMSLIST_H
#define TA_CLIENTSWEBCAMSLIST_H

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/

#define TA_CLIENTSWEBCAMSLIST_NOT_AVAILABLE_WEBCAM ":/images/noAvailable.png"

#define TA_CLIENTSWEBCAMSLIST_NOT_SHOW_ICON ":/images/notshow.png"
#define TA_CLIENTSWEBCAMSLIST_NOT_SHOW_TEXT "Masquée"

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QWidget>

// Local includes
#include "entity/ta_server.h"

//Fwd
class TA_ClickableLabel;
class TA_InfoClient;
class TA_FlowLayout;


/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
   Class to display all clients webcams under list form.
*/
class TA_ClientsWebcamsList : public QWidget{
    Q_OBJECT

    public:
        /*
          Default builder
        */
        TA_ClientsWebcamsList();

    public slots:
        /*
          Updates the webcams frame
          @param client : the client's webcam
          @param webcamId : the webcam identifier to update frame for
        */
        void changedFrame(TA_InfoClient* client, int webcamId);

        /*
          Go to ManyInformationsWebcam on click
        */
        void clickLabel(TA_InfoClient* infoClient, int webcamId);

        /*
            Updates the webcams state
                @param client: the client owning the changed webcams
                @param webcams : the changed webcams' id
        */
        void changedWebcams(TA_InfoClient* client, QVector<int> webcams);

		/*
            Updates the webcams list
        */
        void changedClient();

    protected:
        /*
            Add a cell to the widget
                @param client : the client to owning the webcam
                @param webcamId : the webcam's id
        */
        void addCell(TA_InfoClient* client, int webcamId);

        /*
            Add a cell to the widget
                @param webcam : the webcam structure
        */
        void removeCell(WebcamStruct* webcam);
		
    private:
        int frameSide;
		TA_FlowLayout* flowLayout; // The main layout
        QMap<WebcamStruct*, TA_ClickableLabel*> imgMap; // the map binding a webcam structure to a QLabel
		QMap<WebcamStruct*, QWidget *> cellMap; // the map binding a webcam structure to its cell
        QMap<WebcamStruct*, TA_InfoClient*> clientMap; // the map binding a webcam structure to its client
        QMap<WebcamStruct*, int> idMap; // the map binding a webcam structure to its id
};

#endif // TA_CLIENTSWEBCAMSLIST_H
