/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 05 March 2014                                      |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_InfoClientWidget to display   |
  | widget to display content informations about a choosen client           |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_INFOCLIENTWIDGET_H
#define TA_INFOCLIENTWIDGET_H

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/

#define TA_INFOCLIENTWIDGET_IP_TITLE "Adresse IP : "
#define TA_INFOCLIENTWIDGET_PORT_TITLE "    Port : "

#define TA_INFOCLIENTWIDGET_RETURN_BUTTON_TEXT "Retour"

#define TA_INFOCLIENTWIDGET_NO_WEBCAMS_TEXT "Pas de webcams définies"
#define TA_INFOCLIENTWIDGET_WEBCAM_LABEL "Webcams : "

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QWidget>

// Local
#include "communication/server/ta_infoclient.h"

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
    Class to display many informations about a selected webcam
*/
class TA_InfoClientWidget : public QWidget{
    Q_OBJECT

    public:
        /*
            Display informations about a choosen client
                @param client : the client to display
        */
        TA_InfoClientWidget(TA_InfoClient* client);

    private slots:

            /*
                Cancel action and go back to the previous screen (home screen)
            */
            void cancel();

            /*
                Updates the view
            */
            void changedClient();

    private:
        TA_InfoClient* client;
};

#endif // TA_INFOCLIENTWIDGET_H
