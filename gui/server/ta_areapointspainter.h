/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 13 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_AreaPointsPainter class       |
  | drawing and editing a points sector                                     |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu LOSLIER -- Initial version                            |
  | [-] 1.1 - Valentin COULON -- Version complete for Tactile Anywhere 1.0  |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_AREAPOINTSPAINTER_H
#define TA_AREAPOINTSPAINTER_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QWidget>
#include <QPen>
#include <QPointF>
#include <QTimer>
#include <QColor>
#include <QPoint>

//Local includes
#include "entity/ta_server.h"
#include "ta_sectorviewer.h"

//Fwd
class TA_InfoClient;

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
    TA_AreaPointsPainter represent a derived QPainter to draw
    the area outline and listen mouse events.
*/
class TA_AreaPointsPainter : public QWidget{
    Q_OBJECT

    public:
        /*
            Create an instance of TA_AreaPointsPainter
                @param frame : the frame to display
                @param color : the color to use for pen
                @param outline: the outline to use at the initialization
                @param marker: the marker position
        */
        TA_AreaPointsPainter(WebcamStruct *webcam, QColor color, QList<QPointF> *outline, int marker);

        /*
            Create an instance of TA_AreaPointsPainter
                @param color : the color to use for pen
        */
        TA_AreaPointsPainter(QColor color);
        /*
            Destroy properly this class
        */
        ~TA_AreaPointsPainter();

        /*
            Return the area outline for the currentWebcam adapted to the
            webcam's size
        */
        QList<QPointF> *getOutlineArea();

        /*
            Sets the reference marker position
                @param marker: the position of the marker position
        */
        void setMarker(int marker);

        /*
            get the reference marker position
                @return marker: the position of the marker position
        */
        QPointF getMarkerPosition();

        /*
            Change the frame concerned by the area outline
        */
        void updateImage();

        /*
            Load the points list and draw them on screen
                @param outline : the points list to draw
        */
        void setWebcamAndOutlineArea(WebcamStruct *webcam, QList<QPointF> *outline, int marker);

        /*
            Remove all the points of the area
        */
        void clearOutlineArea();

    signals:
        /*
            Send when a new point is set
        */
        void newPoint();

    private slots:

    /*
        Refresh the markers positions
            @param client : the client informations
            @param webcamId : the id of the webcam
    */
    void newMarkers(TA_InfoClient* client, int webcamId);

    protected:
        /*
            Event to paint the entire surface of QPainter, draw
            points, background and outline
        */
        void paintEvent(QPaintEvent *);

        /*
            Manage the mouse press event for create and edit points
                @param event : event emited
        */
        void mousePressEvent(QMouseEvent *event);

        /*
            Manage the mouse move event for create and edit points
                @param event : event emited
        */
        void mouseMoveEvent(QMouseEvent *event);

        /*
            Return index of the point to edit or -1 if it doesn't exist
            The aim is to find if point is a point existing (with a difference
            +5 or -5 pixel) in the pointsList composing the outline.
                @param point : point to compare with points in the pointsList
        */
        int indexOfPointToEdit(QPointF point);

        /*
            Manage the mouse release event for create and edit points
        */
        void mouseReleaseEvent(QMouseEvent *);

        /*
            Draw a mouse position point from a painter object
            @param painter : the painting object to draw the mouse position
        */
        void drawMousePosition(QPainter *painter);

        /*
            Draw a shape from a painter component
            @param painter : the painter object to draw the shape
        */
        void drawShape(QPainter *painter);

    private:

        /*
            Initialize the class
        */
        void init(QColor color);

        /*
            Update the position of the referenceMarker
        */
        void updateMarkerPoint();

        /*
            get the difference between reel size and visualSize for axe X
        */
        float getDiffX();

        /*
            get the difference between reel size and visualSize for axe Y
        */
        float getDiffY();

        /*
            Check if the point is in surface of QPainter
                @param p : point to compare
        */
        bool isInPainter(QPoint p);

        // shape is completed if the last point is in region of first point at 10 pixel around (in x and y)
        static const int X_SHAPE_COMPLETE = 10;
        static const int Y_SHAPE_COMPLETE = 10;
        // precision in pixel to pass in edit mode
        static const int X_EDIT_POINT = 5;
        static const int Y_EDIT_POINT = 5;

        bool shapeComplete;             // shape close
        bool editMode;                  // mode to edit points
        int indexEditPoint;             // index of point to edit in the list

        QList<QPointF> *pointsList;     // points list for area outline using floating position for scale
        QList<QPointF> *toInit;         // points list to use at the initialization
        QPointF markerPoint;            // marker position
        int markerId;
        QPointF mousePoint;             // the mouse position
        WebcamStruct* webcam; // The webcam concerned by this definition
        TA_SectorViewer* sectorViewer; //the background used for paint

        QPen penCircle;
        QPen penLine;
        QTimer initOutlineTimer;
};

#endif // TA_AREAPOINTSPAINTER_H
