/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 14 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_SystemEventManager namespace       |
  | offering to send events to the system. These events can be get by other |
  | programs                                                                |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu LOSLIER -- Initial version                            |
  | [-] 1.1 - Valentin COULON -- Add reference marker and multiple markers  |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_DRAWAREA_H
#define TA_DRAWAREA_H

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/

#define TA_DRAWAREA_TITLE "Tracer la zone %1" // arg1 : area name

#define TA_DRAWAREA_POINT_TO_POINT_TITLE "Selection point par point"
#define TA_DRAWAREA_POINT_TO_POINT_BUTTON_TEXT "Point par point"

#define TA_DRAWAREA_MULTIPLE_MARKERS_TITLE "Sélection par marqueurs multiples"
#define TA_DRAWAREA_MULTIPLE_MARKERS_BUTTON_TEXT "Marqueurs multiples"
#define TA_DRAWAREA_MULTIPLE_MARKERS_NOT_ENOUGH_MARKERS_WARNING "Nombre de marqueurs insuffisant. \
L'image contient moins de 3 marqueurs visibles, placez d'autres marqueurs devant la webcam pour les sélectionner"

#define TA_DRAWAREA_SELECT_REFERENCE_MARKER_TITLE "Choix du marqueur de référence"
#define TA_DRAWAREA_SELECT_REFERENCE_MARKER_BUTTON_TEXT "Définir un marqueur de référence"
#define TA_DRAWAREA_MULTIPLE_MARKERS_NO_MARKER_WARNING "Aucun marqueur n'a été trouvé sur l'image, \
placez un marqueur devant la webcam pour le sélectionner."
#define TA_DRAWAREA_CANCEL_SELECT_REFERENCE_MARKER_BUTTON_TEXT "Revenir sur la sélection point à point"

#define TA_DRAWAREA_PREVIOUS_WEBCAM_BUTTON_ICON ":/images/previous.png"
#define TA_DRAWAREA_NEXT_WEBCAM_BUTTON_ICON ":/images/next.png"

#define TA_DRAWAREA_RETURN_BUTTON_TEXT "Retour"

#define TA_DRAWAREA_REFRESH_BUTTON_ICON ":/images/update.png"
#define TA_DRAWAREA_REFRESH_BUTTON_TEXT "Rafraîchir"

#define TA_DRAWAREA_CLEAR_BUTTON_ICON ":/images/eraser.png"
#define TA_DRAWAREA_CLEAR_BUTTON_TEXT "Nettoyer"

#define TA_DRAWAREA_END_BUTTON_TEXT "Terminer"


#define TA_DRAWAREA_UNSPECIFIED_WEBCAMS_ERROR_TITLE "Confirmation d'action"
#define TA_DRAWAREA_UNSPECIFIED_WEBCAM_TEXT "\n       - %1" // arg1 : webcam name
#define TA_DRAWAREA_UNSPECIFIED_WEBCAMS_ERROR_TEXT "La zone %1  n'est pas définie sur les webcams suivantes :%2\n\
Etes vous sûr de vouloir enregistrer la zone ?" //arg1 : area name, arg2: unspecified webcams list


/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QWidget>
#include <QButtonGroup>

// Local includes
#include "gui/ta_advancebutton.h"
#include "entity/ta_server.h"
#include "gui/ta_advancebutton.h"
//Fwd
class QPushButton;
class QLabel;
class TA_InfoClient;
class TA_Webcam;
class QVBoxLayout;
class TA_AreaPointsPainter;
class TA_AreaMarkersPainter;
class TA_AdvanceButton;


/*****************************************************************************/
/******                         STRUCTS                                 ******/
/*****************************************************************************/
enum AreaDrawnType {AREADRAWNTYPE_POINTS, AREADRAWNTYPE_MARKERS};


struct AreaDefinition {
    AreaDrawnType type;
    QList<QPointF> *points;
    MarkerStruct *referenceMarker;
    QList<MarkerStruct>* markers;
};

#ifndef TEMP_AREA_STRUCT_H
#define TEMP_AREA_STRUCT_H

typedef struct {
    QString name;
    QString oldname;
    QColor color;
    QString url;
    QString file;
    QString command;
} TempAreaStruct;

#endif

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/
/*
    TA_DrawArea represents the screen to create or edit outlines
    concerning an area filmed by a webcams list
*/
class TA_DrawArea : public QWidget{
    Q_OBJECT

    public:
        /*
            Create an instance of TA_DrawArea
                @param area : the area's information
        */
        TA_DrawArea(TempAreaStruct area);

        /*
            Delete properly this instance
        */
        ~TA_DrawArea();

    private slots:
        /*
            Refresh the markers positions
                @param client : the client informations
                @param webcamId : the id of the webcam
        */
        void newMarkers(TA_InfoClient* client, int webcamId);
        /*
            Activate the clear button
        */
        void activateClearButton();

        /*
            Save the current outline for the currentWebcam and switch to the previous webcam
            or the last if the currentWebcam was the first in webcams list.
        */
        void previous();

        /*
            Save the current outline for the currentWebcam and switch to the next webcam
            or the first if the currentWebcam was the last in webcams list.
        */
        void next();

        /*
            Cancel any modifications on any webcams and return to the webcam selection
        */
        void cancel();

        /*
            Ask to refresh the view
        */
        void refresh();

        /*
            Clear the points on the areaPainter
        */
        void clear();

        /*
            Save outlines for the area specified and apply changes to back end
        */
        void end();

        /*
            Add the reference marker and go to point to point interface
        */
        void referenceMarkerSelected();

        /*
            Updates the current webcam's frame
              @param client : the client of the webcam
              @param webcamId : the webcam identifier
        */
        void changedFrame(TA_InfoClient* client, int webcamId);

        /*
            Go to point to point interface
        */
        void goToPointToPoint();

        /*
            Go to select reference marker interface
        */
        void goToSelectReferenceMarker();

        /*
            Go to multiple markers interface
        */
        void goToMultipleMarkers();

        /*
            Active and unactive the advanced mode
            @param show : true => active, false => unactive
        */
        void setAdvancedMode(bool show);


    private:

        /*
            Change the current webcam
            @param newWebcam : webcam to change
        */
        void changeCurrentWebcam(WebcamStruct* newWebcam);

        /*
            Update the variable map to include current state
        */
        void updateMap();

        /*
            Check if the area is specified
        */
        bool isSpecifiedArea(AreaDefinition* areaDef);


        TempAreaStruct area;                  // the area's informations
        QList<TA_Webcam *> webcamList;        // list of webcam selected concerning this area
        WebcamStruct* currentWebcam;           // the current webcam concerned to draw an outline


        QVBoxLayout *painterLayout;            // layout for the painter
        TA_AreaPointsPainter *painterPoints;               // the painter on wich the outline is drawn
        TA_AreaMarkersPainter *painterReferenceMarker;     //the painter to select the reference marker
        TA_AreaMarkersPainter *painterMarkers;               // the painter on wich the markers is drawn

        QMap<WebcamStruct*, AreaDefinition*> map; // map to keep outlines with floating precision for gui and switch outlines
        QMapIterator<WebcamStruct*, AreaDefinition*> it_map;

        QLabel *SelectionTitle;
        TA_AdvanceButton *advanceButton;
        QPushButton *clearButton;
        QPushButton* multipleMarkersButton;
        QPushButton* pointToPointButton;
        QPushButton* selectReferenceMarkerButton;
        QPushButton* cancelSelectReferenceMarkerButton;
        bool initialization;
};

#endif // TA_DRAWAREA_H
