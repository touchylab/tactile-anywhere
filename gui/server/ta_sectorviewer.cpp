/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 23 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_SectorViewer class drawing the     |
  | sectors of a webcam                                                     |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QPainter>
#include <QPen>
#include <QPaintEvent>
#include <QImage>

//Local includes
#include "gui/ta_mainwindow.h"
#include "ta_sectorviewer.h"

/*
    Create an instance of TA_SectorViewer
        @param client : the client owning the webcam
        @param webcamId: the webcam's id
        @param area: limit the sector's to this area
*/
TA_SectorViewer::TA_SectorViewer(TA_InfoClient* client, int webcamId, QString area) : QWidget(){
    frameSide = TA_MainWindow::getInstance()->getScreenSize().width()/4;
    webcam = TA_Server::getInstance()->getWebcamInfo(client,webcamId);
    this->area = area;
    currentFrame = QPixmap::fromImage(webcam->image);
    if (currentFrame.size() == QSize(0,0)) {
        setMinimumSize(1,1);    // bug if the background frame have a null size
    } else {
        setMinimumSize(frameSide,frameSide);
    }
    //setMaximumSize(frameSide,frameSide);
    QSizePolicy policy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    setSizePolicy(policy);

    penCircle = QPen(QColor(), 5, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    penLine = QPen(QColor(), 2, Qt::DashLine, Qt::RoundCap, Qt::RoundJoin);

    if (webcam->sectors.isEmpty()) {
        return;
    }

    QList<QString> toDisplay;
    if (!area.isEmpty()) {
        toDisplay = getNormalAreas(area);
    }

    foreach(QString sectorId, webcam->sectors.keys()) {
        QString areaName = webcam->sectors[sectorId]->area;
        if (toDisplay.isEmpty() || toDisplay.contains(areaName)) {
            QVector<QPoint> points = TA_Server::getInstance()->convertSectorPoint(sectorId, webcam->sectors[sectorId]);
            addOutlineArea(areaName, points, TA_Server::getInstance()->getAreaInfo(areaName)->color);
        }
    }
    emit TA_Server::getInstance()->selectAndShowAreas(visibleAreas);
    connect(TA_Server::getInstance(), SIGNAL(areaSelectionChanged(QList<QString>)), this, SLOT(setVisibleAreas(QList<QString>)));
}

/*
    Destroy properly this class
*/
TA_SectorViewer::~TA_SectorViewer() {
    foreach(DrawableArea* drawableArea, drawableList){
        delete drawableArea->points;
    }
}

/*****************************************************************************/
/******                             EVENT                               ******/
/*****************************************************************************/

/*
   Event to paint the entire surface of QPainter, draw
   points, background and outline
*/
void TA_SectorViewer::paintEvent(QPaintEvent *) {
    QPainter painter(this);
    paintImageAndSectors(&painter, size());
    painter.end();
}

/*
    Draw a shape from a painter component
    @param painter : the painter object to draw the shape
    @param size : size of the painter
*/
void TA_SectorViewer::drawShape(QPainter *painter, QSize size) {
    for (int i = 0; i < drawableList.size(); i++) {
        if (!visibleAreas.contains(drawableList[i]->areaName)) {
            continue;
        }
        penCircle.setColor(drawableList[i]->color);
        penLine.setColor(drawableList[i]->color);

        QSize orig = webcam->image.size();
        orig = orig == QSize(0, 0)? size: orig;
        float diffX = (float)size.width()/(float)orig.width();
        float diffY = (float)size.height()/(float)orig.height();

        QVector<QPointF> points;
        foreach (QPointF point, *(drawableList[i]->points)){
            points << QPointF(point.x()*diffX, point.y()*diffY);
        }

        // Draw circles foreach points in list
        painter->setPen(penCircle);
        for(int j=0; j < points.size(); j++){
            painter->drawEllipse(points[j],5,5);
        }

        // Draw shapes
        penLine.setStyle(Qt::DashLine);
        painter->setPen(penLine);
        painter->drawPolyline(points);
    }
}

/*
    Change the value of visibleAreas
    @param visibleAreas : the new visibleAreas
*/
void TA_SectorViewer::setVisibleAreas(QList<QString> visibleAreas) {
    this->visibleAreas = visibleAreas;
    update();
    emit importantUpdate();
}

/*****************************************************************************/
/******                             SETTER                              ******/
/*****************************************************************************/

/*
    Change the webcam concerned by the area outline
        @param webcam : webcam to take on board
*/
void TA_SectorViewer::setCurrentFrame(QImage frame) {
    currentFrame = QPixmap::fromImage(frame);
    update();
}

/*
    Load the points list and draw them on screen
        @param areaName : the area name to use
        @param outline : the points vector to draw
        @param color : the color to use
*/
void TA_SectorViewer::addOutlineArea(QString areaName, QVector<QPoint>& outline, QColor color) {
    DrawableArea* drawableArea = new DrawableArea();
    drawableArea->areaName = areaName;
    visibleAreas.append(areaName);
    drawableArea->points = new QVector<QPointF>;

    foreach(QPoint point, outline){
        *(drawableArea->points) << QPointF(point.x(), point.y());
    }
    *(drawableArea->points) << drawableArea->points[0];
    drawableArea->color = color;
    drawableList << drawableArea;
}

/*****************************************************************************/
/******                             OTHER                               ******/
/*****************************************************************************/

/*
    Get the areas defining an area (normal or group)
        @param area: the area's name
        @return : the list of area representing the area
*/
QList<QString> TA_SectorViewer::getNormalAreas(QString area) {
    QList<QString> displayList;
    QList<QString> children;

    children.append(area);
    while(!children.isEmpty()) {
        QString child = children.takeFirst();

        if (TA_Server::getInstance()->getAreaInfo(child)->type == NORMAL_AREA) {
            displayList.append(child);
        }
        foreach (QString areaName, TA_Server::getInstance()->getAreaInfo(child)->down) {
            children.append(areaName);
        }
    }
    return displayList;
}

/*
    Paint the image and the Sectors on the painters
        @pram painter : painter to use
*/
void TA_SectorViewer::paintImageAndSectors(QPainter *painter, QSize size) {
    painter->setRenderHint(QPainter::Antialiasing,true);
    painter->drawPixmap(0, 0, currentFrame.scaled(size));
    drawShape(painter, size);
}

/*
    Emit signal when clicked
*/
void TA_SectorViewer::mousePressEvent(QMouseEvent *) {
    emit clicked();
}
