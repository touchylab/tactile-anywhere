/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 12 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_SystemEventManager namespace       |
  | offering to send events to the system. These events can be get by other |
  | programs                                                                |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu LOSLIER -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_DETECTIONRUNNING_H
#define TA_DETECTIONRUNNING_H

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/
#define TA_DETECTIONRUNNING_LOGGER_TIME_CONVERTION "hh:mm:ss"
#define TA_DETECTIONRUNNING_LOGGER_AVERT_1 "L'initialisation et la détection seront lancées dans "
#define TA_DETECTIONRUNNING_LOGGER_AVERT_2 " secondes. Veuillez quitter le champ des webcams"
#define TA_DETECTIONRUNNING_LOGGER_DETECTION_START " : Lancement de la détection"

#define TA_DETECTIONRUNNING_STOP_BUTTON_ICON ":/images/stop.png"
#define TA_DETECTIONRUNNING_STOP_BUTTON_TEXT "Arrêter la détection"

#define TA_DETECTIONRUNNING_LAUNCH_DELAY 5000

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QWidget>
#include <QTimer>

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
    Class about detection running process and GUI
*/
class TA_DetectionRunning : public QWidget {
    Q_OBJECT
    public:

        /*
            Default builder
        */
        TA_DetectionRunning();

    private slots:
        /*
            Launch the detection
        */
        void launchDetection();

        /*
          Stop the detection
        */
        void stopDetection();

    private:
        QTimer timerDetection;
};

#endif // TA_DETECTIONRUNNING_H
