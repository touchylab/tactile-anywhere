/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 20 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_ManyInfosWebcam to display widget  |
  | to display content informations about a choosen webcam                  |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Sébastien DESTABEAU -- Initial version                        |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QLayout>
#include <QLabel>
#include <QPushButton>

// Local includes
#include "entity/ta_server.h"
#include "communication/server/ta_infoclient.h"
#include "gui/server/ta_servermainwidget.h"
#include "gui/server/ta_sectorviewer.h"
#include "gui/ta_fonts.h"
#include "ta_manyinfoswebcam.h"

/*
    Display informations about a choosen webcam.
    @param infoClient : the client informations about the webcam
    @param webcamId : the choosen webcam identifier
*/
TA_ManyInfosWebcam::TA_ManyInfosWebcam(TA_InfoClient* infoClient, int webcamId) : QWidget(){
    this->client = infoClient;
    this->webcamId = webcamId;
    WebcamStruct* webcam = TA_Server::getInstance()->getWebcamInfo(infoClient, webcamId);

    //Header
    QHBoxLayout* headerLayout = new QHBoxLayout;
    titleLabel = new QLabel(QString(TA_MANYINFOSWEBCAM_TITLE).arg(webcam->name));
    titleLabel->setFont(TA_Fonts::getTitleFont());
    stateLabel = new QLabel();
    stateLabel->setFont(TA_Fonts::getDefaultFont());
    if (!webcam->active || infoClient->getClientState() == DISCONNECTED || infoClient->getClientState() == CLOSED){
        stateLabel->setPixmap(QPixmap(TA_MANYINFOSWEBCAM_NO_CONNECTION_ICON));
        stateLabel->setToolTip(TA_MANYINFOSWEBCAM_NO_CONNECTION_TOOL_TIP);
    } else {
        stateLabel->setPixmap(QPixmap(TA_MANYINFOSWEBCAM_CONNECTION_OK_ICON));
        stateLabel->setToolTip(TA_MANYINFOSWEBCAM_CONNECTION_OK_TOOL_TIP);
    }
    headerLayout->addWidget(titleLabel);
    headerLayout->addWidget(stateLabel);
    headerLayout->setAlignment(Qt::AlignHCenter);

    QVBoxLayout* mainLayout = new QVBoxLayout;
    mainLayout->addLayout(headerLayout);

    // Show button
    QHBoxLayout* showLayout = new QHBoxLayout;
    showButton = new QPushButton;
    changeButtonDisplayWebcam(webcam->isDisplayed);
    connect(showButton,SIGNAL(clicked()),this,SLOT(changeDisplayWebcam()));
    showLayout->addStretch(1);
    showLayout->addWidget(showButton, 0);
    showLayout->addStretch(1);
    mainLayout->addLayout(showLayout);

    // top widget and horizontal layout
    QVBoxLayout* topVLayout = new QVBoxLayout;

    //Display the webcams informations
    QLabel* clientNameLabel = new QLabel(QString(TA_MANYINFOSWEBCAM_CLIENT_NAME_LABEL).arg(webcam->client->getClientName()));
    clientNameLabel->setFont(TA_Fonts::getDefaultFont());
    imageResolutionLabel = new QLabel(QString(TA_MANYINFOSWEBCAM_IMAGE_RESOLUTION_LABEL).arg(webcam->image.size().width()).arg(webcam->image.size().height()));
    imageResolutionLabel->setFont(TA_Fonts::getDefaultFont());
    configLabel = new QLabel(QString(TA_MANYINFOSWEBCAM_CONFIG_LABEL).arg(webcam->sectors.size()).arg(webcam->markers.size()));
    configLabel->setFont(TA_Fonts::getDefaultFont());
    topVLayout->addWidget(clientNameLabel);
    topVLayout->addWidget(imageResolutionLabel);
    topVLayout->addWidget(configLabel);
    topVLayout->setAlignment(Qt::AlignCenter);

    // label picture
    QHBoxLayout *centralLayout = new QHBoxLayout();
    centralLayout->addStretch(1);
    QVBoxLayout* imageLayout = new QVBoxLayout();
    webcamViewer = new TA_SectorViewer(infoClient,webcamId);
    if(webcam->image.isNull() || webcam->image.size() == QSize(0,0) || !webcam->active){
        webcamViewer->setCurrentFrame(QImage(TA_MANYINFOSWEBCAM_NOT_AVAILABLE_WEBCAM_ICON));
    }
    imageLayout->addWidget(webcamViewer);
    imageLayout->setAlignment(Qt::AlignCenter);
    centralLayout->addLayout(imageLayout,4);
    centralLayout->addStretch(1);

    //Footer
    QHBoxLayout* footerLayout = new QHBoxLayout();
    QHBoxLayout* buttonLayout = new QHBoxLayout();
    QPushButton* returnButton = new QPushButton(TA_MANYINFOSWEBCAM_RETURN_BUTTON_TEXT);
    returnButton->setFont(TA_Fonts::getButtonFont());
    buttonLayout->addStretch();
    buttonLayout->addWidget(returnButton, 0, Qt::AlignTop);
    buttonLayout->addStretch();
    footerLayout->addLayout(buttonLayout);
    connect(returnButton, SIGNAL(clicked()), this, SLOT(cancel()));

    mainLayout->addLayout(topVLayout);
    mainLayout->addLayout(centralLayout);
    mainLayout->addLayout(footerLayout);

    setLayout(mainLayout);

    // Connections
    connect(TA_Server::getInstance(), SIGNAL(changedFrame(TA_InfoClient*,int)), this, SLOT(changedFrame(TA_InfoClient*,int)));
    connect(TA_Server::getInstance(), SIGNAL(changedWebcams(TA_InfoClient*,QVector<int>)), this, SLOT(changedWebcams()));
    connect(TA_Server::getInstance(), SIGNAL(changedMarkers(TA_InfoClient*,int)), this, SLOT(changedMarkers(TA_InfoClient*,int)));
    connect(TA_Server::getInstance(), SIGNAL(changedClient()), this, SLOT(changedClient()));

    // Frames
    TA_Server::getInstance()->askFrame(false);
}

/*****************************************************************************/
/******                             SLOTS                               ******/
/*****************************************************************************/

/*
    Cancel action and go back to the previous screen (home screen)
*/
void TA_ManyInfosWebcam::cancel(){
    TA_ServerMainWidget::getInstance()->openHomeScreen();
}

/*
    Updates the webcams state
*/
void TA_ManyInfosWebcam::changedWebcams(){
    titleLabel->setText(QString(TA_MANYINFOSWEBCAM_TITLE).arg(TA_Server::getInstance()->getWebcamInfo(client,webcamId)->name));
    if(!TA_Server::getInstance()->getWebcamInfo(client,webcamId)->active || client->getClientState() == DISCONNECTED || client->getClientState() == CLOSED){
        stateLabel->setPixmap(QPixmap(TA_MANYINFOSWEBCAM_NO_CONNECTION_ICON));
        stateLabel->setToolTip(TA_MANYINFOSWEBCAM_NO_CONNECTION_TOOL_TIP);
        webcamViewer->setCurrentFrame(QImage(TA_MANYINFOSWEBCAM_NOT_AVAILABLE_WEBCAM_ICON));
    }else{
        stateLabel->setPixmap(QPixmap(TA_MANYINFOSWEBCAM_CONNECTION_OK_ICON));
        stateLabel->setToolTip(TA_MANYINFOSWEBCAM_CONNECTION_OK_TOOL_TIP);
    }

    changeButtonDisplayWebcam(TA_Server::getInstance()->getWebcamInfo(client,webcamId)->isDisplayed);
    update();
}

/*
    Updates the webcam's markers
        @param client : the client of the webcam
        @param webcamId : the webcam identifier
*/
void TA_ManyInfosWebcam::changedMarkers(TA_InfoClient* client, int webcamId){
    if(client != this->client || webcamId != this->webcamId)
        return;
    WebcamStruct *webcam = TA_Server::getInstance()->getWebcamInfo(client,webcamId);
    configLabel->setText(QString(TA_MANYINFOSWEBCAM_CONFIG_LABEL).arg(webcam->sectors.size()).arg(webcam->markers.size()));
    update();
}

/*
    Updates the webcam's frame
    @param client : the client of the webcam
    @param webcamId : the webcam identifier
*/
void TA_ManyInfosWebcam::changedFrame(TA_InfoClient* client, int webcamId){
    if(client != this->client || webcamId != this->webcamId)
        return;

    WebcamStruct *webcam = TA_Server::getInstance()->getWebcamInfo(client,webcamId);
    webcamViewer->setCurrentFrame(webcam->image);
    imageResolutionLabel->setText(QString(TA_MANYINFOSWEBCAM_IMAGE_RESOLUTION_LABEL).arg(webcam->image.size().width()).arg(webcam->image.size().height()));
    update();
}

/*
    Updates the view
*/
void TA_ManyInfosWebcam::changedClient(){
    if(!TA_Server::getInstance()->getConfig()->contains(client)){
        TA_ServerMainWidget::getInstance()->openHomeScreen();
    }
}

/*
    Change the displayed mode of the webcam
*/
void TA_ManyInfosWebcam::changeDisplayWebcam() {
    bool displayed = !TA_Server::getInstance()->getWebcamInfo(client,webcamId)->isDisplayed;
    TA_Server::getInstance()->changeDisplayModeWebcam(client,webcamId,displayed);

    changeButtonDisplayWebcam(displayed);
}

/*
    Change the displayed button for the mode of the webcam
    @param visible : true => visible, false => not visible
*/
void TA_ManyInfosWebcam::changeButtonDisplayWebcam(bool visible) {
    if (visible){
        showButton->setIcon(QIcon(TA_MANYINFOSWEBCAM_SHOW_BUTTON_ICON));
        showButton->setText(TA_MANYINFOSWEBCAM_SHOW_BUTTON_TEXT);
        showButton->setToolTip(TA_MANYINFOSWEBCAM_SHOW_BUTTON_TOOL_TIP);
    } else {
        showButton->setIcon(QIcon(TA_MANYINFOSWEBCAM_NOT_SHOW_BUTTON_ICON));
        showButton->setText(TA_MANYINFOSWEBCAM_NOT_SHOW_BUTTON_TEXT);
        showButton->setToolTip(TA_MANYINFOSWEBCAM_NOT_SHOW_BUTTON_TOOL_TIP);
    }
}
