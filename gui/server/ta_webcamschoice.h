/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 13 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_WebcamsChoice to display widget    |
  | to choose webcams about an area                                         |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Sébastien DESTABEAU -- Initial version                        |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_WEBCAMSCHOICE_H
#define TA_WEBCAMSCHOICE_H
/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/

#define TA_WEBCAMSCHOICE_TITLE "Choix des webcams pour %1" // arg1 : area name

#define TA_WEBCAMSCHOICE_RETURN_BUTTON_LABEL "Retour"

#define TA_WEBCAMSCHOICE_ADD_WEBCAM_BUTTON_LABEL "Ajout des webcams"

#define TA_WEBCAMSCHOICE_NOT_AVAILABLE_WEBCAM_ICON ":/images/noAvailable.png"

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QWidget>
#include <QMessageBox>
#include <QLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QCheckBox>
#include <QMovie>

//Local includes
#include "gui/server/ta_servermainwidget.h"
#include "gui/ta_flowlayout.h"
#include "gui/ta_mainwindow.h"
#include "gui/ta_fonts.h"
#include "gui/ta_clickableqwidget.h"
#include "entity/ta_server.h"

//Fwd
class QPushButton;
class QLabel;
class QCheckBox;

/*****************************************************************************/
/******                          STRUCT                                 ******/
/*****************************************************************************/

#ifndef TEMP_AREA_STRUCT_H
#define TEMP_AREA_STRUCT_H
typedef struct {
    QString name;
    QString oldname;
    QColor color;
    QString url;
    QString file;
    QString command;
} TempAreaStruct;
#endif
/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

class TA_WebcamsChoice : public QWidget{
    Q_OBJECT

    public:
        /*
            Builder with an area name
                @param area : the area to create or edit
        */
        TA_WebcamsChoice(TempAreaStruct area);

    private slots:

        /*
            Add the choosen webcams to the webcams list to manage in detection
        */
        void addChoosenWebcams();

        /*
          Cancel action and go back to the previous screen
        */
        void cancel();

        /*
          Checks for selected webcams number.
          @param state : the checkbox state
        */
        void changedCheck(bool state);

        /*
          Updates the webcams frame
          @param client : the client's webcam
          @param webcamId : the webcam identifier to update frame for
        */
        void changedFrame(TA_InfoClient* client, int webcamId);

        /*
            Updates the webcams state
                @param client: the client owning the changed webcams
                @param webcams : the changed webcams' id
        */
        void changedWebcams(TA_InfoClient* client, QVector<int> webcams);

        /*
            Updates the webcams list
        */
        void changedClient();

    protected:
        /*
            Add a cell to the widget
                @param client : the client to owning the webcam
                @param webcamId : the webcam's id
        */
        void addCell(TA_InfoClient* client, int webcamId);

        /*
            Remove a cell to the widget
                @param webcam : the webcam structure
        */
        void removeCell(WebcamStruct* webcam);

    private:
        QPushButton* addWebcamsButton; // button to add selected webcams to a map
        TA_FlowLayout* flowLayout; // The main layout
        TempAreaStruct area; // the area's informations
        int nbChecked; // The number of checked webcams
        int sideFrame; // define the size about picture to display
        QMap<WebcamStruct*, WebcamStruct*> defined; // The already defined webcams
        QMap<WebcamStruct*, QCheckBox*> cbwbMap; // the map binding a checkbox to a webcam structure
        QMap<WebcamStruct*, QLabel*> imgMap; // the map binding a webcam structure to a QLabel
        QMap<WebcamStruct*, TA_ClickableQWidget*> cellMap; // the map binding a webcam structure to its cell
        QMap<WebcamStruct*, TA_InfoClient*> clientMap; // the map binding a webcam structure to its client
        QMap<WebcamStruct*, int> idMap; // the map binding a webcam structure to its id
};

#endif // TA_WEBCAMSCHOICE_H
