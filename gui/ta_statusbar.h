/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 28 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_StatusBar offering specific        |
  | status bar to manage message for tactile anywhere.                      |                                                                         |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu LOSLIER -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_STATUSBAR_H
#define TA_STATUSBAR_H

/*****************************************************************************/
/******                             DEFINES                             ******/
/*****************************************************************************/


//Images
#define TA_STATUSBAR_ERROR ":/images/error.png"
#define TA_STATUSBAR_WARNING ":/images/warning.png"
#define TA_STATUSBAR_INFO ":/images/info.png"
#define TA_STATUSBAR_CLOSE ":/images/close.png"

//Tooltip
#define TA_STATUS_BAR_TOOLTIP "Cliquez sur le message pour le faire disparaître"

//CSS
#define TA_STATUSBAR_RED_CSS "QStatusBar{background-color:rgba(255,0,0,150);}"
#define TA_STATUSBAR_ORANGE_CSS "QStatusBar{background-color:rgba(255,185,15,150);}"
#define TA_STATUSBAR_GREEN_CSS "QStatusBar{background-color:rgba(0,255,0,150);}"
#define TA_STATUSBAR_BUTTON_CSS "QPushButton {text-align:left;} QPushButton:pressed {background-color:rgba(0,0,0,0);}"
#define TA_STATUSBAR_ALPHA 150

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QStatusBar>

//Fwd
class QPushButton;

/*
    This file contains methods of the TA_StatusBar offering specific
    status bar to manage message for tactile anywhere.
*/
class TA_StatusBar : public QStatusBar {
    Q_OBJECT

    public:
        /*
            Instanciates this class with an widget parent
                @param parent : the parent widget
        */
        TA_StatusBar(QWidget *parent=0);

        /*
            Show a message
                @param text : text to display
                @param type : type of message (Warning, Error)
        */
        void showMessage(const QString &text, const QString type = "");

    public slots:
        /*
            Call when end timer is reach and apply normal original palette
        */
        void clearMessage();

    private:
        QPushButton *messageButton;
        QPushButton *closeButton;
};

#endif // TA_STATUSBAR_H
