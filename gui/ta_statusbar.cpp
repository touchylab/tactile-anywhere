/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 28 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_StatusBar offering specific        |
  | status bar to manage message for tactile anywhere.                      |                                                                         |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu LOSLIER -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QPushButton>

//Local includes
#include "gui/ta_fonts.h"
#include "ta_statusbar.h"

/*
    Instanciates this class with an widget parent
        @param parent : the parent widget
*/
TA_StatusBar::TA_StatusBar(QWidget *parent) : QStatusBar(parent){
    messageButton = new QPushButton(this);
    messageButton->setFont(TA_Fonts::getDefaultFont());
    messageButton->setFlat(true);
    messageButton->setToolTip(TA_STATUS_BAR_TOOLTIP);
    messageButton->setStyleSheet(TA_STATUSBAR_BUTTON_CSS);
    closeButton = new QPushButton(QIcon(TA_STATUSBAR_CLOSE),"");
    closeButton->setToolTip("Fermer le message");
    closeButton->setFlat(true);

    connect(closeButton,SIGNAL(pressed()),this,SLOT(clearMessage()));
    connect(messageButton,SIGNAL(pressed()),this,SLOT(clearMessage()));
}

/*
    Show a message
        @param text : text to display
        @param type : type of message (Warning, Error)
*/
void TA_StatusBar::showMessage(const QString &text, const QString type){
    if(type == "Error"){
        messageButton->setIcon(QIcon(TA_STATUSBAR_ERROR));
        setStyleSheet(TA_STATUSBAR_RED_CSS);
    }
    else if(type == "Warning"){
        messageButton->setIcon(QIcon(TA_STATUSBAR_WARNING));
        setStyleSheet(TA_STATUSBAR_ORANGE_CSS);
    }
    else {
        messageButton->setIcon(QIcon(TA_STATUSBAR_INFO));
        setStyleSheet(TA_STATUSBAR_GREEN_CSS);
    }
    messageButton->setText(text);
    addWidget(messageButton,1);
    addWidget(closeButton,0);
    show();
}


/*****************************************************************************/
/******                         SLOTS                                   ******/
/*****************************************************************************/

/*
    Call when end timer is reach and apply normal original palette
*/
void TA_StatusBar::clearMessage(){
    hide();
}
