/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 27 february 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_ToolBar class offering specific    |
  | toolbar animated for Tactile Anywhere                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu Loslier -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_toolbar.h"

/*
    Instanciates this class with an widget parent
        @param parent : the parent widget
*/
TA_ToolBar::TA_ToolBar(QWidget *parent) : QToolBar(){
    this->parent = parent;
    setContextMenuPolicy(Qt::PreventContextMenu);
    setOrientation(Qt::Horizontal);
    setMovable(false);

    setFixedHeight(TA_MainWindow::getInstance()->getScreenSize().height() * TA_TOOLBAR_HEIGHT_TOOLBAR_FACTOR);

    animation = new QPropertyAnimation(this,"geometry");
    animation->setDuration(TA_TOOLBAR_ANIMATION_DURATION_IN_MS);

    connect(animation, SIGNAL(finished()),this,SLOT(finish()));
}

/*
    Update the draw zone for button to hide and show toolbar
*/
void TA_ToolBar::update(){
    rectToolBarEnd = QRect(0, 0, parent->width(), height());
    rectToolBarStart = QRect(0, -height(), parent->width(), 0);
}

/*
    Toogle the toolbar (show/hide)
*/
void TA_ToolBar::slide(){
    update();
    if(!isVisible()){
        show();
        animation->setStartValue(rectToolBarStart);
        animation->setEndValue(rectToolBarEnd);
        toolBarState = SLIDE_IN;
    }
    else {
        animation->setStartValue(rectToolBarEnd);
        animation->setEndValue(rectToolBarStart);
        toolBarState = SLIDE_OUT;
    }
    animation->start();
    update();
}

/*****************************************************************************/
/******                         SLOTS                                   ******/
/*****************************************************************************/

/*
    Call when the animation is finished
*/
void TA_ToolBar::finish(){
    if(toolBarState == SLIDE_OUT){
        hide();
    }
}

