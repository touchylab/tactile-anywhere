/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 25 January 2013                                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_Help class containing a widget to  |
  | display help                                                            |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QVBoxLayout>
#include <QWebView>

//Local includes
#include "ta_help.h"

/*
    Instanciates this class with an html page
        @param htmlPage : the path to the html ressource
*/
TA_Help::TA_Help(QString htmlPage){
    QVBoxLayout* mainLayout = new QVBoxLayout;
    view = new QWebView;
    view->setUrl(QUrl(htmlPage));
    mainLayout->addWidget(view);
    setMaximumWidth(300);
    setMinimumWidth(200);
    setLayout(mainLayout);
}

/*
    Set the html page to this view
        @param htmlPage : the path to the html ressource
*/
void TA_Help::setPage(QString htmlPage){
    view->setUrl(QUrl(htmlPage));
    update();
}
