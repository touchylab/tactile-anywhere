/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 24 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_HalfMoonButton class          |
  | drawing an half moon button                                             |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Sébastien Destabeau -- Initial version                        |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_HALFMOONBUTTON_H
#define TA_HALFMOONBUTTON_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QObject>
#include <QWidget>
#include <QtGui>
#include <QPushButton>
#include <QHBoxLayout>


/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
    TA_HalfMoonButton represents a derived QButton as an half moon shape button.
*/
class TA_HalfMoonButton : public QWidget{
    Q_OBJECT

    public:
        /*
            Create an instance of TA_HalfMoonButton
        */
        TA_HalfMoonButton(QWidget *parent=0);

        /*
            Destroy properly this class
        */
        ~TA_HalfMoonButton();

    public:
        QPushButton *button;

    signals:


};

#endif // TA_HALFMOONBUTTON_H
