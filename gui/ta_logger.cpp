/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 10 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_Logger about logged messages to    |
  | display in gui                                                          |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Sébastien DESTABEAU -- Initial version                        |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QTextBrowser>
#include <QHBoxLayout>
#include <QString>
#include <QTimer>

//Local includes
#include "gui/ta_fonts.h"
#include "gui/ta_logger.h"

TA_Logger* TA_Logger::instance = NULL;

/*
    Destroy properly this instance
*/
TA_Logger::~TA_Logger(){
    instance = NULL;
}

/*
    Get an instance of a logger.
		@return : the unique instance of the logger.
*/
TA_Logger* TA_Logger::getInstance(){
    if(NULL == instance){
        instance = new TA_Logger;
    }
    return instance;
}

/*
    Log a message and append it in GUI Logger.
		@param message : the message to log.
*/
void TA_Logger::log(const QString &message){
    if(instance && loggerContentEditor){
        loggerContentEditor->append(message);
    }
}

/*
    Log a critical message and append it in GUI Logger.
        @param message : the message to log.
*/
void TA_Logger::logCritical(const QString &message){
    if(instance && loggerContentEditor){
        // Save
        int fw = loggerContentEditor->fontWeight();
        QColor tc = loggerContentEditor->textColor();

        // Change
        loggerContentEditor->setFontWeight(QFont::Bold);
        loggerContentEditor->setTextColor(QColor("red"));

        // Append
        loggerContentEditor->append(message);

        // Restore
        loggerContentEditor->setFontWeight( fw );
        loggerContentEditor->setTextColor( tc );
    }
}

/*
    Clear the GUI logger content.
*/
void TA_Logger::clear(){
    if(loggerContentEditor){
        loggerContentEditor->clear();
    }
}

/*
    Get the GUI logger widget. The widget must be deleted after use
		@return : the logger widget object.
*/
QTextBrowser* TA_Logger::getLoggerWidget(){
    if(!loggerContentEditor){
        loggerContentEditor = new QTextBrowser;
    }
    loggerContentEditor->setMinimumHeight(10);
    loggerContentEditor->setMinimumWidth(60);
    loggerContentEditor->setContentsMargins(0, 0, 0, 0);
    loggerContentEditor->setWindowTitle(QString(TA_LOGGER_TITLE));
    loggerContentEditor->setFont(TA_Fonts::getDefaultFont());
    return loggerContentEditor;
}

/*
    Stop the log
*/
void TA_Logger::stopLog(){
    if(loggerContentEditor){
        loggerContentEditor = NULL;
    }
}
