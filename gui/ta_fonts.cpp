/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 10 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methodss of the TA_Fonts class containing utility    |
  | for text fonts                                                          |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Sébastien DESTABEAU -- Initial version                        |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "gui/ta_fonts.h"

/*
	Get a font for default text.
		@return : the default font
*/
QFont TA_Fonts::getDefaultFont(){
    QFont defaultFont;
    defaultFont.setFamily(TA_FONTS_FAMILY);
    defaultFont.setStyle(QFont::StyleNormal);
    defaultFont.setCapitalization(QFont::MixedCase);
    defaultFont.setPixelSize(16);
    defaultFont.setBold(false);
    return defaultFont;
}

/*
    Get a font for title text.
		@return : the title font
*/
QFont TA_Fonts::getTitleFont(){
    QFont defaultFont;
    defaultFont.setFamily(TA_FONTS_FAMILY);
    defaultFont.setStyle(QFont::StyleNormal);
    defaultFont.setCapitalization(QFont::AllUppercase);
    defaultFont.setPixelSize(20);
    defaultFont.setBold(true);
    return defaultFont;
}

/*
    Get a font for subtitle text.
		@return : the subtitle font
*/
QFont TA_Fonts::getSubTitleFont(){
    QFont defaultFont;
    defaultFont.setFamily(TA_FONTS_FAMILY);
    defaultFont.setStyle(QFont::StyleNormal);
    defaultFont.setCapitalization(QFont::AllUppercase);
    defaultFont.setPixelSize(18);
    defaultFont.setBold(false);
    return defaultFont;
}

/*
    Get a font for button text.
		@return : the button font
*/
QFont TA_Fonts::getButtonFont(){
    QFont defaultFont;
    defaultFont.setFamily(TA_FONTS_FAMILY);
    defaultFont.setStyle(QFont::StyleNormal);
    defaultFont.setCapitalization(QFont::SmallCaps);
    defaultFont.setPixelSize(16);
    defaultFont.setBold(true);
    return defaultFont;
}

/*
    Get a font for description text.
		@return : the description font
*/
QFont TA_Fonts::getDescriptionFont(){
    QFont defaultFont;
    defaultFont.setFamily(TA_FONTS_FAMILY);
    defaultFont.setStyle(QFont::StyleNormal);
    defaultFont.setCapitalization(QFont::MixedCase);
    defaultFont.setPixelSize(16);
    defaultFont.setBold(false);
    defaultFont.setItalic(true);
    return defaultFont;
}
