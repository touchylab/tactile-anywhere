﻿/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 17 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_StartDetection presenting the      |
  | screen with a logger during the detection process.                      |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu LOSLIER -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+
*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QTextBrowser>
#include <QTime>
#include <QLabel>
#include <QMovie>

//Local includes
#include "entity/ta_client.h"
#include "gui/client/ta_clientmainwidget.h"
#include "gui/ta_fonts.h"
#include "gui/ta_mainwindow.h"
#include "gui/ta_statusbar.h"
#include "gui/client/ta_reconnectattempt.h"
#include "communication/client/ta_infoserver.h"
#include "communication/client/ta_clientcommunicator.h"
#include "ta_startdetection.h"

/*
    This constructor creates and initializes this class
        @param parent : widget parent
        @param server : server informations
        @param firstConnection : true if it's first connection
*/
TA_StartDetection::TA_StartDetection(QWidget *parent, TA_InfoServer *server, bool firstConnection) : QWidget(parent){
    layout = new QVBoxLayout;

    QLabel* title = new QLabel(TA_STARTDETECTION_TITLE);
    title->setFont(TA_Fonts::getTitleFont());
    layout->addWidget(title,0,Qt::AlignHCenter | Qt::AlignTop);

    QWidget* loggerW = TA_Client::getInstance()->getLogger()->getLoggerWidget();
    layout->addWidget(loggerW);

    // First message logged
    if(firstConnection)
        TA_Client::getInstance()->getLogger()->log(QTime::currentTime().toString("hh:mm:ss") + " : Connecté au logiciel Tactile Anywhere distant");

    //loading gif
    loadingLabel = new QLabel();
    loadingLabel->hide();
    movie = new QMovie(TA_STARTDETECTION_LOADING);
    loadingLabel->setMovie(movie);
    layout->addWidget(loadingLabel,0,Qt::AlignCenter);

    QPushButton *stopButton = new QPushButton(QIcon(TA_STARTDETECTION_STOP), "Se déconnecter");
    stopButton->setFont(TA_Fonts::getButtonFont());

    connect(stopButton, SIGNAL(clicked()), this, SLOT(deconnect()));
    connect(server,SIGNAL(disconnected(TA_InfoServer*)), this, SLOT(disconnected(TA_InfoServer*)));
    connect(TA_Client::getInstance(), SIGNAL(disconnectedFromMainServer(TA_InfoServer*)),this,SLOT(serverClosed(TA_InfoServer*)));

    layout->addWidget(stopButton);

    setLayout(layout);

    TA_MainWindow::getInstance()->setWindowIcon(QIcon(TA_MAINWINDOW_LOGO_ON));
}

/*****************************************************************************/
/******                             SLOTS                               ******/
/*****************************************************************************/

/*
    Call when the client is disconnected to the server
        @param server : server disconnected
*/
void TA_StartDetection::disconnected(TA_InfoServer* server){
    TA_MainWindow::getInstance()->setWindowIcon(QIcon(TA_MAINWINDOW_LOGO));
    TA_Client::getInstance()->getLogger()->log(QTime::currentTime().toString("hh:mm:ss") + " : Connexion interrompue");
    QString msg = " : Tentative de reconnexion au système ";
    QString serverName = server->getServerName();
    QString serverAddr = server->getServerAddress().toString();
    TA_Client::getInstance()->getLogger()->log(QTime::currentTime().toString("hh:mm:ss") + msg + serverName + "["+serverAddr+"]");
    // If server is protected by password
    if(server->getServerProtection()){
        TA_ClientMainWidget::getInstance()->openReconnectAttempting(server);
    } else {
        TA_MainWindow::getInstance()->getStatusBar()->showMessage("Tentative de reconnexion au système distant en cours.","Warning");
        loadingLabel->show();
        movie->start();
        connect(server,SIGNAL(connected(TA_InfoServer*)), this, SLOT(connectedToServer(TA_InfoServer*)));
        TA_Client::getInstance()->connectTo(server);
    }
}

/*
    Disconnect from server with the disconnect button
*/
void TA_StartDetection::deconnect() {
    TA_Client::getInstance()->closeServer(TA_Client::getInstance()->getClientMainServer());
    TA_ClientMainWidget::getInstance()->openSelectServer();
    TA_MainWindow::getInstance()->setWindowIcon(QIcon(TA_MAINWINDOW_LOGO));
}

/*
    Call when the server ask to disconnect this client
*/
void TA_StartDetection::serverClosed(TA_InfoServer *){
    TA_Client::getInstance()->getLogger()->stopLog();
    TA_MainWindow::getInstance()->getStatusBar()->showMessage("La connexion avec le système Tactyle Anywhere distant a été coupée.", "Warning");
    TA_ClientMainWidget::getInstance()->openSelectServer();
    TA_MainWindow::getInstance()->setWindowIcon(QIcon(TA_MAINWINDOW_LOGO));
}
