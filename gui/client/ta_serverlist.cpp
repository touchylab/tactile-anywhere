/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 17 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This class overload a QListWidget to implement specific behavior for    |
  | Tactile Anywhere.                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu LOSLIER -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include "communication/client/ta_infoserver.h"

//Local includes
#include "ta_serverlist.h"

/*
    Add a server to the servers list
*/
void TA_ServerList::add(TA_InfoServer* server){
    setIconSize(QSize(18,18));
    const QString nameItem = QString(server->getServerName() + " [" + server->getServerAddress().toString() + "]");
    QListWidgetItem *item = new QListWidgetItem();
    item->setText(nameItem);
    if(server->getServerProtection())
        item->setIcon(QIcon(TA_SERVERLIST_LOCK));
    else
        item->setIcon(QIcon(TA_SERVERLIST_UNLOCK));

    addItem(item);
    servers << server;   
}

/*
    Remove all servers from the servers list
*/
void TA_ServerList::clearList(){
    this->clear();
    servers.clear();
}

/*
    Get server with id
        @param id : server identifier
*/
TA_InfoServer* TA_ServerList::getServer(int id){
    return servers[id];
}
