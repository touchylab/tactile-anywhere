/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 17 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_ClientMainWidget class offering    |
  | the main widget concerning the client of Tactile Anywhere.              |                                                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Valentin COULON -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/
/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QVBoxLayout>

// Local includes
#include "communication/client/ta_infoserver.h"
#include "ta_webcamselection.h"
#include "ta_serverselection.h"
#include "ta_authentificationform.h"
#include "ta_startdetection.h"
#include "ta_clientnameform.h"
#include "ta_reconnectattempt.h"
#include "gui/ta_mainwindow.h"
#include "gui/ta_statusbar.h"
#include "ta_clientmainwidget.h"

TA_ClientMainWidget* TA_ClientMainWidget::singleton = NULL;


/*****************************************************************************/
/******                        INITIALISATION                           ******/
/*****************************************************************************/

/*
    Initializes an instance of this class as a singleton
*/
void TA_ClientMainWidget::initialize() {
    TA_ClientMainWidget::singleton = new TA_ClientMainWidget();
}

/*
    Gets the unique instance of this class. initialize() must be called before
        @return : the unique instance of this class or NULL if this class is not initialized
*/
TA_ClientMainWidget *TA_ClientMainWidget::getInstance() {
    return TA_ClientMainWidget::singleton;
}

/*
    This constructor creates and initializes this class
*/
TA_ClientMainWidget::TA_ClientMainWidget() : QWidget() {
    mainLayout = new QVBoxLayout;
    setLayout(mainLayout);
    openNameClient();
}

/*****************************************************************************/
/******                         PUBLIC SLOTS                            ******/
/*****************************************************************************/

/*
    Set corresponding help resource and open the client name form
*/
void TA_ClientMainWidget::openNameClient() {
    TA_MainWindow::getInstance()->setHelp(TA_CLIENTMAINWIDGET_MAIN_FORM_HELP);
    changeCurrentWidget(new TA_ClientNameForm);
}

/*
    Set corresponding help resource and open the webcam selection screen
*/
void TA_ClientMainWidget::openSelectWebcam() {
    TA_MainWindow::getInstance()->setHelp(TA_CLIENTMAINWIDGET_WEBCAM_SELECTION_HELP);
    changeCurrentWidget(new TA_WebcamSelection);
}

/*
    Set corresponding help resource and open the server selection screen
*/
void TA_ClientMainWidget::openSelectServer() {
    TA_MainWindow::getInstance()->setHelp(TA_CLIENTMAINWIDGET_SERVER_SELECTION_HELP);
    changeCurrentWidget(new TA_ServerSelection);
}

/*
    Set corresponding help resource and open the authentification form
*/
void TA_ClientMainWidget::openAuthentificationForm(TA_InfoServer *server, TA_DataConnection packet) {
    Q_UNUSED(packet)
    TA_MainWindow::getInstance()->setHelp(TA_CLIENTMAINWIDGET_AUTH_FORM_HELP);
    changeCurrentWidget(new TA_AuthentificationForm(server, &packet));
}

/*
    Set corresponding help resource and open the start detection screen
*/
void TA_ClientMainWidget::openInitDetection(TA_InfoServer *server, bool firstConnection) {
    TA_MainWindow::getInstance()->setHelp(TA_CLIENTMAINWIDGET_INIT_DETECTION_HELP);
    changeCurrentWidget(new TA_StartDetection(this,server, firstConnection));
}

/*
    Set corresponding help resource and open the reconnect screen
*/
void TA_ClientMainWidget::openReconnectAttempting(TA_InfoServer *server){
    changeCurrentWidget(new TA_ReconnectAttempt(this,server));
}

/*****************************************************************************/
/******                           PRIVATE                               ******/
/*****************************************************************************/

/*
    Replace the current widget by a new widget
        @param newWidget : the new widget substituting
*/
void TA_ClientMainWidget::changeCurrentWidget(QWidget *newWidget) {
    if (currentWidget != NULL) {
        delete currentWidget;
    }
    currentWidget = newWidget;
    mainLayout->addWidget(newWidget);
    TA_MainWindow::getInstance()->getStatusBar()->clearMessage();
}


