/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 17 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This class overload a QListWidget to implement specific behavior for    |
  | Tactile Anywhere.                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu LOSLIER -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_SERVERLIST_H
#define TA_SERVERLIST_H

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/

#define TA_SERVERLIST_LOCK ":/images/lock.png"
#define TA_SERVERLIST_UNLOCK ":/images/unlock.png"

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QListWidget>
#include <QList>

//Fwd
class TA_InfoServer;

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
    Class to manage the servers list GUI
*/
class TA_ServerList : public QListWidget {
    Q_OBJECT

    public:

        /*
            Add a server to the servers list
        */
        void add(TA_InfoServer *server);

        /*
            Remove all servers from the servers list
        */
        void clearList();

        /*
            Get server with id
                @param id : server identifier
        */
        TA_InfoServer* getServer(int id);

    private:
        QList<TA_InfoServer*> servers;
};

#endif // TA_SERVERLIST_H
