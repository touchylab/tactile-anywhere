/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 02 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_ReconnectAttempting use to handle  |
  | reconnection needed a password to connect to server.                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu LOSLIER -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+
*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_RECONNECTATTEMPT_H
#define TA_RECONNECTATTEMPT_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QWidget>

//Local includes
#include "communication/data/ta_dataconnection.h"

//Fwd
class QLineEdit;
class TA_InfoServer;

class TA_ReconnectAttempt : public QWidget{
    Q_OBJECT

    public:
        TA_ReconnectAttempt(QWidget *parent=0, TA_InfoServer *server=0);
    private slots:
        void openInitDetection();
        void connectedToServer(TA_InfoServer*);
        void disconnectedToServer(TA_InfoServer*);
        void onPasswordNeeded(TA_InfoServer*, TA_DataConnection);

    private:
        TA_InfoServer *server;
        QLineEdit *passwordEdit;
        TA_DataConnection packet;
};

#endif // TA_RECONNECTATTEMPT_H
