/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 17 February 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu LOSLIER -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QPushButton>
#include <QLineEdit>

// Local includes
#include "communication/client/ta_infoserver.h"
#include "gui/ta_mainwindow.h"
#include "gui/ta_statusbar.h"
#include "gui/client/ta_clientmainwidget.h"
#include "gui/ta_fonts.h"
#include "ta_authentificationform.h"

/*
    Builder to create an authentification form from server informations
    @param server : the server informations
*/
TA_AuthentificationForm::TA_AuthentificationForm(TA_InfoServer *server, TA_DataConnection* data) : QWidget(){
    this->packet = *data;
    this->server = server;
    QVBoxLayout *mainLayout = new QVBoxLayout();

    // header
    QHBoxLayout *headerLayout = new QHBoxLayout();
    QLabel *titleLabel = new QLabel("Connexion au serveur Tactile Anywhere");
    titleLabel->setFont(TA_Fonts::getTitleFont());
    QPushButton *helpButton = new QPushButton(QIcon(":/images/help.png"),"");
    helpButton->setToolTip(QString("L'aide contextuelle apparaît dans une fenêtre à droite au moment où vous cliquez sur ce bouton. Pour cacher l'aide, cliquez à nouveau sur ce même bouton."));
    headerLayout->addWidget(titleLabel,1, Qt::AlignHCenter| Qt::AlignTop);
    headerLayout->addWidget(helpButton,0, Qt::AlignRight| Qt::AlignTop);
    mainLayout->addLayout(headerLayout);

    //cenral layout
    QVBoxLayout *centralLayout = new QVBoxLayout();

    QLabel* messageLabel = new QLabel("La connexion au serveur nécessite un mot de passe.");
    messageLabel->setFont(TA_Fonts::getDefaultFont());
    centralLayout->addWidget(messageLabel,1, Qt::AlignHCenter | Qt::AlignVCenter);

    // form
    QFormLayout *formLayout = new QFormLayout();
    passwordEdit = new QLineEdit();
    passwordEdit->setFont(TA_Fonts::getDefaultFont());
    passwordEdit->setEchoMode(QLineEdit::Password);
    passwordEdit->setInputMethodHints(Qt::ImhHiddenText| Qt::ImhNoPredictiveText|Qt::ImhNoAutoUppercase);
    QLabel* motDePasseLabel = new QLabel("Mot de passe : ");
    motDePasseLabel->setFont(TA_Fonts::getDefaultFont());
    formLayout->addRow(motDePasseLabel, passwordEdit);
    centralLayout->addLayout(formLayout,4);

    mainLayout->addLayout(centralLayout,5);

    // footer
    QHBoxLayout *footerLayout = new QHBoxLayout();
    QPushButton *returnButton = new QPushButton("Retour");
    returnButton->setFont(TA_Fonts::getButtonFont());
    QPushButton *connectButton = new QPushButton("Se connecter");
    connectButton->setFont(TA_Fonts::getButtonFont());
    footerLayout->addStretch();
    footerLayout->addWidget(returnButton);
    footerLayout->addStretch();
    footerLayout->addWidget(connectButton);
    footerLayout->addStretch();
    mainLayout->addLayout(footerLayout,2);

    setLayout(mainLayout);

    connect(returnButton,SIGNAL(clicked()), this, SLOT(openSelectServer()));
    connect(connectButton,SIGNAL(clicked()), this, SLOT(sendPassword()));


    connect(server,SIGNAL(connected(TA_InfoServer*)), this, SLOT(connectedToServer(TA_InfoServer*)));
    connect(server,SIGNAL(disconnected(TA_InfoServer*)), this, SLOT(disconnectedToServer(TA_InfoServer*)));
    connect(server,SIGNAL(closed(TA_InfoServer*)), this, SLOT(disconnectedToServer(TA_InfoServer*)));
    connect(server,SIGNAL(needPassword(TA_InfoServer*, TA_DataConnection)), this, SLOT(onPasswordNeeded(TA_InfoServer*, TA_DataConnection)));
    connect(helpButton,SIGNAL(clicked()), TA_MainWindow::getInstance(), SLOT(help()));
}

/*****************************************************************************/
/******                             SLOTS                               ******/
/*****************************************************************************/

/*
    Open the server selection screen
*/
void TA_AuthentificationForm::openSelectServer(){
    TA_Client::getInstance()->closeServer(server);
    TA_ClientMainWidget::getInstance()->openSelectServer();
}

/*
    Send the password to the server
*/
void TA_AuthentificationForm::sendPassword(){
    QByteArray hash = QCryptographicHash::hash(passwordEdit->text().toUtf8(),QCryptographicHash::Sha1);
    packet.setHash(hash);
    server->sendPacket(&packet);
}

/*
    Open the detection screen after the server accepts the connection
        @param server : the server informations
*/
void TA_AuthentificationForm::connectedToServer(TA_InfoServer* server){
    TA_Client::getInstance()->getLogger()->log(QTime::currentTime().toString("hh:mm:ss") + " : Connecté au système Tactile Anywhere");
    TA_ClientMainWidget::getInstance()->openInitDetection(server);
}

/*
    Open the server selection screen when the connection
    with the server is disconnected
*/
void TA_AuthentificationForm::disconnectedToServer(TA_InfoServer* server){
    TA_MainWindow::getInstance()->getStatusBar()->showMessage("Le serveur spécifié n'est plus disponible sur le réseau.","Warning");
    TA_Client::getInstance()->closeServer(server);
    TA_ClientMainWidget::getInstance()->openSelectServer();
}

/*
    Do nothing if the password is incorrect, just display error message
*/
void TA_AuthentificationForm::onPasswordNeeded(TA_InfoServer*, TA_DataConnection){
    TA_MainWindow::getInstance()->getStatusBar()->showMessage("Mot de passe incorrect.","Error");
    passwordEdit->setStyleSheet(TA_AUTHENTIFICATIONFORM_ERROR_LINE_EDIT_CSS);
}

