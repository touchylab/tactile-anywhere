/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 17 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_ServerSelection class to choose    |
  | the server wich to connect.                                             |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu LOSLIER -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QLabel>
#include <QMovie>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QIcon>
#include <QLabel>

// Local includes
#include "communication/client/ta_clientcommunicator.h"
#include "communication/client/ta_infoserver.h"
#include "gui/ta_mainwindow.h"
#include "gui/client/ta_clientmainwidget.h"
#include "gui/client/ta_serverlist.h"
#include "gui/ta_fonts.h"
#include "gui/ta_statusbar.h"
#include "ta_serverselection.h"

/*
    Instanciates this class to build this widget
*/
TA_ServerSelection::TA_ServerSelection() : QWidget(){
    QVBoxLayout *mainLayout = new QVBoxLayout();
    this->server = NULL;

    // header
    QHBoxLayout *headerLayout = new QHBoxLayout();
    QLabel *titleLabel = new QLabel("Systèmes Tactile Anywhere existants");
    titleLabel->setFont(TA_Fonts::getTitleFont());
    QPushButton *helpButton = new QPushButton(QIcon(TA_SERVERSELECTION_HELP),"");
    helpButton->setToolTip(QString("L'aide contextuelle apparaît dans une fenêtre à droite au moment où vous cliquez sur ce bouton. Pour cacher l'aide, cliquez à nouveau sur ce même bouton."));
    headerLayout->addWidget(titleLabel,1, Qt::AlignHCenter | Qt::AlignTop);
    headerLayout->addWidget(helpButton,0, Qt::AlignRight | Qt::AlignTop);
    mainLayout->addLayout(headerLayout);

    // central
    QPushButton *updateButton = new QPushButton(QIcon(TA_SERVERSELECTION_UPDATE),"");
    mainLayout->addWidget(updateButton,0,Qt::AlignHCenter);

    // fetch server list
    serverListWidget = new TA_ServerList();
    mainLayout->addWidget(serverListWidget,0,Qt::AlignHCenter);

    // footer
    //loading gif
    loadingLabel = new QLabel();
    movie = new QMovie(TA_SERVERSELECTION_LOADING);
    loadingLabel->setMovie(movie);
    movie->start();

    QHBoxLayout *footerLayout = new QHBoxLayout();
    QPushButton *returnButton = new QPushButton("Retour");
    connectButton = new QPushButton("Se connecter");
    connectButton->setFont(TA_Fonts::getButtonFont());
    returnButton->setFont(TA_Fonts::getButtonFont());
    connectButton->setEnabled(false);
    footerLayout->addStretch();
    footerLayout->addWidget(returnButton,0,Qt::AlignVCenter);
    footerLayout->addStretch();
    footerLayout->addWidget(loadingLabel,0,Qt::AlignVCenter);
    footerLayout->addStretch();
    footerLayout->addWidget(connectButton,0,Qt::AlignVCenter);
    footerLayout->addStretch();
    mainLayout->addLayout(footerLayout);

    setLayout(mainLayout);

    updateServerList(); //update server list for the first display

    connect(serverListWidget, SIGNAL(itemSelectionChanged()), this, SLOT(updateConnectButton()));
    connect(updateButton,SIGNAL(clicked()), this, SLOT(updateServerList()));
    connect(returnButton,SIGNAL(clicked()), this, SLOT(openSelectWebcam()));
    connect(connectButton,SIGNAL(clicked()), this, SLOT(openInitDetection()));
    connect(helpButton,SIGNAL(clicked()), TA_MainWindow::getInstance(), SLOT(help()));
    connect(TA_Client::getInstance()->getClientCommunicator(),SIGNAL(newServer(TA_InfoServer*)),this,SLOT(newServer(TA_InfoServer*)));
}

/*****************************************************************************/
/******                             SLOTS                               ******/
/*****************************************************************************/

/*
    Update server list back end and gui
*/
void TA_ServerSelection::updateServerList(){
    movie->start();
    loadingLabel->show();
    TA_Client::getInstance()->cleanServers();
    serverListWidget->clear();
    TA_Client::getInstance()->getClientCommunicator()->sendBeacon();
}

/*
    Add a new server to the servers list
*/
void TA_ServerSelection::newServer(TA_InfoServer* server){
    loadingLabel->hide();
    movie->stop();
    serverListWidget->add(server);
}

/*
    Open select webcam screen
*/
void TA_ServerSelection::openSelectWebcam(){
    TA_Client::getInstance()->closeServer(server);
    TA_ClientMainWidget::getInstance()->openSelectWebcam();
}

/*
    Initialize the detection and open start detection screen
*/
void TA_ServerSelection::openInitDetection(){
    if(!serverListWidget->selectedItems().isEmpty()){
        loadingLabel->show();
        movie->start();
        connectButton->setEnabled(false);
        server = serverListWidget->getServer(serverListWidget->row(serverListWidget->currentItem()));
        connect(TA_Client::getInstance(),SIGNAL(connectedToMainServer(TA_InfoServer*)), this, SLOT(connectedToServer(TA_InfoServer*)));
        connect(TA_Client::getInstance(),SIGNAL(disconnectedFromMainServer(TA_InfoServer*)), this, SLOT(disconnectedToServer(TA_InfoServer*)));
        connect(server,SIGNAL(disconnected(TA_InfoServer*)), this, SLOT(disconnectedToServer(TA_InfoServer*)));
        connect(server,SIGNAL(needPassword(TA_InfoServer*, TA_DataConnection)), this, SLOT(onPasswordNeedded(TA_InfoServer*,TA_DataConnection)));
        TA_Client::getInstance()->connectTo(server);
    }
}

/*
    Open the detection screen after the server accepts the connection
        @param server : server to connect
*/
void TA_ServerSelection::connectedToServer(TA_InfoServer* server){
    TA_Client::getInstance()->getLogger()->log(QTime::currentTime().toString("hh:mm:ss") + QString(" : Connecté au système Tactile Anywhere distant"));
    loadingLabel->hide();
    movie->stop();
    TA_ClientMainWidget::getInstance()->openInitDetection(server);
}

/*
    Open the server selection screen when the connection
    with the server is disconnected
*/
void TA_ServerSelection::disconnectedToServer(TA_InfoServer*){
    loadingLabel->hide();
    movie->stop();
    TA_Client::getInstance()->closeServer(server);
    TA_MainWindow::getInstance()->getStatusBar()->showMessage("Le serveur spécifié n'est plus disponible sur le réseau.","Warning");
}

/*
    Open the authentification form if server needs a password
        @param server : server to connect
        @param server : packet contain password to send

*/
void TA_ServerSelection::onPasswordNeedded(TA_InfoServer* server, TA_DataConnection packet){
    loadingLabel->hide();
    movie->stop();
    TA_ClientMainWidget::getInstance()->openAuthentificationForm(server, packet);
}

/*
    Enable or disable button if no server is selected
*/
void TA_ServerSelection::updateConnectButton(){
    if(serverListWidget->selectedItems().count() > 0)
        connectButton->setEnabled(true);
    else
        connectButton->setEnabled(false);
}
