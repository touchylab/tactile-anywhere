/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 17 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_WebcamSelection class offering     |
  | the webcam selection for the client using by Tactile Anywhere.          |                                        |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu LOSLIER -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QLabel>
#include <QCheckBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>

// Local includes
#include "gui/ta_mainwindow.h"
#include "entity/ta_client.h"
#include "detection/webcam/ta_webcam.h"
#include "gui/client/ta_clientmainwidget.h"
#include "gui/ta_fonts.h"
#include "gui/ta_flowlayout.h"
#include "ta_webcamselection.h"

/*
    Build the main screen and instantiate this class
*/
TA_WebcamSelection::TA_WebcamSelection() : QWidget(){
    TA_Client::getInstance()->initWebcams();
    frameSide = TA_MainWindow::getInstance()->getScreenSize().width()/5;
    TA_FlowLayout* flowLayout = new TA_FlowLayout();

    QWidget *mainWidget= new QWidget();
    mainWidget->setLayout(flowLayout);

    QScrollArea *scrollArea = new QScrollArea();
    scrollArea->setWidgetResizable(true);
    scrollArea->setWidget(mainWidget);

    QVBoxLayout *mainLayout = new QVBoxLayout();

    //Header
    QHBoxLayout *headerLayout = new QHBoxLayout();
    QLabel *titleLabel = new QLabel("Choix des webcams");
    titleLabel->setFont(TA_Fonts::getTitleFont());
    QPushButton *helpButton = new QPushButton(QIcon(TA_WEBCAMSELECTION_HELP),"");
    helpButton->setToolTip(QString("L'aide contextuelle apparaît dans une fenêtre à droite au moment où vous cliquez sur ce bouton. Pour cacher l'aide, cliquez à nouveau sur ce même bouton."));
    headerLayout->addWidget(titleLabel,1, Qt::AlignHCenter| Qt::AlignTop);
    headerLayout->addWidget(helpButton,0, Qt::AlignRight| Qt::AlignTop);
    mainLayout->addLayout(headerLayout);

    //Webcams
    mainLayout->addWidget(scrollArea);

    setLayout(mainLayout);

    QList<TA_Webcam*> webcams = TA_Client::getInstance()->getWebcams();

    // fetch all webcams
    for(int i = 0; i < webcams.size(); i++){

        QVBoxLayout *cellLayout = new QVBoxLayout();

        // top widget and horizontal layout
        QLabel *camTitle = new QLabel(webcams[i]->getName());
        camTitle->setFont(TA_Fonts::getSubTitleFont());
        QHBoxLayout* topHLayout = new QHBoxLayout;
        topHLayout->addWidget(camTitle,0,Qt::AlignHCenter);

        QCheckBox* webcamCheckBox = new QCheckBox();
        topHLayout->addWidget(webcamCheckBox,0,Qt::AlignRight);
        cellLayout->addLayout(topHLayout);
        cbwcMap[webcamCheckBox] = webcams.value(i);

        // label picture
        QLabel* webcamPictureLabel = new QLabel(mainWidget);
        webcamPictureLabel->setFont(TA_Fonts::getDefaultFont());
        cbwbMap[webcamPictureLabel] = webcams.value(i);
        webcamPictureLabel->setFixedSize(frameSide,frameSide);
        QImage frame;
        webcams[i]->getQtFrame(frame);

        if(frame.size() == QSize(0,0)){
            frame.load(TA_WEBCAMSELECTION_NO_AVAILABLE);
        }

        webcamPictureLabel->setPixmap(QPixmap::fromImage(frame).scaled(frameSide,frameSide));
        cellLayout->addWidget(webcamPictureLabel,1,Qt::AlignCenter);

        TA_ClickableQWidget *clickableQWidget = new TA_ClickableQWidget();
        connect(clickableQWidget, SIGNAL(clicked()), webcamCheckBox, SLOT(click()));
        clickableQWidget->setLayout(cellLayout);
        flowLayout->addWidget(clickableQWidget);
    }

    //Footer
    QHBoxLayout* footerLayout = new QHBoxLayout();
    QHBoxLayout* buttonLayout = new QHBoxLayout();
    QPushButton* addWebcamsButton = new QPushButton("Continuer");
    addWebcamsButton->setFont(TA_Fonts::getButtonFont());
    QPushButton* cancelButton = new QPushButton("Retour");
    cancelButton->setFont(TA_Fonts::getButtonFont());
    buttonLayout->addStretch();
    buttonLayout->addWidget(cancelButton,0,Qt::AlignTop);
    buttonLayout->addStretch();
    buttonLayout->addWidget(addWebcamsButton,0,Qt::AlignTop);
    buttonLayout->addStretch();
    footerLayout->addLayout(buttonLayout);

    mainLayout->addLayout(footerLayout);

    // Connections
    connect(addWebcamsButton, SIGNAL(pressed()), this, SLOT(openSelectServer()));
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(cancel()));
    connect(helpButton,SIGNAL(clicked()), TA_MainWindow::getInstance(), SLOT(help()));

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateFrame()));
    timer->start(TA_WEBCAMSELECTION_FRAME_RATE);

    setLayout(mainLayout);
    this->setMinimumSize(frameSide*1.5,frameSide*1.5);
}

/*****************************************************************************/
/******                             SLOTS                               ******/
/*****************************************************************************/

/*
    Click on cancel button return to the name client screen
*/
void TA_WebcamSelection::cancel(){
    delete TA_Client::getInstance();
    TA_ClientMainWidget::getInstance()->openNameClient();
}

/*
    Open select servers screen
*/
void TA_WebcamSelection::openSelectServer(){
    QList<TA_Webcam*> selected;
    foreach (QCheckBox* cb, cbwcMap.keys()){
        if(cb->isChecked()){
            selected.append(cbwcMap.value(cb));
        }
    }
    TA_Client::getInstance()->setSelectedWebcams(selected);
    TA_ClientMainWidget::getInstance()->openSelectServer();
}

/*
    Update frame foreach webcam connected
*/
void TA_WebcamSelection::updateFrame(){
    foreach (QLabel* label, cbwbMap.keys()) {
        QImage frame;
        for(int i = 0; i<3 && !cbwbMap.value(label)->getQtFrame(frame); i++);
        if(frame.size() == QSize(0,0)){
            frame.load(TA_WEBCAMSELECTION_NO_AVAILABLE);
        }

        frame = frame.scaled(frameSide,frameSide);
        label->setPixmap(QPixmap::fromImage(frame));
        repaint();
    }
}
