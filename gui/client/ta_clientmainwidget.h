/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 17 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_ClientMainWidget class offering    |
  | the main widget concerning the client of Tactile Anywhere.              |                                                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Valentin COULON -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_CLIENTMAINWIDGET_H
#define TA_CLIENTMAINWIDGET_H

#define TA_CLIENTMAINWIDGET_MAIN_FORM_HELP "qrc:///html/clientnameform.html"
#define TA_CLIENTMAINWIDGET_WEBCAM_SELECTION_HELP "qrc:///html/clientwebcamselection.html"
#define TA_CLIENTMAINWIDGET_SERVER_SELECTION_HELP "qrc:///html/clientserverselection.html"
#define TA_CLIENTMAINWIDGET_AUTH_FORM_HELP "qrc:///html/clientauthentificationform.html"
#define TA_CLIENTMAINWIDGET_INIT_DETECTION_HELP "qrc:///html/clientinitdetection.html"

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QWidget>

//Local includes
#include "communication/data/ta_dataconnection.h"

//Fwd
class QVBoxLayout;
class TA_InfoServer;
class TA_Webcam;
class TA_MainWindow;
class TA_Client;


/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
    TA_ClientMainWindow is a class representing the client GUI in Tactile Anywhere.
*/

class TA_ClientMainWidget : public QWidget {
    Q_OBJECT

    public :
        /*
            Gets the unique instance of this class. initialize() must be called before
                @return : the unique instance of this class or NULL if this class is not initialized
        */
        static TA_ClientMainWidget* getInstance();

        /*
            Initializes an instance of this class as a singleton
        */
        static void initialize();

    public slots :
        /*
            Set corresponding help resource and open the client name form
        */
        void openNameClient();

        /*
            Set corresponding help resource and open the webcam selection screen
        */
        void openSelectWebcam();

        /*
            Set corresponding help resource and open the server selection screen
        */
        void openSelectServer();

        /*
            Set corresponding help resource and open the authentification form
        */
        void openAuthentificationForm(TA_InfoServer *server, TA_DataConnection packet);

        /*
            Set corresponding help resource and open the start detection screen
        */
        void openInitDetection(TA_InfoServer *server, bool firstConnection=true);

        /*
            Set corresponding help resource and open the reconnect screen
        */
        void openReconnectAttempting(TA_InfoServer *server);
    private :
        /*
            This constructor creates and initializes this class
        */
        TA_ClientMainWidget();

        /*
            Replace the current widget by a new widget
                @param newWidget : the new widget substituting
        */
        void changeCurrentWidget(QWidget* newWidget);


        static TA_ClientMainWidget* singleton; // Single instance of TA_ClientMainWindow

        QVBoxLayout* mainLayout;
        QWidget* currentWidget = NULL;
};

#endif // TA_CLIENTMAINWIDGET_H
