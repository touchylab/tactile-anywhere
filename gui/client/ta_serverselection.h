/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 17 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_ServerSelection class to choose    |
  | the server wich to connect.                                             |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu LOSLIER -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_SERVERSELECTION_H
#define TA_SERVERSELECTION_H

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/

#define TA_SERVERSELECTION_HELP ":/images/help.png"
#define TA_SERVERSELECTION_UPDATE ":/images/update.png"
#define TA_SERVERSELECTION_LOADING ":/images/loading.gif"

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QWidget>

//Local includes
#include "communication/data/ta_dataconnection.h"

//Fwd
class QPushButton;
class QMovie;
class QLabel;
class TA_InfoServer;
class TA_ServerList;

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
    Class to manage the server selection in the servers list
*/
class TA_ServerSelection : public QWidget{
    Q_OBJECT

    public:
        /*
            Instanciates this class to build this widget
        */
        TA_ServerSelection();

    private slots :
        /*
            Update server list back end and gui
        */
        void updateServerList();

        /*
            Add a new server to the servers list
        */
        void newServer(TA_InfoServer* server);

        /*
            Enable or disable button if no server is selected
        */
        void updateConnectButton();

        /*
            Open select webcam screen
        */
        void openSelectWebcam();

        /*
            Initialize the detection and open start detection screen
        */
        void openInitDetection();

        /*
            Open the detection screen after the server accepts the connection
                @param server : server to connect
        */
        void connectedToServer(TA_InfoServer* server);

        /*
            Open the authentification form if server needs a password
                @param server : server to connect
                @param server : packet contain password to send

        */
        void onPasswordNeedded(TA_InfoServer* server, TA_DataConnection packet);

        /*
            Open the server selection screen when the connection
            with the server is disconnected
        */
        void disconnectedToServer(TA_InfoServer*);

    private :
        TA_ServerList *serverListWidget;
        QPushButton *connectButton;
        QMovie *movie;
        QLabel *loadingLabel;
        TA_InfoServer* server;
};

#endif // TA_SERVERSELECTION_H
