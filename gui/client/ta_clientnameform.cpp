/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 24 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_ClientNameForm class asking for a  |
  | name for the client                                                     |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QLayout>
#include <QFormLayout>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>

// Local Includes
#include "entity/ta_client.h"
#include "gui/client/ta_clientmainwidget.h"
#include "gui/ta_mainwindow.h"
#include "gui/ta_fonts.h"
#include "ta_clientnameform.h"

/*
    Instanciates this class with default values
        @param name : the initial name
*/
TA_ClientNameForm::TA_ClientNameForm(QString name) : QWidget() {
    QVBoxLayout *mainLayout = new QVBoxLayout();

    //Header
    QHBoxLayout *headerLayout = new QHBoxLayout();
    QLabel *titleLabel = new QLabel("Informations");
    titleLabel->setFont(TA_Fonts::getTitleFont());
    QPushButton *helpButton = new QPushButton(QIcon(":/images/help.png"),"");
    helpButton->setToolTip(QString("L'aide contextuelle apparaît dans une fenêtre à droite au moment où vous cliquez sur ce bouton. Pour cacher l'aide, cliquez à nouveau sur ce même bouton."));
    headerLayout->addWidget(titleLabel,1, Qt::AlignHCenter| Qt::AlignTop);
    headerLayout->addWidget(helpButton,0, Qt::AlignRight| Qt::AlignTop);
    connect(helpButton,SIGNAL(clicked()), TA_MainWindow::getInstance(), SLOT(help()));
    mainLayout->addLayout(headerLayout);
    mainLayout->addStretch(1);

    //Form layout
    QFormLayout *formLayout = new QFormLayout();

    // Name
    nameLineEdit = new QLineEdit(name);
    nameLineEdit->setFont(TA_Fonts::getDefaultFont());
    nameLineEdit->setMaxLength(100);
    connect(nameLineEdit,SIGNAL(editingFinished()),this,SLOT(checkName()));
    QLabel* nomLabel = new QLabel("Nom : ");
    nomLabel->setFont(TA_Fonts::getDefaultFont());
    formLayout->addRow(nomLabel, nameLineEdit);

    QHBoxLayout *centralLayout = new QHBoxLayout();
    centralLayout->addStretch();
    centralLayout->addLayout(formLayout);
    centralLayout->addStretch();
    mainLayout->addLayout(centralLayout);

    //Footer
    QHBoxLayout *footerLayout = new QHBoxLayout();

    QPushButton *returnButton = new QPushButton("Retour");
    returnButton->setFont(TA_Fonts::getButtonFont());
    connect(returnButton, SIGNAL(clicked()), this, SLOT(cancel()));
    footerLayout->addStretch();
    footerLayout->addWidget(returnButton, 0, Qt::AlignTop);

    QPushButton *launchButton = new QPushButton("Continuer");
    launchButton->setFont(TA_Fonts::getButtonFont());
    connect(launchButton, SIGNAL(clicked()), this, SLOT(next()));
    footerLayout->addStretch();
    footerLayout->addWidget(launchButton, 0, Qt::AlignTop);
    footerLayout->addStretch();
    mainLayout->addStretch(1);
    mainLayout->addLayout(footerLayout);

    setLayout(mainLayout);
}


/*****************************************************************************/
/******                             SLOTS                               ******/
/*****************************************************************************/

/*
    Checks the name of the client field
*/
void TA_ClientNameForm::checkName(){
    if(nameLineEdit->text().isNull() || nameLineEdit->text().size() == 0){
        nameLineEdit->setText(QHostInfo::localHostName());
    }
}

/*
    Launch the client with the default configuration
*/
void TA_ClientNameForm::next(){
    TA_Client::initialize(false);
    TA_Client::getInstance()->setClientName(nameLineEdit->text());
    TA_ClientMainWidget::getInstance()->openSelectWebcam();
}

/*
    Cancel the operation and return to the previous view
*/
void TA_ClientNameForm::cancel() {
    TA_MainWindow::getInstance()->buildMainWidget();
}
