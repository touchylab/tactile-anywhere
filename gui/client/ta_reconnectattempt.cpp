/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 02 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_ReconnectAttempting use to handle  |
  | reconnection needed a password to connect to server.                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu LOSLIER -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+
*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QByteArray>

// Local includes
#include "communication/client/ta_infoserver.h"
#include "gui/ta_mainwindow.h"
#include "gui/client/ta_clientmainwidget.h"
#include "gui/ta_fonts.h"
#include "gui/ta_statusbar.h"
#include "ta_reconnectattempt.h"

TA_ReconnectAttempt::TA_ReconnectAttempt(QWidget *parent, TA_InfoServer *server) : QWidget(parent){
    this->server = server;
    QVBoxLayout *mainLayout = new QVBoxLayout();

    // header
    QHBoxLayout *headerLayout = new QHBoxLayout();
    QLabel *titleLabel = new QLabel("Tentative de reconnexion au système Tactile Anywhere distant");
    titleLabel->setFont(TA_Fonts::getTitleFont());
    QPushButton *helpButton = new QPushButton(QIcon(":/images/help.png"),"");
    helpButton->setToolTip(QString("L'aide contextuelle apparaît dans une fenêtre à droite au moment où vous cliquez sur ce bouton. Pour cacher l'aide, cliquez à nouveau sur ce même bouton."));
    headerLayout->addWidget(titleLabel,1, Qt::AlignHCenter| Qt::AlignTop);
    headerLayout->addWidget(helpButton,0, Qt::AlignRight| Qt::AlignTop);
    mainLayout->addLayout(headerLayout);

    //cenral layout
    QVBoxLayout *centralLayout = new QVBoxLayout();

    QLabel* messageLabel = new QLabel("La reconnexion au système Tactile Anywhere distant nécessite un mot de passe.");
    messageLabel->setFont(TA_Fonts::getDefaultFont());
    centralLayout->addWidget(messageLabel,1, Qt::AlignHCenter | Qt::AlignVCenter);

    // form
    QFormLayout *formLayout = new QFormLayout();
    passwordEdit = new QLineEdit();
    passwordEdit->setFont(TA_Fonts::getDefaultFont());
    passwordEdit->setEchoMode(QLineEdit::Password);
    passwordEdit->setInputMethodHints(Qt::ImhHiddenText| Qt::ImhNoPredictiveText|Qt::ImhNoAutoUppercase);
    QLabel* motDePasseLabel = new QLabel("Mot de passe : ");
    motDePasseLabel->setFont(TA_Fonts::getDefaultFont());
    formLayout->addRow(motDePasseLabel, passwordEdit);
    centralLayout->addLayout(formLayout,4);

    mainLayout->addLayout(centralLayout,5);

    // footer
    QHBoxLayout *footerLayout = new QHBoxLayout();
    QPushButton *cancelButton = new QPushButton("Annuler");
    cancelButton->setFont(TA_Fonts::getButtonFont());
    QPushButton *reconnectButton = new QPushButton("Se reconnecter");
    reconnectButton->setFont(TA_Fonts::getButtonFont());
    footerLayout->addStretch();
    footerLayout->addWidget(cancelButton);
    footerLayout->addStretch();
    footerLayout->addWidget(reconnectButton);
    footerLayout->addStretch();
    mainLayout->addLayout(footerLayout,2);

    setLayout(mainLayout);

    connect(cancelButton,SIGNAL(clicked()), TA_ClientMainWidget::getInstance(), SLOT(openSelectServer()));
    connect(reconnectButton,SIGNAL(clicked()), this, SLOT(openInitDetection()));
    connect(helpButton,SIGNAL(clicked()), TA_MainWindow::getInstance(), SLOT(help()));

    connect(server,SIGNAL(connected(TA_InfoServer*)), this, SLOT(connectedToServer(TA_InfoServer*)));
    connect(TA_Client::getInstance(),SIGNAL(connectedToMainServer(TA_InfoServer*)), this, SLOT(connectedToServer(TA_InfoServer*)));
    connect(TA_Client::getInstance(),SIGNAL(disconnectedFromMainServer(TA_InfoServer*)), this, SLOT(disconnectedToServer(TA_InfoServer*)));
    connect(server,SIGNAL(disconnected(TA_InfoServer*)), this, SLOT(disconnectedToServer(TA_InfoServer*)));
    connect(server,SIGNAL(needPassword(TA_InfoServer*, TA_DataConnection)), this, SLOT(onPasswordNeeded(TA_InfoServer*,TA_DataConnection)));
}


void TA_ReconnectAttempt::connectedToServer(TA_InfoServer*){
    TA_Client::getInstance()->getLogger()->log(QTime::currentTime().toString("hh:mm:ss") + QString(" : Reconnecté au système Tactile Anywhere"));
    TA_ClientMainWidget::getInstance()->openInitDetection(server, false);
}

void TA_ReconnectAttempt::openInitDetection(){
    QByteArray hash = QCryptographicHash::hash(passwordEdit->text().toUtf8(),QCryptographicHash::Sha1);
    packet.setHash(hash);
    server->sendPacket(&packet);
}

void TA_ReconnectAttempt::disconnectedToServer(TA_InfoServer*){
    TA_MainWindow::getInstance()->getStatusBar()->showMessage("Echec lors de la tentative de reconnexion au système Tactile Anywhere distant.", "Error");
    TA_ClientMainWidget::getInstance()->openSelectServer();
}

void TA_ReconnectAttempt::onPasswordNeeded(TA_InfoServer*, TA_DataConnection){
    TA_MainWindow::getInstance()->getStatusBar()->showMessage("Mot de passe incorrect.", "Error");
}
