/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 17 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_WebcamSelection class offering     |
  | the webcam selection for the client using by Tactile Anywhere.          |                                        |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu LOSLIER -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_WEBCAMSELECTION_H
#define TA_WEBCAMSELECTION_H

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/

#define TA_WEBCAMSELECTION_HELP ":/images/help.png"
#define TA_WEBCAMSELECTION_NO_AVAILABLE ":/images/noAvailable.png"
#define TA_WEBCAMSELECTION_FRAME_RATE 500

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QWidget>
#include <QList>
#include <QMap>

#include "gui/ta_clickableqwidget.h"

//Fwd
class QLabel;
class QCheckBox;
class TA_Webcam;


/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
    Class to manage webcams selection in a GUI
*/
class TA_WebcamSelection : public QWidget{
    Q_OBJECT

public:
    /*
        Build the main screen and instantiate this class
    */
    TA_WebcamSelection();

private slots :
    /*
        Click on cancel button return to the name client screen
    */
    void cancel();

    /*
        Open select servers screen
    */
    void openSelectServer();

    /*
        Update frame foreach webcam connected
    */
    void updateFrame();

private :
    QList<TA_Webcam*> webcams;
    int frameSide;

    QMap<QLabel*, TA_Webcam*> cbwbMap;
    QMap<QCheckBox*, TA_Webcam*> cbwcMap;
};

#endif // TA_WEBCAMSELECTION_H
