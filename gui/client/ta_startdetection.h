/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 17 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_StartDetection presenting the      |
  | screen with a logger during the detection process.                      |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu LOSLIER -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+
*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_STARTDETECTION_H
#define TA_STARTDETECTION_H

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/

#define TA_STARTDETECTION_STOP ":/images/stop.png"
#define TA_STARTDETECTION_LOADING ":/images/loading.gif"
#define TA_STARTDETECTION_TITLE "Détails des échanges avec le logiciel distant"

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QWidget>

//Local includes
#include "communication/data/ta_dataconnection.h"

//Fwd
class QVBoxLayout;
class QLabel;
class QMovie;
class TA_InfoServer;


/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
    Class to manage the start detection process in a GUI
*/
class TA_StartDetection : public QWidget{
    Q_OBJECT

    public:

        /*
            This constructor creates and initializes this class
                @param parent : widget parent
                @param server : server informations
                @param firstConnection : true if it's first connection
        */
        TA_StartDetection(QWidget *parent=0, TA_InfoServer *server=0, bool firstConnection=true);

    private slots:
        /*
            Call when the server ask to disconnect this client
        */
        void serverClosed(TA_InfoServer *);

        /*
            Disconnect from server with the disconnect button
        */
        void deconnect();


        /*
            Call when the client is disconnected to the server
                @param server : server disconnected
        */
        void disconnected(TA_InfoServer*server);

    private:
        QVBoxLayout* layout;
        QLabel *loadingLabel;
        QMovie *movie;
};

#endif // TA_STARTDETECTION_H
