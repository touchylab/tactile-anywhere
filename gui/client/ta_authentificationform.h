/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 17 February 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_AutentificationForm class offering |
  | the authentification form if needed during the conection to the server  |
  | of Tactile Anywhere.                                                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu LOSLIER -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/
/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_AUTHENTIFICATIONFORM_H
#define TA_AUTHENTIFICATIONFORM_H

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/

//Images
#define TA_AUTHENTIFICATIONFORM_HELP ":/images/help.png"
#define TA_AUTHENTIFICATIONFORM_ERROR_LINE_EDIT_CSS "QLineEdit{border: 1px solid rgba(255,0,0,150);}"


/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QWidget>
#include <QByteArray>

//Local includes
#include "communication/data/ta_dataconnection.h"

//Fwd
class QLineEdit;
class TA_InfoServer;


/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
    TA_AuthentificationForm is a class use for authentify the client if nedeed in Tactile Anywhere.
*/
class TA_AuthentificationForm : public QWidget{
    Q_OBJECT

    public:
        /*
            Builder to create an authentification form from server informations
                @param server : the server informations
                @param data : the data connection corresponding
        */
        TA_AuthentificationForm(TA_InfoServer *server, TA_DataConnection* data);

    private slots :
        /*
            Open the server selection screen
        */
        void openSelectServer();

        /*
            Send the password to the server
        */
        void sendPassword();

        /*
            Open the detection screen after the server accepts the connection
                @param server : the server informations
        */
        void connectedToServer(TA_InfoServer* server);

        /*
            Do nothing if the password is incorrect, just display message
        */
        void onPasswordNeeded(TA_InfoServer*, TA_DataConnection);

        /*
            Open the server selection screen when the connection
            with the server is disconnected
        */
        void disconnectedToServer(TA_InfoServer*);

    private :
        TA_DataConnection packet;
        TA_InfoServer *server;
        QLineEdit *passwordEdit;
};

#endif // TA_AUTHENTIFICATIONFORM_H
