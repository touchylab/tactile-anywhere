/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 27 february 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_ToolBar class offering specific    |
  | toolbar animated for Tactile Anywhere                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu Loslier -- Initial version                            |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/


#ifndef TA_TOOLBAR_H
#define TA_TOOLBAR_H

/*****************************************************************************/
/******                             DEFINES                             ******/
/*****************************************************************************/

// animation of toolbar is set to 1000 ms
#define TA_TOOLBAR_ANIMATION_DURATION_IN_MS 1000
// toolbar height is 5% of screen height
#define TA_TOOLBAR_HEIGHT_TOOLBAR_FACTOR 0.05

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

//Qt
#include <QWidget>
#include <QLayout>
#include <QToolBar>
#include <QPropertyAnimation>
#include <QMouseEvent>

//Local includes
#include "gui/ta_mainwindow.h"

enum ToolBarState { SLIDE_IN, SLIDE_OUT };

class TA_ToolBar : public QToolBar{
    Q_OBJECT
    public:

        /*
            Instanciates this class with an widget parent
                @param parent : the parent widget
        */
        explicit TA_ToolBar(QWidget *parent = 0);

        /*
            Toogle the toolbar (show/hide)
        */
        void slide();

    private slots:
        /*
            Call when the animation is finished
        */
        void finish();

    private:
        /*
            Update the draw zone for button to hide and show toolbar
        */
        void update();

        QRect rectToolBarStart;
        QRect rectToolBarEnd;
        QPropertyAnimation *animation;
        ToolBarState toolBarState;
        QWidget *parent;
};

#endif // TA_TOOLBAR_H
