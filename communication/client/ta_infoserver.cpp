/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 09 january 2013                                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_InfoServer class containing all    |
  | the information about a server                                          |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_infoserver.h"

/*****************************************************************************/
/******                     CONSTRUCTORS/DESTRUCTOR                     ******/
/*****************************************************************************/

/*
    This constructor creates and initializes this class with the server's address and port.
    Use this constructor to create a distant server.
        @param address : the server's address
        @param port : the server's port
*/
TA_InfoServer::TA_InfoServer(QHostAddress address, quint16 port) {
    // Initializes variables
    this->address = address;
    this->port = port;
    this->local = false;
    this->header = true;
    this->alreadyConnected = false;
    this->nbTryReco = 0;
    setServerSocket(new QTcpSocket());
    stream.setDevice(socket);
    setServerDiffDate(0);
    setServerState(NO_CONNECTION);

    // Connect signal
    connect(socket, SIGNAL(readyRead()), this, SLOT(read()));
    connect(&timerNTP, SIGNAL(timeout()), this, SLOT(sendNTP()));
    connect(&timerSendHello, SIGNAL(timeout()), this, SLOT(sendHello()));
    // Reconnection not completed
    //connect(&timerDisconnect, SIGNAL(timeout()), this, SLOT(disconnect()));
    //connect(&timerReconnect, SIGNAL(timeout()), this, SLOT(connection()));
    connect(&timerDisconnect, SIGNAL(timeout()), this, SLOT(close()));
}

/*
    This constructor creates and initializes this class as a local server.Use this constructor
    to create a communication link between a client and its local server.
*/
TA_InfoServer::TA_InfoServer() {
    // Initializes variables
    this->port = -1;
    this->nbTryReco = 0;
    this->local = true;
    this->header = true;
    this->alreadyConnected = true;
    setServerName(QString(TA_INFOSERVER_LOCAL_NAME));
    setServerSocket(NULL);
    setServerDiffDate(0);
    setServerState(LOCAL);
}

/*
    Cleans properly this class at the destruction
*/
TA_InfoServer::~TA_InfoServer() {
    close(false);
}

/*****************************************************************************/
/******                           OPERATIONS                            ******/
/*****************************************************************************/

/*
    Sends to this server the given packet
        @param data : the packet to send
        @param tryAnyway : true to try to send even if the socket is considerate as inactive
*/
void TA_InfoServer::sendPacket(TA_Data* data,bool tryAnyway) {
    if (!tryAnyway && (getServerState() == CLOSED || getServerState() == DISCONNECTED)) {
        return;
    }

    if (!local) {
        data->sendData(stream);
    } else {
        TA_Server::getInstance()->getServerLocalClient()->manageData(data);
    }
}

/*
    Manages the given data
        @param stream : the data to manage
*/
void TA_InfoServer::manageData(TA_Data* data) {
    // Applies the correct operations
    switch (data->getIdentification()) {
    case TA_DATACONNECTION_IDENT : {  // Connection packet
        // Gets the packet
        TA_DataConnection* conn = (TA_DataConnection*) data;
        timerReconnect.stop();
        if (!timerSendHello.isActive()){
            timerSendHello.start(TA_INFOSERVER_HELLO_INTERVAL_MS);
        }

        if (getServerState() == ASKED_FOR_CONNECTION || getServerState() == DISCONNECTED) {  // If this client asked for a connection
            if (conn->isAuthorized()) {
                setServerState(CONNECTED);
            } else if (conn->isProtected()) {
                setServerState(PASSWORD_NEEDED);
                emit needPassword(this, *conn);
                return;
            } else {
                close();
                return;
            }
        } else if (getServerState() == PASSWORD_NEEDED) {  // If this client sended a password
            if (conn->isAuthorized()) {
                setServerState(CONNECTED);
            } else {
                emit needPassword(this, *conn);
                return;
            }
        }

        if (getServerState() == CONNECTED) {
            alreadyConnected = true;
            sendNTP();
            timerNTP.start(TA_INFOSERVER_NTP_INTERVAL_MS);
            emit connected(this);
        }

        break;
    } case TA_DATACLOSE_IDENT : {  // Close packet
        close();
        break;
    } case TA_DATAHELLO_IDENT : {  // Hello packet
        timerDisconnect.stop();
        timerSendHello.start(TA_INFOSERVER_HELLO_INTERVAL_MS);
        break;
    } case TA_DATANTP_IDENT : {  // NTP packet
        // Gets the packet
        TA_DataNTP* ntp = (TA_DataNTP*) data;

        // Places the last client time
        ntp->setClientSecondDate(boost::posix_time::microsec_clock::local_time().time_of_day().total_milliseconds());

        // Update the difference of date
        setServerDiffDate(ntp->getDiffDate());
        break;
    } case TA_DATASECTOR_IDENT : {  // Sector packet
        emit newSector(this, (*(TA_DataSector*) data));
        break;
    } case TA_DATADETECTION_IDENT : {  // Detection packet
        emit newDetectionOrder(this, (*(TA_DataDetection*) data));
        break;
    } case TA_DATASTREAM_IDENT : {  // Stream packet
        emit newStreamAsk(this, (*(TA_DataStream*) data));
        break;
    } case TA_DATAMARKER_IDENT : {  // Marker packet
        emit newMarkerOrder(this, (*(TA_DataMarker*) data));
        break;
    }
    }
}

/*****************************************************************************/
/******                             GETTERS                             ******/
/*****************************************************************************/

/*
    Gets the server's name.
        @return : the server's name
*/
QString TA_InfoServer::getServerName() const {
    return nameServer;
}

/*
    Gets the server's socket.
        @return : the server's socket
*/
QTcpSocket* TA_InfoServer::getServerSocket() const {
    return socket;
}

/*
    Gets the server's address.
        @return : the server's address
*/
QHostAddress TA_InfoServer::getServerAddress() const {
    return address;
}

/*
    Gets the server's port.
        @return : the server's port
*/
quint16 TA_InfoServer::getServerPort() const {
    return port;
}

/*
    Gets the server's protection.
        @return : true if the server is protected with a password, false else
*/
bool TA_InfoServer::getServerProtection() const {
    return protection;
}

/*
    Gets the state of the connection with this server
        @return : the state of the connection with the server
*/
ConnectionState TA_InfoServer::getServerState() const {
    return state;
}

/*
    Gets the difference of date between this client and the server
        @return : the number of millisecond of difference of date
*/
long long int TA_InfoServer::getServerDiffDate() const {
    return diffDate;
}

/*****************************************************************************/
/******                             SETTERS                             ******/
/*****************************************************************************/

/*
    Sets the server's name.
        @param name : the server's name to set
*/
void TA_InfoServer::setServerName(const QString& name) {
    this->nameServer = name;
}

/*
    Sets the server's socket.
        @param name : the server's socket to set
*/
void TA_InfoServer::setServerSocket(QTcpSocket* socket) {
    this->socket = socket;
    this->stream.setDevice(socket);
}

/*
    Sets the server's protection.
        @param protection : true if the server is protected with a password, false else
*/
void TA_InfoServer::setServerProtection(bool protection) {
    this->protection = protection;
}

/*
    Sets the state of the connection with this server
        @param state : the state of the connection with the server
*/
void TA_InfoServer::setServerState(ConnectionState state) {
    this->state = state;
}

/*
    Sets the difference of date between this client and the server
        @param date : the number of millisecond of difference of date
*/
void TA_InfoServer::setServerDiffDate(long long int date) {
    diffDate = date;
}

/*****************************************************************************/
/******                           SLOTS                                 ******/
/*****************************************************************************/

/*
    Ask for a connection to this server
        @return: true if the connection can be asked, false else
*/
bool TA_InfoServer::connection() {
    if(getServerState() == CLOSED || socket == NULL){
        return false;
    }

    // Creates the connection
    if (nbTryReco >= 4 || (socket->state() != QAbstractSocket::ConnectedState
            && (getServerState() != DISCONNECTED || socket->state() != QAbstractSocket::ConnectingState))) {
        if(getServerState() == DISCONNECTED){
            socket->abort();
            socket->deleteLater();
            setServerSocket(new QTcpSocket);
        }

        socket->connectToHost(address, port);

        // Asks a connection
        if (socket->state() != QAbstractSocket::ConnectedState && !socket->waitForConnected(2000)) {
            return false;
        }
        if(getServerState() == DISCONNECTED){
            QThread::msleep(200);
        }

        this->nbTryReco = 0;
    }

    if (socket->state() == QAbstractSocket::ConnectedState) {
        TA_DataConnection data;
        data.setClientName(TA_Client::getInstance()->getClientName());
        data.setFirstConnection(!alreadyConnected);
        if(getServerState() != DISCONNECTED){
            setServerState(ASKED_FOR_CONNECTION);
        }
        sendPacket(&data,true);
        timerSendHello.start(TA_INFOSERVER_HELLO_INTERVAL_MS);
        nbTryReco++;
    }
    return true;
}

/*
    Reads a paquet from this server
*/
void TA_InfoServer::read() {
    while (socket && socket->bytesAvailable()) {
        // Checks for complete header
        if (header){
            if(socket->bytesAvailable() < TA_INFOSERVER_HEADER_LENGTH){
                return;
            }
            stream >> identPacket >> lengthPacket;
            header = false;
        }

        // Checks for complete packet
        if(socket->bytesAvailable() < lengthPacket){
            return;
        }
        header = true;

        // Reads data and applies the correct operations
        switch (identPacket) {
            case TA_DATACONNECTION_IDENT : {  // Connection packet
                if(getServerState() == CONNECTED || getServerState() == CLOSED){ // If the client is already connected or closed, ignore the packet
                    socket->read(lengthPacket);
                    break;
                }

                // Else, reads it and manage it
                TA_DataConnection conn = TA_DataConnection(stream,lengthPacket);
                manageData(&conn);
                break;
            } case TA_DATACLOSE_IDENT : { // Close packet
                TA_DataClose close;
                manageData(&close);
                break;
            } case TA_DATAHELLO_IDENT : { // Hello packet
                TA_DataHello hello;
                manageData(&hello);
                break;
            }case TA_DATANTP_IDENT : { // NTP packet
                // Reads the packet and manage it
                TA_DataNTP ntp = TA_DataNTP(stream,lengthPacket);
                manageData(&ntp);
                break;
            } case TA_DATADETECTION_IDENT : { // Detection packet
                TA_DataDetection detectionData(stream,lengthPacket);
                manageData(&detectionData);
                break;
            }case TA_DATASTREAM_IDENT : { // Stream packet
                TA_DataStream streamData(stream,lengthPacket);
                manageData(&streamData);
                break;
            }case TA_DATAMARKER_IDENT : { // Marker packet
                TA_DataMarker markerData(stream,lengthPacket);
                manageData(&markerData);
                break;
            }case TA_DATASECTOR_IDENT : { // Sector packet
                TA_DataSector sectorData(stream,lengthPacket);
                manageData(&sectorData);
                break;
            }default: {
                socket->read(lengthPacket);
                break;
            }
        }
    }
}

/*
    Sends a NTP packets to the server to update the clocks
*/
void TA_InfoServer::sendNTP() {
    TA_DataNTP packet;
    packet.setClientFirstDate(boost::posix_time::microsec_clock::local_time().time_of_day().total_milliseconds());
    sendPacket(&packet);
}

/*
    Sends a Hello packets to the server to check for disconnection
*/
void TA_InfoServer::sendHello() {
    timerSendHello.stop();
    TA_DataHello packet;
    sendPacket(&packet);
    timerDisconnect.start(TA_INFOSERVER_DISCONNECT_INTERVAL_MS);
}

/*
    Manages a disconnection
*/
void TA_InfoServer::disconnect() {
    if (getServerState() != LOCAL && getServerState() != CLOSED) {
        socket->abort();
        timerDisconnect.stop();
        timerSendHello.stop();
        timerNTP.stop();
        timerReconnect.start(TA_INFOSERVER_RECONNECT_INTERVAL_MS);
        setServerState(DISCONNECTED);
        this->nbTryReco = 0;
    }
    emit disconnected(this);
}

/*
    Closes the connection with this server
        @param sendSignal : set as true to send the close signal, false else
*/
void TA_InfoServer::close(bool sendSignal) {
    if (socket != NULL) {
        // Stop timer
        timerDisconnect.stop();
        timerSendHello.stop();
        timerReconnect.stop();
        timerNTP.stop();

        if (socket->isOpen() && getServerState() != CLOSED) {
            // Send close data
            TA_DataClose close;
            sendPacket(&close);
            socket->blockSignals(true);
            socket->flush();
            if(socket->bytesToWrite() > 0 && socket->state() == QAbstractSocket::ConnectedState){
                socket->waitForBytesWritten();
            }
        }
        // Close socket
        socket->deleteLater();
        socket = NULL;
    }
    if (getServerState() != LOCAL) {
        setServerState(CLOSED);
    }
    if(sendSignal){
        emit closed(this);
    }
}
