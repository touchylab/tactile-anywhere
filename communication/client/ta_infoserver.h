/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 09 january 2013                                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_InfoServer class containing   |
  | all the information about a server                                      |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_INFOSERVER_H
#define TA_INFOSERVER_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QByteArray>
#include <QDataStream>
#include <QString>
#include <QtNetwork/QTcpSocket>
#include <QtNetwork/QHostAddress>
#include <QTimer>

// Boost
#include "boost/date_time/posix_time/posix_time.hpp"

// Local includes
#include "communication/data/ta_data.h"
#include "communication/data/ta_dataconnection.h"
#include "communication/data/ta_dataclose.h"
#include "communication/data/ta_datahello.h"
#include "communication/data/ta_dataevent.h"
#include "communication/data/ta_datantp.h"
#include "communication/data/ta_datastream.h"
#include "communication/data/ta_datasector.h"
#include "communication/data/ta_datamarker.h"
#include "communication/server/ta_infoclient.h"
#include "entity/ta_client.h"
#include "entity/ta_server.h"

/*****************************************************************************/
/******                             DEFINES                             ******/
/*****************************************************************************/
#define TA_INFOSERVER_LOCAL_NAME "Local"

#define TA_INFOSERVER_NTP_INTERVAL_MS  5000
#define TA_INFOSERVER_HELLO_INTERVAL_MS  500
#define TA_INFOSERVER_DISCONNECT_INTERVAL_MS  20000
#define TA_INFOSERVER_RECONNECT_INTERVAL_MS 5000

#define TA_INFOSERVER_HEADER_LENGTH 6

#ifndef TA_CONNECTIONSTATE
#define TA_CONNECTIONSTATE

/*
    The differents state of the connection
*/
enum ConnectionState { NO_CONNECTION, ASKED_FOR_CONNECTION, PASSWORD_NEEDED, CONNECTED, DISCONNECTED, CLOSED, LOCAL };

#endif

/*****************************************************************************/
/******                              CLASS                              ******/
/*****************************************************************************/

/*
    TA_InfoServer is a class representing a connection with a server and all its informations
*/
class TA_InfoServer : public QObject{
    Q_OBJECT
    public :
        /* Initialization */

        /*
            This constructor creates and initializes this class with the server's address and port.
            Use this constructor to create a distant server.
                @param address : the server's address
                @param port : the server's port
        */
        TA_InfoServer(QHostAddress address, quint16 port);

        /*
            This constructor creates and initializes this class as a local server.Use this constructor
            to create a communication link between a client and its local server.
        */
        TA_InfoServer();

        /*
            Cleans properly this class at the destruction
        */
        ~TA_InfoServer();

        /* Getters */

        /*
            Gets the server's name.
                @return : the server's name
        */
        QString getServerName() const;

        /*
            Gets the server's socket.
                @return : the server's socket
        */
        QTcpSocket* getServerSocket() const;

        /*
            Gets the server's address.
                @return : the server's address
        */
        QHostAddress getServerAddress() const;

        /*
            Gets the server's port.
                @return : the server's port
        */
        quint16 getServerPort() const;

        /*
            Gets the server's protection.
                @return : true if the server is protected with a password, false else
        */
        bool getServerProtection() const;

        /*
            Gets the state of the connection with this server
                @return : the state of the connection with the server
        */
        ConnectionState getServerState() const;

        /*
            Gets the difference of date between this client and the server
                @return : the number of millisecond of difference of date
        */
        long long int getServerDiffDate() const;

        /* Setters */

        /*
            Sets the server's name.
                @param name : the server's name to set
        */
        void setServerName(const QString& nameServer);

        /*
            Sets the server's socket.
                @param socket : the server's socket to set
        */
        void setServerSocket(QTcpSocket* socket);

        /*
            Sets the server's protection.
                @param protection : true if the server is protected with a password, false else
        */
        void setServerProtection(bool protection);

        /*
            Sets the state of the connection with this server
                @param state : the state of the connection with the server
        */
        void setServerState(ConnectionState state);

        /*
            Sets the difference of date between this client and the server
                @param date : the number of millisecond of difference of date
        */
        void setServerDiffDate(long long int date);

        /* Operation */

        /*
            Sends to this server the given packet
                @param data : the packet to send
                @param tryAnyway : true to try to send even if the socket is considerate as inactive
        */
        void sendPacket(TA_Data* data,bool tryAnyway=false);

        /*
            Manages the given data
                @param stream : the data to manage
        */
        void manageData(TA_Data* data);

    private slots :
        /*
            Reads a paquet from the server
        */
        void read();

        /*
            Sends a NTP packets to the server to update the clocks
        */
        void sendNTP();

        /*
            Sends a Hello packets to the server to check for disconnection
        */
        void sendHello();

        /*
            Manages a disconnection
        */
        void disconnect();

    public slots :
        /*
            Ask for a connection to this server
                @return: true if the connection can be asked, false else
        */
        bool connection();

        /*
            Closes the connection with the server
                @param sendSignal : set as true to send the close signal, false else
        */
        void close(bool sendSignal = true);

    signals:

        /*
            Send when the server needs a password
                @param server : this server
                @param paquet : the paquet send by the server
        */
        void needPassword(TA_InfoServer* server, TA_DataConnection packet);

        /*
            Send when the server accepts the connection
                @param server : this server
        */
        void connected(TA_InfoServer* server);

        /*
            Send when the connection with the server is disconnected
                @param server: this server
        */
        void disconnected(TA_InfoServer* server);

        /*
            Send when the server closes the connection.
                @param server : this server
        */
        void closed(TA_InfoServer* server);

        /*
            Send when a new stream is asked by the server
                @param server : this server
                @param stream : the stream ask to manage
        */
        void newStreamAsk(TA_InfoServer* server, TA_DataStream stream);

        /*
            Send when a new sector is declared by the server
                @param server : this server
                @param sector : the sector to manage
        */
        void newSector(TA_InfoServer* server, TA_DataSector sector);

        /*
            Send when a new detection order is sended by the server
                @param server : this server
                @param detection : the detection order to manage
        */
        void newDetectionOrder(TA_InfoServer* server, TA_DataDetection detection);

        /*
            Send when a new marker ask is sended by the server
                @param server : this server
                @param detection : the marker order to manage
        */
        void newMarkerOrder(TA_InfoServer* server, TA_DataMarker marker);

    protected :
        // Informations
        QString nameServer; // server name
        QHostAddress address; // server address
        quint16 port; // server port

        // Read variables
        bool header;
        short identPacket;
        int lengthPacket;

        // Timers
        QTimer timerNTP;
        QTimer timerSendHello;
        QTimer timerDisconnect;
        QTimer timerReconnect;

        // Functionnalities
        QTcpSocket* socket;
        QDataStream stream;
        ConnectionState state;
        long long int diffDate;
        bool alreadyConnected;
        bool protection;
        bool local;
        int nbTryReco;
};

#endif // TA_INFOSERVER_H
