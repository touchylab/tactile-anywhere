/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 14 January 2013                                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_ClientCommunicator class offering  |
  | to search for Tactile Anywhere server on the local network              |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_clientcommunicator.h"

/*****************************************************************************/
/******                     CONSTRUCTORS/DESTRUCTOR                     ******/
/*****************************************************************************/

/*
    This constructor creates and initializes this class to write to the given port.
        @param port : the destination port to use to send the request
*/
TA_ClientCommunicator::TA_ClientCommunicator(quint16 port){
    // Initializes variables
    this->port = port;
    this->bind();

    // Connects signals
    connect(&timer, SIGNAL(timeout()),this, SLOT(sendBeacon()));
    connect(this, SIGNAL(readyRead()),this, SLOT(readBeacon()));
}

/*****************************************************************************/
/******                           OPERATIONS                            ******/
/*****************************************************************************/

/*
    Launches the servers search
*/
void TA_ClientCommunicator::launchSearch(){
    sendBeacon();
    timer.start(TA_CLIENTCOMMUNICATOR_SEND_INTERVAL_MS);
}

/*
    Stops the servers search
*/
void TA_ClientCommunicator::stopSearch(){
    timer.stop();
}

/*
    Cleans the known servers to send newServer when these servers are founded
*/
void TA_ClientCommunicator::cleanServers(){
    servers.clear();
}

/*****************************************************************************/
/******                           SLOTS                                 ******/
/*****************************************************************************/

/*
    Sends a beacon on the local network to search for servers
*/
void TA_ClientCommunicator::sendBeacon(){
    // Prepare the packet
    QByteArray datagram;
    QDataStream stream(&datagram,QIODevice::ReadWrite);
    TA_DataBeacon beacon;
    beacon.sendData(stream);

    // Gets all the IPv4 addresses of this computer
    // Get all interfaces
    QList<QNetworkInterface> ifaces = QNetworkInterface::allInterfaces();

    // For each interface
    for (int i = 0; i < ifaces.size(); i++){
        // Get all IP addresses for the current interface
        QList<QNetworkAddressEntry> addrs = ifaces[i].addressEntries();

        // For any IP address, if it is IPv4 and the interface is active, place its broadcast address into the list
        for (int j = 0; j < addrs.size(); j++){
            if ((addrs[j].ip().protocol() == QAbstractSocket::IPv4Protocol) && (addrs[j].broadcast().toString() != "")){
                addresses.push_back(addrs[j].broadcast());
            }
        }
    }

    // Send in broadcast on all the interfaces
    for (int i = 0; i < addresses.size(); i++){
        writeDatagram(datagram, addresses[i], port);
    }
}

/*
    Reads a beacon from a server
*/
void TA_ClientCommunicator::readBeacon(){
    QHostAddress address;
    quint16 port;
    while(hasPendingDatagrams()){
        // Prepares and reads data
        QByteArray datagram;
        datagram.resize(pendingDatagramSize());
        readDatagram(datagram.data(), datagram.size(),&address,&port);

        // Reads the beacon
        QDataStream stream(datagram);
        TA_DataBeacon beacon(stream);

        // Checks if the server is already know
        if(servers.contains(address.toString())){
            continue;
        }

        // Else, sends the newServer signal
        servers[address.toString()] = beacon.getServerPort();
        TA_InfoServer* server = new TA_InfoServer(address, beacon.getServerPort());        
        server->setServerName(beacon.getServerName());
        server->setServerProtection(beacon.getServerProtection());
        emit newServer(server);
    }
}
