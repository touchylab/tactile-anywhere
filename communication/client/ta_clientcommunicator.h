/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 14 January 2013                                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_ClientCommunicator class      |
  | offering to search for Tactile Anywhere server on the local network     |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_CLIENTCOMMUNICATOR_H
#define TA_CLIENTCOMMUNICATOR_H

/*****************************************************************************/
/******                             DEFINES                             ******/
/*****************************************************************************/

// default port
#define TA_CLIENTCOMMUNICATOR_DEFAULT_PORT 2014
// interval time to send a packet in milliseconds
#define TA_CLIENTCOMMUNICATOR_SEND_INTERVAL_MS 1000

/*****************************************************************************/
/******                             INCLUDES                            ******/
/*****************************************************************************/

// Qt
#include <QtNetwork/QUdpSocket>
#include <QtNetwork/QNetworkAddressEntry>
#include <QTimer>

// Local includes
#include <map>
#include "communication/client/ta_infoserver.h"
#include "communication/data/ta_databeacon.h"

// Namespaces
using namespace std;

/*****************************************************************************/
/******                              CLASS                              ******/
/*****************************************************************************/

/*
 *  TA_ClientUdp is a class offering to this client to find on the local networks all
 *  availables server
 */
class TA_ClientCommunicator : public QUdpSocket{
    Q_OBJECT
    public :
        /*
            This constructor creates and initializes this class to write to the given port.
                @param port : the destination port to use to send the request
        */
        TA_ClientCommunicator(quint16 port = TA_CLIENTCOMMUNICATOR_DEFAULT_PORT);

        /*
            Launches the servers search
        */
        void launchSearch();

        /*
            Stops the servers search
        */
        void stopSearch();

        /*
            Cleans the known servers to send newServer when these servers are founded
        */
        void cleanServers();

    public slots :
        /*
            Sends a beacon on the local network to search for servers
        */
        void sendBeacon();

    private slots :
        /*
            Reads a beacon from a server
        */
        void readBeacon();

    signals :
        /*
            Send when a new server is finded on the local network. If the server was already found,
            this signal is not sended.
                @param server : the finded server
        */
        void newServer(TA_InfoServer* server);

    protected :
        quint16 port;   // port number
        QTimer timer; // timer
        QList<QHostAddress> addresses; // host adresses list
        QMap<QString,quint16> servers; // servers mapped by their name and port
};

#endif // TA_CLIENTCOMMUNICATOR_H
