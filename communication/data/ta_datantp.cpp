/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 06 January 2013                                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_DataNTP class containing the       |
  | informations of a NTP message between a client and a server             |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_datantp.h"

/*****************************************************************************/
/******                  CONSTRUCTORS/DESTRUCTOR                        ******/
/*****************************************************************************/

/*
    This constructor creates this class.
*/
TA_DataNTP::TA_DataNTP(){
    clientFirstDate = -1;
    clientSecondDate = -1;
    serverFirstDate = -1;
    serverSecondDate = -1;
}

/*
    This constructor creates and initialize this class with a packet
    received in the given stream. The stream must already have the
    necessary data to read (TA_DATANTP_DATA_SIZE bytes).
        @param stream : the stream to use to get the packet
        @param length : the data length to get in byte
*/
TA_DataNTP::TA_DataNTP(QDataStream& stream, int length){
    receiveData(stream,length);
}

/*****************************************************************************/
/******                           GETTER                                ******/
/*****************************************************************************/

/*
    Gets the identification number of this type of data
        @return : the identification of this type of data
*/
short int TA_DataNTP::getIdentification() const{
    return TA_DATANTP_IDENT;
}

/*
    Gets the first date sets by the client at the initialization
        @return : a long long int value containing the date as
                  the number of millisecond from the 1st January 1970
*/
long long int TA_DataNTP::getClientFirstDate() const{
    return clientFirstDate;
}

/*
    Gets the second date sets by the client
        @return : a long long int value containing the date as
                  the number of millisecond from the 1st January 1970
*/
long long int TA_DataNTP::getClientSecondDate() const{
    return clientSecondDate;
}

/*
    Gets the first date sets by the server
        @return : a long long int value containing the date as
                  the number of millisecond from the 1st January 1970
*/
long long int TA_DataNTP::getServerFirstDate() const{
    return serverFirstDate;
}

/*
    Gets the second date sets by the server
        @return : a long long int value containing the date as
                  the number of millisecond from the 1st January 1970
*/
long long int TA_DataNTP::getServerSecondDate() const{
    return serverSecondDate;
}

/*
    Gets the difference of date between the server and the client (for the client)
        @return : a long long int value containing the number of millisecond
                  of difference
*/
long long int TA_DataNTP::getDiffDate() const{
    // Calculation of the delay for this communication
    long long int delay = ((getClientSecondDate()-getClientFirstDate())-(getServerSecondDate()-getServerFirstDate()))/2;

    // Calculation of the difference
    return getServerFirstDate()-(getClientFirstDate()+delay);
}

/*****************************************************************************/
/******                           SETTER                                ******/
/*****************************************************************************/

/*
    Sets the first date of the client
        @param date : a long long int value containing the date as
                      the number of millisecond from the 1st January 1970
*/
void TA_DataNTP::setClientFirstDate(long long int date){
    clientFirstDate = date;
}

/*
    Sets the second date of the client
        @param date : a long long int value containing the date as
                      the number of millisecond from the 1st January 1970
*/
void TA_DataNTP::setClientSecondDate(long long int date){
    clientSecondDate = date;
}

/*
    Sets the first date of the server
        @param date : a long long int value containing the date as
                      the number of millisecond from the 1st January 1970
*/
void TA_DataNTP::setServerFirstDate(long long int date){
    serverFirstDate = date;
}

/*
    Sets the second date of the server
        @param date : a long long int value containing the date as
                      the number of millisecond from the 1st January 1970
*/
void TA_DataNTP::setServerSecondDate(long long int date){
    serverSecondDate = date;
}

/*****************************************************************************/
/******                          OPERATION                              ******/
/*****************************************************************************/

/*
    Sends a packet with the data containing in this class
        @param stream : the stream to use to send the packet
*/
void TA_DataNTP::sendData(QDataStream& stream) const{
    // Variables
    QBuffer buffer;
    QDataStream streamB;

    // Write
    buffer.open(QIODevice::WriteOnly);
    streamB.setDevice(&buffer);
    streamB << clientFirstDate << serverFirstDate << serverSecondDate << clientSecondDate;
    sendHeader(stream,TA_DATANTP_IDENT,buffer.size()+4);
    stream << buffer.buffer();
}

/*
    Initializes this class with a packet received in the given stream.
    The stream must already have the necessary data to read (lenght bytes)
        @param stream : the stream to use to get the packet
        @param length : the data length to get in byte
*/
void TA_DataNTP::receiveData(QDataStream& stream, int length){
    // Variables
    QByteArray array;
    QBuffer buffer;
    QDataStream streamB;
    char * buf = new char[length-4];
    int clean;

    // Read
    stream >> clean;
    stream.readRawData(buf,length-4);
    array.setRawData(buf,length-4);
    buffer.setBuffer(&array);
    buffer.open(QIODevice::ReadWrite);
    streamB.setDevice(&buffer);
    streamB >> clientFirstDate >> serverFirstDate >> serverSecondDate >> clientSecondDate;
    delete[] buf;
}
