/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 05 February 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains members of the TA_DataHello class used to check for  |
  | disconnection                                                           |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_datahello.h"

/*****************************************************************************/
/******                  CONSTRUCTORS/DESTRUCTOR                        ******/
/*****************************************************************************/

/*
    This constructor creates this class.
*/
TA_DataHello::TA_DataHello(){}

/*****************************************************************************/
/******                           GETTER                                ******/
/*****************************************************************************/

/*
    Gets the identification number of this type of data
        @return : the identification of this type of data
*/
short int TA_DataHello::getIdentification() const{
    return TA_DATAHELLO_IDENT;
}

/*****************************************************************************/
/******                          OPERATION                              ******/
/*****************************************************************************/

/*
    Sends a packet with the data containing in this class
        @param stream : the stream to use to send the packet
*/
void TA_DataHello::sendData(QDataStream& stream) const{
    sendHeader(stream,TA_DATAHELLO_IDENT,TA_DATAHELLO_DATA_SIZE);
}

/*
    Initializes this class with a packet received in the given stream.
    The stream must already have the necessary data to read (lenght bytes)
        @param stream : the stream to use to get the packet
        @param length : the data length to get in byte
*/
void TA_DataHello::receiveData(QDataStream& stream, int length){
    Q_UNUSED(length)
    Q_UNUSED(stream)
}
