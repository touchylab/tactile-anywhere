/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 10 February 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_DataMarker class containing   |
  | the informations about markers                                          |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere. If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_DATAMARKER_H
#define TA_DATAMARKER_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QMap>
#include <QPoint>

// Local includes
#include "ta_data.h"

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/
#define TA_DATAMARKER_IDENT (0x0A)
#define TA_DATAMARKER_FIXED_DATA_SIZE (sizeof(int))

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
 * TA_DataMarker is a class to ask markers' position to a client and to send it to the
 * server. This exchange is initialized by the server with a respond from the client
*/
class TA_DataMarker : public TA_Data {
    public :
        /*
            This constructor creates this class.
        */
        TA_DataMarker();

        /*
            This constructor creates and initialize this class with a packet
            received in the given stream. The stream must already have the
            necessary data to read.
                @param stream : the stream to use to get the packet
                @param length : the data length to get in byte
        */
        TA_DataMarker(QDataStream& stream, int length);

        /* Getters */

        /*
            Gets the identification number of this type of data
                @return : the identification of this type of data
        */
        short int getIdentification() const;

        /*
            Gets the webcam's id seeing markers
                @return : the webcam's id seeing markers
        */
        int getWebcamId() const;

        /*
            Gets the positions of the center of markers
                @return : a QMap with marker's id as key and position as value
        */
        QMap<quint32,QPoint>& getPositions();

        /* Setters */

        /*
            Sets the webcam's id seeing markers
                @param webcamId : the webcam's id seeing markers
        */
        void setWebcamId(int webcamId);

        /*
            Sets the positions of the center of markers
                @param positions : a QMap with marker's id as key and position as value
        */
        void setPositions(QMap<quint32,QPoint>& positions);

        /* Operation */

        /*
            Sends a packet with the data containing in this class
                @param stream : the stream to use to send the packet
        */
        void sendData(QDataStream& stream) const;

        /*
            Initializes this class with a packet received in the given stream.
            The stream must already have the necessary data to read (lenght bytes)
                @param stream : the stream to use to get the packet
                @param length : the data length to get in byte
        */
        void receiveData(QDataStream& stream, int length);

    private:
        int webcamId; // webcam identifier
        QMap<quint32,QPoint> positions; // markers positions
};

#endif // TA_DATAMARKER_H
