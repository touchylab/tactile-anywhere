/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 14 January 2013                                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_DataBeacon class containing   |
  | the informations about the client and the server coordinates            |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_DATABEACON_H
#define TA_DATABEACON_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QHostAddress>

// Local includes
#include "communication/data/ta_data.h"

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
 *  TA_DataBeacon is a class containing the TCP port and the name of the server.
 *  This packet is created empty by the client to be send to the server. The
 *  server respond with the port of its TCP server and its name.
 */
class TA_DataBeacon : public TA_Data {
    public :
        /*
            This constructor creates this class.
        */
        TA_DataBeacon();

        /*
            This constructor creates and initialize this class with a packet
            received in the given stream.
                @param stream : the stream to use to get the packet
        */
        TA_DataBeacon(QDataStream& stream);

        /* Getters */

        /*
            Gets the identification number of this type of data
                @return : the identification of this type of data
        */
        short int getIdentification() const;

        /*
            Gets the server's port
                @return : the server's port
        */
        quint16 getServerPort() const;

        /*
            Gets the server's name
                @return : the server's name
        */
        QString getServerName() const;

        /*
            Gets the server's protection
                @return : true if the server is protected with a password, false else
        */
        bool getServerProtection() const;

        /* Setters */

        /*
            Sets the server's port
                @param port : the server's port
        */
        void setServerPort(quint16 port);

        /*
            Sets the server's name
                @param name : the server's name
        */
        void setServerName(QString name);

        /*
            Sets the server's protection
                @param protection : true if the server is protected with a password, false else
        */
        void setServerProtection(bool protection);

        /* Operation */

        /*
            Sends a packet with the data containing in this class
                @param stream : the stream to use to send the packet
        */
        void sendData(QDataStream& stream) const;

        /*
            Initializes this class with a packet received in the given stream.
            The stream must already have the necessary data to read (lenght bytes)
                @param stream : the stream to use to get the packet
                @param length : the data length to get in byte
        */
        void receiveData(QDataStream& stream, int length);

    protected :
        QString serverName;
        quint16 serverPort;
        bool serverProtection;
};

#endif // TA_DATABEACON_H
