/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 07 February 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_DataWebcam class containing   |
  | the informations about the webcams of a client                          |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_DATAWEBCAM_H
#define TA_DATAWEBCAM_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QMap>
#include <QString>

// Local includes
#include "ta_data.h"

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/

// data webcam identifier
#define TA_DATAWEBCAM_IDENT (0x09)

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
 *  TA_DataWebcam is a class containing the list of webcams of a client
 */
class TA_DataWebcam : public TA_Data {
    public :
        /*
            This constructor creates this class.
        */
        TA_DataWebcam();

        /*
            This constructor creates and initialize this class with a packet
            received in the given stream. The stream must already have the
            necessary data to read.
                @param stream : the stream to use to get the packet
                @param length : the data length to get in byte
        */
        TA_DataWebcam(QDataStream& stream, int length);

        /* Getters */

        /*
            Gets the identification number of this type of data
                @return : the identification of this type of data
        */
        short int getIdentification() const;

        /*
            Gets the list of webcam
                @return : a QMap with webcam's id has key and webcam's name has value
        */
        QMap<int,QString>* getWebcams();

        /*
            Gets the state of the given webcam
                @return : true for connected webcams, false else
        */
        bool getConnectedState();

        /* Setters */

        /*
            Sets the state of the given webcam
                @param state : set as true for connected webcams, false else
        */
        void setConnectedState(bool state);

        /*
            Sets the list of webcam
                @param webcams : a QMap with webcam's id has key and webcam's name has value
        */
        void setWebcams(QMap<int,QString>* webcams);

        /* Operation */

        /*
            Sends a packet with the data containing in this class
                @param stream : the stream to use to send the packet
        */
        void sendData(QDataStream& stream) const;

        /*
            Initializes this class with a packet received in the given stream.
            The stream must already have the necessary data to read (lenght bytes)
                @param stream : the stream to use to get the packet
                @param length : the data length to get in byte
        */
        void receiveData(QDataStream& stream, int length);

    protected :
       bool connected; // connection state of the webcam
       QMap<int,QString> webcams; // webcams mapped with identifier and name
};

#endif // TA_DATAWEBCAM_H
