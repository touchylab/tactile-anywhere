/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 06 January 2013                                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_DataNTP class containing the  |
  | informations of a NTP message between a client and a server             |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_DATANTP_H
#define TA_DATANTP_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Local includes
#include "ta_data.h"

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/

// data NTP identifier
#define TA_DATANTP_IDENT (0x04)
// data NTP data size
#define TA_DATANTP_DATA_SIZE (4*sizeof(long long int))

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
 *  TA_DataNTP is a class containing data get from a NTP exchange or created
 *  to be send in this exchange. This exchange is initialized by the client to
 *  synchronized its clock
 */
class TA_DataNTP : public TA_Data {
    public :
        /*
            This constructor creates this class.
        */
        TA_DataNTP();

        /*
            This constructor creates and initialize this class with a packet
            received in the given stream. The stream must already have the
            necessary data to read (TA_DATANTP_DATA_SIZE bytes).
                @param stream : the stream to use to get the packet
                @param length : the data length to get in bytes
        */
        TA_DataNTP(QDataStream& stream, int length);

        /* Getters */

        /*
            Gets the identification number of this type of data
                @return : the identification of this type of data
        */
        short int getIdentification() const;

        /*
            Gets the first date sets by the client at the initialization
                @return : a long long int value containing the date as
                          the number of millisecond from the 1st January 1970
        */
        long long int getClientFirstDate() const;

        /*
            Gets the second date sets by the client
                @return : a long long int value containing the date as
                          the number of millisecond from the 1st January 1970
        */
        long long int getClientSecondDate() const;

        /*
            Gets the first date sets by the server
                @return : a long long int value containing the date as
                          the number of millisecond from the 1st January 1970
        */
        long long int getServerFirstDate() const;

        /*
            Gets the second date sets by the server
                @return : a long long int value containing the date as
                          the number of millisecond from the 1st January 1970
        */
        long long int getServerSecondDate() const;

        /*
            Gets the difference of date between the server and the client (for the client)
                @return : a long long int value containing the number of millisecond
                          of difference
        */
        long long int getDiffDate() const;

        /* Setters */

        /*
            Sets the first date of the client
                @param date : a long long int value containing the date as
                              the number of millisecond from the 1st January 1970
        */
        void setClientFirstDate(long long int date);

        /*
            Sets the second date of the client
                @param date : a long long int value containing the date as
                              the number of millisecond from the 1st January 1970
        */
        void setClientSecondDate(long long int date);

        /*
            Sets the first date of the server
                @param date : a long long int value containing the date as
                              the number of millisecond from the 1st January 1970
        */
        void setServerFirstDate(long long int date);

        /*
            Sets the second date of the server
                @param date : a long long int value containing the date as
                              the number of millisecond from the 1st January 1970
        */
        void setServerSecondDate(long long int date);

        /* Operation */

        /*
            Sends a packet with the data containing in this class
                @param stream : the stream to use to send the packet
        */
        void sendData(QDataStream& stream) const;

        /*
            Initializes this class with a packet received in the given stream.
            The stream must already have the necessary data to read (lenght bytes)
                @param stream : the stream to use to get the packet
                @param length : the data length to get in byte
        */
        void receiveData(QDataStream& stream, int length);

    protected :
        long long int clientFirstDate;
        long long int clientSecondDate;
        long long int serverFirstDate;
        long long int serverSecondDate;
};

#endif // TA_DATANTP_H
