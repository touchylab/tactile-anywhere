/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 06 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_DataDetection class used to   |
  | checked the state of detection.                                         |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu LOSLIER -- Initial version                            |
  | [-] 1.1 - Simon BONY -- Version complete for Tactile Anywhere 1.0       |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_datadetection.h"

/*****************************************************************************/
/******                  CONSTRUCTORS/DESTRUCTOR                        ******/
/*****************************************************************************/

/*
    This constructor creates this class.
*/
TA_DataDetection::TA_DataDetection(){
    detectionState = DETECTION_START;
    dimensionRatio = 0.5;
}

/*
    This constructor creates and initialize this class with a packet
    received in the given stream. The stream must already have the
    necessary data to read.
        @param stream : the stream to use to get the packet
        @param length : the data length to get in byte
*/
TA_DataDetection::TA_DataDetection(QDataStream& stream, int length){
    receiveData(stream,length);
}

/*****************************************************************************/
/******                           GETTER                                ******/
/*****************************************************************************/

/*
    Gets the identification number of this type of data
        @return : the identification of this type of data
*/
short int TA_DataDetection::getIdentification() const{
    return TA_DATADETECTION_IDENT;
}

/*
    Gets the detection state
        @return : the detection state
*/
DetectionState TA_DataDetection::getDetectionState() const {
    return (DetectionState) detectionState;
}

/*
    Gets the dimension ratio to apply during the detection
        @return : the dimension ratio to apply
*/
float TA_DataDetection::getDimensionRatio() const{
    return dimensionRatio;
}

/*****************************************************************************/
/******                           SETTER                                ******/
/*****************************************************************************/

/*
    Sets the detection state
        @param detectionState: the detection state
*/
void TA_DataDetection::setDetectionState(DetectionState detectionState){
    this->detectionState = detectionState;
}

/*
    Sets the dimension ratio to apply during the detection
        @param : the dimension ratio to apply
*/
void TA_DataDetection::setDimensionRatio(float dimensionRatio){
    this->dimensionRatio = dimensionRatio;
}

/*****************************************************************************/
/******                          OPERATION                              ******/
/*****************************************************************************/

/*
    Sends a packet with the data containing in this class
        @param stream : the stream to use to send the packet
*/
void TA_DataDetection::sendData(QDataStream& stream) const{
    // Variables
    QBuffer buffer;
    QDataStream streamB;

    // Write
    buffer.open(QIODevice::WriteOnly);
    streamB.setDevice(&buffer);
    streamB << detectionState << dimensionRatio;
    sendHeader(stream,TA_DATADETECTION_IDENT,buffer.size()+4);
    stream << buffer.buffer();
}

/*
    Initializes this class with a packet received in the given stream.
    The stream must already have the necessary data to read (lenght bytes)
        @param stream : the stream to use to get the packet
        @param length : the data length to get in byte
*/
void TA_DataDetection::receiveData(QDataStream& stream, int length){
    // Variables
    QByteArray array;
    QBuffer buffer;
    QDataStream streamB;
    char * buf = new char[length-4];
    int clean;

    // Read
    stream >> clean;
    stream.readRawData(buf,length-4);
    array.setRawData(buf,length-4);
    buffer.setBuffer(&array);
    buffer.open(QIODevice::ReadWrite);
    streamB.setDevice(&buffer);
    streamB >> detectionState >> dimensionRatio;
    delete[] buf;
}
