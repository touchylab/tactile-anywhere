/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 07 February 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_DataWebcam class containing the    |
  | informations about the webcams of a client                              |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_datawebcam.h"

/*****************************************************************************/
/******                     CONSTRUCTORS/DESTRUCTOR                     ******/
/*****************************************************************************/

/*
    This constructor creates this class.
*/
TA_DataWebcam::TA_DataWebcam(){
    connected = true;
}

/*
    This constructor creates and initialize this class with a packet
    received in the given stream. The stream must already have the
    necessary data to read.
        @param stream : the stream to use to get the packet
        @param length : the data length to get in byte
*/
TA_DataWebcam::TA_DataWebcam(QDataStream& stream, int length){
    receiveData(stream,length);
}

/*****************************************************************************/
/******                             GETTERS                             ******/
/*****************************************************************************/

/*
    Gets the identification number of this type of data
        @return : the identification of this type of data
*/
short int TA_DataWebcam::getIdentification() const{
    return TA_DATAWEBCAM_IDENT;
}

/*
    Gets the list of webcam
        @return : a QMap with webcam's id has key and webcam's name has value
*/
QMap<int,QString>* TA_DataWebcam::getWebcams(){
    return &webcams;
}

/*
    Gets the state of the given webcam
        @return : true for connected webcams, false else
*/
bool TA_DataWebcam::getConnectedState(){
    return connected;
}

/*****************************************************************************/
/******                             SETTERS                             ******/
/*****************************************************************************/

/*
    Sets the state of the given webcam
        @param state : set as true for connected webcams, false else
*/
void TA_DataWebcam::setConnectedState(bool state){
    connected = state;
}

/*
    Sets the list of webcam
        @param webcams : a QMap with webcam's id has key and webcam's name has value
*/
void TA_DataWebcam::setWebcams(QMap<int,QString>* webcams){
    this->webcams = *webcams;
}

/*****************************************************************************/
/******                          OPERATION                              ******/
/*****************************************************************************/

/*
    Sends a packet with the data containing in this class
        @param stream : the stream to use to send the packet
*/
void TA_DataWebcam::sendData(QDataStream& stream) const{
    // Variables
    QBuffer buffer;
    QDataStream streamB;

    // Write
    buffer.open(QIODevice::WriteOnly);
    streamB.setDevice(&buffer);
    streamB << webcams << connected;
    sendHeader(stream,TA_DATAWEBCAM_IDENT,buffer.size()+4);
    stream << buffer.buffer();
}

/*
    Initializes this class with a packet received in the given stream.
    The stream must already have the necessary data to read (lenght bytes)
        @param stream : the stream to use to get the packet
        @param length : the data length to get in byte
*/
void TA_DataWebcam::receiveData(QDataStream& stream, int length){
    // Variables
    QByteArray array;
    QBuffer buffer;
    QDataStream streamB;
    char * buf = new char[length-4];
    int clean;

    // Read
    stream >> clean;
    stream.readRawData(buf,length-4);
    array.setRawData(buf,length-4);
    buffer.setBuffer(&array);
    buffer.open(QIODevice::ReadWrite);
    streamB.setDevice(&buffer);
    streamB >> webcams >> connected;
    delete[] buf;
}
