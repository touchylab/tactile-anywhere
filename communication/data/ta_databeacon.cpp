/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 14 January 2013                                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_DataBeacon class containing the    |
  | informations about the client and the server coordinates                |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_databeacon.h"

/*****************************************************************************/
/******                  CONSTRUCTORS/DESTRUCTOR                        ******/
/*****************************************************************************/

/*
    This constructor creates this class.
*/
TA_DataBeacon::TA_DataBeacon(){
    serverPort = -1;
    serverName = QString("");
    serverProtection = false;
}

/*
    This constructor creates and initialize this class with a packet
    received in the given stream.
        @param stream : the stream to use to get the packet
*/
TA_DataBeacon::TA_DataBeacon(QDataStream& stream){
    receiveData(stream,0);
}

/*****************************************************************************/
/******                           GETTER                                ******/
/*****************************************************************************/

/*
    Gets the identification number of this type of data
        @return : the identification of this type of data
*/
short int TA_DataBeacon::getIdentification() const{
    return -1;
}

/*
    Gets the server's port
        @return : the server's port
*/
quint16 TA_DataBeacon::getServerPort() const{
    return serverPort;
}

/*
    Gets the server's name
        @return : the server's name
*/
QString TA_DataBeacon::getServerName() const{
    return serverName;
}

/*
    Gets the server's protection
        @return : true if the server is protected with a password, false else
*/
bool TA_DataBeacon::getServerProtection() const{
    return serverProtection;
}

/*****************************************************************************/
/******                           SETTER                                ******/
/*****************************************************************************/

/*
    Sets the server's port
        @param port : the server's port
*/
void TA_DataBeacon::setServerPort(quint16 port){
    serverPort = port;
}

/*
    Sets the server's name
        @param name : the server's name
*/
void TA_DataBeacon::setServerName(QString name){
    serverName = name;
}

/*
    Sets the server's protection
        @param protection : true if the server is protected with a password, false else
*/
void TA_DataBeacon::setServerProtection(bool protection){
    serverProtection = protection;
}

/*****************************************************************************/
/******                          OPERATION                              ******/
/*****************************************************************************/

/*
    Sends a packet with the data containing in this class
        @param stream : the stream to use to send the packet
*/
void TA_DataBeacon::sendData(QDataStream& stream) const{
    stream << serverPort << serverName << serverProtection;
}

/*
    Initializes this class with a packet received in the given stream.
    The stream must already have the necessary data to read (lenght bytes)
        @param stream : the stream to use to get the packet
        @param length : the data length to get in byte
*/
void TA_DataBeacon::receiveData(QDataStream& stream, int length){
    Q_UNUSED(length);
    stream >> serverPort >> serverName >> serverProtection;
}
