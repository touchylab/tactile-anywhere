/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 06 February 2014                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_DataDetection class used to   |
  | checked the state of detection.                                         |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Mathieu LOSLIER -- Initial version                            |
  | [-] 1.1 - Simon BONY -- Version complete for Tactile Anywhere 1.0       |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_DATADETECTION_H
#define TA_DATADETECTION_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/
#include "ta_data.h"

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/
#define TA_DATADETECTION_IDENT (0x07)
#define TA_DATADETECTION_DATA_SIZE (sizeof(int) + sizeof(float))

/*
    The possibles states of detection
*/
enum DetectionState { DETECTION_START, DETECTION_STOP, DETECTION_PAUSE };

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
 *  TA_DataDetection is a class to check for detection is started, halted
 *  (client side) or stopped between server and clients.
 */
class TA_DataDetection : public TA_Data {
    public :
        /*
            This constructor creates this class.
        */
        TA_DataDetection();

        /*
            This constructor creates and initialize this class with a packet
            received in the given stream. The stream must already have the
            necessary data to read.
                @param stream : the stream to use to get the packet
                @param length : the data length to get in byte
        */
        TA_DataDetection(QDataStream& stream, int length);

        /* Getters */

        /*
            Gets the identification number of this type of data
                @return : the identification of this type of data
        */
        short int getIdentification() const;

        /*
            Gets the detection state
                @return : the detection state
        */
        DetectionState getDetectionState() const;

        /*
            Gets the dimension ratio to apply during the detection
                @return : the dimension ratio to apply
        */
        float getDimensionRatio() const;

        /* Setters */

        /*
            Sets the detection state
                @param detectionState: the detection state
        */
        void setDetectionState(DetectionState detectionState);

        /*
            Sets the dimension ratio to apply during the detection
                @param : the dimension ratio to apply
        */
        void setDimensionRatio(float dimensionRatio);

        /* Operation */

        /*
            Sends a packet with the data containing in this class
                @param stream : the stream to use to send the packet
        */
        void sendData(QDataStream& stream) const;

        /*
            Initializes this class with a packet received in the given stream.
            The stream must already have the necessary data to read (lenght bytes)
                @param stream : the stream to use to get the packet
                @param length : the data length to get in byte
        */
        void receiveData(QDataStream& stream, int length);

        protected :
            int detectionState;
            float dimensionRatio;
        };

#endif // TA_DATADETECTION_H
