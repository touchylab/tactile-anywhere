/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 07 January 2013                                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_DataConnection class containing    |
  | the informations of a connection message between a client and a server  |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_dataconnection.h"

/*****************************************************************************/
/******                  CONSTRUCTORS/DESTRUCTOR                        ******/
/*****************************************************************************/

/*
    This constructor creates this class.
*/
TA_DataConnection::TA_DataConnection(){
    clientName = QString("");
    this->hash = QByteArray("");
    firstConnection = false;
    protectionState = false;
    authorization = false;
}

/*
    This constructor creates and initialize this class with a packet
    received in the given stream. The stream must already have the
    necessary data to read (length parameter).
        @param stream : the stream to use to get the packet
        @param length : the data length to get in byte
*/
TA_DataConnection::TA_DataConnection(QDataStream& stream, int length){
    receiveData(stream,length);
}

/*****************************************************************************/
/******                           GETTER                                ******/
/*****************************************************************************/

/*
    Gets the identification number of this type of data
        @return : the identification of this type of data
*/
short int TA_DataConnection::getIdentification() const{
    return TA_DATACONNECTION_IDENT;
}

/*
    Gets the client's name.
        @return : a pointer to the client's name string or NULL if
                  the client don't already specify it's name
*/
QString TA_DataConnection::getClientName() const{
    return clientName;
}

/*
    Checks if the client was not already connected
        @return : true if the client was not already connected,
                  false else
*/
bool TA_DataConnection::isFirstConnection() const{
    return firstConnection;
}

/*
    Checks if the server authorized the connection
        @return : true if the server authorized this connection,
                  false else
*/
bool TA_DataConnection::isAuthorized() const{
    return authorization;
}

/*
    Checks if the server is protected by a password
        @return : true if the server is protected by a password,
                  false else
*/
bool TA_DataConnection::isProtected() const{
    return protectionState;
}

/*
    Gets the password hash send by the client
        @return : the password hash send by the client
*/
QByteArray& TA_DataConnection::getHash(){
    return hash;
}

/*****************************************************************************/
/******                           SETTER                                ******/
/*****************************************************************************/

/*
    Sets the client's name.
        @param name : the client's name to set
*/
void TA_DataConnection::setClientName(const QString& name){
    clientName = name;
}

/*
    Sets if the client was not already connected
        @param first : true if the client was not already connected,
                       false else
*/
void TA_DataConnection::setFirstConnection(bool first){
    this->firstConnection = first;
}

/*
    Sets the server's authorization for this connection
        @param authorization : true if the server authorized this,
                               connection false else
*/
void TA_DataConnection::setAuthorisation(bool authorization){
    this->authorization = authorization;
}

/*
    Sets the server protection state
        @return : true if the server is protected by a password,
                  false else
*/
void TA_DataConnection::setProtectionState(bool state){
    protectionState = state;
}

/*
    Sets the password hash to check
        @return : the password hash to check
*/
void TA_DataConnection::setHash(const QByteArray& hash){
    this->hash = hash;
}

/*****************************************************************************/
/******                          OPERATION                              ******/
/*****************************************************************************/

/*
    Sends a packet with the data containing in this class
        @param stream : the stream to use to send the packet
*/
void TA_DataConnection::sendData(QDataStream& stream) const{
    // Variables
    QBuffer buffer;
    QDataStream streamB;

    // Write
    buffer.open(QIODevice::WriteOnly);
    streamB.setDevice(&buffer);
    streamB << clientName << firstConnection << authorization << protectionState << hash;

    sendHeader(stream,TA_DATACONNECTION_IDENT,buffer.size()+4);
    stream << buffer.buffer();
}

/*
    Initializes this class with a packet received in the given stream.
    The stream must already have the necessary data to read (lenght bytes)
        @param stream : the stream to use to get the packet
        @param length : the data length to get in byte
*/
void TA_DataConnection::receiveData(QDataStream& stream, int length){
    // Variables
    QByteArray array;
    QBuffer buffer;
    QDataStream streamB;
    char * buf = new char[length-4];
    int clear;

    // Read
    stream >> clear;
    stream.readRawData(buf,length-4);
    array.setRawData(buf,length-4);
    buffer.setBuffer(&array);
    buffer.open(QIODevice::ReadWrite);
    streamB.setDevice(&buffer);
    streamB >> clientName >> firstConnection >> authorization >> protectionState >> hash;

    delete[] buf;
}
