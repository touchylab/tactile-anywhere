/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 07 February 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_DataSector class containing the    |
  | informations about a sector for a client                                |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_datasector.h"

/*****************************************************************************/
/******                     CONSTRUCTORS/DESTRUCTOR                     ******/
/*****************************************************************************/

/*
    This constructor creates this class.
*/
TA_DataSector::TA_DataSector(){
    areaName = QString("");
    sectorName = QString("");
    webcamId = -1;
    dimensionRatio = 0.5;
}

/*
    This constructor creates and initialize this class with a packet
    received in the given stream. The stream must already have the
    necessary data to read.
        @param stream : the stream to use to get the packet
        @param length : the data length to get in byte
*/
TA_DataSector::TA_DataSector(QDataStream& stream, int length){
    receiveData(stream,length);
}

/*****************************************************************************/
/******                             GETTERS                             ******/
/*****************************************************************************/

/*
    Gets the identification number of this type of data
        @return : the identification of this type of data
*/
short int TA_DataSector::getIdentification() const{
    return TA_DATASECTOR_IDENT;
}

/*
    Gets the concerned webcam's name
        @return : the concerned webcam's name
*/
int TA_DataSector::getWebcamId() const{
    return webcamId;
}

/*
    Gets the concerned area's name
        @return : the concerned area's name
*/
QString TA_DataSector::getAreaName() const{
    return areaName;
}

/*
    Gets the concerned sector's name
        @return : the concerned sector's name
*/
QString TA_DataSector::getSectorName() const{
    return sectorName;
}

/*
    Gets the concerned sector's points
        @return : a pointer to a QVector containing the points representing the sector
*/
QVector<QPoint>* TA_DataSector::getSector(){
    return &sector;
}

/*
    Gets the dimension ratio to apply during the detection
        @return : the dimension ratio to apply
*/
float TA_DataSector::getDimensionRatio() const{
    return dimensionRatio;
}

/*****************************************************************************/
/******                             SETTERS                             ******/
/*****************************************************************************/

/*
    Sets webcam id associate with opencv
        @param name : the concerned area's name
*/
void TA_DataSector::setWebcamId(int webcamId){
    this->webcamId = webcamId;
}

/*
    Sets the concerned area's name
        @param name : the concerned area's name
*/
void TA_DataSector::setAreaName(QString name){
    this->areaName = name;
}

/*
    Sets the concerned sector's name
        @param name : the concerned sector's name
*/
void TA_DataSector::setSectorName(QString name){
    this->sectorName = name;
}

/*
    Sets the concerned sector's points
        @param sector : a pointer to a QVector containing the points representing the sector
*/
void TA_DataSector::setSector(QVector<QPoint>* sector){
    this->sector = *sector;
}

/*
    Sets the dimension ratio to apply during the detection
        @param : the dimension ratio to apply
*/
void TA_DataSector::setDimensionRatio(float dimensionRatio){
    this->dimensionRatio = dimensionRatio;
}

/*****************************************************************************/
/******                          OPERATION                              ******/
/*****************************************************************************/

/*
    Sends a packet with the data containing in this class
        @param stream : the stream to use to send the packet
*/
void TA_DataSector::sendData(QDataStream& stream) const{
    // Variables
    QBuffer buffer;
    QDataStream streamB;

    // Write
    buffer.open(QIODevice::WriteOnly);
    streamB.setDevice(&buffer);
    streamB << webcamId << areaName << sectorName << dimensionRatio << sector ;
    sendHeader(stream,TA_DATASECTOR_IDENT,buffer.size()+4);
    stream << buffer.buffer();
}

/*
    Initializes this class with a packet received in the given stream.
    The stream must already have the necessary data to read (lenght bytes)
        @param stream : the stream to use to get the packet
        @param length : the data length to get in byte
*/
void TA_DataSector::receiveData(QDataStream& stream, int length){
    // Variables
    QByteArray array;
    QBuffer buffer;
    QDataStream streamB;
    char * buf = new char[length-4];
    int clean;

    // Read
    stream >> clean;
    stream.readRawData(buf,length-4);
    array.setRawData(buf,length-4);
    buffer.setBuffer(&array);
    buffer.open(QIODevice::ReadWrite);
    streamB.setDevice(&buffer);
    streamB >> webcamId >> areaName >> sectorName >> dimensionRatio >> sector;
    delete[] buf;
}
