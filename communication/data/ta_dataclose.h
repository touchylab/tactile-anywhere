/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 04 February 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_DataClose class send when a   |
  | connection must be closed                                               |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_DATACLOSE_H
#define TA_DATACLOSE_H

/*****************************************************************************/
/******                             DEFINES                             ******/
/*****************************************************************************/

// data close identifier
#define TA_DATACLOSE_IDENT (0x02)
// data close data size
#define TA_DATACLOSE_DATA_SIZE (0)

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Local includes
#include "ta_data.h"

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
 *  TA_DataClose is a class send when the client or the server want to close the
 *  connection. This exchange can be initialized by the client or the server.
 */
class TA_DataClose : public TA_Data {
    public :
        /*
            This constructor creates this class.
        */
        TA_DataClose();

        /* Getters */

        /*
            Gets the identification number of this type of data
                @return : the identification of this type of data
        */
        short int getIdentification() const;

        /* Operation */

        /*
            Sends a packet of this data
                @param stream : the stream to use to send the packet
        */
        void sendData(QDataStream& stream) const;

        /*
            Initializes this class with a packet received in the given stream.
                @param stream : the stream to use to get the packet
                @param length : the data length to get in byte
        */
        void receiveData(QDataStream& stream, int length);
};

#endif // TA_DATACLOSE_H
