/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 09 January 2013                                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_DataEvent class containing    |
  | the informations about an event from a client                           |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_DATAEVENT_H
#define TA_DATAEVENT_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QVector>
#include <QString>

// Local includes
#include "ta_data.h"

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/
#define TA_DATAEVENT_IDENT (0x05)
#define TA_DATAEVENT_DATA_FIXED_SIZE (sizeof(long long int) + sizeof(int))

typedef struct{
    QString areaName;
    QString sectorId;
    bool state;
} EventContacts;

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
 *  TA_DataEvent is a class containing data about some contact events from a client or created
 *  to be send to the server. This exchange is initialized by the client to when events occur
 */
class TA_DataEvent : public TA_Data {
    public :
        /*
            This constructor creates this class.
        */
        TA_DataEvent();

        /*
            This constructor creates and initialize this class with a packet
            received in the given stream. The stream must already have the
            necessary data to read.
                @param stream : the stream to use to get the packet
                @param length : the data length to get in byte
        */
        TA_DataEvent(QDataStream& stream, int length);

        /* Getters */

        /*
            Gets the identification number of this type of data
                @return : the identification of this type of data
        */
        short int getIdentification() const;

        /*
            Gets the date of events
                @return : a long long int value containing the date as
                          the number of millisecond from the 1st January 1970
        */
        long long int getEventDate() const;

        /*
            Gets the concerned webcam's id
                @return : the concerned webcam's id
        */
        int getWebcamId() const;

        /*
            Gets the concerned events
                @return : a pointer to a QVector containing the events
        */
        QVector<EventContacts>* getEvents();

        /* Setters
         *
         *
            Sets the date of events
                @param date : a long long int value containing the date as
                              the number of millisecond from the 1st January 1970
        */
        void setEventDate(long long int date);

        /*
            Sets the concerned webcam's name
                @param name : the concerned webcam's name
        */
        void setWebcamId(int webcamId);

        /*
            Sets the concerned events
                @param name : a pointer to a QVector containing the events
        */
        void setEvents(QVector<EventContacts>* events);

        /* Operation */

        /*
            Sends a packet with the data containing in this class
                @param stream : the stream to use to send the packet
        */
        void sendData(QDataStream& stream) const;

        /*
            Initializes this class with a packet received in the given stream.
            The stream must already have the necessary data to read (lenght bytes)
                @param stream : the stream to use to get the packet
                @param length : the data length to get in byte
        */
        void receiveData(QDataStream& stream, int length);

    protected :
        long long int date;
        int webcamId; // webcam identifier
        QVector<EventContacts> events;
};

#endif // TA_DATAEVENT_H
