/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 09 February 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_DataStream class containing the    |
  | informations about a stream from a webcam                               |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Sébastien DESTABEAU -- Initial version                        |
  | [-] 1.1 - Simon BONY -- Complete version for Tactile Anywhere V1.0      |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere. If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_datastream.h"

/*****************************************************************************/
/******                     CONSTRUCTORS/DESTRUCTOR                     ******/
/*****************************************************************************/

/*
    This constructor creates this class.
*/
TA_DataStream::TA_DataStream(){
    identifier = 0;
    image = QImage();
    unique = false;
    stop = false;
}

/*
    This constructor creates and initialize this class with a packet
    received in the given stream. The stream must already have the
    necessary data to read.
        @param stream : the stream to use to get the packet
        @param length : the data length to get in byte
*/
TA_DataStream::TA_DataStream(QDataStream& stream, int length){
    receiveData(stream, length);
}

/*****************************************************************************/
/******                             GETTERS                             ******/
/*****************************************************************************/

/*
    Gets the identification number of this type of data
        @return : the identification of this type of data
*/
short int TA_DataStream::getIdentification() const{
    return TA_DATASTREAM_IDENT;
}

/*
    Gets the compressed buffered image by the server
        @return :the webcam frame compressed and buffered
*/
QImage TA_DataStream::getImage() const {
    return image;
}

/*
    Gets the identifier about frame webcam
        @return : the identifier about frame webcam
*/
int TA_DataStream::getIdentifier() const{
    return identifier;
}

/*
    Checks if the server asks about launch a stream
        @return : true if the server ask about launch a stream, false else
*/
bool TA_DataStream::toLaunch() const{
    return !stop;
}

/*
    Checks if the server asks for an unique frame
        @return : true if the server asks for an unique frame, false else
*/
bool TA_DataStream::uniqueFrame() const{
    return unique;
}

/*****************************************************************************/
/******                             SETTERS                             ******/
/*****************************************************************************/

/*
    Sets the compressed buffered image to send to server
        @param compressedBufferedImage : the webcam frame compressed and buffered
*/
void TA_DataStream::setImage(QImage img){
    image = img;
}

/*
    Sets the identifier about frame webcam
        @param id : the identifier about frame webcam
*/
void TA_DataStream::setIdentifier(int id){
    identifier = id;
}

/*
    Sets if the server asks about launch a stream
        @param action : true if the server ask about launch a stream, false else
*/
void TA_DataStream::setToLaunch(bool action){
    this->stop = !action;
}

/*
    Sets if the server asks for an unique frame
        @return : true if the server asks for an unique frame, false else
*/
void TA_DataStream::setUniqueFrame(bool unique){
    this->unique = unique;
}

/*****************************************************************************/
/******                          OPERATION                              ******/
/*****************************************************************************/

/*
    Sends a packet with the data containing in this class
        @param stream : the stream to use to send the packet
*/
void TA_DataStream::sendData(QDataStream& stream) const{
    // Variables
    QByteArray arrayImage;
    QBuffer bufferImage(&arrayImage);
    QBuffer buffer;
    QDataStream streamB;

    // Write
    buffer.open(QIODevice::WriteOnly);
    streamB.setDevice(&buffer);
    image.save(&bufferImage,"PNG");
    streamB << identifier << unique << stop << bufferImage.buffer();
    sendHeader(stream,TA_DATASTREAM_IDENT,buffer.size()+4);
    stream << buffer.buffer();
}

/*
    Initializes this class with a packet received in the given stream.
    The stream must already have the necessary data to read (lenght bytes)
        @param stream : the stream to use to get the packet
        @param length : the data length to get in byte
*/
void TA_DataStream::receiveData(QDataStream& stream, int length){
    // Variables
    QByteArray array;
    QByteArray arrayImage;
    QBuffer buffer;
    QDataStream streamB;
    char * buf = new char[length-4];
    int clean;

    // Read
    stream >> clean;
    stream.readRawData(buf,length-4);
    array.setRawData(buf,length-4);
    buffer.setBuffer(&array);
    buffer.open(QIODevice::ReadWrite);
    streamB.setDevice(&buffer);
    streamB >> identifier >> unique >> stop >> arrayImage;
    image.loadFromData(arrayImage,"PNG");
    delete[] buf;
}
