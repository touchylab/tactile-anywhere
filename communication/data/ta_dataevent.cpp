/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 09 January 2013                                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_DataEvent class containing the     |
  | informations about an event from a client                               |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_dataevent.h"

/*****************************************************************************/
/******                     CONSTRUCTORS/DESTRUCTOR                     ******/
/*****************************************************************************/

/*
    This constructor creates this class.
*/
TA_DataEvent::TA_DataEvent(){
    date = -1;
    webcamId = -1;
}

/*
    This constructor creates and initialize this class with a packet
    received in the given stream. The stream must already have the
    necessary data to read.
        @param stream : the stream to use to get the packet
        @param length : the data length to get in byte
*/
TA_DataEvent::TA_DataEvent(QDataStream& stream, int length){
    receiveData(stream,length);
}

/*****************************************************************************/
/******                             GETTERS                             ******/
/*****************************************************************************/

/*
    Gets the identification number of this type of data
        @return : the identification of this type of data
*/
short int TA_DataEvent::getIdentification() const{
    return TA_DATAEVENT_IDENT;
}

/*
    Gets the date of events
        @return : a long long int value containing the date as
                  the number of millisecond from the 1st January 1970
*/
long long int TA_DataEvent::getEventDate() const{
    return date;
}

/*
    Gets the concerned webcam's name
        @return : the concerned webcam's name
*/
int TA_DataEvent::getWebcamId() const{
    return webcamId;
}

/*
    Gets the concerned events
        @return : a pointer to a QVector containing the events
*/
QVector<EventContacts>* TA_DataEvent::getEvents(){
    return &events;
}

/*****************************************************************************/
/******                             SETTERS                             ******/
/*****************************************************************************/

/*
    Sets webcam id associate with opencv
        @param name : the concerned area's name
*/
void TA_DataEvent::setWebcamId(int webcamId){
    this->webcamId = webcamId;
}

/*
    Sets the date of events
        @param date : a long long int value containing the date as
                      the number of millisecond from the 1st January 1970
*/
void TA_DataEvent::setEventDate(long long int date){
    this->date = date;
}

/*
    Sets the concerned events
        @param name : a pointer to a QVector containing the events
*/
void TA_DataEvent::setEvents(QVector<EventContacts>* events){
    this->events = *events;
}

/*****************************************************************************/
/******                          OPERATION                              ******/
/*****************************************************************************/

/*
    Add to QDataStream the capability to get EventContacts
        @param out : the QDatasteam to fill
        @param contact : the contact to add
        @return: the filled QDataStream
*/
QDataStream &operator<<(QDataStream &out, const EventContacts& contact){
    out << contact.areaName << contact.sectorId << contact.state;
    return out;
}

/*
    Add to QDataStream the capability to set EventContacts
        @param in : the QDatasteam to manage
        @param contact : the contact to fill
        @return: the managed QDataStream
*/
QDataStream &operator>>(QDataStream &in, EventContacts& contact){
    in >> contact.areaName >> contact.sectorId >> contact.state;
    return in;
}

/*
    Sends a packet with the data containing in this class
        @param stream : the stream to use to send the packet
*/
void TA_DataEvent::sendData(QDataStream& stream) const{
    // Variables
    QBuffer buffer;
    QDataStream streamB;

    // Write
    buffer.open(QIODevice::WriteOnly);
    streamB.setDevice(&buffer);
    streamB << date << webcamId << events;
    sendHeader(stream,TA_DATAEVENT_IDENT,buffer.size()+4);
    stream << buffer.buffer();
}

/*
    Initializes this class with a packet received in the given stream.
    The stream must already have the necessary data to read (lenght bytes)
        @param stream : the stream to use to get the packet
        @param length : the data length to get in byte
*/
void TA_DataEvent::receiveData(QDataStream& stream, int length){
    // Variables
    QByteArray array;
    QBuffer buffer;
    QDataStream streamB;
    char * buf = new char[length-4];
    int clean;

    // Read
    stream >> clean;
    stream.readRawData(buf,length-4);
    array.setRawData(buf,length-4);
    buffer.setBuffer(&array);
    buffer.open(QIODevice::ReadWrite);
    streamB.setDevice(&buffer);
    streamB >> date >> webcamId >> events;
    delete[] buf;
}
