/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 10 February 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_DataMarker class containing the    |
  | informations about a marker                                             |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere. If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_datamarker.h"

/*****************************************************************************/
/******                     CONSTRUCTORS/DESTRUCTOR                     ******/
/*****************************************************************************/

/*
    This constructor creates this class.
*/
TA_DataMarker::TA_DataMarker(){
    webcamId = -1;
}

/*
    This constructor creates and initialize this class with a packet
    received in the given stream. The stream must already have the
    necessary data to read.
        @param stream : the stream to use to get the packet
        @param length : the data length to get in byte
*/
TA_DataMarker::TA_DataMarker(QDataStream& stream, int length){
    receiveData(stream, length);
}

/*****************************************************************************/
/******                             GETTERS                             ******/
/*****************************************************************************/

/*
    Gets the identification number of this type of data
        @return : the identification of this type of data
*/
short int TA_DataMarker::getIdentification() const{
    return TA_DATAMARKER_IDENT;
}

/*
    Gets the webcam's id seeing markers
        @return : the webcam's id seeing markers
*/
int TA_DataMarker::getWebcamId() const{
    return webcamId;
}

/*
    Gets the positions of the center of markers
        @return : a QMap with marker's id as key and position as value
*/
QMap<quint32,QPoint>& TA_DataMarker::getPositions(){
    return positions;
}

/*****************************************************************************/
/******                             SETTERS                             ******/
/*****************************************************************************/

/*
    Sets the webcam's id seeing markers
        @param webcamId : the webcam's id seeing markers
*/
void TA_DataMarker::setWebcamId(int webcamId){
    this->webcamId = webcamId;
}

/*
    Sets the positions of the center of markers
        @param positions : a QMap with marker's id as key and position as value
*/
void TA_DataMarker::setPositions(QMap<quint32,QPoint>& positions){
    this->positions = positions;
}

/*****************************************************************************/
/******                          OPERATION                              ******/
/*****************************************************************************/

/*
    Sends a packet with the data containing in this class
        @param stream : the stream to use to send the packet
*/
void TA_DataMarker::sendData(QDataStream& stream) const{
    // Variables
    QBuffer buffer;
    QDataStream streamB;

    // Write
    buffer.open(QIODevice::WriteOnly);
    streamB.setDevice(&buffer);
    streamB << webcamId << positions;
    sendHeader(stream,TA_DATAMARKER_IDENT,buffer.size()+4);
    stream << buffer.buffer();
}

/*
    Initializes this class with a packet received in the given stream.
    The stream must already have the necessary data to read (lenght bytes)
        @param stream : the stream to use to get the packet
        @param length : the data length to get in byte
*/
void TA_DataMarker::receiveData(QDataStream& stream, int length){
    // Variables
    QByteArray array;
    QBuffer buffer;
    QDataStream streamB;
    char * buf = new char[length-4];
    int clean;

    // Read
    stream >> clean;
    stream.readRawData(buf,length-4);
    array.setRawData(buf,length-4);
    buffer.setBuffer(&array);
    buffer.open(QIODevice::ReadWrite);
    streamB.setDevice(&buffer);
    streamB >> webcamId >> positions;
    delete[] buf;
}
