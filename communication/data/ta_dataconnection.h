/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 07 January 2013                                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_DataConnection class          |
  | containing the informations of a connection message between a client    |
  | and a server                                                            |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_DATACONNECTION_H
#define TA_DATACONNECTION_H

/*****************************************************************************/
/******                             DEFINES                             ******/
/*****************************************************************************/

// data connection identifier
#define TA_DATACONNECTION_IDENT (0x01)
// data connection static data size
#define TA_DATACONNECTION_STATIC_DATA_SIZE (sizeof(char)+3*sizeof(bool))

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QString>

// Local includes
#include "ta_data.h"

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
 *  TA_DataConnection is a class containing data get from a Connection exchange or
 *  created to be send in this exchange. This exchange is initialized by the server
 *  at the client connection to identify it and to allow access.
 */
class TA_DataConnection : public TA_Data {
    public :
        /*
            This constructor creates this class.
        */
        TA_DataConnection();

        /*
            This constructor creates and initialize this class with a packet
            received in the given stream. The stream must already have the
            necessary data to read (length parameter).
                @param stream : the stream to use to get the packet
                @param length : the data length to get in byte
        */
        TA_DataConnection(QDataStream& stream, int length);

        /* Getters */

        /*
            Gets the identification number of this type of data
                @return : the identification of this type of data
        */
        short int getIdentification() const;

        /*
            Gets the client's name.
                @return : the client's name
        */
        QString getClientName() const;

        /*
            Checks if the client was not already connected
                @return : true if the client was not already connected,
                          false else
        */
        bool isFirstConnection() const;

        /*
            Checks if the server authorized the connection
                @return : true if the server authorized this connection,
                          false else
        */
        bool isAuthorized() const;

        /*
            Checks if the server is protected by a password
                @return : true if the server is protected by a password,
                          false else
        */
        bool isProtected() const;

        /*
            Gets the password hash send by the client
                @return : the password hash send by the client
        */
        QByteArray &getHash();

        /* Setters */

        /*
            Sets the client's name.
                @param name : the client's name to set
        */
        void setClientName(const QString& name);

        /*
            Sets if the client was not already connected
                @param first : true if the client was not already connected,
                               false else
        */
        void setFirstConnection(bool first);

        /*
            Sets the server's authorization for this connection
                @param authorization : true if the server authorized this,
                                       connection false else
        */
        void setAuthorisation(bool authorization);

        /*
            Sets the server protection state
                @return : true if the server is protected by a password,
                          false else
        */
        void setProtectionState(bool state);

        /*
            Sets the password hash to check
                @return : the password hash to check
        */
        void setHash(const QByteArray &hash);

        /* Operation */

        /*
            Sends a packet with the data containing in this class
                @param stream : the stream to use to send the packet
        */
        void sendData(QDataStream& stream) const;

        /*
            Initializes this class with a packet received in the given stream.
            The stream must already have the necessary data to read (lenght bytes)
                @param stream : the stream to use to get the packet
                @param length : the data length to get in byte
        */
        void receiveData(QDataStream& stream, int length);

    protected :
        QString clientName; // client name
        QByteArray hash;
        bool firstConnection,authorization, protectionState;

};

#endif // TA_DATACONNECTION_H
