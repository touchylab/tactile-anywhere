/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 07 February 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_DataSector class containing   |
  | the informations about a sector for a client                            |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_DATASECTOR_H
#define TA_DATASECTOR_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QVector>
#include <QPoint>
#include <QString>

// Local includes
#include "ta_data.h"

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/

// data sector identifier
#define TA_DATASECTOR_IDENT (0x08)
// data sector data fixed size
#define TA_DATASECTOR_DATA_FIXED_SIZE (sizeof(int) + sizeof(float))

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
 *  TA_DataSector is a class containing the list of points concerning a sector on a webcam
 *  to be send to a client.
 */
class TA_DataSector : public TA_Data {
    public :
        /*
            This constructor creates this class.
        */
        TA_DataSector();

        /*
            This constructor creates and initialize this class with a packet
            received in the given stream. The stream must already have the
            necessary data to read.
                @param stream : the stream to use to get the packet
                @param length : the data length to get in byte
        */
        TA_DataSector(QDataStream& stream, int length);

        /* Getters */

        /*
            Gets the identification number of this type of data
                @return : the identification of this type of data
        */
        short int getIdentification() const;

        /*
            Gets the concerned webcam's id
                @return : the concerned webcam's id
        */
        int getWebcamId() const;

        /*
            Gets the concerned area's name
                @return : the concerned area's name
        */
        QString getAreaName() const;

        /*
            Gets the concerned sector's name
                @return : the concerned sector's name
        */
        QString getSectorName() const;

        /*
            Gets the concerned sector's points
                @return : a pointer to a QVector containing the points representing the sector
        */
        QVector<QPoint>* getSector();

        /*
            Gets the dimension ratio to apply during the detection
                @return : the dimension ratio to apply
        */
        float getDimensionRatio() const;

        /* Setters */

        /*
            Sets the concerned webcam's name
                @param name : the concerned webcam's name
        */
        void setWebcamId(int webcamId);

        /*
            Sets the concerned area's name
                @param name : the concerned area's name
        */
        void setAreaName(QString name);

        /*
            Sets the concerned sector's name
                @param name : the concerned sector's name
        */
        void setSectorName(QString name);

        /*
            Sets the concerned sector's points
                @param sector : a pointer to a QVector containing the points representing the sector
        */
        void setSector(QVector<QPoint>* sector);

        /*
            Sets the dimension ratio to apply during the detection
                @param : the dimension ratio to apply
        */
        void setDimensionRatio(float dimensionRatio);

        /* Operation */

        /*
            Sends a packet with the data containing in this class
                @param stream : the stream to use to send the packet
        */
        void sendData(QDataStream& stream) const;

        /*
            Initializes this class with a packet received in the given stream.
            The stream must already have the necessary data to read (lenght bytes)
                @param stream : the stream to use to get the packet
                @param length : the data length to get in byte
        */
        void receiveData(QDataStream& stream, int length);

    protected :
        int webcamId; // webcam identifier
        QString areaName; // area name
        QString sectorName; // sector name
        QVector<QPoint> sector; // sector points
        float dimensionRatio;
};

#endif // TA_DATASECTOR_H
