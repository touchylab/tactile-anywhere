/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 09 February 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_DataStream class containing   |
  | the informations about a stream from a webcam                           |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Sébastien DESTABEAU -- Initial version                        |
  | [-] 1.1 - Simon BONY -- Complete version for Tactile Anywhere V1.0      |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere. If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_DATASTREAM_H
#define TA_DATASTREAM_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QImage>

// Local includes
#include "ta_data.h"

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/

// data stream identifier
#define TA_DATASTREAM_IDENT (0x06)
// data stream data fixed size
#define TA_DATASTREAM_DATA_FIXED_SIZE (sizeof(int) + sizeof(bool)*2)

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
 * TA_DataStream is a class to periodically send client webcam frames. This exchange is
 * initialized by the client with a respond from the server
*/
class TA_DataStream : public TA_Data {
    public :
        /*
            This constructor creates this class.
        */
        TA_DataStream();

        /*
            This constructor creates and initialize this class with a packet
            received in the given stream. The stream must already have the
            necessary data to read (TA_DATASTREAM_DATA_FIXED_SIZE bytes).
                @param stream : the stream to use to get the packet
                @param length : the data length to get in byte
        */
        TA_DataStream(QDataStream& stream, int length);

        /* Getters */

        /*
            Gets the identification number of this type of data
                @return : the identification of this type of data
        */
        short int getIdentification() const;

        /*
            Gets the compressed buffered image by the server
                @return :the webcam frame compressed and buffered
        */
        QImage getImage() const;

        /*
            Sets the compressed buffered image to send to server
                @param compressedBufferedImage : the webcam frame compressed and buffered
        */
        void setImage(QImage image);

        /*
            Gets the identifier about frame webcam
                @return : the identifier about frame webcam
        */
        int getIdentifier() const;

        /*
            Sets the identifier about frame webcam
                @param id : the identifier about frame webcam
        */
        void setIdentifier(int id);

        /*
            Checks if the server asks about launch a stream
                @return : true if the server ask about launch a stream, false else
        */
        bool toLaunch() const;

        /*
            Sets if the server asks about launch a stream
                @param action : true if the server ask about launch a stream, false else
        */
        void setToLaunch(bool action);

        /*
            Checks if the server asks for an unique frame
                @return : true if the server asks for an unique frame, false else
        */
        bool uniqueFrame() const;

        /*
            Sets if the server asks for an unique frame
                @return : true if the server asks for an unique frame, false else
        */
        void setUniqueFrame(bool unique);

        /* Operation */

        /*
            Sends a packet with the data containing in this class
                @param stream : the stream to use to send the packet
        */
        void sendData(QDataStream& stream) const;

        /*
            Initializes this class with a packet received in the given stream.
            The stream must already have the necessary data to read (lenght bytes)
                @param stream : the stream to use to get the packet
                @param length : the data length to get in byte
        */
        void receiveData(QDataStream& stream, int length);

    private:
        QImage image; // compressed buffered image
        int identifier; // identifier of the webcam for the frame
        bool stop;  // Set as true to stop the diffusion, false to start
        bool unique; // set as true to ask an unique frame, false else
};

#endif // TA_DATASTREAM_H
