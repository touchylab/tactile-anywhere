/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 06 January 2013                                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_Data class containing the     |
  | informations of a message between the server and clients                |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_DATA_H
#define TA_DATA_H

/*****************************************************************************/
/******                             DEFINES                             ******/
/*****************************************************************************/

// length about data field identifier
#define TA_DATA_FIELD_IDENT_LENGTH  sizeof(short int)
// length about data field data size
#define TA_DATA_FIELD_DSIZE_LENGTH  sizeof(int)

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QByteArray>
#include <QBuffer>
#include <QDataStream>
#include <QPoint>

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
 *  TA_Data is a basic class containing raw data get from a communication or created
 *  to be send into a communication
 */
class TA_Data {
    public :
        /*
            This destructor delete this class
        */
        virtual ~TA_Data(){}

        /*
            Gets the identification number of this type of data
                @return : the identification of this type of data
        */
        virtual short int getIdentification() const =0;

        /*
            Sends a packet with the data containing in this class
                @param stream : the stream to use to send the packet
        */
        virtual void sendData(QDataStream& stream) const =0;

        /*
            Initializes this class with a packet received in the given stream.
            The stream must already have the necessary data to read (lenght bytes)
                @param stream : the stream to use to get the packet
                @param length : the data length to get in byte
        */
        virtual void receiveData(QDataStream& stream, int length)=0;

    protected :
        /*
            Sends the header of a packet with its identification and its data size
                @param stream : the stream to use to send the packet
                @param ident : the identification to send
                @param size  : the data size to send in byte
        */
        void sendHeader(QDataStream& stream, short int ident, int size) const;
};

#endif // TA_DATA_H
