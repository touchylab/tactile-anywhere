/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 14 january 2013                                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_ServerUdp class offering to   |
  | specify to clients the address and the port of the TCP server           |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_SERVERUDP_H
#define TA_SERVERUDP_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/
// Qt
#include <QtNetwork/QUdpSocket>

// Local includes
#include "communication/data/ta_databeacon.h"
#include "entity/ta_server.h"

/*****************************************************************************/
/******                             DEFINES                             ******/
/*****************************************************************************/

// default UDP server port
#define TA_SERVERUDP_DEFAULT_PORT 2014

/*****************************************************************************/
/******                              CLASS                              ******/
/*****************************************************************************/

/*
 *  TA_ServerUdp is a class offering to the server to specify to clients the address and the port
 *  of the TCP server to communicate with them
 */
class TA_ServerUdp : public QUdpSocket{
    Q_OBJECT
    public :

        /*
            This constructor creates and initializes this class to listen on the given port
                @param tcpPort : the tcp port to use for communication
                @param udpPort : the port to listen on this socket for client request
        */
        TA_ServerUdp(quint16 tcpPort, quint16 udpPort = TA_SERVERUDP_DEFAULT_PORT);

    private slots :
        /*
            Reads a received beacon from a client and responds with the server informations
        */
        void readAndRespond();

    signals :
        /*
            Sends when a new client request the TCP server informations.
                @param address : the IP address of this computer
                @param port : the port of the TCP server
        */
        void newRequest(QHostAddress address, quint16 port);

    protected :
        quint16 tcpPort; // TCP port
};

#endif // TA_SERVERUDP_H
