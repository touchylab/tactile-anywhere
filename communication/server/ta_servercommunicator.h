/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 16 january 2013                                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_ServerCommunicator class      |
  | managing communications with clients                                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_SERVERCOMMUNICATOR_H
#define TA_SERVERCOMMUNICATOR_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Local includes
#include "ta_serverudp.h"
#include "ta_servertcp.h"
#include "ta_infoclient.h"

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
 *  TA_ServerCommunicator is a class offering to the server to manage TCP connections
 *  with clients.
 */
class TA_ServerCommunicator : public QObject{
    Q_OBJECT
    public :
        /*
            This constructor creates, initializes and lauches the two sockets
            for UDP client's beacon and TCP client communication
                @param port : the port to use for the UDP socket. This port will be used by
                              clients to find servers on the local network
        */
        TA_ServerCommunicator(quint16 port = TA_SERVERUDP_DEFAULT_PORT);

        /*
            Destroy properly this class
        */
        ~TA_ServerCommunicator();

        /*
            Closes the two sockets.The generated TA_InfoClients must be closed.
        */
        void close();

    signals :
        /*
            Sends when a new client request the TCP server informations.
                @param address : the IP address of this computer
                @param port : the port of the TCP server
        */
        void newRequest(QHostAddress address, quint16 port);

        /*
            Sends when a new client asked a connection.
                @param client : a pointer to the new client. It must be delete after use.
        */
        void newClient(TA_InfoClient* client);

    protected:
        TA_ServerUdp* udp;
        TA_ServerTcp* tcp;
};

#endif // TA_SERVERCOMMUNICATOR_H
