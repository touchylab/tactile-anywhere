/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 14 january 2013                                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_ServerUdp class offering to        |
  | specify to clients the address and the port of the TCP server           |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_serverudp.h"

/*****************************************************************************/
/******                  CONSTRUCTORS/DESTRUCTOR                        ******/
/*****************************************************************************/

/*
    This constructor creates and initializes this class to listen on the given port
        @param tcpPort : the tcp port to use for communication
        @param udpPort : the port to listen on this socket for client request
*/
TA_ServerUdp::TA_ServerUdp(quint16 tcpPort, quint16 udpPort){
    this->tcpPort = tcpPort;
    bind(udpPort, QUdpSocket::ShareAddress);
    connect(this, SIGNAL(readyRead()),this, SLOT(readAndRespond()));
}

/*****************************************************************************/
/******                           SLOTS                                 ******/
/*****************************************************************************/

/*
    Reads a received beacon from a client and responds with the server informations
*/
void TA_ServerUdp::readAndRespond(){
    QHostAddress address;
    quint16 port;

    while(hasPendingDatagrams()){
        // Prepares and reads data
        QByteArray datagramReceived;
        datagramReceived.resize(pendingDatagramSize());
        readDatagram(datagramReceived.data(), datagramReceived.size(),&address,&port);

        // Reads the beacon
        QDataStream streamReceived(datagramReceived);
        TA_DataBeacon beacon(streamReceived);

        emit newRequest(address, port);

        // Completes the beacon and sends it to the client
        beacon.setServerPort(tcpPort);
        beacon.setServerName(TA_Server::getInstance()->getServerName());
        beacon.setServerProtection(!TA_Server::getInstance()->getServerPassword().isEmpty());

        QByteArray datagramSend;
        QDataStream streamSend(&datagramSend,QIODevice::ReadWrite);
        beacon.sendData(streamSend);
        writeDatagram(datagramSend, address, port);
    }
}
