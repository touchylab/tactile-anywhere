/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 28 december 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_ServerTcp class offering to   |
  | communicate with clients                                                |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_SERVERTCP_H
#define TA_SERVERTCP_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QtNetwork/QTcpServer>
#include <QMap>

// Local includes
#include "ta_infoclient.h"

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/

enum ClientState {OK, CONNECTION_DISCONNECTED};

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
 *  TA_ServerTcp is a class offering to the server to manage TCP connections
 *  with clients.
 */
class TA_ServerTcp : public QTcpServer{
    Q_OBJECT
    public :
        /*
            This constructor creates and initializes this class with the actual configuration.
        */
        TA_ServerTcp();

    private slots :
        /*
            Connects a client to this server
        */
        void serve();

        /*
            Called when the connection with the client is reconnected
                @param client : the client
        */
        void reconnected(TA_InfoClient* client);

        /*
            Called when the connection with the client is disconnected
                @param client : the client
        */
        void disconnected(TA_InfoClient* client);

        /*
            Called when the client closes the connection.
                @param client : the client
        */
        void closed(TA_InfoClient* client);

    signals :
        /*
            Sends when a new client asked a connection.
                @param client : a pointer to the new client. It must be delete after use.
        */
        void newClient(TA_InfoClient* client);

    private:
        QMap<qint32,ClientState> states; // clients states
        QMap<qint32,TA_InfoClient*> clients; // clients
};

#endif // TA_SERVERTCP_H
