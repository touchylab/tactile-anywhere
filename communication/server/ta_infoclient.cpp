/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 8 January 2013                                     |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_InfoClient class containing all    |
  | the information about a client                                          |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_infoclient.h"

/*****************************************************************************/
/******                     CONSTRUCTORS/DESTRUCTOR                     ******/
/*****************************************************************************/

/*
    This constructor creates and initializes this class with a socket. Use this constructor to
    creates a distant client.
        @param socket: the socket linking this server with the client
*/
TA_InfoClient::TA_InfoClient(QTcpSocket* socket){
    // Initializes members
    this->socket = NULL;
    setClientSocket(socket);
    this->accepted = false;
    this->local = false;
    this->header = true;
    this->detection = false;
    setClientName(QString("TA_INFOCLIENT_DEFAULT_NAME"));
    setClientState(ASKED_FOR_CONNECTION);

    // Connects signals
    // Reconnection not completed
    //QObject::connect(&timerDisconnect, SIGNAL(timeout()),this, SLOT(disconnect()));
    QObject::connect(&timerDisconnect, SIGNAL(timeout()),this, SLOT(close()));
    timerDisconnect.start(TA_INFOCLIENT_DISCONNECT_INTERVAL_MS);
}

/*
    This constructor creates and initializes this class as a local client. Use this constructor
    to create a communication link between a server and its local client.
*/
TA_InfoClient::TA_InfoClient(){
    // Initializes members
    this->socket = NULL;
    this->local = true;
    this->header = true;
    this->accepted = true;
    this->detection = false;
    setClientName(QString(TA_INFOCLIENT_LOCAL_NAME));
    setClientState(LOCAL);
}

/*
    Cleans properly this class at the destruction
*/
TA_InfoClient::~TA_InfoClient(){
    close(false);
}

/*****************************************************************************/
/******                             GETTERS                             ******/
/*****************************************************************************/

/*
    Gets the client's name.
        @return : the client's name
*/
QString TA_InfoClient::getClientName() const{
    return name;
}

/*
    Gets the client's address
        @return : the client's address
*/
QHostAddress TA_InfoClient::getClientAddress() const{
    return address;
}


/*
    Gets the client's socket.
        @return : the client's socket
*/
QTcpSocket* TA_InfoClient::getClientSocket() const{
    return socket;
}

/*
    Gets the state of the connection with this client
        @return : the state of the connection with the client
*/
ConnectionState TA_InfoClient::getClientState() const{
    return state;
}

/*
    Checks if the client is accepted on this server. The client is or was already
    connected to this server.
        @return : true if the client is accepted, false else
*/
bool TA_InfoClient::isAccepted() const{
    return accepted;
}

/*
    Checks if this class linked this server to its local client
        @return : true if this class linked this server to its local client, false else
*/
bool TA_InfoClient::isLocal() const{
    return local;
}

/*
    Checks if the client is authorized for the actual detection.
        @return : true if the client is authorized, false else
*/
bool TA_InfoClient::isAuthorizedForDetection() const{
    return detection;
}

/*****************************************************************************/
/******                             SETTERS                             ******/
/*****************************************************************************/

/*
    Sets the client's name.
        @param name : the client's name to set
*/
void TA_InfoClient::setClientName(const QString& name){
    this->name = name;
}

/*
    Sets the client's socket.
        @param name : the client's socket to set
*/
void TA_InfoClient::setClientSocket(QTcpSocket* socket){
    if(this->socket != NULL){
        this->socket->abort();
        this->socket->deleteLater();
    }
    this->socket = socket;
    if(socket != NULL){
        this->stream.setDevice(socket);
        this->address = socket->peerAddress();
        QObject::connect(socket, SIGNAL(readyRead()),this, SLOT(read()));
    }
}

/*
    Sets the state of the connection with this client
        @param state : the state of the connection with the client
*/
void TA_InfoClient::setClientState(ConnectionState state){
    this->state = state;
}

/*
    Sets the authorization for the client to detect during the actual detection
        @param authorization: true if the client is authorized, false else
*/
void TA_InfoClient::setClientDetectionAuthorization(bool authorization){
    this->detection = authorization;
}

/*****************************************************************************/
/******                           SLOTS                                 ******/
/*****************************************************************************/
/*
    Reads a paquet from this client
*/
void TA_InfoClient::read(){
    while(socket && socket->bytesAvailable()>0){
        // Checks for complete header
        if (header){
            if(socket->bytesAvailable() < TA_INFOCLIENT_HEADER_LENGTH){
                return;
            }
            stream >> identPacket >> lengthPacket;
            header = false;
        }
        // Checks for complete packet
        if(socket->bytesAvailable() < lengthPacket){
            return;
        }
        header = true;

        // Reads it and applies the correct operations
        switch(identPacket){
            case TA_DATACONNECTION_IDENT : { // Connection packet
                if(getClientState() == CONNECTED){ // If the client is already connected, ignore the packet
                    socket->read(lengthPacket);
                    break;
                }
                // Else, reads it and manage it
                TA_DataConnection conn = TA_DataConnection(stream,lengthPacket);
                manageData(&conn);
                break;
            } case TA_DATACLOSE_IDENT :{ // Close packet
                TA_DataClose close;
                manageData(&close);
                break;
            }case TA_DATAHELLO_IDENT :{ // Hello packet
                TA_DataHello hello;
                manageData(&hello);
                break;
            }case TA_DATANTP_IDENT :{ // NTP packet
                TA_DataNTP ntp = TA_DataNTP(stream,lengthPacket);
                manageData(&ntp);
                break;
            } case TA_DATAWEBCAM_IDENT :{ // Webcam packet
                TA_DataWebcam webcam = TA_DataWebcam(stream,lengthPacket);
                manageData(&webcam);
                break;
            }case TA_DATAEVENT_IDENT :{ // Event packet
                if(getClientState() != CONNECTED && getClientState() != LOCAL){ // If the client is not connected, ignore the packet
                    socket->read(lengthPacket);
                    break;
                }
                TA_DataEvent event = TA_DataEvent(stream,lengthPacket);
                manageData(&event);
                break;
            }case TA_DATASTREAM_IDENT : { // Stream packet
                TA_DataStream streamData(stream,lengthPacket);
                manageData(&streamData);
                break;
            } case TA_DATAMARKER_IDENT : { // Marker packet
                TA_DataMarker markerData(stream,lengthPacket);
                manageData(&markerData);
                break;
            }default: {
                socket->read(lengthPacket);
                break;
            }
        }
    }
}

/*
    Manages a disconnection
*/
void TA_InfoClient::disconnect(){
    if(getClientState() != LOCAL && getClientState() != CLOSED){
        timerDisconnect.stop();
        socket->abort();
        setClientState(DISCONNECTED);
    }
    emit disconnected(this);
}

/*
    Closes the connection with this client
        @param sendSignal : set as true to send the close signal, false else
*/
void TA_InfoClient::close(bool sendSignal){
    emit toForget(this);
    if(socket != NULL){
        // Timer
        timerDisconnect.stop();

        if(socket->isOpen() && getClientState() != CLOSED){
            // Send close data
            TA_DataClose close;
            sendPacket(&close);
            socket->blockSignals(true);
            socket->flush();
            while(socket->bytesToWrite() > 0 && socket->state() == QAbstractSocket::ConnectedState){
                socket->waitForBytesWritten();
            }
        }
        // Close socket
        socket->deleteLater();
        socket = NULL;
    }
    if(getClientState() != LOCAL){
        setClientState(CLOSED);
    }
    accepted = false;
    if(sendSignal){
        emit closed(this);
    }
}

/*****************************************************************************/
/******                           OPERATIONS                            ******/
/*****************************************************************************/

/*
    Sends to this client the given packet
        @param data : the packet to send
*/
void TA_InfoClient::sendPacket(TA_Data* data){
    if(getClientState() == DISCONNECTED || getClientState() == CLOSED){
        return;
    }

    if(!local){
        data->sendData(stream);
    } else {
        TA_Client::getInstance()->getClientMainServer()->manageData(data);
    }
}

/*
    Manages the given data
        @param stream : the data to manage
*/
void TA_InfoClient::manageData(TA_Data* data){
    // Applies the correct operations
    switch(data->getIdentification()){
        case TA_DATACONNECTION_IDENT : { // Connection packet
            // Gets the packet
            TA_DataConnection* conn = (TA_DataConnection*)data;

            if(getClientState() != PASSWORD_NEEDED){ // If the client asked for a connection
                setClientName(conn->getClientName());
                if(!TA_Server::getInstance()->getServerPassword().isEmpty() && !isAccepted()){
                    conn->setAuthorisation(false);
                    conn->setProtectionState(true);
                    setClientState(PASSWORD_NEEDED);
                } else {
                    conn->setAuthorisation(true);
                    conn->setProtectionState(false);
                    setClientState(CONNECTED);
                }
            } else { // If the client need to send a password
                if(conn->getHash() == TA_Server::getInstance()->getServerPassword()){
                    conn->setAuthorisation(true);
                    conn->setProtectionState(false);
                    setClientState(CONNECTED);
                } else {
                    conn->setAuthorisation(false);
                    conn->setProtectionState(true);
                }
            }

            conn->sendData(stream);
            if(getClientState() == CONNECTED){
                if(conn->isFirstConnection()){
                    setClientDetectionAuthorization(false);
                }
                if(!accepted){
                    accepted = true;
                    emit connected(this);
                } else {
                    emit reconnected(this);
                }
            }

            break;
        } case TA_DATAHELLO_IDENT : { // Hello packet
            timerDisconnect.stop();
            TA_DataHello hello;
            sendPacket(&hello);
            timerDisconnect.start(TA_INFOCLIENT_DISCONNECT_INTERVAL_MS);
            break;
        }case TA_DATACLOSE_IDENT : { // Close packet
            close();
            break;
        } case TA_DATANTP_IDENT : { // NTP packet : Set the server times and sends it
            // Gets the packet
            TA_DataNTP* ntp = (TA_DataNTP*)data;

            // Places the server times
            ntp->setServerFirstDate(boost::posix_time::microsec_clock::local_time().time_of_day().total_milliseconds());
            ntp->setServerSecondDate(boost::posix_time::microsec_clock::local_time().time_of_day().total_milliseconds());

            // Returns it to the client
            ntp->sendData(stream);
            break;
        }case TA_DATAWEBCAM_IDENT : { // Webcam packet
            emit newWebcam(this, *((TA_DataWebcam*)data));
            break;
        }case TA_DATAEVENT_IDENT : { // Event packet
            emit newEvent(this, *((TA_DataEvent*)data));
            break;
        }case TA_DATASTREAM_IDENT : { // Stream packet
            emit newStream(this, *((TA_DataStream*) data));
            break;
        } case TA_DATAMARKER_IDENT : {// Marker packet
            emit newMarkerPosition(this,*((TA_DataMarker*) data));
            break;
        }
    }
}
