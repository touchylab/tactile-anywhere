/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 16 january 2013                                    |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_ServerCommunicator class managing  |
  | communications with clients                                             |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_servercommunicator.h"

/*****************************************************************************/
/******                  CONSTRUCTORS/DESTRUCTOR                        ******/
/*****************************************************************************/

/*
    This constructor creates, initializes and lauches the two sockets
    for UDP client's beacon and TCP client communication
        @param port : the port to use for the UDP socket. This port will be used by
                      clients to find servers on the local network
*/
TA_ServerCommunicator::TA_ServerCommunicator(quint16 port){
    tcp = new TA_ServerTcp();
    udp = new TA_ServerUdp(tcp->serverPort(),port);
    connect(udp, SIGNAL(newRequest(QHostAddress,quint16)),this,SIGNAL(newRequest(QHostAddress,quint16)));
    connect(tcp, SIGNAL(newClient(TA_InfoClient*)),this,SIGNAL(newClient(TA_InfoClient*)));
}

/*
    Destroy properly this class
*/
TA_ServerCommunicator::~TA_ServerCommunicator(){
    close();
    if(udp != NULL) delete udp;
    if(tcp != NULL) delete tcp;
}

/*
    Closes the two sockets.The generated TA_InfoClients must be closed.
*/
void TA_ServerCommunicator::close(){
    udp->close();
    tcp->close();
}
