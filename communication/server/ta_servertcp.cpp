/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 28 december 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_ServerTcp class offering to        |
  | communicate with clients                                                |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_servertcp.h"

/*****************************************************************************/
/******                  CONSTRUCTORS/DESTRUCTOR                        ******/
/*****************************************************************************/

/*
    This constructor creates and initializes this class with the actual configuration.
*/
TA_ServerTcp::TA_ServerTcp(){
    listen(QHostAddress::Any);
    QObject::connect(this, SIGNAL(newConnection()),this, SLOT(serve()));
}

/*****************************************************************************/
/******                           SLOTS                                 ******/
/*****************************************************************************/

/*
    Connects a client to this server
*/
void TA_ServerTcp::serve(){
    QTcpSocket* socket = nextPendingConnection();
    if(clients.contains(socket->peerAddress().toIPv4Address())){
        if(states[socket->peerAddress().toIPv4Address()] == OK){
            socket->abort();
        } else {
            clients[socket->peerAddress().toIPv4Address()]->setClientSocket(socket);
        }
    } else {
        TA_InfoClient* client = new TA_InfoClient(socket);
        clients.insert(socket->peerAddress().toIPv4Address(),client);
        states.insert(socket->peerAddress().toIPv4Address(),OK);
        connect(client,SIGNAL(disconnected(TA_InfoClient*)),this,SLOT(disconnected(TA_InfoClient*)));
        connect(client,SIGNAL(reconnected(TA_InfoClient*)),this,SLOT(reconnected(TA_InfoClient*)));
        connect(client,SIGNAL(toForget(TA_InfoClient*)),this,SLOT(closed(TA_InfoClient*)));
        emit newClient(client);
    }
}

/*
    Called when the connection with the client is reconnected
        @param client : the client
*/
void TA_ServerTcp::reconnected(TA_InfoClient* client){
    states[client->getClientAddress().toIPv4Address()] = OK;
}

/*
    Called when the connection with the client is disconnected
        @param client : the client
*/
void TA_ServerTcp::disconnected(TA_InfoClient* client){
    states[client->getClientAddress().toIPv4Address()] = CONNECTION_DISCONNECTED;
}

/*
    Called when the client closes the connection.
        @param client : the client
*/
void TA_ServerTcp::closed(TA_InfoClient* client){
    if(!client->isLocal()){
        states.remove(client->getClientAddress().toIPv4Address());
        clients.remove(client->getClientAddress().toIPv4Address());
    }
}
