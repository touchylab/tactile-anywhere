/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 8 January 2013                                     |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_InfoClient class containing   |
  | all the information about a client                                      |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_INFOCLIENT_H
#define TA_INFOCLIENT_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QtNetwork/QTcpSocket>
#include <QtNetwork/QHostAddress>

// Boost
#include "boost/date_time/posix_time/posix_time.hpp"

// Local includes
#include "communication/data/ta_data.h"
#include "communication/data/ta_dataconnection.h"
#include "communication/data/ta_dataclose.h"
#include "communication/data/ta_dataevent.h"
#include "communication/data/ta_datantp.h"
#include "communication/data/ta_datawebcam.h"
#include "communication/data/ta_datamarker.h"
#include "communication/client/ta_infoserver.h"
#include "communication/data/ta_datastream.h"
#include "entity/ta_server.h"
#include "entity/ta_client.h"

/*****************************************************************************/
/******                             DEFINES                             ******/
/*****************************************************************************/

// Default values
#define TA_INFOCLIENT_DEFAULT_NAME  "Unknown"
#define TA_INFOCLIENT_LOCAL_NAME  "Local"

// disconnection interval in milliseconds
#define TA_INFOCLIENT_DISCONNECT_INTERVAL_MS  20000

// Header length
#define TA_INFOCLIENT_HEADER_LENGTH 6

#ifndef TA_CONNECTIONSTATE
#define TA_CONNECTIONSTATE

/*
    The differents state of the connection
*/
enum ConnectionState { NO_CONNECTION, ASKED_FOR_CONNECTION, PASSWORD_NEEDED, CONNECTED, DISCONNECTED, CLOSED, LOCAL };

#endif

/*****************************************************************************/
/******                              CLASS                              ******/
/*****************************************************************************/

/*
    TA_InfoClient is a class representing a connection with a client and all its informations
*/
class TA_InfoClient : public QObject{
    Q_OBJECT
public :
    /* Initialization */

    /*
        This constructor creates and initializes this class with a socket. Use this constructor to
        create a distant client.
            @param socket: the socket linking this server with the client
    */
    TA_InfoClient(QTcpSocket* socket);

    /*
        This constructor creates and initializes this class as a local client. Use this constructor
        to create a communication link between a server and its local client.
    */
    TA_InfoClient();

    /*
        Cleans properly this class at the destruction
    */
    ~TA_InfoClient();

    /* Getters */

    /*
        Gets the client's name.
            @return : the client's name
    */
    QString getClientName() const;

    /*
        Gets the client's address
            @return : the client's address
    */
    QHostAddress getClientAddress() const;

    /*
        Gets the client's socket.
            @return : the client's socket
    */
    QTcpSocket* getClientSocket() const;

    /*
        Gets the state of the connection with this client
            @return : the state of the connection with the client
    */
    ConnectionState getClientState() const;

    /*
        Checks if the client is accepted on this server. The client is or was already
        connected to this server.
            @return : true if the client is accepted, false else
    */
    bool isAccepted() const;

    /*
        Checks if this class linked this server to its local client
            @return : true if this class linked this server to its local client, false else
    */
    bool isLocal() const;

    /*
        Checks if the client is authorized for the actual detection.
            @return : true if the client is authorized, false else
    */
    bool isAuthorizedForDetection() const;

    /* Setters */

    /*
        Sets the client's name.
            @param name : the client's name to set
    */
    void setClientName(const QString& name);

    /*
        Sets the client's socket.
            @param socket : the client's socket to set
    */
    void setClientSocket(QTcpSocket* socket);

    /*
        Sets the state of the connection with this client
            @param state : the state of the connection with the client
    */
    void setClientState(ConnectionState state);

    /*
        Sets the authorization for the client to detect during the actual detection
            @param authorization: true if the client is authorized, false else
    */
    void setClientDetectionAuthorization(bool authorization);

    /* Operation */

    /*
        Sends to this client the given packet
            @param data : the packet to send
    */
    void sendPacket(TA_Data* data);

    /*
        Manages the given data
            @param stream : the data to manage
    */
    void manageData(TA_Data* data);

private slots :
    /*
        Reads a paquet from this client
    */
    void read();

    /*
        Manages a disconnection
    */
    void disconnect();

public slots :
    /*
        Closes the connection with this client
            @param sendSignal : set as true to send the close signal, false else
    */
    void close(bool sendSignal = true);

signals:
    /*
        Send when this server accepts the connection
            @param client : this client
    */
    void connected(TA_InfoClient* client);

    /*
        Send when the client is reconnected
            @param client : this client
    */
    void reconnected(TA_InfoClient* client);

    /*
        Send when the connection with the client is disconnected
            @param client : this client
    */
    void disconnected(TA_InfoClient* client);

    /*
        Send when the server must be forget
            @param client : this client
    */
    void toForget(TA_InfoClient* client);

    /*
        Send when the client closes the connection.
            @param client : this client
    */
    void closed(TA_InfoClient* client);

    /*
        Send when a new event occurs on the client
            @param client : this client
            @param event : the occured event
    */
    void newEvent(TA_InfoClient* client,TA_DataEvent event);

    /*
        Send a new created stream frame on the client
            @param client : this client
            @param event : the occured stream frame
    */
    void newStream(TA_InfoClient* client,TA_DataStream stream);

    /*
        Send when a new list of webcam coming from the client
            @param client : this client
            @param webcams : the webcams
    */
    void newWebcam(TA_InfoClient* client,TA_DataWebcam webcams);

    /*
        Send when a new marker packet is sended by the client
            @param client : this client
            @param marker : the marker position to manage
    */
    void newMarkerPosition(TA_InfoClient* client, TA_DataMarker marker);

protected :
    // Informations
    QString name;
    QHostAddress address;

    // Read variables
    bool header;
    short identPacket;
    int lengthPacket;

    // Timers
    QTimer timerDisconnect;

    // Functionnalities
    QTcpSocket* socket;
    QDataStream stream;
    ConnectionState state;
    bool local,accepted,detection;
};

#endif // TA_INFOCLIENT_H
