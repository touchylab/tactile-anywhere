/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 01 february 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_Client class representing the |
  | Client in Tactile Anywhere                                              |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  | [-] 1.1 - Sébastien DESTABEAU -- Reemit frame and send event            |
  | [-] 1.2 - Simon BONY -- Complete for Tatile Anywhere V1.0               |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_CLIENT_H
#define TA_CLIENT_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QHostInfo>
#include <QList>
#include <QObject>
#include <QString>
#include <QImageWriter>
#include <QCamera>

// Local includes
#include "gui/ta_logger.h"
#include "detection/webcam/ta_webcam.h"
#include "communication/data/ta_datadetection.h"
#include "communication/data/ta_datasector.h"
#include "communication/data/ta_datadetection.h"
#include "communication/data/ta_datamarker.h"
#include "communication/data/ta_datastream.h"

// Local classes
class TA_InfoServer;
class TA_ClientCommunicator;

/*****************************************************************************/
/******                              CLASS                              ******/
/*****************************************************************************/

/*
    TA_Client is a class representing a client in Tactile Anywhere. A client manages its webcams
    and performs the detection.
*/
class TA_Client : public QObject{
    Q_OBJECT
    public :
        /* Initialization */

        /*
            Initializes an instance of this class as a singleton
                @param local : true if this client is on the same computer as its server, false else
        */
        static void initialize(bool local);

        /*
            Cleans properly this class at the destruction
        */
        ~TA_Client();

        /*
            Initialize webcam list
        */
        void initWebcams();

        /* Getters */

        /*
            Gets the unique instance of this class. initialize(bool) must be called before
                @return : the unique instance of this class or NULL if this class is not initialized
        */
        static TA_Client* getInstance();

        /*
            Gets the client's name.
                @return : the client's name
        */
        QString getClientName() const;

        /*
            Gets the client's webcams.
                @return : the client's webcams
        */
        QList<TA_Webcam*> getWebcams() const;

        /*
            Gets the client's communicator.
                @return : the client's communicator
        */
        TA_ClientCommunicator* getClientCommunicator() const;

        /*
            Gets the client's main server.
                @return : the client's main server
        */
        TA_InfoServer* getClientMainServer() const;

        /*
            Gets the client's servers list.
                @return : the client's servers list
        */
        const QList<TA_InfoServer*>* getClientServersList() const;

        /*
            Finds and returns the free webcams of the systems
                @return: a map containing free webcams' id as key and capture as value
        */
        static QMap<int,VideoCapture *> getFreeWebcams();

        /*
            Get the logger to display logs messages.
        */
        TA_Logger* getLogger();

        /* Setters */

        /*
            Sets the client's name.
                @param name : the client's name to set
        */
        void setClientName(const QString& name);

        /*
            Sets the client's webcams.
                @param name : the client's webcams list
        */
        void setSelectedWebcams(const QList<TA_Webcam*> selectedWebcams);
        /* Operations */

        /*
            Initializes a connection with the given server
                @param server : the new server to connect
        */
        void connectTo(TA_InfoServer* server);

        /*
            Cleans all the sectors
        */
        void cleanSectors();

        /*
            Cleans the known servers
        */
        void cleanServers();

    public slots :
        /*
            Sends the webcams to the server
        */
        void sendWebcams();

        /*
            Removes this server from the list of the client
                @param server : the closed server
        */
        void closeServer(TA_InfoServer* server);

    private slots :
        /*
            Adds a server to the list of this client. Called when a new server comes from the communication module
                @param server : the new server to add. It must be delete after use
        */
        void newServer(TA_InfoServer* server);

        /*
            Accept the server as the main server
                @param server : the new main server
        */
        void connectedServer(TA_InfoServer* server);

        /*
            Called when the connection with the server is disconnected
                @param server : the main server
        */
        void disconnectedServer(TA_InfoServer* server);


        /*
            Manages the stream asking from a server
                @param server : the main server
                @param stream : the ask to manage
        */
        void manageStreamAsk(TA_InfoServer* server, TA_DataStream stream);

        /*
            Re emit frame from a picture from a webcam
                @param webcam : the webcam objet
                @param image : the picture from the webcam frame
        */
        void reemitFrame(TA_Webcam* webcam, QImage image);

        /*
            Applies a sector to the concerned webcam
                @param server : the main server
                @param sector : the sector to manage
        */
        void applySector(TA_InfoServer* server, TA_DataSector sector);

        /*
            Manage detection on all connected webcams
                @param server : the main server
                @param detection: detection packet to launch or stop detection
        */
        void manageDetection(TA_InfoServer* server, TA_DataDetection detection);

        /*
            Retransmit events to the server
                @param webcam : the webcam concerned by the event
                @param contacts : events to transmit
        */
        void retransmitEvent(TA_Webcam *webcam, VectorEvents contacts);

        /*
            Retransmit to the server the changement of state of a webcam
                @param webcam : the webcam concerned by the changement
        */
        void changedStateWebcam(TA_Webcam *webcam);

        /*
            Retransmit markers to the server
                @param server : the main server
                @param marker: marker packet asked for markers
        */
        void manageMarker(TA_InfoServer* server, TA_DataMarker marker);

    signals:
        /*
            Sends when the connection with the server main is accepted
                @param server : the connected server
        */
        void connectedToMainServer(TA_InfoServer* server);

        /*
            Sends when the connection with the main server is broken
                @param server : the disconnected server
        */
        void disconnectedFromMainServer(TA_InfoServer* server);

    private :
        /* Methods */

        /*
            This constructor creates and initializes this class and instantiates is related classes
                @param local : true if this client is on the same computer as its server, false else
        */
        TA_Client(bool local);

        /* Variables */
        // Instances
        static TA_Client* singleton; // Single instance of TA_Client
        TA_ClientCommunicator* communicator; // The communication module

        // Informations
        QString name; // Name of the client. Default: hostname
        bool local; // True if the client is on the same computer as the server, false else
        bool detectionLaunched; // True if the detection was launched by the server, false else
        bool detectionAuthorized; // True if the detection event can be lauchned to the server
        QList<TA_Webcam*> webcams; // The list of connected webcams

        // Servers
        TA_InfoServer* server; // The connected server
        QList<TA_InfoServer*> servers; // The list of available servers
};

#endif // TA_CLIENT_H
