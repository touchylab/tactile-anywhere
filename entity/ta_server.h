/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 01 february 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_Server class representing the |
  | Server in Tactile Anywhere                                              |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_SERVER_H
#define TA_SERVER_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QColor>
#include <QList>
#include <QCryptographicHash>
#include <QHostInfo>
#include <QObject>
#include <QString>
#include <QTimer>

// Local Includes
#include "event/ta_systemeventmanager.h"
#include "communication/data/ta_dataevent.h"
#include "communication/data/ta_datasector.h"
#include "communication/data/ta_datawebcam.h"
#include "communication/data/ta_datastream.h"
#include "communication/data/ta_datamarker.h"
#include "gui/ta_logger.h"
#include "gui/ta_statusbar.h"
#include "gui/ta_mainwindow.h"

// Local classes
class TA_InfoClient;
class TA_ServerCommunicator;
class TA_Logger;

// Namespaces
using namespace std;

/*****************************************************************************/
/******                             DEFINES                             ******/
/*****************************************************************************/

// The sectors and area states
enum SectorState {TOUCHED, NOT_TOUCHED, IGNORED};

// The type of sector
enum SectorType {POINTS_SECTOR, UNIQUE_MARKER_SECTOR, MULTI_MARKER_SECTOR};

// The type of area
enum AreaType {NORMAL_AREA, GROUP_AREA};

// Contains informations about the state of a sector and to find it on the configurations
typedef struct{
    SectorState state; // The state of the sector for the detection
    TA_InfoClient* client; // The client owning the webcam concerned by this sector
    int webcamId; // The id of the webcam concerned by this sector
}StateSectorStruct;

// Contains informations about an area
typedef struct{
    AreaType type; // The type of area: Normal or Group
    SectorState state;  // The state of the aera for the detection
    QColor color; // The color of area
    int nbTouched; // Number of touched children
    int nbNotTouched; // Number of not touched children
    QMap<QString,StateSectorStruct*> sectors; // The sectors representing this aera
    QList<QString> up; //The group areas containing this area
    QList<QString> down; //The areas containing in this group area
    QString url; // The url to launch at touch event
    QString file; // The path to the file to open at touch event
    QString command; // The command to launch at touch event
}AreaStruct;

// Contains all the informations about the areas
typedef QMap<QString,AreaStruct*> AreaTree; // Key : aera's name | Value : aera's information

// Contains informations about a marker on a webcam
typedef struct {
    quint32 id; // The id of the marker
    QPoint position; // The position of this marker
} MarkerStruct;

// Contains informations about a sector
typedef struct{
    QString area; // the aera's name represented by this sector
    SectorType type; // The type of sector : points, unique marker or multiple markers
    QVector<QPoint> points; // The points of the sector use for points and unique marker sector
    QVector<MarkerStruct> markers; // The markers used for unique marker and multiple markers sector
}SectorStruct;

// Contains informations about a webcam
typedef struct {
    QString name; // The webcam's name
    QImage image; // The webcam's view
    QSize size; // The webcam's view
    bool active; // The webcam's state
    TA_InfoClient* client; // The webcam's client
    int id; // The webcam's id
    QMap<QString,SectorStruct*> sectors; // Sectors seeing by the webcam
    QMap<quint32,QPoint> markers; // The markers seeing by the webcam
    bool isSelected; // True if the webcam is selected, false else
    bool isDisplayed; // True if the webcam must be displayed, false else
}WebcamStruct;

// Contains informations about the client's configuration
typedef QMap<int,WebcamStruct*> ClientTree; // Key : webcam's ID | Value : webcam's information

// Contains all the informations about the server configuration
typedef QMap<TA_InfoClient*,ClientTree*> ConfigTree; // Key : client's connector | Value : client's webcam

#define TA_SERVER_MARKER_ASK_INTERVAL_MS 3000

/*****************************************************************************/
/******                              CLASS                              ******/
/*****************************************************************************/

/*
    TA_Server is a class representing a server in Tactile Anywhere. A server manages clients
    and configuration.
*/
class TA_Server : public QObject{
    Q_OBJECT
public :
    /****************************/
    /*      Initialization      */
    /****************************/

    /*
        Initializes an instance of this class as a singleton
            @param name : the server's name
            @param password : the server password
    */
    static void initialize(QString name = QHostInfo::localHostName(), QString password = QString(""));

    /*
        Cleans properly this class at the destruction
    */
    ~TA_Server();

    /*********************/
    /*      Getters      */
    /*********************/

    /*
        Gets the unique instance of this class. initialize() must be called before
            @return : the unique instance of this class or NULL if this class is not initialized
    */
    static TA_Server* getInstance();

    /*
        Gets the unique instance of the server logger
        @return : the unique instance of the server logger
    */
    static TA_Logger* getLogger();

    /*
        Gets the server's name.
            @return : the server's name
    */
    QString getServerName() const;

    /*
        Gets the hash of the server's password.
            @return : the hash of the server's password
    */
    QByteArray getServerPassword() const;

    /*
        Gets the server's password.
            @return : the server's password
    */
    QString getServerClearPassword() const;

    /*
        Gets the dimension ratio to apply during the detection
            @return : the dimension ratio to apply
    */
    float getDimensionRatio() const;

    /*
        Gets the server's communicator.
            @return : the server's communicator
    */
    TA_ServerCommunicator* getServerCommunicator() const;

    /*
        Gets the server's local client.
            @return : the server's local client.
    */
    TA_InfoClient* getServerLocalClient() const;


    /*********************/
    /*      Setters      */
    /*********************/

    /*
        Sets the server's name.
            @param name : the server's name to set
    */
    void setServerName(const QString& name);

    /*
        Sets the server's password.
            @param password : the server's password to set. Set as QString("") to remove password
    */
    void setServerPassword(const QString& password);

    /*
        Sets the dimension ratio to apply during the detection
            @param : the dimension ratio to apply
    */
    void setDimensionRatio(float dimensionRatio);

    /************************/
    /*      Operations      */
    /************************/

    /*
        Convert any type of sector into a list of points as a Points sector. If the
        sector is invalid, return an empty list.
            @param sectorId : the ID of the sector
            @param sector : the sector to convert
            @return : the points representing a sector. No points if the vector is invalid
    */
    QVector<QPoint> convertSectorPoint(QString sectorID, SectorStruct* sector);

    /******************************************/
    /*      Lists Management : Accessors      */
    /******************************************/

    /*
        Gets the area's states of this server
            @return : a pointer to the AreaTree of this server
    */
    AreaTree* getAreas();

    /*
        Gets the configuration of this server
            @return : a pointer to the ConfigTree of this server
    */
    ConfigTree* getConfig();

    /*
        Gets the informations about a client
            @param area : the area concerned by the sector
            @param sectorId : the ID of a sector owned by the desired client
            @return : the informations about the client or NULL if the area or the sector don't exist
    */
    TA_InfoClient* getClientInfo(QString area, QString sector);

    /*
        Gets the state of an area. Don't checks if it is logical
        with its children's states.
            @param area : the area's name
            @return : the state of the area
    */
    SectorState getAreaState(QString area) const;

    /*
        Sets the state of an area but don't considerate the children or the parents
            @param area : the area's name
            @param state: the state of the area
            @return: true if the state was changed, false if the sector don't exist
    */
    bool setAreaState(QString area,SectorState state);

    /*
        Gets the state of a sector
            @param area : the area's name
            @param sector : the sector's name
            @return : the state of the sector
    */
    SectorState getSectorState(QString area,QString sector) const;

    /*
        Sets the state of a sector but don't considerate the parents
            @param area : the area's name
            @param sector : the sector's name
            @param state : the state of the sector
            @return: true if the state was changed, false if the sector don't exist
    */
    bool setSectorState(QString area,QString sector,SectorState state);

    /*
        Gets the informations about a webcam
            @param client : the client's owner
            @param webcamId : the ID of the webcam
            @return : the informations about the webcam or NULL if it don't exist
    */
    WebcamStruct* getWebcamInfo(TA_InfoClient* client, int webcamId);

    /*
        Gets the informations about a webcam
            @param area : the area concerned by the sector
            @param sectorId : the ID of a sector owned by the desired webcam
            @return : the informations about the webcam or NULL if it don't exist
    */
    WebcamStruct* getWebcamInfo(QString area, QString sectorId);

    /*
        Gets the informations about an area
            @param area : the area's name
            @return : the informations about the area or NULL if it don't exist
    */
    AreaStruct* getAreaInfo(QString area);

    /*
        Gets the informations about a sector
            @param client : the client's owner
            @param webcamId : the ID of the webcam concerned by this sector
            @param sector : the sector's name
            @return : the informations about the sector or NULL if it don't exist
    */
    SectorStruct* getSectorInfo(TA_InfoClient* client, int webcamId, QString sector);

    /*
        Gets the informations about a sector
            @param area : the area concerned by the sector
            @param sector : the sector's name
            @return : the informations about the sector or NULL if it don't exist
    */
    SectorStruct* getSectorInfo(QString area, QString sector);

    /*
        Get the saved state
    */
    bool isSaved();

    /**********************************************/
    /*      Lists Management : Modifications      */
    /**********************************************/

    /*
        Clear the configuration
            @param all : clears the clients and the webcams too
    */
    void clearConfiguration(bool all = false);

    /*
        Adds a client to the configuration if it don't exist
            @param client: the client to add
            @return: true if the client was added, false if it already exist
    */
    bool addClient(TA_InfoClient* client);

    /*
        Removes a client from the configuration and delete it
            @param client: the client to remove
            @return: true if the client was removed, false if it don't exist
    */
    bool removeClient(TA_InfoClient* client);

    /*
        Ignores a client during the detection
            @param client: the client to ignore
            @return: true if the client was ignored, false if it don't exist
    */
    bool ignoreClient(TA_InfoClient* client);

    /*
        Adds a webcam to the configuration if it don't exist
            @param client: the owner of webcams
            @param webcamId: the webcam's id
            @param webcamName: the webcam's name
            @return: true if the webcam was added, false if the client don't exist
    */
    bool addWebcam(TA_InfoClient* client, int webcamId, QString webcamName);

    /*
        Inactivates a webcam
            @param client: the owner of webcams
            @param webcamId: the webcam's id
            @return: true if the webcam was inactivated, false if the client or the webcam don't exist
    */
    bool inactivateWebcam(TA_InfoClient* client, int webcamId);

    /*
        Display or not the webcam in the gui
            @param client: the owner of webcams
            @param webcamId: the webcam's id
            @param displayed: set as true to display the webcam, false else
            @return: true if the webcam was modified, false if the client or the webcam don't exist
    */
    bool changeDisplayModeWebcam(TA_InfoClient* client, int webcamId, bool displayed);

    /*
        Removes a webcam from the configuration
            @param client : the owner of the webcam
            @param webcamId: the id of the webcam
            @return: true if the webcam was removed, false if the client or the webcam don't exist
    */
    bool removeWebcam(TA_InfoClient* client, int webcamId);

    /*
        Clears the markers for a webcam
            @param client : the owner of the webcam
            @param webcamId: the id of the webcam
            @return: true if the markers was cleared, false if the client or the webcam don't exist
    */
    bool clearMarkers(TA_InfoClient* client, int webcamId);

    /*
        Adds an empty normal area to the configuration if it don't exist
            @param name: the area's name
            @param color: the area's color
            @return: true if the area was added, false if it already exist
    */
    bool addArea(QString name, QColor color = QColor(Qt::black));

    /*
        Adds a group area in the configuration if it don't exist.
            @param name: the group area's name
            @param down: the areas contained in this group area
            @param color: the area's color
            @return: true if the area was added, false if it already exist
    */
    bool addGroupArea(QString name, QList<QString>& down, QColor color = QColor(Qt::black));

    /*
        Removes an area from the configuration
            @param name: the area's name
            @return: true if the area was removed, false if it don't exist
    */
    bool removeArea(QString name);

    /*
        Renames an area
            @param last: the actual area's name
            @param update : the new area's name
            @return : true if the area is renamed, false if the area don't exist
    */
    bool renameArea(QString last, QString update);

    /*
        Creates a link from a parent to a child area
            @param parent: the parent's name
            @param child: the child's name
            @return : true if the link is set, false if the parent or the child don't exist
    */
    bool linkArea(QString parent, QString child);

    /*
        Removes a link from a parent to a child area
            @param parent: the parent's name
            @param child: the child's name
            @return : true if the link is removed, false if the parent or the child don't exist
    */
    bool unLinkArea(QString parent, QString child);

    /*
        Updates the sector's state with the new value and update parents states.
            @param area: the area's name
            @param sector: the sector's name
            @param state : the new state of the child
            @param changed : the changed areas
    */
    void updateSectorState(QString area, QString sector, SectorState state, QList<QString>& changed);

    /*
        Updates the area's state with the new child state and update parents if it's state changed
            @param area: the area's name
            @param childOldState : the old state of the child
            @param childNewState : the new state of the child
            @param changed : the changed areas
    */
    void updateAreaState(QString area, SectorState childOldState, SectorState childNewState, QList<QString>& changed);

    /*
        Adds a point sector to the configuration
            @param client: the owner of the webcam concerned by the sector
            @param webcam: the id of the webcam concerned by the sector
            @param area: the area's name
            @param color: the area's color for its creation if it don't exist
            @param points: the points representing the sector
            @return: true if the sector is added, false if the client or the webcam don't exist
    */
    bool addPointSector(TA_InfoClient* client, int webcamId, QString area, QColor color, QVector<QPoint>& points);

    /*
        Adds an unique marker sector to the configuration
            @param client: the owner of the webcam concerned by the sector
            @param webcam: the id of the webcam concerned by the sector
            @param area: the area's name
            @param color: the area's color for its creation if it don't exist
            @param marker : the marker used for the sector. If the position of the marker is not know, set marker.position at (-1,-1)
            @param points: the points representing the sector (the difference between the point position and the center of marker)
            @return: true if the sector is added, false if the client or the webcam don't exist
    */
    bool addUniqueMarkerSector(TA_InfoClient* client, int webcamId, QString area, QColor color, MarkerStruct marker, QVector<QPoint>& points);

    /*
        Adds a multiple marker sector to the configuration
            @param client: the owner of the webcam concerned by the sector
            @param webcam: the id of the webcam concerned by the sector
            @param area: the area's name
            @param color: the area's color for its creation if it don't exist
            @param sectorId: the if of the sector to add
            @param markers : the markers used for the sector. If the position of a marker is not know, set marker.position at (-1,-1)
            @return: true if the sector is added, false if the client or the webcam don't exist
    */
    bool addMultiMarkerSector(TA_InfoClient* client, int webcamId, QString area, QColor color, QVector<MarkerStruct> markers);

    /*
        Removes a sector from the configuration
            @param client: the owner of the webcam concerned by the sector
            @param webcam: the id of the webcam concerned by the sector
            @param sectorId: the if of the sector to remove
            @return: true if the sector is removed, false if it don't exist
    */
    bool removeSector(TA_InfoClient* client, int webcamId, QString sectorId);

    /*
        Removes a sector from the configuration
            @param area: the area represented by the sector
            @param sectorId: the if of the sector to remove
            @return: true if the sector is removed, false if it don't exist
    */
    bool removeSector(QString area, QString sectorId);

    /*
        Set the saved state
            @param saved : state to set
    */
    void isSaved(bool saved);

public slots:
    /*
        Launches the detection for this server and all its client
    */
    void launchDetection();

    /*
        Stops the detection for this server and all its client
    */
    void stopDetection();

    /*
        Adds a client to this server. Called when a new client comes from the communication module
            @param client : the new client to add. It must be delete after use
    */
    void newClient(TA_InfoClient* client);

    /*
        Updates the informations of an area with the informations of this event
            @param client : the client sending this event
            @param event : the occurred event
    */
    void newEvent(TA_InfoClient* client, TA_DataEvent event);

    /*
        Updates the informations about the client webcam
            @param client : the client sending this event
            @param webcams : the webcams
    */
    void newWebcam(TA_InfoClient* client, TA_DataWebcam webcams);

    /*
        Updates the frame of a webcam
            @param client : the client sending this event
            @param frame : the new frame packet
    */
    void newFrame(TA_InfoClient* client, TA_DataStream frame);

    /*
        Ask to clients to send webcams frames
            @param unique : set as true to ask an unique frame, false for a stream
    */
    void askFrame(bool unique);

    /*
        Ask to clients to stop to send webcams frames
    */
    void stopFrame();

    /*
        Get the position of a marker
            @param client : the client sending this event
            @param marker : the arrived marker
    */
    void newMarker(TA_InfoClient* client, TA_DataMarker marker);

    /*
        Ask to clients to send the markers positions
    */
    void askMarkers();

    /*
        Initializes this client for a connection with the server
            @param client : the connected client
    */
    void connectedClient(TA_InfoClient* client);

    /*
        Updates the client status
            @param client : the reconnected client
    */
    void reconnectedClient(TA_InfoClient* client);

    /*
        Called when the connection with the client is disconnected
            @param client : the disconnected client
    */
    void disconnectedClient(TA_InfoClient* client);

    /*
        Removes this client from the lists of the server
            @param client : the closed client
    */
    void closeClient(TA_InfoClient* client);
signals:

    /*
        Send when a client is add, modified or removed
    */
    void changedClient();

    /*
        Send when a modification occurs on the webcam list
            @param client: the client owning the webcams
            @param webcams : the changed webcams' id
    */
    void changedWebcams(TA_InfoClient* client, QVector<int> webcams);

    /*
        Send when a new frame comes from a client for a webcam
            @param client : the client sending the new frame
            @param webcamId: the id of the webcam
    */
    void changedFrame(TA_InfoClient* client, int webcamId);

    /*
        Send when new markers comes from a client for a webcam
            @param client : the client sending new markers
            @param webcamId: the id of the webcam
    */
    void changedMarkers(TA_InfoClient* client, int webcamId);

    /*
        Send when an area is changed (add, remove or edit)
    */
    void changedArea();
    /*
        Send when we need to change the selection and the items selected
            @param areas : the area's to show and select
    */
    void selectAndShowAreas(QList<QString> areas);

    /*
        Send when we need to see all the area list. All areas are visible and unselected
    */
    void showAllAreas();

    /*
        Send when we need to see all the area list. All areas are visible but the selection don't change
    */
    void showAllAreasNoChange();

    /*
        Send when we need to unselect all the area list
    */
    void unselectAllAreas();

    /*
        Send when we need to select an area
            @param area: area's name
    */
    void selectArea(QString area);

    /*
        Send when the area selection change
            @param areasSelected : the area's selected
    */
    void areaSelectionChanged(QList<QString> areasSelected);


private :
    /******************************/
    /*      Privates Methods      */
    /******************************/

    /*
        This constructor creates and initializes this class and instantiates is related classes
            @param name : the server's name
            @param password : the server password. Set as QString("") to don't set a password
    */
    TA_Server(QString name, QString password);

    /*
        This method is called after the initialisation
    */
    void afterInit();

    /***********************/
    /*      Variables      */
    /***********************/

    // Instances
    static TA_Server* singleton; // Single instance of TA_Server
    TA_ServerCommunicator* communicator; // The communication module
    static TA_Logger* logger; // Single instance of TA_Logger

    // Informations
    QString name; // Name of the server. Default: hostname
    QString cpasswd; // The password
    QByteArray passwd; // The hash of the password
    float dimensionRatio; // The dimension ratio to apply during the detection

    // Configurations
    ConfigTree configurations; // The configurations of clients
    AreaTree areas; // The areas informations
    TA_InfoClient* localClient; // The local client
    bool saved;   // Configuration is saved

    // Timers
    QTimer markerTimer; // The timer used to ask markers

};

#endif // TA_SERVER_H
