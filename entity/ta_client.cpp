/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 01 february 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_Client class representing the      |
  | Client in Tactile Anywhere                                              |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  | [-] 1.1 - Sébastien DESTABEAU -- Reemit frame and send event            |
  | [-] 1.2 - Simon BONY -- Complete for Tatile Anywhere V1.0               |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_client.h"
#include "communication/client/ta_clientcommunicator.h"
#include "communication/client/ta_infoserver.h"

TA_Client* TA_Client::singleton = NULL;

/*****************************************************************************/
/******                     CONSTRUCTORS/DESTRUCTOR                     ******/
/*****************************************************************************/

/*
    Initializes an instance of this class as a singleton
        @param local : true if this client is on the same computer as its server, false else
*/
void TA_Client::initialize(bool local){
    TA_Client::singleton = new TA_Client(local);
}

/*
    This constructor creates and initializes this class and instantiates is related classes
        @param local : true if this client is on the same computer as its server, false else
*/
TA_Client::TA_Client(bool local){
    // Variables
    setClientName(QHostInfo::localHostName());
    this->local = local;
    this->detectionAuthorized = false;
    this->detectionLaunched = false;
    communicator = NULL;

    // Launch communicator
    if(!local){
        server = NULL;
        communicator = new TA_ClientCommunicator();
        connect(communicator, SIGNAL(newServer(TA_InfoServer*)), this,SLOT(newServer(TA_InfoServer*)));
        communicator->launchSearch();
    } else {
        server = new TA_InfoServer();
        QObject::connect(server, SIGNAL(newSector(TA_InfoServer*,TA_DataSector)), this, SLOT(applySector(TA_InfoServer*,TA_DataSector)));
        QObject::connect(server, SIGNAL(newDetectionOrder(TA_InfoServer*,TA_DataDetection)), this, SLOT(manageDetection(TA_InfoServer*,TA_DataDetection)));
        QObject::connect(server, SIGNAL(newMarkerOrder(TA_InfoServer*,TA_DataMarker)), this, SLOT(manageMarker(TA_InfoServer*,TA_DataMarker)));
        QObject::connect(server, SIGNAL(newStreamAsk(TA_InfoServer*,TA_DataStream)), this, SLOT(manageStreamAsk(TA_InfoServer*,TA_DataStream)));
    }
}


/*
    Cleans properly this class at the destruction
*/
TA_Client::~TA_Client(){
    // Unlinks this instance
    TA_Client::singleton = NULL;

    // Closes the communication module
    if(communicator != NULL){
        delete communicator;
    }

    // Cleans the servers
    for(int i = 0; i < servers.size(); i++){
        delete servers[i];
    }

    if(server != NULL) {delete server;}

    // Cleans the webcams
    foreach(TA_Webcam* webcam, webcams){
        delete webcam;
    }
}

/*
    Initialize webcam list
*/

void TA_Client::initWebcams() {
    if(!webcams.isEmpty()){
        foreach(TA_Webcam* webcam, webcams){
            delete webcam;
        }
        webcams.clear();
        QThread::msleep(200);
    }
    QMap<int,VideoCapture*> webcamList = getFreeWebcams();
    for(QMap<int,VideoCapture*>::Iterator itWebcams = webcamList.begin(); itWebcams != webcamList.end();itWebcams++){
        TA_Webcam* webcam = new TA_Webcam(itWebcams.value(),itWebcams.key());

        bool connected = false;
        Mat frame;
        for(int i = 0; i < 3 ; i++){
            if(webcam->getFrame(frame)){
                connected = true;
                break;
            }
        }

        if(!connected){
            delete webcam;
            continue;
        }

        webcams.append(webcam);

        connect(webcam, SIGNAL(newFrame(TA_Webcam*,QImage)), this, SLOT(reemitFrame(TA_Webcam*,QImage)));
        connect(webcam, SIGNAL(newEvents(TA_Webcam*,VectorEvents)), this, SLOT(retransmitEvent(TA_Webcam*,VectorEvents)));
        connect(webcam,SIGNAL(disconnected(TA_Webcam*)),this,SLOT(changedStateWebcam(TA_Webcam*)));
        connect(webcam,SIGNAL(reconnected(TA_Webcam*)),this,SLOT(changedStateWebcam(TA_Webcam*)));
    }
}

/*****************************************************************************/
/******                             GETTERS                             ******/
/*****************************************************************************/

/*
    Gets the unique instance of this class
        @return : the unique instance of this class or NULL if this class is not initialized
*/
TA_Client* TA_Client::getInstance(){
    return TA_Client::singleton;
}

/*
    Gets the client's name.
        @return : the client's name
*/
QString TA_Client::getClientName() const{
    return name;
}

/*
    Gets the client's webcams.
        @return : the client's webcams
*/
QList<TA_Webcam *> TA_Client::getWebcams() const {
    return webcams;
}

/*
    Gets the client's communicator.
        @return : the client's communicator
*/
TA_ClientCommunicator* TA_Client::getClientCommunicator() const{
    return communicator;
}

/*
    Gets the client's main server.
        @return : the client's main server
*/
TA_InfoServer* TA_Client::getClientMainServer() const{
    return server;
}

/*
    Gets the client's servers list.
        @return : the client's servers list
*/
const QList<TA_InfoServer*>* TA_Client::getClientServersList() const{
    return &servers;
}

/*
    Finds and returns the free webcams of the systems
        @return: a map containing free webcams' id as key and capture as value
*/
QMap<int,VideoCapture *> TA_Client::getFreeWebcams(){
    QMap<int,VideoCapture *> webcams;
    VideoCapture *cap;
    int nbWebcams = QCamera::availableDevices().size();
    for(int n=0;n < nbWebcams;n++) {
        if ((cap = new VideoCapture(n)) == NULL)
            continue;
        webcams[n] = cap;
    }
    return webcams;
}

/*
    Get the logger to display logs messages.
*/
TA_Logger* TA_Client::getLogger(){
    return TA_Logger::getInstance();
}

/*****************************************************************************/
/******                             SETTERS                             ******/
/*****************************************************************************/

/*
    Sets the client's name.
        @param name : the client's name to set
*/
void TA_Client::setClientName(const QString& name){
    this->name = name;
}

void TA_Client::setSelectedWebcams(const QList<TA_Webcam*> selectedWebcams){
    QList<TA_Webcam*> toRemove;
    foreach(TA_Webcam* webcam, webcams){
        if(!selectedWebcams.contains(webcam)){
            toRemove << webcam;
            delete webcam;
        }
    }

    foreach(TA_Webcam* webcam, toRemove){
        webcams.removeOne(webcam);
    }
}


/*****************************************************************************/
/******                         OPERATIONS                              ******/
/*****************************************************************************/

/*
    Initializes a connection with the given server
        @param server : the new server to connect
*/
void TA_Client::connectTo(TA_InfoServer* server){
    if(this->server != NULL){
        closeServer(this->server);
    }
    servers.removeOne(server);
    this->server = server;
    QObject::connect(server, SIGNAL(connected(TA_InfoServer*)), this, SLOT(connectedServer(TA_InfoServer*)));
    QObject::connect(server, SIGNAL(disconnected(TA_InfoServer*)), this, SLOT(disconnectedServer(TA_InfoServer*)));
    QObject::connect(server, SIGNAL(closed(TA_InfoServer*)), this, SLOT(closeServer(TA_InfoServer*)));
    server->connection();
}

/*
    Sends the webcams to the server
*/
void TA_Client::sendWebcams(){
    if(server == NULL)
        return;

    QMap<int,QString> toSend;
    foreach (TA_Webcam* webcam, webcams) {
        toSend[webcam->getWebcamId()] = webcam->getName();
    }
    TA_DataWebcam packet;
    packet.setWebcams(&toSend);
    packet.setConnectedState(true);
    server->sendPacket(&packet);
}

/*
    Cleans all the sectors
*/
void TA_Client::cleanSectors(){
    foreach (TA_Webcam* webcam, webcams) {
        webcam->cleanMasks();
    }
}

/*
    Cleans the known servers
*/
void TA_Client::cleanServers(){
    foreach(TA_InfoServer* server, servers){
        delete server;
    }
    servers.clear();
    communicator->cleanServers();
}

/*****************************************************************************/
/******                           SLOTS                                 ******/
/*****************************************************************************/

/*
    Adds a server to the list of this client. Called when a new server comes from the communication module
        @param server : the new server to add. It must be delete after use
*/
void TA_Client::newServer(TA_InfoServer* server){
    servers.push_back(server);
}

/*
    Accept the server as the main server
        @param server : the new main server
*/
void TA_Client::connectedServer(TA_InfoServer* server){
    communicator->stopSearch();
    QObject::connect(server, SIGNAL(newSector(TA_InfoServer*,TA_DataSector)), this, SLOT(applySector(TA_InfoServer*,TA_DataSector)));
    QObject::connect(server, SIGNAL(newDetectionOrder(TA_InfoServer*,TA_DataDetection)), this, SLOT(manageDetection(TA_InfoServer*,TA_DataDetection)));
    QObject::connect(server, SIGNAL(newMarkerOrder(TA_InfoServer*,TA_DataMarker)), this, SLOT(manageMarker(TA_InfoServer*,TA_DataMarker)));
    QObject::connect(server, SIGNAL(newStreamAsk(TA_InfoServer*,TA_DataStream)), this, SLOT(manageStreamAsk(TA_InfoServer*,TA_DataStream)));
    sendWebcams();

    emit connectedToMainServer(server);
}

/*
    Called when the connection with the server is disconnected
        @param server : the main server
*/
void TA_Client::disconnectedServer(TA_InfoServer* server){
    Q_UNUSED(server)
    detectionAuthorized = false;
}

/*
    Removes this server from the list of the client
        @param server : the closed server
*/
void TA_Client::closeServer(TA_InfoServer* server){
    if(server != NULL && server == this->server){
        this->server = NULL;
        server->deleteLater();
        foreach (TA_Webcam* webcam, getWebcams()){
            webcam->stopDetection();
            webcam->stopSequencedFrame();
        }
        this->cleanServers();
        communicator->launchSearch();
        emit disconnectedFromMainServer(server);
    }
}

/*
    Applies a sector to the concerned webcam
        @param server : the main server
        @param sector : the sector to manage
*/
void TA_Client::applySector(TA_InfoServer* server, TA_DataSector sector){
    Q_UNUSED(server)
    foreach (TA_Webcam* webcam, webcams) {
        if(webcam->getWebcamId() == sector.getWebcamId()){
            webcam->setDimensionRatio(sector.getDimensionRatio());
            webcam->addMask(sector.getAreaName(),sector.getSectorName(),*(sector.getSector()));
            return;
        }
    }
}

/*
    Manage detection on all connected webcams
        @param server : the main server
        @param detection: detection packet to launch or stop detection
*/
void TA_Client::manageDetection(TA_InfoServer* server, TA_DataDetection detection){
    Q_UNUSED(server)

    // Start the detection
    if(detection.getDetectionState() == DETECTION_START){
        // Launch the detection for each webcam if it is a new order
        if(!detectionLaunched){
            foreach (TA_Webcam* webcam, getWebcams()){
                webcam->launchDetection();
            }
            if(!local)
                getLogger()->log(QTime::currentTime().toString("hh:mm:ss") + QString(" : ") + QString("La détection est lancée"));
        }
        detectionLaunched = true;
        detectionAuthorized = true;
    }
    // Stop the detection
    else {
        detectionAuthorized = false;
        if(detectionLaunched){
            foreach (TA_Webcam* webcam, getWebcams()){
                webcam->stopDetection();
            }
            if(!local)
                getLogger()->log(QTime::currentTime().toString("hh:mm:ss") + QString(" : ") + QString("La détection est arrêtée"));
        }
        detectionLaunched = false;
   }
}

/*
    Retransmit events to the server
        @param webcam : the webcam concerned by the event
        @param contacts : events to transmit
*/
void TA_Client::retransmitEvent(TA_Webcam* webcam, VectorEvents contacts){
    if(server==NULL || !detectionAuthorized)
        return;

    TA_DataEvent event;
    event.setWebcamId(webcam->getWebcamId());
    event.setEvents(&contacts);
    event.setEventDate(boost::posix_time::microsec_clock::local_time().time_of_day().total_milliseconds() + server->getServerDiffDate());
    server->sendPacket(&event);

    if(!local){
        foreach(EventContacts event, contacts){
            if(event.state)
                getLogger()->log(QTime::currentTime().toString("hh:mm:ss") + QString(" : ") + event.areaName + QString(" est touché selon la webcam ") + webcam->getName());
            else
                getLogger()->log(QTime::currentTime().toString("hh:mm:ss") + QString(" : ") + event.areaName + QString(" n'est pas touché selon la webcam ") + webcam->getName());
        }
    }
}

/*
    Retransmit to the server the changement of state of a webcam
        @param webcam : the webcam concerned by the changement
*/
void TA_Client::changedStateWebcam(TA_Webcam *webcam){
    if(server==NULL)
        return;

    TA_DataWebcam packet;
    QMap<int,QString> webcams;
    webcams[webcam->getWebcamId()] = webcam->getName();
    packet.setWebcams(&webcams);
    packet.setConnectedState(webcam->isConnected());
    server->sendPacket(&packet);

    if(!local){
        if(webcam->isConnected())
            getLogger()->log(QTime::currentTime().toString("hh:mm:ss") + QString(" : ") + QString("La webcam ") + webcam->getName() + QString(" est reconnectée"));
        else
            getLogger()->log(QTime::currentTime().toString("hh:mm:ss") + QString(" : ") + QString("La webcam ") + webcam->getName() + QString(" est déconnectée"));
    }
}

/*
    Retransmit markers to the server
        @param server : the main server
        @param marker: marker packet asked for markers
*/
void TA_Client::manageMarker(TA_InfoServer* server, TA_DataMarker marker){
    QMap<quint32,QPoint> positions;

    foreach(TA_Webcam* webcam, webcams){
        if(webcam->getWebcamId() == marker.getWebcamId()){
            positions = webcam->getMarkerPositions();
            marker.setPositions(positions);
            break;
        }
    }
    server->sendPacket(&marker);
}

/*
    Manages the stream asking from a server
        @param server : the main server
        @param stream : the ask to manage
*/
void TA_Client::manageStreamAsk(TA_InfoServer* server, TA_DataStream stream){
    Q_UNUSED(server)
    foreach(TA_Webcam* webcam, webcams){
        if(webcam->getWebcamId() == stream.getIdentifier()){
            if(!stream.toLaunch()){
                webcam->stopSequencedFrame();
            } else {
                if(!stream.uniqueFrame()){
                    webcam->launchSequencedFrame(1000);
                } else {
                    webcam->sendFrame();
                }
            }
            break;
        }
    }
}

/*
    Re emit frame from a picture from a webcam
        @param webcam : the webcam objet
        @param image : the picture from the webcam frame
*/
void TA_Client::reemitFrame(TA_Webcam* webcam, QImage img){
    if(server == NULL)
        return;
    TA_DataStream stream;
    stream.setIdentifier(webcam->getWebcamId());
    stream.setImage(img);
    server->sendPacket(&stream);
}

