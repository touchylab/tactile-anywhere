/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 01 february 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_Server class representing the      |
  | Server in Tactile Anywhere                                              |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_server.h"
#include "ta_xmlparser.h"
#include "communication/server/ta_infoclient.h"
#include "communication/server/ta_servercommunicator.h"

TA_Server* TA_Server::singleton = NULL;

/*****************************************************************************/
/******                     CONSTRUCTORS/DESTRUCTOR                     ******/
/*****************************************************************************/

/*
    Initializes an instance of this class as a singleton
        @param name : the server's name
        @param password : the server password
*/
void TA_Server::initialize(QString name, QString password){
    TA_Server::singleton = new TA_Server(name,password);
    TA_Server::singleton->afterInit();
}

/*
    This constructor creates and initializes this class and instantiates is related classes
        @param name : the server's name
        @param password : the server password. Set as QString("") to don't set a password
*/
TA_Server::TA_Server(QString name, QString password){
    // Initializes local values
    setServerName(name);
    setServerPassword(password);
    setDimensionRatio(0.5);

    // Initializes client and communications
    TA_Client::initialize(true);
    TA_Client::getInstance()->initWebcams();

    communicator = new TA_ServerCommunicator();
    QObject::connect(communicator, SIGNAL(newClient(TA_InfoClient*)), this, SLOT(newClient(TA_InfoClient*)));
    localClient = new TA_InfoClient();
    saved = true;

    // Connects communication events to their slot
    QObject::connect(localClient, SIGNAL(newEvent(TA_InfoClient*, TA_DataEvent)), this, SLOT(newEvent(TA_InfoClient*, TA_DataEvent)));
    QObject::connect(localClient, SIGNAL(newMarkerPosition(TA_InfoClient*,TA_DataMarker)), this, SLOT(newMarker(TA_InfoClient*,TA_DataMarker)));
    QObject::connect(localClient, SIGNAL(newWebcam(TA_InfoClient*,TA_DataWebcam)), this, SLOT(newWebcam(TA_InfoClient*,TA_DataWebcam)));
    QObject::connect(localClient, SIGNAL(closed(TA_InfoClient*)), this, SLOT(closeClient(TA_InfoClient*)));
    QObject::connect(localClient, SIGNAL(newStream(TA_InfoClient*,TA_DataStream)), this, SLOT(newFrame(TA_InfoClient*,TA_DataStream)));
 }

/*
    This method is called after the initialisation
*/
void TA_Server::afterInit(){
    // Manage local client
    addClient(localClient);
    TA_Client::getInstance()->sendWebcams();

    // Ask Markers
    askMarkers();
    connect(&markerTimer,SIGNAL(timeout()),this,SLOT(askMarkers()));
    markerTimer.start(TA_SERVER_MARKER_ASK_INTERVAL_MS);
}

/*
    Get the logger to display logs messages.
*/
TA_Logger* TA_Server::getLogger(){
    return TA_Logger::getInstance();
}

/*
    Cleans properly this class at the destruction
*/
TA_Server::~TA_Server(){
    // Unlinks this instance
    TA_Server::singleton = NULL;

    // Clears the configuration
    clearConfiguration(true);

    // Closes the communication module
    delete communicator;

    // Close the client
    delete TA_Client::getInstance();
}


/*****************************************************************************/
/******                             GETTERS                             ******/
/*****************************************************************************/

/*
    Gets the unique instance of this class
        @return : the unique instance of this class or NULL if this class is not initialized
*/
TA_Server* TA_Server::getInstance(){
    return TA_Server::singleton;
}

/*
    Gets the server's name.
        @return : the server's name
*/
QString TA_Server::getServerName() const{
    return name;
}

/*
    Gets the hash of the server's password.
        @return : the hash of the server's password
*/
QByteArray TA_Server::getServerPassword() const{
    return passwd;
}

/*
    Gets the server's password.
        @return : the server's password
*/
QString TA_Server::getServerClearPassword() const{
    return cpasswd;
}

/*
    Gets the dimension ratio to apply during the detection
        @return : the dimension ratio to apply
*/
float TA_Server::getDimensionRatio() const{
    return dimensionRatio;
}

/*
    Gets the server's communicator.
        @return : the server's communicator
*/
TA_ServerCommunicator* TA_Server::getServerCommunicator() const{
    return communicator;
}

/*
    Gets the server's local client.
        @return : the server's local client.
*/
TA_InfoClient* TA_Server::getServerLocalClient() const{
    return localClient;
}

bool TA_Server::isSaved(){
    return saved;
}

/*****************************************************************************/
/******                             SETTERS                             ******/
/*****************************************************************************/

/*
    Sets the server's name.
        @param name : the server's name to set
*/
void TA_Server::setServerName(const QString& name){
    this->name = name;
}

/*
    Sets the server's password.
        @param password : the server's password to set. Set as QString("") to remove password
*/
void TA_Server::setServerPassword(const QString& password){
    if(password == QString("")){
        this->passwd = QByteArray();
        this->cpasswd = QString();
    } else {
        this->passwd = QCryptographicHash::hash(password.toUtf8(),QCryptographicHash::Sha1);
        this->cpasswd = password;
    }
}

/*
    Sets the dimension ratio to apply during the detection
        @param : the dimension ratio to apply
*/
void TA_Server::setDimensionRatio(float dimensionRatio){
    this->dimensionRatio = dimensionRatio;
}

void TA_Server::isSaved(bool saved){
    this->saved = saved;
}

/*****************************************************************************/
/******                           OPERATIONS                            ******/
/*****************************************************************************/

/*
    Convert any type of sector into a list of points as a Points sector. If the
    sector is invalid, return an empty list.
        @param sectorId : the ID of the sector
        @param sector : the sector to convert
        @return : the points representing a sector. No points if the vector is invalid
*/
QVector<QPoint> TA_Server::convertSectorPoint(QString sectorID, SectorStruct* sector){
    QVector<QPoint> points;
    WebcamStruct* webcam = getWebcamInfo(sector->area,sectorID);

    // Point Sector
    if(sector->type == POINTS_SECTOR){
        if(sector->points.size() < 3){
            return points;
        }

        foreach(QPoint point, sector->points){
            if(point.x() < 0 || point.x() > webcam->size.width() || point.y() < 0 || point.y() > webcam->size.height()){
                return points;
            }
        }

        return sector->points.size() >= 3 ? sector->points : points;
    }

    // Unique Marker Sector
    if(sector->type == UNIQUE_MARKER_SECTOR){
        if(sector->points.size() < 3)
            return points;

        QPoint central;
        // Search for the position of the marker
        if(sector->markers[0].position != QPoint(-1,-1)){
            central = sector->markers[0].position;
        } else {
            if(!webcam->markers.contains(sector->markers[0].id)){
                return points;
            }
            central = webcam->markers[sector->markers[0].id];
        }

        foreach(QPoint point, sector->points){
            point = central + point;
            if(point.x() < 0 || point.x() > webcam->size.width() || point.y() < 0 || point.y() > webcam->size.height()){
                points.clear();
                return points;
            }
            points << point;
        }
    }

    // Multiple Marker Sector
    if(sector->type == MULTI_MARKER_SECTOR){
        if(sector->markers.size() < 3)
            return points;

        // Search for the position of markers
        foreach(MarkerStruct marker, sector->markers){
            if(marker.position != QPoint(-1,-1)){
                points << marker.position;
            } else {
                if(!webcam->markers.contains(marker.id)){
                    points.clear();
                    return points;
                }
                points << webcam->markers[marker.id];
            }
        }
    }
    return points;
}

/*****************************************************************************/
/******                 LISTS MANAGEMENT : ACCESSORS                    ******/
/*****************************************************************************/

/*
    Gets the area's states of this server
        @return : a pointer to the AreaTree of this server
*/
AreaTree* TA_Server::getAreas(){
    return &areas;
}

/*
    Gets the configuration of this server
        @return : a pointer to the ConfigTree of this server
*/
ConfigTree* TA_Server::getConfig(){
    return &configurations;
}

/*
    Gets the informations about a client
        @param area : the area concerned by the sector
        @param sectorId : the ID of a sector owned by the desired client
        @return : the informations about the client or NULL if the area or the sector don't exist
*/
TA_InfoClient* TA_Server::getClientInfo(QString area, QString sector){
    if(!areas.contains(area) || !areas[area]->sectors.contains(sector)){
        return NULL;
    }

    return getAreaInfo(area)->sectors[sector]->client;
}

/*
    Gets the state of an area. Don't checks if it is logical
    with its children's states.
        @param area : the area's name
        @return : the state of the area
*/
SectorState TA_Server::getAreaState(QString area) const{
    return areas[area]->state;
}

/*
    Sets the state of an area but don't considerate the children or the parents
        @param area : the area's name
        @param state: the state of the area
        @return: true if the state was changed, false if the sector don't exist
*/
bool TA_Server::setAreaState(QString area,SectorState state){
    if(!areas.contains(area)){
        return false;
    }
    areas[area]->state = state;
    return true;
}

/*
    Gets the state of a sector
        @param area : the area's name
        @param sector : the sector's name
        @return : the state of the sector
*/
SectorState TA_Server::getSectorState(QString area,QString sector) const{
    return areas[area]->sectors[sector]->state;
}

/*
    Sets the state of a sector but don't considerate the parents
        @param area : the area's name
        @param sector : the sector's name
        @param state : the state of the sector
        @return: true if the state was changed, false if the sector don't exist
*/
bool TA_Server::setSectorState(QString area,QString sector,SectorState state){
    if(!areas.contains(area) || !areas[area]->sectors.contains(sector)){
        return false;
    }

    areas[area]->sectors[sector]->state = state;
    return true;
}

/*
    Gets the informations about a webcam
        @param client : the client's owner
        @param webcamId : the ID of the webcam
        @return : the informations about the webcam or NULL if it don't exist
*/
WebcamStruct* TA_Server::getWebcamInfo(TA_InfoClient* client, int webcamId){
    if(!configurations.contains(client) || !configurations[client]->contains(webcamId)){
        return NULL;
    }

    return (*(configurations[client]))[webcamId];
}

/*
    Gets the informations about a webcam
        @param area : the area concerned by the sector
        @param sectorId : the ID of a sector owned by the desired webcam
        @return : the informations about the webcam or NULL if it don't exist
*/
WebcamStruct* TA_Server::getWebcamInfo(QString area, QString sectorId){
    if(!areas.contains(area) || !areas[area]->sectors.contains(sectorId)){
        return NULL;
    }

    return getWebcamInfo(areas[area]->sectors[sectorId]->client,areas[area]->sectors[sectorId]->webcamId);
}

/*
    Gets the informations about an area
        @param area : the area's name
        @return : the informations about the area or NULL if it don't exist
*/
AreaStruct* TA_Server::getAreaInfo(QString area){
    if(!areas.contains(area)){
        return NULL;
    }
    return areas[area];
}

/*
    Gets the informations about a sector
        @param client : the client's owner
        @param webcamId : the ID of the webcam concerned by this sector
        @param sector : the sector's name
        @return : the informations about the sector or NULL if it don't exist
*/
SectorStruct* TA_Server::getSectorInfo(TA_InfoClient* client, int webcamId, QString sector){
    if(!configurations.contains(client) || !configurations[client]->contains(webcamId) || !getWebcamInfo(client,webcamId)->sectors.contains(sector)){
        return NULL;
    }

    return getWebcamInfo(client,webcamId)->sectors[sector];
}

/*
    Gets the informations about a sector
        @param area : the area concerned by the sector
        @param sector : the sector's name
        @return : the informations about the sector or NULL if it don't exist
*/
SectorStruct* TA_Server::getSectorInfo(QString area, QString sector){
    if(!areas.contains(area) || !areas[area]->sectors.contains(sector)){
        return NULL;
    }

    return getWebcamInfo(area,sector)->sectors[sector];
}

/*****************************************************************************/
/******               LISTS MANAGEMENT : MODIFICATIONS                  ******/
/*****************************************************************************/

/*
    Clear the configuration
        @param all : clears the clients and the webcams too
*/
void TA_Server::clearConfiguration(bool all){
    // Configurations
    for(ConfigTree::Iterator itClient = configurations.begin() ; itClient != configurations.end() ; ++itClient){
        for(ClientTree::Iterator itWebcam = itClient.value()->begin() ; itWebcam != itClient.value()->end() ; ++itWebcam){
            for(QMap<QString,SectorStruct*>::Iterator itSector = itWebcam.value()->sectors.begin() ; itSector != itWebcam.value()->sectors.end() ; ++itSector){
                delete itSector.value();
            }
            itWebcam.value()->sectors.clear();
            if(all){
                delete itWebcam.value();
            }
        }
        if(all){
            delete itClient.value();
            delete itClient.key();
        }
    }

    // Areas
    for(AreaTree::Iterator itArea = areas.begin() ; itArea != areas.end(); itArea++){
        for(QMap<QString,StateSectorStruct*>::Iterator itSector = itArea.value()->sectors.begin() ; itSector != itArea.value()->sectors.end(); itSector++){
            delete itSector.value();
        }
        delete itArea.value();
    }
    areas.clear();
    saved = true;
}

/*
    Adds a client to the configuration if it don't exist
        @param client: the client to add
        @return: true if the client was added, false if it already exist
*/
bool TA_Server::addClient(TA_InfoClient* client){
    if(configurations.contains(client)){
        return false;
    }

    ClientTree* clientTree = new ClientTree;
    configurations[client] = clientTree;

    emit changedClient();
    return true;
}

/*
    Removes a client from the configuration and delete it
        @param client: the client to remove
        @return: true if the client was removed, false if it don't exist
*/
bool TA_Server::removeClient(TA_InfoClient* client){
    if(!configurations.contains(client)){
        return false;
    }
    // Delete each sector
    for(ClientTree::Iterator itWebcam = configurations[client]->begin() ; itWebcam != configurations[client]->end() ; ++itWebcam){
        for(QMap<QString,SectorStruct*>::Iterator itSector = itWebcam.value()->sectors.begin() ; itSector != itWebcam.value()->sectors.end() ; ++itSector){
            if(areas[itSector.value()->area]->sectors.contains(itSector.key())){
               delete areas[itSector.value()->area]->sectors[itSector.key()];
               areas[itSector.value()->area]->sectors.remove(itSector.key());
            }
            delete itSector.value();
            saved = false;
        }
        delete itWebcam.value();
    }

    // Remove the client
    delete configurations[client];
    configurations.remove(client);
    client->deleteLater();

    emit changedClient();
    emit changedArea();
    return true;
}

/*
    Ignores a client during the detection
        @param client: the client to ignore
        @return: true if the client was ignored, false if it don't exist
*/
bool TA_Server::ignoreClient(TA_InfoClient* client){
    if(!configurations.contains(client)){
        return false;
    }

    if(markerTimer.isActive()){
        return true;
    }

    QList<QString> changed;

    // Applies the modifications
    // For each webcam
    for(ClientTree::Iterator itWebcam = configurations[client]->begin() ; itWebcam != configurations[client]->end() ; ++itWebcam){
        // For each sector, set as ignore
        for(QMap<QString,SectorStruct*>::Iterator itSector = itWebcam.value()->sectors.begin() ; itSector != itWebcam.value()->sectors.end() ; ++itSector){
            updateSectorState(itSector.value()->area,itSector.key(),IGNORED, changed);
        }
    }

    // Send events for changed areas
    foreach(QString area, changed){
        if(getAreaState(area) == IGNORED)
            TA_SystemEventManager::sendIgnoreEvent(area);
        else if(getAreaState(area) == NOT_TOUCHED)
            TA_SystemEventManager::sendUntouchEvent(area);
        else
            TA_SystemEventManager::sendTouchEvent(area);
    }
    return true;
}

/*
    Adds a webcam to the configuration if it don't exist
        @param client: the owner of webcams
        @param webcamId: the webcam's id
        @param webcamName: the webcam's name
        @return: true if the webcam was added, false if the client don't exist
*/
bool TA_Server::addWebcam(TA_InfoClient* client, int webcamId, QString webcamName){
    if(!configurations.contains(client)){
        return false;
    }

    if(configurations[client]->contains(webcamId)){
        getWebcamInfo(client,webcamId)->name = webcamName;
        getWebcamInfo(client,webcamId)->active = true;
        return true;
    }

    (*(configurations[client]))[webcamId] = new WebcamStruct;
    (*(configurations[client]))[webcamId]->client = client;
    (*(configurations[client]))[webcamId]->id = webcamId;
    (*(configurations[client]))[webcamId]->size = QSize(0,0);
    (*(configurations[client]))[webcamId]->name = webcamName;
    (*(configurations[client]))[webcamId]->isSelected = false;
    (*(configurations[client]))[webcamId]->isDisplayed = true;
    (*(configurations[client]))[webcamId]->active = true;
    return true;
}

/*
    Inactivates a webcam
        @param client: the owner of webcams
        @param webcamId: the webcam's id
        @return: true if the webcam was inactivated, false if the client or the webcam don't exist
*/
bool TA_Server::inactivateWebcam(TA_InfoClient* client, int webcamId){
    if(!configurations.contains(client) || !configurations[client]->contains(webcamId)){
        return false;
    }

    getWebcamInfo(client,webcamId)->active = false;

    if(markerTimer.isActive()){
        return true;
    }

    QList<QString> changed;

    // Applies the modifications
    // For each sector
    for(QMap<QString,SectorStruct*>::Iterator itSector = getWebcamInfo(client,webcamId)->sectors.begin() ; itSector != getWebcamInfo(client,webcamId)->sectors.end() ; ++itSector){
        updateSectorState(itSector.value()->area,itSector.key(),IGNORED, changed);
    }

    // Send events for changed areas
    foreach(QString area, changed){
        if(getAreaState(area) == IGNORED)
            TA_SystemEventManager::sendIgnoreEvent(area);
        else if(getAreaState(area) == NOT_TOUCHED)
            TA_SystemEventManager::sendUntouchEvent(area);
        else
            TA_SystemEventManager::sendTouchEvent(area);
    }
    return true;
}

/*
    Display or not the webcam in the gui
        @param client: the owner of webcams
        @param webcamId: the webcam's id
        @param displayed: set as true to display the webcam, false else
        @return: true if the webcam was modified, false if the client or the webcam don't exist
*/
bool TA_Server::changeDisplayModeWebcam(TA_InfoClient* client, int webcamId, bool displayed){
    if(!configurations.contains(client) || !configurations[client]->contains(webcamId)){
        return false;
    }

    getWebcamInfo(client,webcamId)->isDisplayed = displayed;

    QVector<int> webcams;
    webcams << webcamId;
    emit changedWebcams(client,webcams);
    return true;
}

/*
    Removes a webcam from the configuration
        @param client : the owner of the webcam
        @param webcamId: the id of the webcam
        @return: true if the webcam was removed, false if the client or the webcam don't exist
*/
bool TA_Server::removeWebcam(TA_InfoClient* client, int webcamId){
    if(!configurations.contains(client) || !configurations[client]->contains(webcamId)){
        return false;
    }

    // Delete each sector
    for(QMap<QString,SectorStruct*>::Iterator itSector = getWebcamInfo(client,webcamId)->sectors.begin() ; itSector != getWebcamInfo(client,webcamId)->sectors.end() ; ++itSector){
        if(areas[itSector.value()->area]->sectors.contains(itSector.key())){
           delete areas[itSector.value()->area]->sectors[itSector.key()];
           areas[itSector.value()->area]->sectors.remove(itSector.key());
        }
        delete itSector.value();
        saved = false;
    }

    // Remove the webcam
    delete getWebcamInfo(client,webcamId);
    configurations[client]->remove(webcamId);
    emit changedArea();
    return true;
}

/*
    Clears the markers for a webcam
        @param client : the owner of the webcam
        @param webcamId: the id of the webcam
        @return: true if the markers was cleared, false if the client or the webcam don't exist
*/
bool TA_Server::clearMarkers(TA_InfoClient* client, int webcamId){
    if(!configurations.contains(client) || !configurations[client]->contains(webcamId)){
        return false;
    }

    getWebcamInfo(client,webcamId)->markers.clear();
    return true;
}

/*
    Adds an empty area to the configuration if it don't exist
        @param name: the area's name
        @param color: the area's color
        @return: true if the area was added, false if it already exist
*/
bool TA_Server::addArea(QString name, QColor color){
    if(areas.contains(name)){
        return false;
    }
    AreaStruct* info = new AreaStruct;
    areas[name] = info;
    areas[name]->type = NORMAL_AREA;
    areas[name]->state = NOT_TOUCHED;
    areas[name]->color = color;
    saved = false;
    emit changedArea();
    return true;
}

/*
    Adds a group area in the configuration if it don't exist.
        @param name: the group area's name
        @param down: the areas contained in this group area
        @param color: the area's color
        @return: true if the area was added, false if it already exist
*/
bool TA_Server::addGroupArea(QString name, QList<QString>& down, QColor color){
    if(areas.contains(name)){
        return false;
    }

    // Creates the area
    AreaStruct* info = new AreaStruct;
    areas[name] = info;
    areas[name]->type = GROUP_AREA;
    areas[name]->state = NOT_TOUCHED;
    areas[name]->down = down;
    areas[name]->color = color;

    // Adds the group area to the childrens
    foreach(QString area , down){
        areas[area]->up.push_back(name);
    }
    saved = false;
    emit changedArea();
    return true;
}

/*
    Removes an area from the configuration
        @param name: the area's name
        @return: true if the area was removed, false if it don't exist
*/
bool TA_Server::removeArea(QString name){
    if(!areas.contains(name)){
        return false;
    }

    // Remove each sector
    foreach(QString sectorId, areas[name]->sectors.keys()){
        removeSector(name,sectorId);
    }

    // Remove up group links
    foreach(QString group, areas[name]->up){
        areas[group]->down.removeOne(name);
    }

    // Remove down group links
    foreach(QString group, areas[name]->down){
        areas[group]->up.removeOne(name);
    }

    // Remove the area
    delete areas[name];
    areas.remove(name);
    saved = false;
    emit changedArea();
    return true;
}

/*
    Renames an area
        @param last: the actual area's name
        @param update : the new area's name
        @return : true if the area is renamed, false if the area don't exist
*/
bool TA_Server::renameArea(QString last, QString update){
    if(!areas.contains(last) || areas.contains(update)){
        return false;
    }

    // Updates sectors
    int i = 0;
    foreach(QString sectorId, areas[last]->sectors.keys()){
        getSectorInfo(last,sectorId)->area = update;
        getWebcamInfo(last,sectorId)->sectors[update + QString::number(i)] = getWebcamInfo(last,sectorId)->sectors[sectorId];
        getWebcamInfo(last,sectorId)->sectors.remove(sectorId);

        areas[last]->sectors[update + QString::number(i)] = areas[last]->sectors[sectorId];
        areas[last]->sectors.remove(sectorId);
        i++;
    }

    // Updates up links
    foreach(QString group, areas[last]->up){
        areas[group]->down.removeOne(last);
        areas[group]->down.push_back(update);
    }

    // Updates down links
    foreach(QString group, areas[last]->down){
        areas[group]->up.removeOne(last);
        areas[group]->up.push_back(update);
    }

    areas[update] = areas[last];
    areas.remove(last);
    saved = false;
    emit changedArea();
    return true;
}

/*
    Creates a link from a parent to a child area
        @param parent: the parent's name
        @param child: the child's name
        @return : true if the link is set, false if the parent or the child don't exist
*/
bool TA_Server::linkArea(QString parent, QString child){
    if(!areas.contains(parent) || !areas.contains(child)){
        return false;
    }
    getAreaInfo(parent)->down << child;
    getAreaInfo(child)->up << parent;
    saved = false;
    emit changedArea();
    return true;
}

/*
    Removes a link from a parent to a child area
        @param parent: the parent's name
        @param child: the child's name
        @return : true if the link is removed, false if the parent or the child don't exist
*/
bool TA_Server::unLinkArea(QString parent, QString child){
    if(!areas.contains(parent) || !areas.contains(child)){
        return false;
    }
    getAreaInfo(parent)->down.removeOne(child);
    getAreaInfo(child)->up.removeOne(parent);
    saved = false;
    emit changedArea();
    return true;
}

/*
    Updates the sector's state with the new value and update parents states.
        @param area: the area's name
        @param sector: the sector's name
        @param state : the new state of the child
        @param changed : the changed areas
*/
void TA_Server::updateSectorState(QString area, QString sector, SectorState state, QList<QString>& changed){
    SectorState old = getSectorState(area,sector);
    if(old == state){
        return;
    }
    setSectorState(area,sector,state);
    updateAreaState(area,old,state,changed);
}

/*
    Updates the area's state with the new child state and update parents if it's state changed
        @param area: the area's name
        @param childOldState : the old state of the child
        @param childNewState : the new state of the child
        @param changed : the changed areas
*/
void TA_Server::updateAreaState(QString area, SectorState childOldState, SectorState childNewState, QList<QString>& changed){
    AreaStruct* areaInfo = getAreaInfo(area);
    SectorState oldState = areaInfo->state;

    // Remove old state and add newState
    childOldState == TOUCHED ? areaInfo->nbTouched -= 1 : childOldState == NOT_TOUCHED ? areaInfo->nbNotTouched -= 1:areaInfo->nbNotTouched;
    childNewState == TOUCHED ? areaInfo->nbTouched += 1 : childNewState == NOT_TOUCHED ? areaInfo->nbNotTouched += 1:areaInfo->nbNotTouched;

    // Check for modification
    if(areaInfo->type == NORMAL_AREA){
        if(areaInfo->nbNotTouched > 0){ // Can be new 'NOT_TOUCHED'
            if(oldState != NOT_TOUCHED){
                areaInfo->state = NOT_TOUCHED;
            } else {
                return;
            }
        } else if(areaInfo->nbTouched > 0){ // Can be new 'TOUCHED'
            if(oldState != TOUCHED){
                areaInfo->state = TOUCHED;
            } else {
                return;
            }
        } else { //is 'IGNORED'
            areaInfo->state = IGNORED;
        }
    } else {
        if(areaInfo->nbTouched > 0){ // Can be new 'TOUCHED'
            if(oldState != TOUCHED){
                areaInfo->state = TOUCHED;
            } else {
                return;
            }
        } else if(areaInfo->nbNotTouched > 0){ // Can be new 'NOT_TOUCHED'
            if(oldState != NOT_TOUCHED){
                areaInfo->state = NOT_TOUCHED;
            } else {
                return;
            }
        } else { //is 'IGNORED'
            areaInfo->state = IGNORED;
        }
    }

    // If modification occurs
    // Add the area in the changed list
    changed << area;

    // Update the parents
    foreach(QString parent, areaInfo->up){
        updateAreaState(parent,oldState,areaInfo->state,changed);
    }
}

/*
    Adds a point sector to the configuration
        @param client: the owner of the webcam concerned by the sector
        @param webcam: the id of the webcam concerned by the sector
        @param area: the area's name
        @param color: the area's color for its creation if it don't exist
        @param points: the points representing the sector
        @return: true if the sector is added, false if the client or the webcam don't exist
*/
bool TA_Server::addPointSector(TA_InfoClient* client, int webcamId, QString area, QColor color, QVector<QPoint>& points){
    if(!configurations.contains(client) || !configurations[client]->contains(webcamId)){
        return false;
    }

    // Search for a sector id
    QString sectorId = area + QString::number(0);
    if(!areas.contains(area)){
        addArea(area, color);
    } else {
        for(int i=1;areas[area]->sectors.contains(sectorId);i++){
            sectorId = area + QString::number(i);
        }
    }

    // Add the sector in the config tree
    getWebcamInfo(client,webcamId)->sectors[sectorId] = new SectorStruct;
    getWebcamInfo(client,webcamId)->sectors[sectorId]->area = area;
    getWebcamInfo(client,webcamId)->sectors[sectorId]->type = POINTS_SECTOR;
    getWebcamInfo(client,webcamId)->sectors[sectorId]->points = points;

    // Add the sector in the area tree
    areas[area]->sectors[sectorId] = new StateSectorStruct;
    areas[area]->sectors[sectorId]->client = client;
    areas[area]->sectors[sectorId]->webcamId = webcamId;
    areas[area]->sectors[sectorId]->state = NOT_TOUCHED;
    saved = false;
    return true;
}

/*
    Adds an unique marker sector to the configuration
        @param client: the owner of the webcam concerned by the sector
        @param webcam: the id of the webcam concerned by the sector
        @param area: the area's name
        @param color: the area's color for its creation if it don't exist
        @param marker : the marker used for the sector. If the position of the marker is not know, set marker.position at (-1,-1)
        @param points: the points representing the sector (the difference between the point position and the center of marker)
        @return: true if the sector is added, false if the client or the webcam don't exist
*/
bool TA_Server::addUniqueMarkerSector(TA_InfoClient* client, int webcamId, QString area, QColor color, MarkerStruct marker, QVector<QPoint>& points){
    if(!configurations.contains(client) || !configurations[client]->contains(webcamId)){
        return false;
    }

    // Search for a sector id
    QString sectorId = area + QString::number(0);
    if(!areas.contains(area)){
        addArea(area, color);
    } else {
        for(int i=1;areas[area]->sectors.contains(sectorId);i++){
            sectorId = area + QString::number(i);
        }
    }

    // Add the sector in the config tree
    getWebcamInfo(client,webcamId)->sectors[sectorId] = new SectorStruct;
    getWebcamInfo(client,webcamId)->sectors[sectorId]->area = area;
    getWebcamInfo(client,webcamId)->sectors[sectorId]->type = UNIQUE_MARKER_SECTOR;
    getWebcamInfo(client,webcamId)->sectors[sectorId]->points = points;
    getWebcamInfo(client,webcamId)->sectors[sectorId]->markers << marker;

    // Add the sector in the area tree
    areas[area]->sectors[sectorId] = new StateSectorStruct;
    areas[area]->sectors[sectorId]->client = client;
    areas[area]->sectors[sectorId]->webcamId = webcamId;
    areas[area]->sectors[sectorId]->state = NOT_TOUCHED;
    saved = false;
    return true;
}

/*
    Adds a multiple marker sector to the configuration
        @param client: the owner of the webcam concerned by the sector
        @param webcam: the id of the webcam concerned by the sector
        @param area: the area's name
        @param color: the area's color for its creation if it don't exist
        @param sectorId: the if of the sector to add
        @param markers : the markers used for the sector. If the position of a marker is not know, set marker.position at (-1,-1)
        @return: true if the sector is added, false if the client or the webcam don't exist
*/
bool TA_Server::addMultiMarkerSector(TA_InfoClient* client, int webcamId, QString area, QColor color, QVector<MarkerStruct> markers){
    if(!configurations.contains(client) || !configurations[client]->contains(webcamId)){
        return false;
    }

    // Search for a sector id
    QString sectorId = area + QString::number(0);
    if(!areas.contains(area)){
        addArea(area, color);
    } else {
        for(int i=1;areas[area]->sectors.contains(sectorId);i++){
            sectorId = area + QString::number(i);
        }
    }

    // Add the sector in the config tree
    getWebcamInfo(client,webcamId)->sectors[sectorId] = new SectorStruct;
    getWebcamInfo(client,webcamId)->sectors[sectorId]->area = area;
    getWebcamInfo(client,webcamId)->sectors[sectorId]->type = MULTI_MARKER_SECTOR;
    getWebcamInfo(client,webcamId)->sectors[sectorId]->markers = markers;

    // Add the sector in the area tree
    areas[area]->sectors[sectorId] = new StateSectorStruct;
    areas[area]->sectors[sectorId]->client = client;
    areas[area]->sectors[sectorId]->webcamId = webcamId;
    areas[area]->sectors[sectorId]->state = NOT_TOUCHED;
    saved = false;
    return true;
}

/*
    Removes a sector from the configuration
        @param client: the owner of the webcam concerned by the sector
        @param webcam: the id of the webcam concerned by the sector
        @param sectorId: the if of the sector to remove
        @return: true if the sector is removed, false if it don't exist
*/
bool TA_Server::removeSector(TA_InfoClient* client, int webcamId, QString sectorId){
    if(!configurations.contains(client) || !configurations[client]->contains(webcamId) || !getWebcamInfo(client,webcamId)->sectors.contains(sectorId)){
        return false;
    }

    delete getAreaInfo(getSectorInfo(client,webcamId,sectorId)->area)->sectors[sectorId];
    getAreaInfo(getSectorInfo(client,webcamId,sectorId)->area)->sectors.remove(sectorId);
    delete getSectorInfo(client,webcamId,sectorId);
    getWebcamInfo(client,webcamId)->sectors.remove(sectorId);
    saved = false;
    return true;
}

/*
    Removes a sector from the configuration
        @param area: the area represented by the sector
        @param sectorId: the if of the sector to remove
        @return: true if the sector is removed, false if it don't exist
*/
bool TA_Server::removeSector(QString area, QString sectorId){
    if(!areas.contains(area) || !areas[area]->sectors.contains(sectorId)){
        return false;
    }
    delete getSectorInfo(area,sectorId);
    getWebcamInfo(area,sectorId)->sectors.remove(sectorId);
    delete areas[area]->sectors[sectorId];
    areas[area]->sectors.remove(sectorId);
    saved = false;
    return true;
}

/*****************************************************************************/
/******                           SLOTS                                 ******/
/*****************************************************************************/

/*
    Launches the detection for this server and all its client
*/
void TA_Server::launchDetection(){
    markerTimer.stop();
    QVector<QPoint> points;

    // Filled the map with all the area set as ignored
    QMap<QString,QString> ignored;
    QList<QString> notIgnored;
    for(AreaTree::Iterator itArea = areas.begin() ; itArea != areas.end() ; ++itArea){
        ignored[itArea.key()] = itArea.key();
        setAreaState(itArea.key(),IGNORED);
        itArea.value()->nbTouched = 0;
        itArea.value()->nbNotTouched = 0;
    }

    // Prepares packets
    TA_DataSector packetSector;
    packetSector.setDimensionRatio(this->dimensionRatio);
    TA_DataDetection packetDetection;
    packetDetection.setDimensionRatio(this->dimensionRatio);
    packetDetection.setDetectionState(DETECTION_START);

    // For each client
    for(ConfigTree::Iterator itClient = configurations.begin() ; itClient != configurations.end() ; ++itClient){
        // For each webcam
        for(ClientTree::Iterator itWebcam = itClient.value()->begin() ; itWebcam != itClient.value()->end() ; ++itWebcam){
            // For each sector, send configuration
            for(QMap<QString,SectorStruct*>::Iterator itSector = itWebcam.value()->sectors.begin() ; itSector != itWebcam.value()->sectors.end() ; ++itSector){
                // Set initially as ignored
                setSectorState(itSector.value()->area,itSector.key(),IGNORED);
                points = convertSectorPoint(itSector.key(), itSector.value());
                if(points.isEmpty()){
                    TA_Server::getLogger()->log(QTime::currentTime().toString("hh:mm:ss") + QString(" : ") + "La zone " + itSector.value()->area + " est mal définie pour la webcam " + itWebcam.value()->name);
                    continue;
                }

                // If the sector is correctly formed and the client is operational
                if((itClient.key()->getClientState() == CONNECTED || itClient.key()->getClientState() == LOCAL) && itWebcam.value()->active){
                    packetSector.setSector(&points);
                    packetSector.setSectorName(itSector.key());
                    packetSector.setAreaName(itSector.value()->area);
                    packetSector.setWebcamId(itWebcam.key());
                    itClient.key()->sendPacket(&packetSector);

                    updateSectorState(itSector.value()->area,itSector.key(),NOT_TOUCHED,notIgnored);
                } else {
                    TA_Server::getLogger()->log(QTime::currentTime().toString("hh:mm:ss") + QString(" : ") + "La zone " + itSector.value()->area + " est ne peut être detectée par la webcam " + itWebcam.value()->name);
                }
            }
        }
        // Order to the client to begin the detection
        if(itClient.key()->getClientState() == CONNECTED || itClient.key()->getClientState() == LOCAL){
            itClient.key()->setClientDetectionAuthorization(true);
            itClient.key()->sendPacket(&packetDetection);
        } else {
            itClient.key()->setClientDetectionAuthorization(false);
        }
    }

    // Send a launch detection event
    TA_SystemEventManager::sendLaunchDetectionEvent();

    // Remove not ignored area
    foreach(QString area,notIgnored){
        ignored.remove(area);
    }

    // Send ignored event for each ignored areas
    foreach(QString area, ignored){
        TA_SystemEventManager::sendIgnoreEvent(area);
    }
}

/*
    Stops the detection for this server and all its client
*/
void TA_Server::stopDetection(){
    TA_DataDetection packetDetection;
    packetDetection.setDetectionState(DETECTION_STOP);
    // Order to each client to stop the detection
    for(ConfigTree::Iterator itClient = configurations.begin() ; itClient != configurations.end() ; ++itClient){
        itClient.key()->setClientDetectionAuthorization(false);
        itClient.key()->sendPacket(&packetDetection);
    }

    // Send a stop detection event
    TA_SystemEventManager::sendStopDetectionEvent();

    // Ask for markers
    markerTimer.start(TA_SERVER_MARKER_ASK_INTERVAL_MS);
}

/*
    Adds a client to this server. Called when a new client comes from the communication module
        @param client : the new client to add. It must be delete after use
*/
void TA_Server::newClient(TA_InfoClient* client){
    if(configurations.contains(client)){
        return;
    }
    addClient(client);
    QObject::connect(client, SIGNAL(newEvent(TA_InfoClient*, TA_DataEvent)), this, SLOT(newEvent(TA_InfoClient*, TA_DataEvent)));
    QObject::connect(client, SIGNAL(newWebcam(TA_InfoClient*,TA_DataWebcam)), this, SLOT(newWebcam(TA_InfoClient*,TA_DataWebcam)));
    QObject::connect(client, SIGNAL(newMarkerPosition(TA_InfoClient*,TA_DataMarker)), this, SLOT(newMarker(TA_InfoClient*,TA_DataMarker)));
    QObject::connect(client, SIGNAL(connected(TA_InfoClient*)), this, SLOT(connectedClient(TA_InfoClient*)));
    QObject::connect(client, SIGNAL(reconnected(TA_InfoClient*)), this, SLOT(reconnectedClient(TA_InfoClient*)));
    QObject::connect(client, SIGNAL(disconnected(TA_InfoClient*)), this, SLOT(disconnectedClient(TA_InfoClient*)));
    QObject::connect(client, SIGNAL(closed(TA_InfoClient*)), this, SLOT(closeClient(TA_InfoClient*)));
    QObject::connect(client, SIGNAL(newStream(TA_InfoClient*,TA_DataStream)), this, SLOT(newFrame(TA_InfoClient*,TA_DataStream)));
}

/*
    Updates the informations of an area with the informations of this event
        @param client : the client sending this event
        @param event : the occurred event
*/
void TA_Server::newEvent(TA_InfoClient* client, TA_DataEvent event){
    if(!configurations.contains(client)){
        return;
    }

    QList<QString> changed;

    // Applies the modifications
    foreach(EventContacts contact, *(event.getEvents())){
        updateSectorState(contact.areaName,contact.sectorId,contact.state?TOUCHED:NOT_TOUCHED,changed);
    }

    // Send events
    foreach(QString area, changed){
        if(getAreaState(area) == TOUCHED)
            TA_SystemEventManager::sendTouchEvent(area);
        else
            TA_SystemEventManager::sendUntouchEvent(area);
    }
}

/*
    Updates the frame of a webcam
        @param client : the client sending this event
        @param frame : the new frame packet
*/
void TA_Server::newFrame(TA_InfoClient* client, TA_DataStream frame){
    if(!getWebcamInfo(client,frame.getIdentifier())){
        return;
    }

    getWebcamInfo(client,frame.getIdentifier())->image = frame.getImage();

    if(frame.getImage().size() != QSize(0,0)){
        getWebcamInfo(client,frame.getIdentifier())->size = frame.getImage().size();
    }

    emit changedFrame(client,frame.getIdentifier());
}

/*
    Ask to clients to send webcams frames
        @param unique : set as true to ask an unique frame, false for a stream
*/
void TA_Server::askFrame(bool unique){
    TA_DataStream packet;
    packet.setToLaunch(true);
    packet.setUniqueFrame(unique);

    // For each client
    for(ConfigTree::Iterator itClient = configurations.begin() ; itClient != configurations.end() ; ++itClient){
        // For each webcam
        for(ClientTree::Iterator itWebcam = itClient.value()->begin() ; itWebcam != itClient.value()->end() ; ++itWebcam){
            packet.setIdentifier(itWebcam.key());
            itClient.key()->sendPacket(&packet);
        }
    }
}

/*
    Ask to clients to stop to send webcams frames
*/
void TA_Server::stopFrame(){
    TA_DataStream packet;
    packet.setToLaunch(false);

    // For each client
    for(ConfigTree::Iterator itClient = configurations.begin() ; itClient != configurations.end() ; ++itClient){
        // For each webcam
        for(ClientTree::Iterator itWebcam = itClient.value()->begin() ; itWebcam != itClient.value()->end() ; ++itWebcam){
            packet.setIdentifier(itWebcam.key());
            itClient.key()->sendPacket(&packet);
        }
    }
}

/*
    Ask to clients to send the markers positions
*/
void TA_Server::askMarkers(){
    TA_DataMarker packet;

    // For each client
    for(ConfigTree::Iterator itClient = configurations.begin() ; itClient != configurations.end() ; ++itClient){
        // For each webcam
        for(ClientTree::Iterator itWebcam = itClient.value()->begin() ; itWebcam != itClient.value()->end() ; ++itWebcam){
            packet.setWebcamId(itWebcam.key());
            itClient.key()->sendPacket(&packet);
        }
    }
}

/*
    Get the position of a marker
        @param client : the client sending this event
        @param marker : the arrived marker
*/
void TA_Server::newMarker(TA_InfoClient* client, TA_DataMarker marker){
    if(!getWebcamInfo(client,marker.getWebcamId())){
        return;
    }
    for(QMap<quint32,QPoint>::Iterator it = marker.getPositions().begin(); it!= marker.getPositions().end() ; it++){
        getWebcamInfo(client,marker.getWebcamId())->markers[it.key()] = it.value();
    }

    if(!marker.getPositions().isEmpty()){
        emit changedMarkers(client,marker.getWebcamId());
    }
}

/*
    Updates the informations about the client webcam
        @param client : the client sending this event
        @param webcams : the webcams
*/
void TA_Server::newWebcam(TA_InfoClient* client, TA_DataWebcam webcams){
    if(!configurations.contains(client)){
        return;
    }
    QVector<int> changed;

    // Prepares a packet to ask frame
    TA_DataStream packet;
    packet.setToLaunch(true);
    packet.setUniqueFrame(true);

    // Adds webcams and asks for frame
    for(QMap<int,QString>::Iterator itWebcam = webcams.getWebcams()->begin(); itWebcam != webcams.getWebcams()->end();itWebcam++){
        if(webcams.getConnectedState()){
            if(getWebcamInfo(client,itWebcam.key()))
                TA_Server::getLogger()->log(QTime::currentTime().toString("hh:mm:ss") + QString(" : ") + "La webcam " + getWebcamInfo(client,itWebcam.key())->name + " du client " + client->getClientName() + QString(" est reconnectée"));

            addWebcam(client,itWebcam.key(),itWebcam.value());
            packet.setIdentifier(itWebcam.key());
            client->sendPacket(&packet);
        } else {
            inactivateWebcam(client,itWebcam.key());
            TA_Server::getLogger()->log(QTime::currentTime().toString("hh:mm:ss") + QString(" : ") + "La webcam " + getWebcamInfo(client,itWebcam.key())->name + " du client " + client->getClientName() + QString(" est déconnectée"));
        }
        changed << itWebcam.key();
    }
    emit changedWebcams(client, changed);
}

/*
    Initializes this client for a connection with the server
        @param client : the connected client
*/
void TA_Server::connectedClient(TA_InfoClient* client){
    TA_DataDetection packet;
    packet.setDimensionRatio(getDimensionRatio());
    packet.setDetectionState(client->isAuthorizedForDetection()?DETECTION_START:DETECTION_STOP);
    client->sendPacket(&packet);
    TA_MainWindow::getInstance()->getStatusBar()->showMessage(QString("Le client ") + client->getClientName() + QString(" est connecté"));
    emit changedClient();
}

/*
    Updates the client status
        @param client : the reconnected client
*/
void TA_Server::reconnectedClient(TA_InfoClient* client){
    TA_DataDetection packet;
    packet.setDimensionRatio(getDimensionRatio());
    packet.setDetectionState(client->isAuthorizedForDetection()?DETECTION_START:DETECTION_STOP);
    client->sendPacket(&packet);
    TA_Server::getLogger()->log(QTime::currentTime().toString("hh:mm:ss") + QString(" : ") + client->getClientName() + QString(" est reconnecté"));
    TA_MainWindow::getInstance()->getStatusBar()->showMessage(QString("Le client ") + client->getClientName() + QString(" est reconnecté"));
    emit changedClient();
}

/*
    Called when the connection with the client is disconnected
        @param client : the disconnected client
*/
void TA_Server::disconnectedClient(TA_InfoClient* client){
    ignoreClient(client);
    TA_Server::getLogger()->log(QTime::currentTime().toString("hh:mm:ss") + QString(" : ") + client->getClientName() + QString(" est déconnecté"));
    TA_MainWindow::getInstance()->getStatusBar()->showMessage(QString("Le client ") + client->getClientName() + QString(" est déconnecté"),"Warning");
    emit changedClient();
}

/*
    Removes this client from the lists of the server
        @param client : the closed client
*/
void TA_Server::closeClient(TA_InfoClient* client){
    TA_Server::getLogger()->log(QTime::currentTime().toString("hh:mm:ss") + QString(" : ") + client->getClientName() + QString(" a coupé la connexion"));
    TA_MainWindow::getInstance()->getStatusBar()->showMessage(QString("La connexion avec le client ") + client->getClientName() + QString(" est coupée"),"Error");
    ignoreClient(client);
    removeClient(client);
}
