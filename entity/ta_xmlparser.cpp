/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 07 february 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_XmlParser class managing the load  |
  | and save operation for the configuration                                |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Valentin COULON -- Initial version                            |
  | [-] 1.1 - Simon BONY -- Error management during load operation, markers |
  |                         areas and group areas now managed               |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_xmlparser.h"

QList<ClientConfiguration*> TA_XmlParser::clientsConfiguration; // The configuration of clients
QList<GroupAreaConfiguration*> TA_XmlParser::groupAreasConfiguration; // The configuration of group areas
QList<AreaConfiguration*> TA_XmlParser::areasConfiguration; // The configuration of empty areas
QString TA_XmlParser::error = QString(""); // The error message

/*****************************************************************************/
/******                             PUBLICS                             ******/
/*****************************************************************************/

/*
    Save the current configuration
        @param path : the path of the file to save
        @return : the enum ConfSaveReturn. Must return CONFSAVE_SUCCESS or an error is occured
*/
ConfSaveReturn TA_XmlParser::saveConfiguration(QString path) {
    // Open the file
    QFile *output = getConfigurationFile(path,false);

    if (output == NULL) {
        return CONFSAVE_CANTCREATEDIR;
    }

    if (!output->open(QIODevice::WriteOnly)) {
        delete output;
        return CONFSAVE_CANTCREATEFILE;
    }

    // Initialize the configuration
    initConfiguration();

    QXmlStreamWriter stream(output);
    stream.setAutoFormatting(true);

    stream.writeStartDocument();
    stream.writeStartElement(TA_XMLPARSER_ROOT); // Begin : Configuration

    // Clients configuration
    stream.writeStartElement(TA_XMLPARSER_CLIENTS_ROOT); // Begin : Clients
    foreach (ClientConfiguration* client, clientsConfiguration) {
        stream.writeStartElement(TA_XMLPARSER_CLIENT_ENTITY); // Begin : Client
        stream.writeAttribute(TA_XMLPARSER_CLIENT_ADDRESS, client->name);
        foreach (WebcamConfiguration* webcam, client->webcams) {
            stream.writeStartElement(TA_XMLPARSER_WEBCAM_ROOT); // Begin : Webcam
            stream.writeAttribute(TA_XMLPARSER_ID, QString::number(webcam->id));
            foreach (SectorConfiguration* sector, webcam->sectors) {
                stream.writeStartElement(sector->type == POINTS_SECTOR? TA_XMLPARSER_POINTS_AREA : sector->type == UNIQUE_MARKER_SECTOR? TA_XMLPARSER_UNIQUE_MARKER_AREA : TA_XMLPARSER_MULTI_MARKER_AREA); // Begin : Area
                stream.writeAttribute(TA_XMLPARSER_NAME, sector->area);
                stream.writeAttribute(TA_XMLPARSER_COLOR, sector->color.name());

                if(sector->type == UNIQUE_MARKER_SECTOR){
                    stream.writeEmptyElement(TA_XMLPARSER_MARKER);
                    stream.writeAttribute(TA_XMLPARSER_ID, QString::number(sector->markers[0].id));
                }
                if(sector->type == POINTS_SECTOR || sector->type == UNIQUE_MARKER_SECTOR){
                    stream.writeStartElement(TA_XMLPARSER_POINTS); // Begin : Points
                    foreach (QPoint point, sector->polygon) {
                        stream.writeEmptyElement(TA_XMLPARSER_POINT);
                        stream.writeAttribute(TA_XMLPARSER_X, QString::number(point.x()));
                        stream.writeAttribute(TA_XMLPARSER_Y, QString::number(point.y()));
                    }
                    stream.writeEndElement(); // End : Points
                } else {
                    stream.writeStartElement(TA_XMLPARSER_MARKERS); // Begin : Markers
                    foreach (MarkerStruct marker, sector->markers) {
                        stream.writeEmptyElement(TA_XMLPARSER_MARKER);
                        stream.writeAttribute(TA_XMLPARSER_ID, QString::number(marker.id));
                    }
                    stream.writeEndElement(); // End : Markers
                }
                stream.writeEndElement(); // End : Area
            }
            stream.writeEndElement(); // End : Webcam
        }
        stream.writeEndElement(); // End : Client
    }
    stream.writeEndElement(); // End : Clients

    // Areas
    stream.writeStartElement(TA_XMLPARSER_AREAS); // Begin : Areas
    foreach(AreaConfiguration* areadef , areasConfiguration){
        stream.writeStartElement(TA_XMLPARSER_AREA); // Begin : Areas
        stream.writeAttribute(TA_XMLPARSER_NAME, areadef->name);
        stream.writeAttribute(TA_XMLPARSER_COLOR, areadef->color.name());
        if(!areadef->file.isEmpty()){
            stream.writeTextElement(TA_XMLPARSER_FILE,areadef->file);
        }
        if(!areadef->url.isEmpty()){
            stream.writeTextElement(TA_XMLPARSER_URL,areadef->url);
        }
        if(!areadef->command.isEmpty()){
            stream.writeTextElement(TA_XMLPARSER_COMMAND,areadef->command);
        }
        stream.writeEndElement(); // End : Area
    }
    stream.writeEndElement(); // End : Areas

    // Groups areas
    stream.writeStartElement(TA_XMLPARSER_GROUP_AREAS); // Begin : Group_Areas
    foreach(GroupAreaConfiguration* group , groupAreasConfiguration){
        stream.writeStartElement(TA_XMLPARSER_GROUP); // Begin : Group
        stream.writeAttribute(TA_XMLPARSER_NAME,group->name);
        stream.writeAttribute(TA_XMLPARSER_COLOR, group->color.name());
        foreach(QString child,group->areas){
            stream.writeEmptyElement(TA_XMLPARSER_CHILD);
            stream.writeAttribute(TA_XMLPARSER_NAME, child);
        }
        if(!group->file.isEmpty()){
            stream.writeTextElement(TA_XMLPARSER_FILE,group->file);
        }
        if(!group->url.isEmpty()){
            stream.writeTextElement(TA_XMLPARSER_URL,group->url);
        }
        if(!group->command.isEmpty()){
            stream.writeTextElement(TA_XMLPARSER_COMMAND,group->command);
        }
        stream.writeEndElement(); // End : Group
    }
    stream.writeEndElement(); // End : Group_Areas

    stream.writeEndElement(); // End : Configuration
    stream.writeEndDocument();
    output->close();
    delete output;
    clearXMLConfiguration();
    return CONFSAVE_SUCCESS;
}

/*
    Load a configuration
        @param path : the path of the file to load
        @return : the enum ConfReturn. Must return CONFLOAD_SUCCESS or an error is occured
*/
ConfLoadReturn TA_XmlParser::loadConfiguration(QString path) {
    // Opens the file
    QFile* input = getConfigurationFile(path,true);

    if (input == NULL) {
        return CONFLOAD_CANTCREATEDIR;
    }

    if (!input->open(QIODevice::ReadOnly)) {
        delete input;
        return CONFLOAD_CANTOPENFILE;
    }

    // Initializes validator
    QXmlSchema schema;
    schema.load(QUrl(TA_XMLPARSER_XSD_FILE));
    QXmlSchemaValidator validator(schema);

    // Validate
    if(!validator.validate(input)){
        input->close();
        delete input;
        return CONFLOAD_BADFORMAT;
    }

    input->reset();

    // Clears the configurations
    clearXMLConfiguration();

    // Declares variables
    QMap<QString, QString> knows;
    ClientConfiguration* client;
    WebcamConfiguration* webcam;
    SectorConfiguration* sector;
    GroupAreaConfiguration* group;
    AreaConfiguration* area;
    bool isGroup = false;

    // Reads
    QXmlStreamReader reader;
    reader.setDevice(input);

    for(reader.readNext();!reader.atEnd();reader.readNext()){
        if (reader.isStartElement()){
            if (reader.name() == TA_XMLPARSER_CLIENT_ENTITY){
                client = new ClientConfiguration;
                client->name = reader.attributes().value(TA_XMLPARSER_CLIENT_ADDRESS).toString();
                continue;
            }

            if (reader.name() == TA_XMLPARSER_WEBCAM_ROOT){
                webcam = new WebcamConfiguration;
                webcam->id = reader.attributes().value(TA_XMLPARSER_ID).toString().toInt();
                continue;
            }

            if (reader.name() == TA_XMLPARSER_POINTS_AREA || reader.name() == TA_XMLPARSER_UNIQUE_MARKER_AREA || reader.name() == TA_XMLPARSER_MULTI_MARKER_AREA){
                sector = new SectorConfiguration;
                sector->type = reader.name() == TA_XMLPARSER_POINTS_AREA? POINTS_SECTOR : reader.name() == TA_XMLPARSER_UNIQUE_MARKER_AREA? UNIQUE_MARKER_SECTOR : MULTI_MARKER_SECTOR;
                sector->area = reader.attributes().value(TA_XMLPARSER_NAME).toString();
                sector->color = QColor(reader.attributes().value(TA_XMLPARSER_COLOR).toString());

                if(sector->area.size() >= 200){
                    error = QString("Le nom de zone \"") + sector->area + QString("\" est trop long (200 caractères maximum)");
                    delete sector;
                    delete webcam;
                    delete client;
                    input->close();
                    delete input;
                    clearXMLConfiguration();
                    return CONFLOAD_BADFORMAT;
                }

                knows[sector->area] = sector->area;
                continue;
            }

            if(reader.name() == TA_XMLPARSER_MARKER){
                MarkerStruct marker;
                marker.id = reader.attributes().value(TA_XMLPARSER_ID).toString().toInt();

                if(marker.id > 1023){
                    error = QString("Le marker d'identifiant' \"") + QString::number(marker.id) + QString("\" n'existe pas (Identifiants de 0 à 1023)");
                    delete sector;
                    delete webcam;
                    delete client;
                    input->close();
                    delete input;
                    clearXMLConfiguration();
                    return CONFLOAD_BADFORMAT;
                }

                marker.position = QPoint(-1,-1);
                sector->markers << marker;
            }

            if(reader.name() == TA_XMLPARSER_POINT){
                sector->polygon << QPoint(reader.attributes().value("x").toString().toInt(),reader.attributes().value("y").toString().toInt());
            }

            if(reader.name() == TA_XMLPARSER_AREA){
                area = new AreaConfiguration;
                area->name = reader.attributes().value(TA_XMLPARSER_NAME).toString();
                area->color = QColor(reader.attributes().value(TA_XMLPARSER_COLOR).toString());

                if(area->name.size() >= 200){
                    error = QString("Le nom de zone \"") + area->name + QString("\" est trop long (200 caractères maximum)");
                    delete area;
                    input->close();
                    delete input;
                    clearXMLConfiguration();
                    return CONFLOAD_BADFORMAT;
                }
                knows[area->name] = area->name;
            }

            if(reader.name() == TA_XMLPARSER_GROUP){
                isGroup = true;
                group = new GroupAreaConfiguration;
                group->name = reader.attributes().value(TA_XMLPARSER_NAME).toString();
                group->color = QColor(reader.attributes().value(TA_XMLPARSER_COLOR).toString());

                if(group->name.size() >= 200){
                    error = QString("Le nom de zone \"") + group->name + QString("\" est trop long (200 caractères maximum)");
                    delete group;
                    input->close();
                    delete input;
                    clearXMLConfiguration();
                    return CONFLOAD_BADFORMAT;
                } else if(knows.contains(group->name)){
                    error = QString("Le nom de groupe de zone \"") + group->name + QString("\" est déjà  utilisé");
                    delete group;
                    input->close();
                    delete input;
                    clearXMLConfiguration();
                    return CONFLOAD_BADFORMAT;
                }
                knows[group->name] = group->name;
            }

            if(reader.name() == TA_XMLPARSER_CHILD){
                group->areas << reader.attributes().value(TA_XMLPARSER_NAME).toString();

                if(!knows.contains(group->name)){
                    error = QString("La zone \"") + reader.attributes().value("name").toString() + QString("\" n'existe pas pour le groupe de zone " + group->name);
                    delete group;
                    input->close();
                    delete input;
                    clearXMLConfiguration();
                    return CONFLOAD_BADFORMAT;
                }
            }

            if(reader.name() == TA_XMLPARSER_FILE) {
                if(isGroup){
                    group->file = reader.readElementText();
                } else {
                    area->file = reader.readElementText();
                }
            }

            if(reader.name() == TA_XMLPARSER_URL){
                if(isGroup){
                    group->url = reader.readElementText();
                } else {
                    area->url = reader.readElementText();
                }
            }

            if(reader.name() == TA_XMLPARSER_COMMAND){
                if(isGroup){
                    group->command = reader.readElementText();
                } else {
                    area->command = reader.readElementText();
                }
            }

        } else {
            if (reader.name() == TA_XMLPARSER_CLIENT_ENTITY){
                clientsConfiguration << client;
                continue;
            }

            if (reader.name() == TA_XMLPARSER_WEBCAM_ROOT){
                client->webcams << webcam;
                continue;
            }

            if (reader.name() == TA_XMLPARSER_POINTS_AREA || reader.name() == TA_XMLPARSER_UNIQUE_MARKER_AREA || reader.name() == TA_XMLPARSER_MULTI_MARKER_AREA){
                webcam->sectors << sector;
                continue;
            }

            if(reader.name() == TA_XMLPARSER_AREA){
                areasConfiguration << area;
            }

            if(reader.name() == TA_XMLPARSER_GROUP){
                isGroup = false;
                groupAreasConfiguration << group;
                continue;
            }
        }
    }

    input->close();
    delete input;
    applyConfiguration();
    clearXMLConfiguration();

    return CONFLOAD_SUCCESS;
}

/*
    Gets error message if a CONFLOAD_BADFORMAT error occurs
        @return : a QString containing the error message
*/
QString TA_XmlParser::getErrorMessage(){
    return error;
}

/*****************************************************************************/
/******                            PRIVATES                             ******/
/*****************************************************************************/

/*
    Get the file from the given path
        @param path : the path of the file
        @param load : set as true if the file is open for load, false else
        @return : the link to the file or NULL if the directory can't be created
*/
QFile* TA_XmlParser::getConfigurationFile(QString path, bool load){
    if(!load){
        // Generate the path
        int lastIndexS = path.lastIndexOf("/");
        int lastIndexA = path.lastIndexOf("\\");

        QDir dir(path.left(lastIndexA > lastIndexS? lastIndexA+1:lastIndexS+1));
        if (lastIndexA == lastIndexS || !dir.mkpath(".")){
            return NULL;
        }
    }

    // Open the configuration file
    return new QFile(path);
}

/*
    Initializes the variables of the configuration with current server state
*/
void TA_XmlParser::initConfiguration() {
    clearXMLConfiguration();

    // Creates variables
    QMap<QString, QString> knows;
    QMap<QString, QString> areaList;
    ClientConfiguration* client;
    WebcamConfiguration* webcam;
    SectorConfiguration* sector;
    GroupAreaConfiguration* group;
    AreaConfiguration* areadef;

    // Initializes them
    ConfigTree* config = TA_Server::getInstance()->getConfig();

    foreach(QString area, (*(TA_Server::getInstance()->getAreas())).keys()){
        areaList[area] = area;
    }

    // Initializes the client configuration
    // For each client
    for(ConfigTree::Iterator itClient = config->begin() ; itClient != config->end() ; ++itClient){
        // Initializes the client
        client = new ClientConfiguration;
        if(itClient.key()->getClientState() == LOCAL)
            client->name = TA_XMLPARSER_LOCAL_NAME;
        else
            client->name = itClient.key()->getClientAddress().toString();

        // For each webcam
        for(ClientTree::Iterator itWebcam = itClient.value()->begin() ; itWebcam != itClient.value()->end() ; ++itWebcam){
            webcam = new WebcamConfiguration;
            webcam->id = itWebcam.key();

            // For each sector
            for(QMap<QString,SectorStruct*>::Iterator itSector = itWebcam.value()->sectors.begin() ; itSector != itWebcam.value()->sectors.end() ; ++itSector){
                sector = new SectorConfiguration;
                sector->area = itSector.value()->area;
                sector->color = TA_Server::getInstance()->getAreaInfo(itSector.value()->area)->color;
                sector->type = itSector.value()->type;
                sector->polygon = itSector.value()->points;
                sector->markers = itSector.value()->markers;
                webcam->sectors << sector;

                knows[sector->area] = sector->area;
            }
            client->webcams << webcam;
        }
        clientsConfiguration << client;
    }

    // Initializes the areas configuration
    while(!areaList.isEmpty()){
        // Write firstly the groups areas with already defined children
        foreach(QString area , areaList.keys()){
            bool defined = true;
            foreach(QString child, TA_Server::getInstance()->getAreaInfo(area)->down){
                if(!knows.contains(child)){
                    defined = false;
                    break;
                }
            }
            if(defined){
                areaList.remove(area);
                knows[area] = area;

                // Group area
                if(TA_Server::getInstance()->getAreaInfo(area)->type == GROUP_AREA){
                    group = new GroupAreaConfiguration;
                    group->name = area;
                    group->color = TA_Server::getInstance()->getAreaInfo(area)->color;
                    group->areas = TA_Server::getInstance()->getAreaInfo(area)->down;
                    group->file = TA_Server::getInstance()->getAreaInfo(area)->file;
                    group->url = TA_Server::getInstance()->getAreaInfo(area)->url;
                    group->command = TA_Server::getInstance()->getAreaInfo(area)->command;
                    groupAreasConfiguration << group;
                } else { // Simple area
                   areadef = new AreaConfiguration;
                   areadef->name = area;
                   areadef->color = TA_Server::getInstance()->getAreaInfo(area)->color;
                   areadef->file = TA_Server::getInstance()->getAreaInfo(area)->file;
                   areadef->url = TA_Server::getInstance()->getAreaInfo(area)->url;
                   areadef->command = TA_Server::getInstance()->getAreaInfo(area)->command;
                   areasConfiguration << areadef;
                }
            }
        }
    }
}

/*
    Initializes the server with the load configuration
*/
void TA_XmlParser::applyConfiguration() {
    TA_Server::getInstance()->clearConfiguration();

    // Load clients
    foreach(ClientConfiguration* client, clientsConfiguration){
        // Checks if the client exist
        TA_InfoClient* linkClient = NULL;
        if(client->name == TA_XMLPARSER_LOCAL_NAME){
            linkClient = TA_Server::getInstance()->getServerLocalClient();
        } else {
            foreach(TA_InfoClient* infoClient, TA_Server::getInstance()->getConfig()->keys()){
                if(client->name == infoClient->getClientAddress().toString()){
                    linkClient = infoClient;
                    break;
                }
            }
        }

        // If it exist, load webcams
        foreach(WebcamConfiguration* webcam, client->webcams){
            // Checks if the webcam exist
            bool existWebcam = true;
            if(linkClient == NULL || !(*(TA_Server::getInstance()->getConfig()))[linkClient]->contains(webcam->id)){
                existWebcam = false;
            }

            // Load each sector
            foreach(SectorConfiguration* sector , webcam->sectors){
                if(!existWebcam){
                    TA_Server::getInstance()->addArea(sector->area,sector->color);
                    continue;
                }
                if(sector->type == POINTS_SECTOR){
                    TA_Server::getInstance()->addPointSector(linkClient,webcam->id,sector->area,sector->color,sector->polygon);
                } else if(sector->type == UNIQUE_MARKER_SECTOR){
                    TA_Server::getInstance()->addUniqueMarkerSector(linkClient,webcam->id,sector->area,sector->color,sector->markers[0],sector->polygon);
                } else {
                    TA_Server::getInstance()->addMultiMarkerSector(linkClient,webcam->id,sector->area,sector->color,sector->markers);
                }
            }
        }
    }

    // Load Simple Areas
    foreach(AreaConfiguration* areadef, areasConfiguration){
        TA_Server::getInstance()->addArea(areadef->name,areadef->color);
        TA_Server::getInstance()->getAreaInfo(areadef->name)->url = areadef->url;
        TA_Server::getInstance()->getAreaInfo(areadef->name)->file = areadef->file;
        TA_Server::getInstance()->getAreaInfo(areadef->name)->command = areadef->command;
    }

    // Load Group Areas
    foreach(GroupAreaConfiguration* group, groupAreasConfiguration){
        TA_Server::getInstance()->addGroupArea(group->name,group->areas,group->color);
        TA_Server::getInstance()->getAreaInfo(group->name)->url = group->url;
        TA_Server::getInstance()->getAreaInfo(group->name)->file = group->file;
        TA_Server::getInstance()->getAreaInfo(group->name)->command = group->command;
    }
}

/*
    Clear the XML configurations
*/
void TA_XmlParser::clearXMLConfiguration(){
    foreach(ClientConfiguration* client, clientsConfiguration){
        foreach(WebcamConfiguration* webcam, client->webcams){
            foreach(SectorConfiguration* sector, webcam->sectors){
                delete sector;
            }
            delete webcam;
        }
        delete client;
    }

    foreach(AreaConfiguration* area , areasConfiguration){
        delete area;
    }

    foreach(GroupAreaConfiguration* group , groupAreasConfiguration){
        delete group;
    }

    clientsConfiguration.clear();
    areasConfiguration.clear();
    groupAreasConfiguration.clear();
}
