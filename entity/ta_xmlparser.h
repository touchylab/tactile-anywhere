/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 07 february 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_XmlParser class managing the  |
  | load and save operation for the configuration                           |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Valentin COULON -- Initial version                            |
  | [-] 1.1 - Simon BONY -- Error management during load operation, markers |
  |                         areas and group areas now managed               |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef TA_XMLPARSER_H
#define TA_XMLPARSER_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QList>
#include <QtXml>
#include <QDir>
#include <QXmlSchema>
#include <QXmlSchemaValidator>

// Local includes
#include "ta_server.h"
#include "communication/server/ta_infoclient.h"

/*****************************************************************************/
/******                             DEFINES                             ******/
/*****************************************************************************/
// Tags
#define TA_XMLPARSER_ROOT "configuration"
#define TA_XMLPARSER_CLIENTS_ROOT "clients"
#define TA_XMLPARSER_CLIENT_ENTITY "client"
#define TA_XMLPARSER_CLIENT_ADDRESS "address"
#define TA_XMLPARSER_WEBCAM_ROOT "webcam"
#define TA_XMLPARSER_ID "id"
#define TA_XMLPARSER_POINTS_AREA "points_area"
#define TA_XMLPARSER_UNIQUE_MARKER_AREA "unique_marker_area"
#define TA_XMLPARSER_MULTI_MARKER_AREA "multi_marker_area"
#define TA_XMLPARSER_NAME "name"
#define TA_XMLPARSER_COLOR "color"
#define TA_XMLPARSER_MARKER "marker"
#define TA_XMLPARSER_MARKERS "markers"
#define TA_XMLPARSER_POINTS "points"
#define TA_XMLPARSER_POINT "point"
#define TA_XMLPARSER_X "x"
#define TA_XMLPARSER_Y "y"
#define TA_XMLPARSER_AREAS "areas"
#define TA_XMLPARSER_AREA "area"
#define TA_XMLPARSER_FILE "file"
#define TA_XMLPARSER_URL "url"
#define TA_XMLPARSER_COMMAND "command"
#define TA_XMLPARSER_GROUP_AREAS "group_areas"
#define TA_XMLPARSER_GROUP "group"
#define TA_XMLPARSER_CHILD "child"
#define TA_XMLPARSER_LOCAL_NAME "LOCAL"

//XSD
#define TA_XMLPARSER_XSD_FILE "qrc:///xml/validation.xsd"

/*****************************************************************************/
/******                              ENUMS                              ******/
/*****************************************************************************/

//enum used to catch return of the fonctions save configuration
enum ConfSaveReturn {CONFSAVE_SUCCESS, CONFSAVE_CANTCREATEDIR, CONFSAVE_CANTCREATEFILE};

//enum used to catch return of the fonctions load configuration
enum ConfLoadReturn {CONFLOAD_SUCCESS, CONFLOAD_CANTCREATEDIR, CONFLOAD_CANTOPENFILE, CONFLOAD_BADFORMAT};

/*****************************************************************************/
/******                            STRUCTURES                           ******/
/*****************************************************************************/

struct SectorConfiguration {
    QString area; // The area name
    QColor color; // The area's color
    SectorType type; // The type of sector
    QVector<QPoint> polygon; // The list of points for a points sector or an unique marker sector
    QVector<MarkerStruct> markers; // The list of markers an unique marker sector or a multiple marker sector
};

struct GroupAreaConfiguration {
    QString name; // The group area's name
    QColor color; // The area's color
    QList<QString> areas; // The areas of this group sector
    QString file; // The file to launch in case of touch
    QString url; // The url to launch in case of touch
    QString command; // The command to launch in case of touch
};

struct AreaConfiguration {
    QString name; // The area's name
    QColor color; // The area's color
    QString file; // The file to launch in case of touch
    QString url; // The url to launch in case of touch
    QString command; // The command to launch in case of touch
};

struct WebcamConfiguration {
    int id; // The webcam's id
    QList<SectorConfiguration*> sectors; // The masks used for sectors
};

struct ClientConfiguration {
    QString name; //The client hostAddress
    QList<WebcamConfiguration*> webcams; // The webcams associated to client
};

/*****************************************************************************/
/******                              CLASS                              ******/
/*****************************************************************************/

/*
    TA_XmlParser regroups statics methods to manage a save and load XML operation
    of the server configuration
*/
class TA_XmlParser {
    public:
        /*
            Save the current configuration
                @param path : the path of the file to save
                @return : the enum ConfSaveReturn. Must return CONFSAVE_SUCCESS or an error is occured
        */
        static ConfSaveReturn saveConfiguration(QString path);

        /*
            Load a configuration
                @param path : the path of the file to load
                @return : the enum ConfReturn. Must return CONFLOAD_SUCCESS or an error is occured
        */
        static ConfLoadReturn loadConfiguration(QString path);

        /*
            Gets error message if a CONFLOAD_BADFORMAT error occurs
                @return : a QString containing the error message
        */
        static QString getErrorMessage();

    private:

        /*
            Private constructor to protect against instantiation
        */
        TA_XmlParser(){}

        /*
            Get the file from the given path
                @param path : the path of the file
                @param load : set as true if the file is open for load, false else
                @return : the link to the file or NULL if the directory can't be created
        */
        static QFile* getConfigurationFile(QString path, bool load);

        /*
            Initializes the variables of the configuration with current server state
        */
        static void initConfiguration();

        /*
            Initializes the server with the load configuration
        */
        static void applyConfiguration();

        /*
            Clear the XML configurations
        */
        static void clearXMLConfiguration();

        static QList<ClientConfiguration*> clientsConfiguration; // The configuration of clients
        static QList<GroupAreaConfiguration*> groupAreasConfiguration; // The configuration of group areas
        static QList<AreaConfiguration*> areasConfiguration; // The configuration of areas
        static QString error; // The error message
};

#endif // TA_XMLPARSER_H
