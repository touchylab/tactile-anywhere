/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 02 february 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains the main function launching Tactile Anywhere         |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

/*****************************************************************************/
/******                             INCLUDES                            ******/
/*****************************************************************************/
#include <iostream>
#include <QApplication>
#include <QLocalSocket>
#include <QLocalServer>
#include <QTranslator>
#include "gui/ta_mainwindow.h"
#include "gui/ta_logger.h"

using namespace std;

/*****************************************************************************/
/******                             DEFINES                             ******/
/*****************************************************************************/
#define TA_MAIN_SERVER_NAME "Tactile Anywhere Only Instance Server"
#define TA_MAIN_ERROR_ALREADY_EXIST_MESSAGE "An instance of Tactile Anywhere is already launched"

/*****************************************************************************/
/******                            FUNCTIONS                            ******/
/*****************************************************************************/

/*
    Main method to ask for choice between Tactile Anywhere client or server GUI
*/
int main(int argc, char *argv[]) {
    // Initialization
    QApplication app(argc, argv);

    // Check for an already instance of Tactile Anywhere
    QLocalSocket lSocket;
    lSocket.connectToServer(TA_MAIN_SERVER_NAME);
    if (lSocket.waitForConnected(500)) {
        cerr << TA_MAIN_ERROR_ALREADY_EXIST_MESSAGE << endl;
        return 1;
    }

    QLocalServer* lServer = new QLocalServer();
    lServer->listen(TA_MAIN_SERVER_NAME);

    // Translate the program in french
    QTranslator translator;
    translator.load(":/translation/qt_fr.qm");
    app.installTranslator(&translator);

    // Initialize ressources and launch
    Q_INIT_RESOURCE(resources);
    TA_MainWindow::initialize();
    TA_MainWindow::getInstance()->show();

    int ret = app.exec();

    // Close instances
    delete TA_MainWindow::getInstance();
    delete TA_Logger::getInstance();
    lServer->close();
    delete lServer;

    return ret;
}
