/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 02 december 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains the methods of the TA_Webcam class representing in   |
  | this programm a webcam and all its functionnalities with the detection  |
  | too. It also accept video to simulate a webcam.                         |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  | [-] 1.1 - Sébastien DESTABEAU -- Webcam frames extraction and events    |
  | [-] 1.2 - Simon BONY -- Complete for Tactile Anywhere V1.0              |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_webcam.h"

/*****************************************************************************/
/******                  CONSTRUCTORS/DESTRUCTOR                        ******/
/*****************************************************************************/

/*
    This constructor creates and initializes this object with the given capture.
    It also offer to reduce the video image to increase the performances during the detection.
        @param capture : the capture to use
        @param id : the id of the device
        @param dimensionRatio : the coefficient to apply to resize the image. The dimension
                                will be multiply with it. The resize is only applied during the detection.
*/
TA_Webcam::TA_Webcam(VideoCapture* capture, int id, float dimensionRatio) {
    init(capture, id, dimensionRatio);
    threadDetection->start();
}

/*
    This destructor will clean this class at the end of this object
*/
TA_Webcam::~TA_Webcam() {
    stopDetection();
    stopSequencedFrame();

    if (threadDetection) {
        threadDetection->stopThread();
    }

    if (webcam) {
        webcam->release();
        delete webcam;
    }

    if (detector) delete detector;
}

/*
    This method initializes this object with the given capture.
    It also offer to reduce the webcam image to increase the performances during the detection.
        @param id : the id of the device to use
        @param dimensionRatio : the coefficient to apply to resize the image. The dimension
                                will be multiply with it. The resize is only applied during the detection.
*/
void TA_Webcam::init(VideoCapture* capture, int id, float dimensionRatio) {
    this->webcam = capture;
    webcamId = id;
    name = QCamera::deviceDescription(QCamera::availableDevices().takeAt(id));
    // Initializes the other members
#ifdef __linux
    webcam->set(CV_CAP_PROP_FRAME_WIDTH, 640);
    webcam->set(CV_CAP_PROP_FRAME_HEIGHT, 480);
#endif
    size = Size(webcam->get(CV_CAP_PROP_FRAME_WIDTH), webcam->get(CV_CAP_PROP_FRAME_HEIGHT));
    dimRatio = dimensionRatio;
    detector = new TA_bgsDetect();

    threadDetection = new TA_ThreadDetection(this);

    connect(&frameTimer, SIGNAL(timeout()), this, SLOT(sendFrame()));
    connect(&checkConnectTimer, SIGNAL(timeout()), this, SLOT(checkConnection()));
    checkConnectTimer.start(TA_WEBCAM_CHECKCONNECTION_INTERVAL_MS);
    connected = true;
    isResizing = false;
}

/*****************************************************************************/
/******                         GETTERS                                 ******/
/*****************************************************************************/

/*
    Offers the current frame of this webcam. A webcam can take a little time before
    its true initialization. If this method return false and if the initialization
    is recent, wait and retry.
        @param frame : the container of the current frame. Contains NULL if the webcam
                       is disconnected if the video simulating the webcam is finished).
        @param tryAnyway : set as true to try to get a frame even if the webcam is disconnected, false else
        @return : true in case of success, false else (webcam disconnected or end of
                  the video)
*/
bool TA_Webcam::getFrame(Mat& frame, bool tryAnyway) {
    if (isResizing || (!isConnected() && !tryAnyway)) {
        return false;
    }

    if (!webcam->read(frame)) {
        QThread::msleep(80);
        if (!webcam->read(frame)) {
            return false;
        }
    }
    return true;
}

/*
    Offers the current frame of this webcam. A webcam can take a little time before
    its true initialization. If this method return false and if the initialization
    is recent, wait and retry.
        @param frame : the container of the current frame
        @return : true in case of success, false else (webcam disconnected or end of
                  the video)
*/
bool TA_Webcam::getQtFrame(QImage& frame) {
    Mat img;
    // Gets the next frame
    if (!getFrame(img)) {
        return false;
    }
    // Convert Mat frame to a QImage
    frame = QImage((const uchar*) img.data, img.cols, img.rows, img.step, QImage::Format_RGB888).rgbSwapped();
    return true;
}

/*
    Get the webcam's id
        @return : webcam's id
*/
int TA_Webcam::getWebcamId() const {
    return webcamId;
}

/*
    Get the webcam's name
        @return : webcam's name
*/
QString TA_Webcam::getName() const {
    return name;
}

/*
    Gets the dimension ratio to apply during the detection
        @return : the dimension ratio to apply
*/
float TA_Webcam::getDimensionRatio() const {
    return dimRatio;
}

/*
    Get the position of the center of seeing markers
        @return : a QMap with marker's id as key and a QPoint containing the position of
                  the center of each marker as value
*/
QMap<quint32, QPoint> TA_Webcam::getMarkerPositions() {
    QMap<quint32, QPoint> positions;
    vector<Marker> markers;
    Mat frame;

    if (!getFrame(frame)) {
        return positions;
    }

    markerDetector.detect(frame, markers, camParam);

    foreach(Marker marker, markers) {
        Point2f point = marker.getCenter();
        positions[marker.id] = QPoint(point.x, point.y);
    }

    return positions;
}

/*
    Checks if the webcam is connected
        @return: true if the webcam is connected, false else
*/
bool TA_Webcam::isConnected() const {
    return connected;
}

/*****************************************************************************/
/******                         SETTERS                                 ******/
/*****************************************************************************/

/*
    Set the webcam's name
        @param name : webcam's name
*/
void TA_Webcam::setName(const QString &name) {
    this->name = name;
}

/*
    Sets the dimension ratio to apply during the detection
        @param dimensionRatio: the dimension ratio to apply
*/
void TA_Webcam::setDimensionRatio(float dimensionRatio) {
    this->dimRatio = dimensionRatio;
}

/*
    Sets the connected state for this webcam
        @param state: true for connected webcam, false else
*/
void TA_Webcam::setConnectedState(bool state) {
    this->connected = state;
}

/*
    Apply or not the dimension ratio to the webcam resolution
        @param apply: true to apply the dimension ratio, false to restore the initial resolution
*/
void TA_Webcam::applyDimensionRatio(bool apply){
    isResizing = true;
    if(apply){
        if(getDimensionRatio() == 1.0)
            return;
        webcam->set(CV_CAP_PROP_FRAME_WIDTH, size.width*getDimensionRatio());
        webcam->set(CV_CAP_PROP_FRAME_HEIGHT, size.height*getDimensionRatio());

        // Wait for changement of new ratio
        Mat frame;
        QThread::msleep(30);
        for(int i = 0; i<3 && !getFrame(frame) ; i++){
            QThread::msleep(30);
        }
    } else {
        webcam->set(CV_CAP_PROP_FRAME_WIDTH, size.width);
        webcam->set(CV_CAP_PROP_FRAME_HEIGHT, size.height);
    }
    isResizing = false;
}

/*****************************************************************************/
/******                          MASKS                                  ******/
/*****************************************************************************/

/*
    Adds or replaces a mask corresponding to the given segment to apply to this webcam.
    Each contact on this segment will be detected after.
        @param area : the area of the mask.
        @param sector : the sector of the mask.
        @param segment : the list of point constituing the segment to track.
*/
void TA_Webcam::addMask(QString area, QString sector, const QVector<QPoint>& segment) {
    while (!threadDetection->detectionIsStopped()) {
        QThread::msleep(10);
    }

    masks[sector] = TA_Mask(area, sector, segment, size, dimRatio);
    states[sector] = false;
}

/*
    Removes from this webcam the mask identified by the given sector.
        @param sector : the sector of the mask.
*/
void TA_Webcam::removeMask(QString sector) {
    masks.remove(sector);
    masks.remove(sector);
}

/*
    Removes from this webcam all the masks
*/
void TA_Webcam::cleanMasks() {
    masks.clear();
    states.clear();
}

/*****************************************************************************/
/******                        OPERATIONS                               ******/
/*****************************************************************************/

/*
    Initializes the detection by taking the background image used into the treatment.
    The scene taken by the webcam will be consider as a "clean" view. Each difference
    will be detected after.
        @return: true if the detection is initialized, false else
*/
bool TA_Webcam::initDetection() {
    if (!isConnected()) {
        return false;
    }

    // Creates or recreates the detector
    detector->init();

    // Initializes the detector
    VectorEvents contacts;
    return getContacts(contacts);
}

/*
    Check if the webcam is still connected or try to reconnect it if false
*/
void TA_Webcam::checkConnection() {
    Mat frame;
    if (!connected) {
#ifdef _WIN32
        webcam = new VideoCapture(webcamId);
        if (webcam && getFrame(frame, true)) {
            setConnectedState(true);
            emit reconnected(this);
        } else {
            delete webcam;
            webcam = NULL;
        }
#else  // For UNIX system, wait for the good number of webcams
        VideoCapture *cap;
        int nbWebcams = QCamera::availableDevices().size();

        for (int n = 0; n < nbWebcams; n++) {
            if ((cap = new VideoCapture(n)) == NULL || !cap->isOpened()) {
                delete cap;
                return;
            }
            delete cap;
        }
        webcam = new VideoCapture(webcamId);
        setConnectedState(true);
        emit reconnected(this);
#endif
    }
}

/*
    Apply the detection treatment and offers the actuals contacts on the given sectors
    with addMask(). The method initializeDetection() must have been called before.
        @param contacts : the container to get the constact.
        @return : true if there was no problem during the detection
                  false if the next frame can't be read from the webcam (or from the video)
*/
bool TA_Webcam::getContacts(VectorEvents& contacts) {
    Mat frame, foreground;
    return getContacts(contacts, frame, foreground);
}

/*
    Apply the detection treatment and offers the actuals contacts on the given sectors
    with addMask(). The method initializeDetection() must have been called before.
        @param contacts : the container to get the constact.
        @param frame : the container to get the current frame used in the detection.
        @param foreground : the container to get the foreground of the detection.
        @return : true if there was no problem during the detection
                  false if the next frame can't be read from the webcam (or from the video)
*/
bool TA_Webcam::getContacts(VectorEvents& contacts, Mat& frame, Mat& foreground) {
    // Clears the contacts
    contacts.clear();

    // Gets the next frame an applies the detector on this frame
    if (!getFrame(frame) || !detector->operator ()(frame, foreground)) {
        return false;
    }

    // Applies the masks and searchs for contacts
    for (QMap<QString, TA_Mask>::Iterator it = masks.begin(); it != masks.end(); ++it) {
        if (!it.value().isClean(foreground) != states[it.key()]) {
            states[it.key()] = (!states[it.key()]);
            EventContacts contact;
            contact.areaName = it.value().getArea();
            contact.sectorId = it.value().getSector();
            contact.state = states[it.key()];
            contacts << contact;
        }
    }
    return true;
}

/*
    Gets the states of each sectors contained in this webcams
        @return : a QMap with sector's id as key and state as value
*/
QMap<QString, bool>* TA_Webcam::getSectorsState() {
    return &states;
}

/*
    Launches the detection on this webcam. A newEvents signal will be launched on each new detected
    event. Call stopDetection() to stop it. If initDetection() was not called before, it will be called
    here.
        @return: true if the detection is launched, false else
*/
bool TA_Webcam::launchDetection() {
    if (!isConnected())
        return false;

    // Wait for the end of the last detection
    while (!threadDetection->detectionIsStopped()) {
        QThread::msleep(10);
    }

    // Don't do anything if there is nothing to detect
    if (masks.isEmpty()) {
        return false;
    }

    // Apply the dimension ratio
    applyDimensionRatio(true);

    // Initializes the sectors states
    for (QMap<QString, TA_Mask>::Iterator it = masks.begin(); it != masks.end(); ++it) {
        states[it.key()] = false;
    }

    // Initializes the detector
    if (!detector->isInitialized()) {
        initDetection();
    }

    // Launch the detector
    threadDetection->launchDetection();
    return true;
}

/*
    Stop the detection on this webcam.
*/
void TA_Webcam::stopDetection() {
    if (threadDetection) {
        threadDetection->stopDetection();
    }
    detector->setInitialization(false);
}

/*
    Send a frame.
*/
void TA_Webcam::sendFrame() {
    QImage img;
    if (!getQtFrame(img)) {
        return;
    }
    emit newFrame(this, img);
}

/*
    Start frame timer for a sequence of defined milliseconds.
        @param ms : number of milliseconds a frame is launched.
        @return: true if the stream is initialized, false else
*/
bool TA_Webcam::launchSequencedFrame(int ms) {
    frameTimer.start(ms);
    return true;
}

/*
    Stop webcam frames timer.
*/
void TA_Webcam::stopSequencedFrame() {
    frameTimer.stop();
}
