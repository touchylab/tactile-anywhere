/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 02 december 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains the declaration of the TA_Webcam class representing  |
  | in this programm a webcam and all its functionnalities with the         |
  | detection too. It also accept video to simulate a webcam.               |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  | [-] 1.1 - Sébastien DESTABEAU -- Webcam frames extraction and events    |
  | [-] 1.2 - Simon BONY -- Complete for Tactile Anywhere V1.0              |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef TA_WEBCAM_H
#define TA_WEBCAM_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QMap>
#include <QVector>
#include <QString>
#include <QObject>
#include <QTimer>
#include <QImage>
#include <QCamera>

// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

// Libraries includes
#include "aruco.h"

// Local includes
#include "ta_mask.h"
#include "ta_threaddetection.h"
#include "detection/bgs/ta_bgsdetect.h"
#include "communication/data/ta_dataevent.h"

// Namespaces
using namespace aruco;
using namespace cv;

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/

#define TA_WEBCAM_CHECKCONNECTION_INTERVAL_MS 2000

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
 *  TA_Webcam is an object reprenting a webcam in this programm. It offer the
 *  management of this component (configuration, activation, halt, getting flow, etc.)
 *  and also the sectors contact detection for this webcam. TA_Webcam can be initialized
 *  with a video to simulate a webcam.
 */
class TA_Webcam:public QObject {
    Q_OBJECT

 public:
    // Constructors

    /*
        This constructor creates and initializes this object with the given capture.
        It also offer to reduce the video image to increase the performances during the detection.
            @param capture : the capture to use
            @param id : the id of the device
            @param dimensionRatio : the coefficient to apply to resize the image. The dimension
                                    will be multiply with it. The resize is only applied during the detection.
    */
    TA_Webcam(VideoCapture* capture, int id, float dimensionRatio = 0.5);

    /*
        This destructor will clean this class at the end of this object
    */
    virtual ~TA_Webcam();

    /*
        This method initializes this object with the given capture.
        It also offer to reduce the webcam image to increase the performances during the detection.
            @param id : the id of the device to use
            @param dimensionRatio : the coefficient to apply to resize the image. The dimension
                                    will be multiply with it. The resize is only applied during the detection.
    */
    virtual void init(VideoCapture* capture, int id, float dimensionRatio = 0.5);

    // Getters

    /*
        Offers the current frame of this webcam. A webcam can take a little time before
        its true initialization. If this method return false and if the initialization
        is recent, wait and retry.
            @param frame : the container of the current frame. Contains NULL if the webcam
                           is disconnected if the video simulating the webcam is finished).
            @param tryAnyway : set as true to try to get a frame even if the webcam is disconnected, false else
            @return : true in case of success, false else (webcam disconnected or end of
                      the video)
    */
    virtual bool getFrame(Mat& frame, bool tryAnyway = false);

    /*
        Offers the current frame of this webcam. A webcam can take a little time before
        its true initialization. If this method return false and if the initialization
        is recent, wait and retry.
            @param frame : the container of the current frame
            @return : true in case of success, false else (webcam disconnected or end of
                      the video)
    */
    virtual bool getQtFrame(QImage& frame);

    /*
        Get the webcam's name
            @return : webcam's name
    */
    QString getName() const;

    /*
        Get the webcam's id
            @return : webcam's id
    */
    int getWebcamId() const;

    /*
        Gets the dimension ratio to apply during the detection
            @return : the dimension ratio to apply
    */
    float getDimensionRatio() const;

    /*
        Get the position of the center of seeing markers
            @return : a QMap with marker's id as key and a QPoint containing the position of
                      the center of each marker as value
    */
    QMap<quint32, QPoint> getMarkerPositions();

    /*
        Checks if the webcam is connected
            @return: true if the webcam is connected, false else
    */
    bool isConnected() const;

    // Setters

    /*
        Set the webcam's name
            @param name : webcam's name
    */
    void setName(const QString &name);

    /*
        Sets the dimension ratio to apply during the detection
            @param dimensionRatio: the dimension ratio to apply
    */
    void setDimensionRatio(float dimensionRatio);

    /*
        Sets the connected state for this webcam
            @param state: true for connected webcam, false else
    */
    void setConnectedState(bool state);

    /*
        Apply or not the dimension ratio to the webcam resolution
            @param apply: true to apply the dimension ratio, false to restore the initial resolution
    */
    void applyDimensionRatio(bool apply);

    // Mask

    /*
        Adds or replaces a mask corresponding to the given segment to apply to this webcam.
        Each contact on this segment will be detected after.
            @param area : the area of the mask.
            @param sector : the sector of the mask.
            @param segment : the list of point constituing the segment to track.
    */
    virtual void addMask(QString area, QString sector, const QVector<QPoint>& segment);

    /*
        Removes from this webcam the mask identified by the given sector.
            @param sector : the sector of the mask.
    */
    virtual void removeMask(QString sector);

    /*
        Removes from this webcam all the masks
    */
    virtual void cleanMasks();

    // Detection

    /*
        Initializes the detection by taking the background image used into the treatment.
        The scene taken by the webcam will be consider as a "clean" view. Each difference
        will be detected after.
            @return: true if the detection is initialized, false else
    */
    virtual bool initDetection();

    /*
        Lauches the detection on this webcam. A newEvents signal will be launched on each new detected
        event. Call stopDetection() to stop it. If initDetection() was not called before, it will be called
        here.
            @return: true if the detection is launched, false else
    */
    virtual bool launchDetection();

    /*
        Stop the detection on this webcam.
    */
    virtual void stopDetection();

    /*
        Apply the detection treatment and offers the actuals contacts on the given sectors
        with addMask(). The method initializeDetection() must have been called before.
            @param contacts : the container to get the constact.
            @return : true if there was no problem during the detection
                      false if the next frame can't be read from the webcam (or from the video)
    */
    virtual bool getContacts(VectorEvents& contacts);

    /*
        Gets the states of each sectors contained in this webcams
            @return : a QMap with sector's id as key and state as value
    */
    virtual QMap<QString, bool>* getSectorsState();

    /*
        Apply the detection treatment and offers the actuals contacts on the given sectors
        with addMask(). The method initializeDetection() must have been called before.
            @param contacts : the container to get the constact.
            @param frame : the container to get the current frame used in the detection.
            @param foreground : the container to get the foreground of the detection.
            @return : true if there was no problem during the detection
                      false if the next frame can't be read from the webcam (or from the video)
    */
    virtual bool getContacts(VectorEvents& contacts, Mat& frame, Mat& foreground);

    // Stream

    /*
        Start frame timer for a sequence of defined milliseconds.
            @param ms : number of milliseconds a frame is launched.
            @return: true if the stream is initialized, false else
    */
    bool launchSequencedFrame(int ms);

    /*
        Stop webcam frames timer.
    */
    void stopSequencedFrame();

 public slots:
    /*
        Send a frame.
    */
    void sendFrame();

    /*
        Check if the webcam is still connected or try to reconnect it if false
    */
    void checkConnection();

 signals:
    /*
        Create a new frame from a webcam converted in a picture.
            @param webcam : the webcam to listen and extract frame
            @param image : the picture to extract from.
    */
    void newFrame(TA_Webcam* webcam, QImage img);

    /*
        Send when new events occurs on this webcam.
            @param webcam : the webcam concerned by the event
            @param contacts : the detected contacts
    */
    void newEvents(TA_Webcam* webcam, VectorEvents contacts);

    /*
        Send when this webcam is reconnected
            @param webcam : the reconnected webcam
    */
    void reconnected(TA_Webcam* webcam);

    /*
        Send when this webcam is disconnected
            @param webcam : the disconnected webcam
    */
    void disconnected(TA_Webcam* webcam);

 protected:
    // Webcam informations
    VideoCapture* webcam;  // The webcam capture
    int webcamId;  // The webcam openCV id
    Size size;  // The webcam size
    QString name;  // The webcam name
    bool connected;  // True if the webcam is connected, false else

    // Detection informations
    TA_ThreadDetection* threadDetection; // The thread used for the detection
    float dimRatio;  // The coefficient to apply to resize the image
    bool isResizing; // True if the webcam is resizing its resolution false else
    QMap<QString, TA_Mask> masks;  // The masks representing the sectors
    QMap<QString, bool> states;  // The states of the sectors
    TA_bgsDetect* detector;  // The detector used to find the modifications on the view
    MarkerDetector markerDetector;  // The dectector used to find markers
    CameraParameters camParam;  // The paramaters of the webcam

    // Timers
    QTimer frameTimer;   // Timer used to send frames
    QTimer checkConnectTimer; // Tiemr used to check the connection status
};

#endif  // TA_WEBCAM_H
