/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 30 november 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains the declaration of the TA_Mask class offering to     |
  | isolate a part of an image and to apply actions on it                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef TA_MASK_H
#define TA_MASK_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QVector>
#include <QPoint>
#include <QString>

// OpenCV
#include <opencv2/imgproc/imgproc.hpp>

// Namespaces
using namespace cv;
using namespace std;

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
 *  TA_Mask is an object offering to isolate a part of an image with a mask.
 *  It also offer to detect if the image only have black pixels
 */
class TA_Mask
{
    public:
        // Constructors

        /*
            This constructor creates this object. It must be initialized later.
        */
        TA_Mask();

        /*
            This constructor creates and initializes this object with a mask adapted to the given polygon. If the
            detector resized its given images, the same coefficient must be given as dimensionRatio.
                @param area : the area of the sector
                @param sector : the sector represented by this mask
                @param polygon : the polygon specifying the part of the image to isolate. The points
                                 in this list will be links into a polygon.
                @param size : the size of the mask. Must be the same as given images.
                @param dimensionRatio : the coefficient to apply to resize the image. The dimension
                                        will be multiply with it. The resize is only applied during the detection.
        */
        TA_Mask(QString area, QString sector, const QVector<QPoint>& polygon, const Size& size, float dimensionRatio=1);

        /*
            This method initializes this class with a mask adapted to the given polygon. If the
            detector resized its given images, the same coefficient must be given as dimensionRatio.
                @param area : the area of the sector
                @param sector : the sector represented by this mask
                @param polygon : the polygon specifying the part of the image to isolate. The points
                                 in this list will be links into a polygon.
                @param size : the size of the mask. Must be the same as given images.
                @param dimensionRatio : the coefficient to apply to resize the image. The dimension
                                        will be multiply with it. The resize is only applied during the detection.
        */
        virtual void init(QString area, QString sector, const QVector<QPoint>& polygon, const Size& size, float dimensionRatio=1);

        // Getters

        /*
            Gets the sector's name
                @return : the sector's name
        */
        QString getSector() const;

        /*
            Gets the area's name
                @return : the area's name
        */
        QString getArea() const;

        /*
            This method offer the current mask
                @return : the current mask
        */
        Mat getMask() const;

        // Actions

        /*
            This method applies the mask on the given image. Only the pixels in the polygon used for the
            construction will remain, the others will be turn into black. The mask must be initialized.
                @param image: the image to process.
                @param masked: the same image with the mask applied.
        */
        virtual void apply(const Mat& image, OutputArray masked);

        /*
            This method checks if the given image is composed only of black pixels on the part
            of this image isolated by this mask.
                @param image : the image to check.
                @param level : the minimum percentage of non black pixels to reach in the isolated
                               part to consider the image as not clean. Must be between 0 and 100.
                @return : true is the image is clean, false else.
        */
        virtual bool isClean(const Mat& image, int level=0);

    protected:
        // The sector and area
        QString sectorName;
        QString areaName;

        // The mask to apply
        Mat mask;

        // The number of pixel in the mask
        int nPixelsArea;
};

#endif // TA_MASK_H
