/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 06 february 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_ThreadDetection class managing the |
  | detection of a webcam                                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_threaddetection.h"
#include "ta_webcam.h"

/*****************************************************************************/
/******                     CONSTRUCTORS/DESTRUCTOR                     ******/
/*****************************************************************************/

/*
    This constructor creates this thread for the given webcam
        @param webcam : the webcam to manage

*/
TA_ThreadDetection::TA_ThreadDetection(TA_Webcam* webcam) {
    this->webcam = webcam;
    this->detectTimer = NULL;
    this->active = false;
    this->pause = true;
    this->stop = false;
    this->countGetFrameFail = 0;
    qRegisterMetaType<VectorEvents>("VectorEvents");
    connect(this, SIGNAL(newEvents(TA_Webcam*, VectorEvents)), webcam, SIGNAL(newEvents(TA_Webcam*, VectorEvents)), Qt::QueuedConnection);
    connect(this, SIGNAL(disconnected(TA_Webcam*)), webcam, SIGNAL(disconnected(TA_Webcam*)), Qt::QueuedConnection);
}

/*
    Cleans this class properly
*/
TA_ThreadDetection::~TA_ThreadDetection() {
    if (detectTimer != NULL) delete detectTimer;
}

/*****************************************************************************/
/******                              SLOTS                              ******/
/*****************************************************************************/

/*
    Proceed to the contact detection
*/
void TA_ThreadDetection::detect() {
    detectTimer->stop();

    if (stop) {
        this->deleteLater();
        this->exit();
        return;
    }

    if (!active && !pause) {
        webcam->applyDimensionRatio(false);
        webcam->cleanMasks();
        pause = true;
    }

    if (webcam->isConnected()) {
        Mat frame;
        if (active && webcam->getContacts(contacts)) {
            if (countGetFrameFail > TA_NUMBER_OF_FRAME_FAIL_BEFORE_DISCONNECT) {
                qDebug() << "reconnected";
            }
            countGetFrameFail = 0;
            if (!contacts.isEmpty()) {
                emit newEvents(webcam, contacts);
            }
        } else if ((active && countGetFrameFail++ >= TA_NUMBER_OF_FRAME_FAIL_BEFORE_DISCONNECT)
                   || (!active && !webcam->getFrame(frame) && countGetFrameFail++ >= TA_NUMBER_OF_FRAME_FAIL_BEFORE_DISCONNECT/10)) {
            countGetFrameFail = 0;
            webcam->setConnectedState(false);
            emit disconnected(webcam);
        }
    }

    detectTimer->start(active && webcam->isConnected() ? TA_THREADDETECTION_DETECTION_INTERVAL_MS : TA_THREADDETECTION_CHECK_INTERVAL_MS);
}

/*****************************************************************************/
/******                            OPERATIONS                           ******/
/*****************************************************************************/

/*
    Launches the thread
*/
void TA_ThreadDetection::run() {
    if (detectTimer == NULL) {
        detectTimer = new QTimer();
        connect(detectTimer, SIGNAL(timeout()), this, SLOT(detect()), Qt::DirectConnection);
    }
    detectTimer->start(TA_THREADDETECTION_CHECK_INTERVAL_MS);
    exec();
}

/*
    Launches the detection for this webcam
*/
void TA_ThreadDetection::launchDetection() {
    active = true;
    pause = false;
}

/*
    Stops the detection for this webcam
*/
void TA_ThreadDetection::stopDetection() {
    active = false;
}

/*
    Checks if the detection is stopped for this webcam
        @return: true if the detection is stopped, false else
*/
bool TA_ThreadDetection::detectionIsStopped() const{
    return !active && pause;
}

/*
    Stops the thread
*/
void TA_ThreadDetection::stopThread() {
    stop = true;
}
