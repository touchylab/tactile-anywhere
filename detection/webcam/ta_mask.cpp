/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 29 november 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains the methods of the TA_Mask class offering to isolate |
  | a part of an image and to apply actions on it                           |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_mask.h"

/*****************************************************************************/
/******                  CONSTRUCTORS/DESTRUCTOR                        ******/
/*****************************************************************************/

/*
    This constructor creates this object. It must be initialized later.
*/
TA_Mask::TA_Mask(){}

/*
    This constructor creates and initializes this object with a mask adapted to the given polygon. If the
    detector resized its given images, the same coefficient must be given as dimensionRatio.
        @param area : the area of the sector
        @param sector : the sector represented by this mask
        @param polygon : the polygon specifying the part of the image to isolate. The points
                         in this list will be links into a polygon.
        @param size : the size of the mask. Must be the same as given images.
        @param dimensionRatio : the coefficient to apply to resize the image. The dimension
                                will be multiply with it. The resize is only applied during the detection.
*/
TA_Mask::TA_Mask(QString area, QString sector, const QVector<QPoint>& polygon, const Size& size, float dimensionRatio){
    init(area,sector,polygon,size, dimensionRatio);
}

/*
    This method initializes this class with a mask adapted to the given polygon. If the
    detector resized its given images, the same coefficient must be given as dimensionRatio.
        @param area : the area of the sector
        @param sector : the sector represented by this mask
        @param polygon : the polygon specifying the part of the image to isolate. The points
                         in this list will be links into a polygon.
        @param size : the size of the mask. Must be the same as given images.
        @param dimensionRatio : the coefficient to apply to resize the image. The dimension
                                will be multiply with it. The resize is only applied during the detection.
*/
void TA_Mask::init(QString area, QString sector, const QVector<QPoint>& polygon, const Size& size, float dimensionRatio){
    this->areaName = area;
    this->sectorName = sector;

    // Create the background of the mask
    mask.create(Size(size.width*dimensionRatio, size.height*dimensionRatio),CV_8U);
    rectangle(mask,Point(0,0),Point(size.width*dimensionRatio, size.height*dimensionRatio),Scalar(0,0,0),-1,8);

    // Create the points
    Point** points = new Point*[1];
    points[0] = new Point[polygon.size()];
    for(int i=0 ; i < polygon.size() ; i++){
        points[0][i].x = (polygon[i]).x()*dimensionRatio;
        points[0][i].y = (polygon[i]).y()*dimensionRatio;
    }

    // Draw the filled polygon on the mask
    const Point* poly[1] = { points[0] };
    int npoints[1] = { (int)polygon.size() };

    fillPoly(mask, poly, npoints, 1, Scalar( 255, 255, 255 ), 8);

    // Calculate the number of pixels in the area
    vector<Point> contour;
    for(int i=0 ; i < polygon.size() ; i++){
        contour.push_back(points[0][i]);
    }

    nPixelsArea = contourArea(contour);
}

/*****************************************************************************/
/******                         GETTERS                                 ******/
/*****************************************************************************/

/*
    Gets the sector's name
        @return : the sector's name
*/
QString TA_Mask::getSector() const{
    return sectorName;
}

/*
    Gets the area's name
        @return : the area's name
*/
QString TA_Mask::getArea() const{
    return areaName;
}

/*
    This method offer the current mask
        @return : the current mask
*/
Mat TA_Mask::getMask() const{
    return mask;
}

/*****************************************************************************/
/******                        OPERATIONS                               ******/
/*****************************************************************************/

/*
    This method applies the mask on the given image. Only the pixels in the polygon used for the
    construction will remain, the others will be turn into black. The mask must be initialized.
        @param image: the image to process.
        @param masked: the same image with the mask applied.
*/
void TA_Mask::apply(const Mat& image, OutputArray masked){
    if(mask.rows != image.rows || mask.cols != image.cols){
        Mat dst;
        resize(mask,dst,Size(image.cols,image.rows));
        mask = dst;
    }

    cv::bitwise_and(mask, image, masked);
}

/*
    This method checks if the given image is composed only of black pixels on the part
    of this image isolated by this mask.
        @param image : the image to check.
        @param level : the minimum percentage of non black pixels to reach in the isolated
                       part to consider the image as not clean. Must be between 0 and 100.
        @return : true is the image is clean, false else.
*/
bool TA_Mask::isClean(const Mat& image, int level){
    /* Applies the mask */
    Mat masked;
    apply(image,masked);

    /* Counts the non black pixels and checks if they are more than the asked level */
    return countNonZero(masked)<=(((float)level)/100)*nPixelsArea;
}
