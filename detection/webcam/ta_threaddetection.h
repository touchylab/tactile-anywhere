/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 06 february 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains the declaration of the TA_ThreadDetection class      |
  | managing the detection of a webcam                                      |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef TA_THREADDETECTION_H
#define TA_THREADDETECTION_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QThread>
#include <QMap>
#include <QTimer>
#include <QMetaType>

// Local includes
#include "communication/data/ta_dataevent.h"

// Local classes
class TA_Webcam;

// Type definitions
typedef QVector<EventContacts> VectorEvents;

/*****************************************************************************/
/******                         DEFINES                                ******/
/*****************************************************************************/

// thread detection interval in milliseconds
#define TA_THREADDETECTION_DETECTION_INTERVAL_MS    20
// thread detection checking process interval in milliseconds
#define TA_THREADDETECTION_CHECK_INTERVAL_MS    2000
// minimum fail number of getting frame to considerate the webcam as disconnected
#define TA_NUMBER_OF_FRAME_FAIL_BEFORE_DISCONNECT    50

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
 *  TA_ThreadDetection is a thread managing the detection for a webcam
 */
class TA_ThreadDetection : public QThread {
    Q_OBJECT
 public:
    // Constructors

    /*
        This constructor creates this thread for the given webcam
            @param webcam : the webcam to manage
    */
    TA_ThreadDetection(TA_Webcam* webcam);

    /*
        Cleans this class properly
    */
    ~TA_ThreadDetection();

    // Operations

    /*
        Launches the thread
    */
    void run();

    /*
        Launches the detection for this webcam
    */
    void launchDetection();

    /*
        Stops the detection for this webcam
    */
    void stopDetection();

    /*
        Checks if the detection is stopped for this webcam
            @return: true if the detection is stopped, false else
    */
    bool detectionIsStopped() const;

    /*
        Stops the thread
    */
    void stopThread();

 private slots:
    /*
        Proceed to the contact detection
    */
    void detect();

 signals:
    /*
        Send when new events occurs on the webcam.
            @param webcam : the webcam concerned by the event
            @param contacts : the detected contacts
    */
    void newEvents(TA_Webcam* webcam, VectorEvents contacts);

    /*
        Send when the webcam is disconnected
            @param webcam, the disconnected webcam
    */
    void disconnected(TA_Webcam* webcam);

 private:
    VectorEvents contacts;
    TA_Webcam* webcam;
    QTimer* detectTimer;
    bool active, pause, stop;
    int countGetFrameFail;
};

#endif  // TA_THREADDETECTION_H
