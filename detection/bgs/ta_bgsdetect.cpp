/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 29 november 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains the methods of the Background Substractor used in    |
  | Tactile Anywhere to detect the changes in the environment               |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_bgsdetect.h"

/*****************************************************************************/
/******                  CONSTRUCTORS/DESTRUCTOR                        ******/
/*****************************************************************************/

/*
    This constructor initializes this class with a configuration adapted to Tactile Anywhere
*/
TA_bgsDetect::TA_bgsDetect(){
    initialized = false;
    bgMog = NULL;
}

/*
    Destroy properly this instance
*/
TA_bgsDetect::~TA_bgsDetect(){
    if(bgMog){
        delete bgMog;
    }
}

/*****************************************************************************/
/******                        OPERATIONS                               ******/
/*****************************************************************************/

/*
    Checks if the Background Subtraction is initialized
        @return : true if it is initialized, false else
*/
bool TA_bgsDetect::isInitialized() const{
    return initialized;
}

/*
    Sets if the Background Subtraction is initialized
        @param state: true if it is initialized, false else
*/
void TA_bgsDetect::setInitialization(bool state){
    initialized = state;
}

/*
    Initializes this Background Subtraction
*/
void TA_bgsDetect::init(){
    if(bgMog){
        delete bgMog;
    }
    bgMog = new BackgroundSubtractorMOG();
    setInitialization(true);
}

/*
    This method update this BackgroundSubtractor
        @param frame: the next video frame
        @param fore: the ouput as the current foreground mask (8-bit binary image)
        @return: true if the Background Subtraction is applied, false else
*/
bool TA_bgsDetect::operator()(Mat frame, OutputArray fore){
    if(!initialized || frame.rows == 0){
        return false;
    }

    // Apply the Bakground Subtraction
    bgMog->operator ()(frame,fore);

    // Remove the isolated pixels
    cv::erode(fore,fore,cv::Mat());
    cv::dilate(fore,fore,cv::Mat());
    return true;
}
