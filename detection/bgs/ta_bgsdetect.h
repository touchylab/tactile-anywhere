/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 29 november 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains the declaration of the Background Substractor used in|
  | Tactile Anywhere to detect the changes in the environment               |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef TA_BGSDETECT_H
#define TA_BGSDETECT_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/
// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/background_segm.hpp>
#include <opencv2/imgproc/imgproc.hpp>

// Namespace
using namespace cv;

/*****************************************************************************/
/******                          CLASS                                  ******/
/*****************************************************************************/

/*
 *  TA_bgsDetect is a BackgroundSubstractor adapted to the project Tactile Anywhere.
 */
class TA_bgsDetect : public BackgroundSubtractor
{
    public:
        /*
            This constructor initializes this class with a configuration adapted to Tactile Anywhere
        */
        TA_bgsDetect();

        /*
            Destroy properly this instance
        */
        ~TA_bgsDetect();

        /*
            Initializes this Background Subtraction
        */
        virtual void init();

        /*
            Checks if the Background Subtraction is initialized
                @return : true if it is initialized, false else
        */
        virtual bool isInitialized() const;

        /*
            Sets if the Background Subtraction is initialized
                @param state: true if it is initialized, false else
        */
        virtual void setInitialization(bool state);

        /*
            This method update this BackgroundSubtractor
                @param frame: the next video frame
                @param fore: the ouput as the current foreground mask (8-bit binary image)
                @return: true if the Background Subtraction is applied, false else
        */
        virtual bool operator()(Mat frame, OutputArray fore);

    protected:
        /* The Background Subtraction used */
        BackgroundSubtractorMOG* bgMog;

        /* A boolean to know about initialization */
        bool initialized;
};

#endif // TA_BGSDETECT_H
