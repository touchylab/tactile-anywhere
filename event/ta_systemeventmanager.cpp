/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 10 december 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains methods of the TA_SystemEventManager namespace       |
  | offering to send events to the system. These events can be get by other |
  | programs                                                                |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ta_systemeventmanager.h"

/*****************************************************************************/
/******                        NAMESPACE                                ******/
/*****************************************************************************/

/*
 *  TA_SystemEventManager is namespace offering to manage system events. It
 *  offer to initialize the system communication service with other process
 *  and to send events to this system. It also offer to close this system properly.
 */
namespace TA_SystemEventManager {

/*****************************************************************************/
/******                            PRIVATES                             ******/
/*****************************************************************************/
namespace{
	// Variables
	long long unsigned int *oldest = NULL; // The index to the oldest event place in the buffer
	long long unsigned int *newest = NULL; // The index to the next event place in the buffer
	char* buffer = NULL; // The buffer containing events

#ifdef _WIN32
	HFILE fileToMap = HFILE_ERROR; // The file to map containing all shared informations
	HANDLE mapFile = NULL;   // The mapping of the file
	LPVOID fileData = NULL; // The data contains in the file
#else
	int fileToMap = -1; // The file to map containing all shared informations
	char* fileData = NULL; // The data contains in the file
#endif

	// Functions

	/*
				Notify to other process the deconnection of Tactile Anywhere
			*/
	void notifyEnd(){
		TA_SystemEvent endEvent = {(char)255,(long long unsigned int)time(NULL),"Tactile Anywhere is disconnected"};
		sendSystemEvent(endEvent);
	}
}

/*****************************************************************************/
/******                         INITIALIZATIONS                         ******/
/*****************************************************************************/

/*
        Initializes the SystemEventManager, preparing this programm to send events
        with sendSystemEvent(const TA_SystemEvent& event).
            @return: EVENTINIT_SUCCESS if the initialization is successful
                     EVENTINIT_CANTOPENFILE if the event file can't be opened or correctly created
                     EVENTINIT_CANTMAP if the event file can't be mapped
                     EVENTINIT_CANTSHARED if shared variables can't be created
    */
EventInitializationReturn init(){
#ifdef _WIN32
    char path[1024];
    OFSTRUCT fileStruct;

    // Creates or opens the file
    sprintf(path,"%s\\%s",getenv("TEMP"),TA_SYSEVENT_FILE_PATH);

    if(!QFile::exists(path)){
        QFile creator(path);
        if(!creator.open(QIODevice::ReadWrite))
            return EVENTINIT_CANTOPENFILE;
        creator.close();
    }

    fileToMap = OpenFile(path,&fileStruct,OF_READWRITE);
    if (fileToMap == HFILE_ERROR)
        return EVENTINIT_CANTOPENFILE;

    // Maps the file
    mapFile = CreateFileMapping((HANDLE)fileToMap,NULL,PAGE_READWRITE,0,TA_SYSEVENT_FILE_SIZE,NULL);
    if(mapFile == NULL){
        destroy();
        return EVENTINIT_CANTMAP;
    }

    // Gets the shared space
    fileData = MapViewOfFile(mapFile,FILE_MAP_READ | FILE_MAP_WRITE,0,0,TA_SYSEVENT_FILE_SIZE);
    if(fileData == NULL){
        destroy();
        return EVENTINIT_CANTSHARED;
    }
#else
    // Creates or opens the file
    if ((fileToMap = open(TA_SYSEVENT_FILE_PATH, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) == -1) {
        return EVENTINIT_CANTOPENFILE;
    }

    // Fills the file with random data (Place the pointer to the last byte to fill and write something)
    if (lseek(fileToMap, TA_SYSEVENT_FILE_SIZE-1, SEEK_SET) == -1 || write(fileToMap, "", 1) == -1) {
        destroy();
        return EVENTINIT_CANTOPENFILE;
    }

    // Maps the file
    if ((fileData = (char*) mmap(0, TA_SYSEVENT_FILE_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fileToMap, 0)) == MAP_FAILED) {
        destroy();
        return EVENTINIT_CANTMAP;
    }
#endif

    // Creates the shared variables
    oldest = (long long unsigned int *)fileData + TA_SYSEVENT_OLDEST_POS;
    newest = (long long unsigned int *)fileData + TA_SYSEVENT_NEWEST_POS;
    buffer = (char*)fileData + TA_SYSEVENT_BUFFER_POS;

    *oldest = 0;
    *newest = 0;

    // Identify this programm
    *((long long unsigned int *)fileData + TA_SYSEVENT_IDENT_POS) = time(NULL);

    // Send a initialization event
    TA_SystemEvent initEvent = {(char)254,(long long unsigned int)time(NULL),"Tactile Anywhere is connected"};
    sendSystemEvent(initEvent);

    return EVENTINIT_SUCCESS;
}

/*
        Destroys properly the SystemEventManager. To use again this system, use
        init() function.
    */
void destroy(){
#ifdef _WIN32
    if(fileData != NULL){
        notifyEnd();
        UnmapViewOfFile(fileData);
        fileData = NULL;
    }
    if(mapFile != NULL){
        CloseHandle(mapFile);
        mapFile = NULL;
    }
    if(fileToMap != HFILE_ERROR){
        CloseHandle((HANDLE)fileToMap);
        fileToMap = HFILE_ERROR;
    }
    buffer = NULL;
    newest = NULL;
    oldest = NULL;
#else
    if(fileData != NULL){
        notifyEnd();
        munmap(fileData, TA_SYSEVENT_FILE_SIZE);
        fileData = NULL;
    }
    if(fileToMap != -1){
        close(fileToMap);
        fileToMap = -1;
    }
    buffer = NULL;
    newest = NULL;
    oldest = NULL;
#endif
}

/*****************************************************************************/
/******                           OPERATIONS                            ******/
/*****************************************************************************/

/*
        Sends this event to the system communication service to be read by others
        process.
            @param event : the event to send to other process
            @return : true if the emission is successful, false else
    */
bool sendSystemEvent(const TA_SystemEvent& event){
    if(buffer == NULL){
        return false;
    }

    // Updates oldest
    if(*newest+1 > TA_SYSEVENT_BUFFER_LENGTH){
        *oldest = *oldest+1;
    }

    // Adds the event manually (to be sure that the memory organization is respected)
    buffer[TA_SYSEVENT_BUFFER_INDEX(*newest)] = event.ident;
    *((long long unsigned int *)(buffer+TA_SYSEVENT_BUFFER_INDEX(*newest)+1)) = event.time;
    memcpy(buffer + TA_SYSEVENT_BUFFER_INDEX(*newest)+1+TA_SYSEVENT_TYPE_TIME_LENGTH, event.data, TA_SYSEVENT_TYPE_BUF_LENGTH);

    // Updates newest
    *newest = *newest+1;

    return true;
}

/*
        Sends a launch detection event to the system communication service to be read by others
        process.
            @return : true if the emission is successful, false else
    */
bool sendLaunchDetectionEvent(){
    TA_SystemEvent lauchedEvent = {(char)0,(long long unsigned int)time(NULL),"Detection is launched"};
    return sendSystemEvent(lauchedEvent);
}

/*
        Sends a stop detection event to the system communication service to be read by others
        process.
            @return : true if the emission is successful, false else
    */
bool sendStopDetectionEvent(){
    TA_SystemEvent stopEvent = {(char)1,(long long unsigned int)time(NULL),"Detection is stoped"};
    return sendSystemEvent(stopEvent);
}

/*
        Sends a touch event to the system communication service to be read by others
        process.
            @param area : the touched area
            @return : true if the emission is successful, false else
    */
bool sendTouchEvent(QString area){
    TA_Server::getLogger()->log(QTime::currentTime().toString("hh:mm:ss") + QString(" : ") + area + QString(" est touchée"));
    TA_SystemEvent touchEvent = {(char)2,(long long unsigned int)time(NULL),""};
    strcpy(touchEvent.data,area.toStdString().c_str());
    bool ret = sendSystemEvent(touchEvent);

    AreaStruct* areaStruct = TA_Server::getInstance()->getAreaInfo(area);
    if(!areaStruct->url.isEmpty() || areaStruct->url.size() != 0){
        openUrl(areaStruct->url);
    }

    if(!areaStruct->file.isEmpty() || areaStruct->file.size() != 0){
        openFile(areaStruct->file);
    }

    if(!areaStruct->command.isEmpty() || areaStruct->command.size() != 0){
        launchCommand(areaStruct->command);
    }

    return ret;
}

/*
        Sends an untouch event to the system communication service to be read by others
        process.
            @param area : the untouched area
            @return : true if the emission is successful, false else
    */
bool sendUntouchEvent(QString area){
    TA_Server::getLogger()->log(QTime::currentTime().toString("hh:mm:ss") + QString(" : ") + area + QString(" n'est pas touchée"));

    TA_SystemEvent untouchEvent = {(char)3,(long long unsigned int)time(NULL),""};
    strcpy(untouchEvent.data,area.toStdString().c_str());
    return sendSystemEvent(untouchEvent);
}

/*
        Sends an ignore event to the system communication service to be read by others
        process.
            @param area : the ignored area
            @return : true if the emission is successful, false else
    */
bool sendIgnoreEvent(QString area){
    TA_Server::getLogger()->log(QTime::currentTime().toString("hh:mm:ss") + QString(" : ") + area + QString(" est ignorée (aucune webcam ne la visionne actuellement)"));
    TA_SystemEvent ignoreEvent = {(char)4,(long long unsigned int)time(NULL),""};
    strcpy(ignoreEvent.data,area.toStdString().c_str());
    return sendSystemEvent(ignoreEvent);
}

/*
        Open the given url
            @param url : the url to open
            @return : true if the java plug-in is functionnal, false else
    */
bool openUrl(QString url){
#ifdef __linux
    return QProcess::startDetached(QString("java -jar /usr/bin/ta_desktopbinding.jar \"") + url + QString("\""));
#else
    return QProcess::startDetached(QString("java -jar ta_desktopbinding.jar \"") + url + QString("\""));
#endif
}

/*
        Open the given file
            @param file : the file path to open
            @return : true if the java plug-in is functionnal, false else
    */
bool openFile(QString file){
#ifdef __linux
    return QProcess::startDetached(QString("java -jar /usr/bin/ta_desktopbinding.jar \"") + file + QString("\""));
#else
    return QProcess::startDetached(QString("java -jar ta_desktopbinding.jar \"") + file + QString("\""));
#endif
}

/*
        Launch the given command on the system
            @param command : the command to launch
    */
void launchCommand(QString command){
#ifdef _WIN32
    command = QString("cmd.exe /C ") + command;
#endif
    QProcess::startDetached(command);
}
}
