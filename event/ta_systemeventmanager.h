/*==========================================================================+
  |                                                                         |
  | ESIPE - MLV - IR3                                                       |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | IDENTIFICATION     :                                                    |
  | --------------                                                          |
  | Project Name       : Tactile Anywhere                                   |
  | Creator            : TouchyLab                                          |
  | Creation Date      : 02 december 2013                                   |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | FILE DESCRIPTION   :                                                    |
  | ----------------                                                        |
  | This file contains declarations of the TA_SystemEventManager namespace  |
  | offering to send events to the system. These events can be get by other |
  | programs.                                                               |
  |                                                                         |
  |-------------------------------------------------------------------------|
  |                                                                         |
  | VERSIONS   :                                                            |
  | ----------------                                                        |
  | [-] 1.0 - Simon BONY -- Initial version                                 |
  |                                                                         |
  |                                                                         |
  ==========================================================================+*/

/*
    This file is part of Tactile Anywhere.

    Tactile Anywhere is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tactile Anywhere is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Tactile Anywhere.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef TA_SYSTEMEVENTMANAGER_H
#define TA_SYSTEMEVENTMANAGER_H

/*****************************************************************************/
/******                         INCLUDES                                ******/
/*****************************************************************************/

// Qt
#include <QString>
#include <QProcess>
#include <QTime>
#include <QFile>

// Local includes
#include <iostream>
#include <ctime>
#include <stdlib.h>
#include "entity/ta_server.h"

#ifdef _WIN32
#undef UNICODE
#include <windows.h>
#else
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/mman.h>
#endif

// Namespaces
using namespace std;

/*****************************************************************************/
/******                         DEFINES                                 ******/
/*****************************************************************************/
// SystemEvent
#define TA_SYSEVENT_TYPE_BUF_LENGTH     (512)
#define TA_SYSEVENT_TYPE_TIME_LENGTH    sizeof(long long unsigned int)

//Indentification
#define TA_SYSEVENT_IDENT_SIZE          sizeof(long long unsigned int)
#define TA_SYSEVENT_IDENT_POS           (0)

//Indexes
#define TA_SYSEVENT_INDEXES_SIZE        sizeof(long long unsigned int)
#define TA_SYSEVENT_OLDEST_POS          (1)
#define TA_SYSEVENT_NEWEST_POS          (2)

// Buffer
#define TA_SYSEVENT_BUFFER_LENGTH       (1024)
#define TA_SYSEVENT_BUFFER_CASE_SIZE    (TA_SYSEVENT_TYPE_BUF_LENGTH + 1 + TA_SYSEVENT_TYPE_TIME_LENGTH)
#define TA_SYSEVENT_BUFFER_SIZE         (TA_SYSEVENT_BUFFER_LENGTH * TA_SYSEVENT_BUFFER_CASE_SIZE)
#define TA_SYSEVENT_BUFFER_POS          (TA_SYSEVENT_INDEXES_SIZE*2 + TA_SYSEVENT_IDENT_SIZE)
#define TA_SYSEVENT_BUFFER_INDEX(x)     (((x)%TA_SYSEVENT_BUFFER_LENGTH) * TA_SYSEVENT_BUFFER_CASE_SIZE)

// File
#define TA_SYSEVENT_FILE_SIZE           (TA_SYSEVENT_BUFFER_SIZE + 2*TA_SYSEVENT_INDEXES_SIZE + TA_SYSEVENT_IDENT_SIZE)

#ifdef _WIN32
#define TA_SYSEVENT_FILE_PATH       "Tactile_Anywhere_Event_File"
#else
#define TA_SYSEVENT_FILE_PATH       "/tmp/Tactile_Anywhere_Event_File"
#endif

/*****************************************************************************/
/******                              ENUMS                              ******/
/*****************************************************************************/

//enum used to catch return of the fonctions save configuration
enum EventInitializationReturn {EVENTINIT_SUCCESS, EVENTINIT_CANTOPENFILE, EVENTINIT_CANTMAP, EVENTINIT_CANTSHARED};

/*****************************************************************************/
/******                          TYPES                                  ******/
/*****************************************************************************/

/*
    TA_SystemEvent represents an event to send to the system with the
    TA_SystemEventManager.
*/
struct TA_SystemEvent{
    char ident;      // An ID used to specify the type of event
    long long unsigned int time;     // The date of the event
    char data[TA_SYSEVENT_TYPE_BUF_LENGTH];     // The data specify informations about this event
};

/*****************************************************************************/
/******                        NAMESPACE                                ******/
/*****************************************************************************/

/*
 *  TA_SystemEventManager is namespace offering to manage system events. It
 *  offer to initialize the system communication service with other process
 *  and to send events to this system. It also offer to close this system properly.
 */
namespace TA_SystemEventManager {

    /* Initialization */

    /*
        Initializes the SystemEventManager, preparing this programm to send events
        with sendSystemEvent(const TA_SystemEvent& event).
            @return: EVENTINIT_SUCCESS if the initialization is successful
                     EVENTINIT_CANTOPENFILE if the event file can't be opened or correctly created
                     EVENTINIT_CANTMAP if the event file can't be mapped
                     EVENTINIT_CANTSHARED if shared variables can't be created
    */
    EventInitializationReturn init();

    /*
        Destroys properly the SystemEventManager. To use again this system, use
        init() function.
    */
    void destroy();

    /* Operations */

    /*
        Sends this event to the system communication service to be read by others
        process.
            @param event : the event to send to other process
            @return : true if the emission is successful, false else
    */
    bool sendSystemEvent(const TA_SystemEvent& event);

    /*
        Sends a launch detection event to the system communication service to be read by others
        process.
            @return : true if the emission is successful, false else
    */
    bool sendLaunchDetectionEvent();

    /*
        Sends a stop detection event to the system communication service to be read by others
        process.
            @return : true if the emission is successful, false else
    */
    bool sendStopDetectionEvent();

    /*
        Sends a touch event to the system communication service to be read by others
        process.
            @param area : the touched area
            @return : true if the emission is successful, false else
    */
    bool sendTouchEvent(QString area);

    /*
        Sends an untouch event to the system communication service to be read by others
        process.
            @param area : the untouched area
            @return : true if the emission is successful, false else
    */
    bool sendUntouchEvent(QString area);

    /*
        Sends an ignore event to the system communication service to be read by others
        process.
            @param area : the ignored area
            @return : true if the emission is successful, false else
    */
    bool sendIgnoreEvent(QString area);

    /*
        Open the given url
            @param url : the url to open
            @return : true if the java plug-in is functionnal, false else
    */
    bool openUrl(QString url);

    /*
        Open the given file
            @param file : the file path to open
            @return : true if the java plug-in is functionnal, false else
    */
    bool openFile(QString file);

    /*
        Launch the given command on the system
            @param command : the command to launch
    */
    void launchCommand(QString command);

}

#endif // TA_SYSTEMEVENTMANAGER_H
