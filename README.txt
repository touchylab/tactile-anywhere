////////////////////////////////////////////
///                                      ///
///     Tactile Anywhere - TouchyLab     ///
///                                      ///
////////////////////////////////////////////

	Merci d'avoir t�l�charg� le logiciel Tactile Anywhere, d�velopp� par TouchyLab.

	Nous vous invitons � nous transmettre toute remarque, observation ou suggestion � cette adresse: http://tactile-anywhere.fr/contact

	Ce fichier vous indique les op�rations � suivre pour compiler Tactile Anywhere.

/////////////////////////////
///		Compilation
/////////////////////////////

Pour sa compilation, Tactile Anywhere n�cessite 4 librairies:
	- Qt 5.2.0 ou sup�rieur
	- Boost 1.55.0 ou sup�rieur
	- OpenCV 2.4.7 ou sup�rieur
	- Aruco 1.2.4 ou sup�rieur
	
Ces librairies devront �tre compatibles avec le compilateur que vous souhaitez utiliser pour compiler Tactile Anywhere. Aussi, nous vous
conseillons de compiler ces librairies vous-m�me pour assurer le bon fonctionnement du projet.

D�s lors, il vous est possible de compiler Tactile Anywhere avec la m�thode de votre choix:
	- Directement via votre compilateur :
		+ Ajoutez le r�pertoire o� sont situ�s les sources comme dossier de r�f�rence
		+ Placez dans les d�pendances les 4 librairies pr�c�demment nomm�es
		
	- Via un IDE :
		+ Placez tous les fichiers t�l�charg�s (sauf ce README) dans le projet de votre IDE
		+ Ajoutez au "build path" les 4 librairies pr�c�demment nomm�es
		
Si vous rencontrez des probl�mes pour compiler Tactile Anywhere, nous sommes � votre disposition � cette adresse: http://tactile-anywhere.fr/contact